/*
 * Gen_Control.h
 *
 *  Created on: Dec 10, 2020
 *      Author: KeithKolb
 */

#ifndef HEADER_GEN_CONTROL_H_
#define HEADER_GEN_CONTROL_H_

#include "F2837S_files/F28x_Project.h"

struct GenControl_Params {
    float LOOP_HZ;  //!<Loop frequency in Hz
    float T_LOOP;   //!<Loop period in seconds

    //!<PID gains for the charge controller
    float pi_kp;    //!<Proportional gain
    float pi_ki;    //!<Integral gain
    float pi_kd;    //!<Differential gain

    float FF_LPF_Hz;    //!<LPF frequency in Hz for feedforward term
    float FF_LPF_Alpha; //!<LPF alpha for filter based on FF_LPF_Hz (calculated)
    float DCP_LPF_Hz;   //!<LPF for DC power measurement in Hz
    float DCP_LPF_Alpha;//!<LPF alpha for DC power filter based on FF_LPF_Hz (calculated)
    float FF_Gain;      //!<Gain for feedforward term

    float StartingAmps;     //!<Engine cranking current in Amps
    float StartingTime_ms;  //!<Engine cranking time in milliseconds
    float MotionCheck_sec;   //!<Time in seconds before GenControlMotionOK will spit out a fault if system is not spinning
    float MotionCheck_rpm;  //!<Minimum RPM required to pass GenControlMotionOK
    float MaxAmpsCmd;       //!<Maximum possible generator control current command

    float StartAttempt_timeout_s;   //!<How long between failed start attempts
    uint16_t StartAttempts;         //!<How many failed start attempts before we fault out?
    uint16_t StartRPMThresh;        //!<RPM at which the state machine will transition out of EngineStart state

    double PowerTorqueLUT;          //!<Slope for Power->Torque LUT
    double PSTIntLUT;               //!<Intercept for Power->Torque LUT

    float TorqueDemandDeratingFactor; //!<Derating factor (A/degC) in overheat conditions

    uint16_t MinPowerThresh;        //!<Minimum power in W at which regen can be enabled
    uint16_t MaxGenPower;           //!<Maximum power in W past which battery will take on any additional load
};

extern struct GenControl_Params  GenControlParams;

#endif /* HEADER_GEN_CONTROL_H_ */
