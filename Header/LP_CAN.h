/*
 * LP_CAN.h
 *
 *
 *  Created on: Aug 10, 2016
 *      Author: vvarahamurthy
 */
#ifndef LP_CAN_H
#define LP_CAN_H

#include "F2837S_files/F28x_Project.h"
#include "driverlib/can.h"
#include "Header/Globals.h"

#define CAN_BITRATE 1000000L

#define NUM_OF_BROADCAST_MSG 16
#define MAX_BROADCAST_RATE 100

// General LaunchPoint Controller Status ID

#define BMS_BASE_ID 0xB
//Master IDs
#define BMS_CTRL_RX_ID         (0x2 << 6)
#define BMS_STATUS_RX_ID       (0xC << 6)

#define IVP_TX_ID             (0x11 << 6)        //These must be added to base ID
#define TEMPS_TX_ID           (0x13 << 6)
#define SGQB_RESPONSE_TX_ID   (0x17 << 6)
#define UNIV_RPM_ID           (0x1A << 6)
#define UNIV_FLT_ID           (0x1B << 6)
#define UNIV_WARN_ID          (0x1D << 6)
#define UNIV_STATES_ID        (0x19 << 6)
#define UNIV_CMD_FBK          (0x1F << 6)

#define GENSET_INFO_ID        (0x1C << 6)
#define GENSET_TEMPS_ID        (0x1E << 6)

//RX
#define GEN_CTRL_RX_ID            (0x04 << 6)
#define SGQB_RX_ID            (0x08 << 6)

struct CANMessageHandle {
    tCANMsgObject MsgObj;
    unsigned char MsgData[8];
    uint32_t ObjID;
    uint32_t DLC;
    void (*process)(struct CANMessageHandle* );
    uint32_t ErrorCounter;
};

struct CANIDs {

    uint32_t CANID_GEN_BMSCTRL_RX;
    uint32_t CANID_GEN_BMSSTATUS_RX;

    uint32_t CANID_IVP_TX;
    uint32_t CANID_GCSTATUS_TX;
    uint32_t CANID_TEMPS_TX;
    uint32_t CANID_SGQB_RESPONSE_TX;

    uint32_t CANID_UNIV_CTRL_RX;
    uint32_t CANID_SGQB_RX;

    uint32_t CANID_UNIV_STATES_TX;
    uint32_t CANID_UNIV_RPM_TX;
    uint32_t CANID_UNIV_FLT_TX;
    uint32_t CANID_UNIV_WARN_TX;
    uint32_t CANID_UNIV_CTL_FBK;
    uint32_t CANID_GEN_INFO_TX;
    uint32_t CANID_GEN_TEMPS_TX;
};

enum RelayStatus {
    RELAY_CLOSED = 0,
    RELAY_OPEN = 1
};


typedef struct{
    float           PackVoltage;
    float           PackCurrent;
    float           DesiredCurrent;
    bool            Fresh;
}ProcMsg_BMSCtrl_t;

#define PROC_BMSCTRL_DEFAULTS {(float)0.0,(float)0.0,(float)0.0,(int)0}

typedef struct  {
    uint32_t Monitors;
    uint16_t RelayOpenTimer;
    enum RelayStatus Relay_Status;
    bool             Fresh;
}ProcMsg_BMSStatus_t;

#define PROC_BMSSTATUS_DEFAULTS {0,0,(enum RelayStatus)0,0}

enum CANOpCodes{
    opCANParamSet = 0,
    opCANParamGet = 1,
    opCANSysValBroadcast = 2,
};

typedef struct
{
    unsigned char opcode;
    unsigned char parameter;
    float value;
    bool valid;
    bool fresh;
    enum GenControlStateVariables mode;
    float ref;
}ProcMsg_GenCtrl_t;
#define PROC_GENCTRL_DEFAULTS {0,0,0,0}

typedef struct
{
    enum CANOpCodes opcode;
    unsigned char parameter;
    float value;
    int rate;
    bool valid;
    bool fresh;
}ProcMsg_SGQB_t;
#define PROC_SGQB_DEFAULTS {(enum CANOpCodes)0,0,0,0,0,0}

struct Broadcast_t
{
    uint32_t index;
    char rate;
    uint16_t timer;
};


typedef struct {
    uint16_t swVersionMajor;
    uint16_t swVersionMinor;
    uint16_t swVersionBuild;
}ProcMsg_Version_t;


#if 0
typedef struct  {
    uint32_t rpm;
    uint16_t timesincereset;
} universal_rpm_tx_t;

typedef struct  {
    uint32_t lastcritfault;
    uint32_t lasterrorcode;
} universal_flt_tx_t;

typedef struct  {
    uint32_t adcwarnings;
    uint32_t otherwarnings; //TBD, unused rn
} universal_warn_tx_t;

typedef struct  {
    char mainstate;
    char gencontrolstate;
    char mchighlevelstate;
    char mclowlevelstate;
    char eculowlevelstate;
    char elmo1state;
    char elmo2state;
} universal_state_tx_t;

typedef struct  {
    uint16_t stator1;
    uint16_t stator2;
    unsigned char heatsink;
    unsigned char powerboard;
    unsigned char logicboard;
    unsigned char cpu;
} universal_temps_tx_t;

typedef struct  {
    int16_t dccurrent;
    int16_t dcvoltage;
    int32_t dcpower;
} universal_ivp_tx_t;

union can_copy_union {
    universal_ivp_tx_t struct_in;
    unsigned char data[8];
};

typedef struct  {
    unsigned char cht1;
    unsigned char cht2;
    unsigned char intake;
    unsigned char radiator1;
    unsigned char radiator2;
    unsigned char oil;
} genset_temps_tx_t;

/* FOR FUTURE USE
typedef struct  {
    uint16_t fuellevel;
    uint16_t oillevel;
} genset_env_tx_t; */

typedef struct  {
    uint16_t torquecmd;
    uint16_t enginethrottle;
    uint16_t coolingduty1;
    uint16_t coolingduty2;
} genset_info_tx_t;

/*CAN Message Instantiation */
universal_rpm_tx_t universal_rpm_tx;
universal_flt_tx_t universal_flt_tx;
universal_warn_tx_t universal_warn_tx;;
universal_state_tx_t universal_state_tx;
universal_temps_tx_t universal_temps_tx;;
universal_ivp_tx_t universal_ivp_tx;
genset_temps_tx_t genset_temps_tx;
genset_info_tx_t genset_info_tx;
#endif


#endif /* LP_CAN_H_ */
