/*
 * Controller.h
 *
 *  Created on: October 25, 2020
 *      Author: mricci
 *
 *      Header files for the gate controller/motor control interrupt code
 */

#ifndef CONTROLLER_DEFINES_H_
#define CONTROLLER_DEFINES_H_

enum SourceSel {
    Step = 1,
    SineDebugger = 2,
    SineFromREF = 3
};


typedef struct {
    bool active;
    enum SourceSel Source;
    float StepU;
    float StepV;
    float StepW;
    int   StepCycles;
    float SineAmplitude;        // Amplitude is normalized [0,1] relative to 100% duty cycle
    float SineFreq;             // frequency is normalize [0,1] relative to "base frequency" defined by inverterparams
} TESTPARAMETERS;

#define TEST_DEFAULTS            { 0,\
                                SineFromREF, \
                                -1.0, \
                                -1.0, \
                                -1.0, \
                                -1,    \
                                .01, \
                                2000.0 \
                                }

#endif /* CONTROLLER_H_ */
