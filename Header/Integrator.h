/*
 * Integrator.h
 *
 *  Created on: March 22, 2018
 *      Author: VV
 */


#ifndef __INTEGRATOR_H__
#define __INTEGRATOR_H__


typedef struct {  _iq  Err;   			//!< Input: error to integrate
				  _iq  Out;   			//!< Output: controller output
				  _iq  Ki;			    //!< Parameter: integral gain
				  _iq  ui;				//!< Data: integral term
				  _iq  i1;				//!< Data: integrator storage: ui(k-1)
				} INTEGRATOR;



/*-----------------------------------------------------------------------------
Default initalizer for the SMOPOS object.
-----------------------------------------------------------------------------*/
#define INTEGRATOR_DEFAULTS {0,0,0,0,0}


/*------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
#define INTEGRATOR_MACRO(v)												\
																\
																\
	/* integral term */ 										\
	v.ui = v.Ki*v.Err + v.i1;									\
	v.i1 = v.ui;												\
																\
	/* control output */ 										\
	v.Out= v.ui;

#endif
