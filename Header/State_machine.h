/*
 * State_machine.h
 *
 *  Created on: December 10,2016
 *      Author: MRicci
 */

#ifndef STATEMACH_H_
#define STATEMACH_H_


#define MAIN_SM_RATE 200

enum OnceAroundTriggerStates
{
    OnceEventState = 0,
    SinglePulseState,
    Wait
};
//Methods that may be called external to this module
extern enum MainStateVariables MainState_Get(void);
extern enum CritFaultVariables GetLastCritFault(void);

#endif
