/*
 * String.h
 *
 *  Created on: Oct 4, 2018
 *      Author: guydeong
 */

#ifndef HEADER_STRING_H_
#define HEADER_STRING_H_

void ftoa(float , char * , int);

void transmitDatalog();
void graphDatalog();

#endif /* HEADER_STRING_H_ */
