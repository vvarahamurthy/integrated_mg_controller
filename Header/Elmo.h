/*
 * Elmo.h
 *
 *  Created on: 2020.09.15
 *      Author: vvarahamurthy
 */

#ifndef HEADER_ELMO_H_
#define HEADER_ELMO_H_

#include "GPIO.h"
#include "IQMathLib.h"
#include "Header/pid_reg3mod.h"
#include "PWMDAC.h"
#include "Header/Globals.h"



#define NUM_OF_ELMOS 2


#define ELMO_DOWNTIME_MS 100.0    //!<Elmo downtime in milliseconds - time before trying to reenable from fault
#define LIM_ELMO_FAULTS 5
#define ELMO_AV_GAIN 100.0       //!<Gain set in EASII
#define ELMO_AV_OFFSET 2.5      //!<Offset set in EASII
#define ELMO_MAX_PWM_V 5.0      //!<Voltage on the torque command input to the elmo at 100%

#define ELMO_TC_FPWM 20000.0        //!<PWM frequency of the torque command PWMDAC output

enum ElmoStateVariables GetElmo0State(void);
enum ElmoStateVariables GetElmo1State(void);

/*
 * Each Elmo object has a PWMDAC handler that controls the
 * torque command to each elmo, as well as an independent state,
 * stateticker, and PID controller. This allows each elmo to be controlled
 * independently - wanted to code this in a way that it's possible to
 * use N elmos if ever required rather than hard-setting it to 2
 */

typedef struct ElmoHandler {
    int16_t GPIO_EN_NUM;        //!< GPIO number that an Elmo's INHIBIT input is connected to

    struct PWMDACHandler pwmdach; //!< PWMDAC handler for the analog signal command to an Elmo

    enum ElmoStateVariables state; //!< State variable for an Elmo
    uint32_t stateticker; //!< State ticker (counter) that tracks how long an Elmo has been in a certain state
    uint16_t FLT;         //!< Flag that goes high when an Elmo has faulted
    uint16_t numfaults;  //!< Counter that tracks how many times an Elmo has faulted in a single run

    float TC;   //!< Torque command [-1.0,1.0] that maps to 0-5V analog command to the Elmo
}elmo_handler_t;

struct ElmoParams {
    float LOOP_FREQ_HZ;     //!<Frequency in Hz at which the elmo state machine is called in the main ISR

    uint16_t downtime_ms;   //!<Downtime between disabling a faulted Elmo and re-enabling in milliseconds
    uint16_t lim_faults;    //!<Limit to the number of times we can attempt to re-enable an Elmo after it has faulted before flagging a critical shutdown fault

    float av_gain;          //!<Gain in amps per volt set in EASII
    float av_offset;        //!<Offset in volts set in EASII
    float av_max;           //!<Maximum possible voltage that the Elmo will see at its analog input
    float max_amps;         //!<Calculated based on above values

    float pwmdac_hz;        //!<Frequency in Hz of the PWM signal that sets the analog voltage command to the Elmos
};

extern volatile struct ElmoParams Elmo_Params;

#endif /* HEADER_PWMDAC_H_ */
