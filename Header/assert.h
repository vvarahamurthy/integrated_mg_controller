/*
 * assert.h
 *
 *  Created on: December 19,2016
 *      Author: MRicci
 */

#ifndef ASSERT_H_
#define ASSERT_H_

#define ASSERT(cond)        \
	if ( ! (cond))			\
	{						\
		asm ("      INTR NMI");	\
		for(;;);			\
	}

#endif
