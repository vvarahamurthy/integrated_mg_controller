/*
 * State_machine.h
 *
 *  Created on: September 10, 2018
 *      Author: MRicci
 */

#ifndef SAVERESTOR_H_
#define SAVERESTOR_H_

#include "F2837S_files/F28x_Project.h"
#include "GUIC_Header.h"
#include "ECU.h"

#define EEPROM_PROGRAMMED_FLAG 0xF0F0
#define INVERTERSTARTADDRESS  256   // Starting address in EEPROM for the inverter data m-- inverter data should be after motor data based on how checks for structure size OK are defined
#define MOTORSTARTADDRESS 0        // Starting address in EEPROM for the motor data
#define ECUSTARTADDRESS 0           //Replace motor with ECU when we are a generator
#define MAX_EEPROM_ADDRESS_BYTES 1023       // 8 kb eeprom = 1 kB eeprom


union MotorData {
     struct MotorParameters Parameters;
     uint16_t Data[sizeof(struct MotorParameters)];
 };

 union InverterData {
     struct InverterParameters Parameters;
     uint16_t Data[sizeof(struct InverterParameters)];
 };

 union ECUData {
     struct ECUParameters Parameters;
     uint16_t Data[sizeof(struct ECUParameters)];
 };

 union StartOfRecord
 {
     struct
     {
             uint16_t ProgrammedFlag;
             uint16_t WordCount;
             uint32_t Version;
     };
     uint16_t Data[4];
 };

 enum SRSuccess
 {
     SR_None    = 0,
     MotorSave = 1 << 0,
     MotorLoad  = 1 << 1,
     InverterSave = 1 << 2,
     InverterLoad = 1 << 3,
     ECUSave = 1 << 4,
     ECULoad = 1 << 5
 };

#endif
