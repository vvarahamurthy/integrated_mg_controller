/*
 * PWMDAC.h
 *
 *  Created on: Jul 10, 2019
 *      Author: vvarahamurthy
 */

#ifndef HEADER_PWMDAC_H_
#define HEADER_PWMDAC_H_

struct PWMDACHandler {
    int pwm_num;                //!< Which PWM peripheral (x in EpwmxRegs)
    int chan;                   //!< Which channel on the PWM peripheral (1 is A, 2 is B)
    float freq;                 //!< Frequency in Hz of the PWM signal
    unsigned int PeriodMax;                   //!< Parameter: PWMDAC half period in number of clocks  (Q0)
    float duty_offset_min;              //!<desired duty cycle at 0% command 1
    float duty_offset_max;              //!<desired duty cycle at 100% command 1
    int PwmDacIn;               //!< Actual PWM setting sent to the EpwmRegs
};

#endif /* HEADER_PWMDAC_H_ */
