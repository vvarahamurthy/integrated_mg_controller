/*
 * ErrorHandler.h
 *
 *  Created on: Oct 1, 2018
 *      Author: guydeong
 */

#ifndef HEADER_ERRORHANDLER_H_
#define HEADER_ERRORHANDLER_H_

// Define errors
#define INVALID_STATE               0
#define REBOOT_FAILED               1
#define FAILED_TO_ACCESS_EEPROM     2
#define FAILED_TO_INITIALIZE        3
#define DRIVER_DISCONNECTED         4
#define DIVIDE_BY_ZERO              5
#define OVERFLOW                    6
#define UNDERFLOW                   7
#define INSUFFICIENT_SPACE          8
#define FAILED_RX_MSG               9
#define FAILED_TX_MSG               10
#define POINTER_OUT_OF_BOUND        11
#define FAILED_TO_PAIR              12
#define INVALID_NEG_VALUE           13
#define ILLEGAL_ACCESS              14
#define TIMEDOUT                    15
#define WATCHEDDOG                  16

// Transmit errors through serial com
void transmitMsgSCI();

// Handle errors
void error(unsigned int errorCode);

// Returns true, if all files initialize correctly.
bool verifyAllInit();
#endif /* HEADER_ERRORHANDLER_H_ */
