/*
 * I2C.h
 *
 *
 *  Created on: August 17, 2017
 *      Author: VVarahamurthy
 */

#ifndef I2C_H_
#define I2C_H_

#include "F2837S_files/F28x_Project.h"

//-------------------------------------------
// User Defines
//-------------------------------------------
#define I2C_SLAVE_ADDR        0b1010000            // 0x00 to 0x7F provide the 7 bit address for the slave
#define I2C_NUMBYTES          8                    // Number of bytes to write for a full buffer load
#define I2C_EEPROM_HIGH_ADDR  0x00
#define I2C_EEPROM_LOW_ADDR   0x00
#define I2C_EEPROM_PAGE     16                  // Page size for EEPROM

#define I2C_MAX_BUFFER_SIZE     16

#define I2C_MAX_ATTEMPTS	  50

enum RWFlag{

    Standby = 0,
	WRITEmotor = 1,
	READmotor = 2,
    WRITEinverter = 3,
    READinverter = 4,
    READECU = 5,
    WRITEECU = 6,
	WRITEI2C = 7,
	READI2C = 8,
	BUFFER_ONES = 9,
	BUFFER_COUNT = 10

};

//
// I2C Message Structure
//
struct I2CMSG {
  uint16_t MsgStatus;           // Word stating what state msg is in:
                                //  I2C_MSGCMD_INACTIVE = do not send msg
                                //  I2C_MSGCMD_BUSY = msg start has been sent,
                                //                    awaiting stop
                                //  I2C_MSGCMD_SEND_WITHSTOP = command to send
                                //    master trans msg complete with a stop bit
                                //  I2C_MSGCMD_SEND_NOSTOP = command to send
                                //    master trans msg without the stop bit
                                //  I2C_MSGCMD_RESTART = command to send a
                                //    restart as a master receiver with a
                                //    stop bit
  uint16_t SlaveAddress;          // I2C address of slave msg is intended for
  uint16_t NumOfBytes;            // Num of valid bytes in (or to be put
                                // in MsgBuffer)
  uint16_t MemoryHighAddr;        // EEPROM address of data associated with
                                // msg (high byte)
  uint16_t MemoryLowAddr;         // EEPROM address of data associated with
                                // msg (low byte)
  uint16_t MsgBuffer[I2C_MAX_BUFFER_SIZE];  // Array holding msg data - max that
                                          // MAX_BUFFER_SIZE can be is 16 due
                                          // to the FIFO's
};

#endif //endif for entire module
