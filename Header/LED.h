/*
 * LED.h
 *
 *  Created on: November 28,2016
 *      Author: MRicci
 */

#ifndef LED_H_
#define LED_H_

enum BlinkRate
{
	SLOW = 3,
	MEDIUM = 2,
	FAST = 1

};
#endif
