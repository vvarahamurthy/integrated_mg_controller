/*
 * CRC.h
 *
 *  Created on: Jun 26, 2018
 *      Author: guydeong
 *
 *  Description: This program does a CRC calculation by utilizing the VCU. The CRC
 *  struct contains the start marker, op code, parameter, values, and crc result.
 *  Also, this can handle 8, 16, 32 bit calculation.
 *
 *  //! \brief CRC Object
	//!	 |Start code | OP Code | Source | Target | Parameter |	Value0....ValueN	| CRC	|
 */
#ifndef CRC_H_
#define CRC_H_

#include "crc_tbl.h"
#include "vcu2_types.h"
#include "vcu2_crc.h"
#include "examples_setup.h"
#include "hw_types.h"

#define MAX_NUM_VALUES                 10
#define MAX_MSG_BYTES                  13

// A structure to save the messages
typedef struct {
    unsigned char   startCode;              // 1 byte
    unsigned char   opCode;                 // 1 byte
    unsigned char   parameter;              // 1 byte
    float           values[MAX_NUM_VALUES]; // 4 bytes each
    unsigned char   crc;                    // 2 bytes
}Parse_Object;

// Initialize VCU//
void initVCU2();

// update some variables in CRC object
void updateCRCObject(unsigned short);

// 32-bit CRC VCU using polynomial 0x1edc6f41
uint32_t run32CrcVcu( unsigned short, Parse_Object *);

// 16-bit CRC VCU using polynomial 0x1021
unsigned int run16CrcVcu( unsigned short, Parse_Object *);
// 8-bit CRC VCU using polynomial 0x7
uint8_t run8CrcVcu( unsigned short, Parse_Object * );

unsigned int run16CrcCANVcu(unsigned char *);
void updateCRCCANObject(CRC_Obj *);


#endif /* CRC_H_ */
