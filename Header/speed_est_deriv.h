/* =================================================================================
File name:        SPEED_EST_DERIV.H
===================================================================================*/


#ifndef __SPEED_EST_D_H__
#define __SPEED_EST_D_H__

typedef struct {
       _iq EstimatedSpeed;  	// Input: Electrical angle (pu)
       _iq OldEstimatedSpeed;   // History: Electrical angle at previous step (pu)
       _iq EstimatedSpeedDerivative;      // Output: Estimated speed in per-unit  (pu)
       _iq21 K1;       			// Parameter: Constant for differentiator (Q21) - independently with global Q
       _iq K2;     				// Parameter: Constant for low-pass filter (pu)
       _iq K3;     				// Parameter: Constant for low-pass filter (pu)
       _iq Temp;				// Variable : Temp variable
       } SPEED_D_ESTIMATION;  	// Data type created


/*-----------------------------------------------------------------------------
Default initalizer for the SPEED_ESTIMATION object.
-----------------------------------------------------------------------------*/                     
#define SPEED_D_ESTIMATION_DEFAULTS   { 0, \
                                	  0, \
                                	  0, \
                                      0, \
                                      0, \
                                      0, \
                                      0, \
                                    }

/*------------------------------------------------------------------------------
 SPEED_EST Macro Definition
------------------------------------------------------------------------------*/


#define SED_MACRO(v)																    \
/* Synchronous speed derivative (acceleration) computation   */												\
    v.Temp = v.EstimatedSpeed - v.OldEstimatedSpeed;		                    	\
                                      			\
    v.Temp = _IQmpy(v.K1,v.Temp);		                               				\
																					\
/* Low-pass filter */																\
/* Q21 = GLOBAL_Q*Q21 + GLOBAL_Q*Q21 */												\
	v.Temp = _IQmpy(v.K2,_IQtoIQ21(v.EstimatedSpeedDerivative))+_IQmpy(v.K3,v.Temp);			\
																					\
/* Saturate the output */															\
	v.Temp=_IQsat(v.Temp,_IQ21(1),_IQ21(-1));										\
	v.EstimatedSpeedDerivative = _IQ21toIQ(v.Temp);											\
																					\
/* Update the electrical angle */													\
	v.OldEstimatedSpeed = v.EstimatedSpeed;


#endif // __SPEED_EST_D_H__
