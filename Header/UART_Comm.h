#ifndef UART_COMM_H_
#define UART_COMM_H_

#include "F2837S_files/F28x_Project.h"        // F2837xS Headerfile Include File

#define INDCHAR 0               // Have not checked that INDCHAR still works since finishing FIFOINT mode- MR
#define FIFOINT 1

#define SCI_MODE FIFOINT

#define SCIREGS 		    ScibRegs
/************************Baud rate options**********************************/
//      9600,   14400,  19200,  28800,  38400,
//      56000,  57600,  115200, 128000, 153600,
//      230400, 256000, 460800, 9210600
/************************************************************************/

#define SCI_BAUD_RATE	    230400U
#define SCI_ECU_BAUD_RATE   115200U

#if(SCI_MODE==FIFOINT)
#define RX_FIFO_LEVEL 15
#else
#define RX_FIFO_LEVEL 1
#endif
#define TX_FIFO_LEVEL 0


void Init_SCI(void);
void setup_sci_interrupt(void);

void sci_echoback_init(void);
void sci_xmit(int a);
void sci_start_xmit(void);

void decrementNewCharCounter();

uint16_t getNewCharCount();
uint16_t getReceivedChar(void);

void Write8bitByteToCOM(unsigned char c);

void printCom(char *);
bool SCI_CheckRxStatus(void);

void SCI_Reset(void);

__interrupt void sciRxFifoIsr(void);
__interrupt void sciTxFifoIsr(void);


#endif
