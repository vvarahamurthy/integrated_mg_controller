/*
 * ParseRoutine.h
 *
 *  Created on: Jun 22, 2018
 *      Author: guydeong
 *  Description: This is a Parse routine that has:
	- purge();									// delete data
	- rewind();									// points back to the beginning of the buffer
	- underflowed();							// set flag when buffer is empty
	- getFloat(uint32_t *);						// get an IEEE float then parse the data
	- getCRC(unsigned char *);					// get the CRC value in the message
	- discard(unsigned short);					//
	- getValues(unsigned char *);				// get Values
	- getOpCode(unsigned char *);				// get Op code
	- getParameter(unsigned char *);			// get Parameter
	- getStartCode(unsigned char *);			// get start code
	- getByte();								// get byte
	- context();								// get the message
 */

#ifndef PARSEROUTINE_H_
#define PARSEROUTINE_H_

#include "RingBuffer.h"
#include "State_machine.h"
#include "GUIC_Header.h"

#define NOTHING							0
#define	STARTCODE						1
#define	OPCODE							2
#define PARAMETER						3
#define VALUES							4
#define CRC								5

#ifndef NULL
#define NULL							0
#endif

float getAlphaVoltageCMD();
float getBetaVoltageCMD();
float getRampAngle();
float getRampCurrent();
float getAngle();
float getQCurrentCMD();
float getAngle();

RINGBUFFER * getTxRingBuffer();
RINGBUFFER * getRxRingBuffer();

void InitParseRoutine();
bool checkInitParseRoutine();

// Parsing the receiver message
void purge();
void fillParseBuffer();

bool isMasterSlaveConnected();

bool pRewind();
bool underflowed();
bool discard(unsigned short);
bool isParseStillProcessing();

float getMasterValue1_sig();
float getMasterValue2_sig();

uint32_t getFailedMsgCounter();

char * parseRoutine(unsigned char );
// ==========================================================
//                          Receive
// ==========================================================
bool getFloat(float *);
bool getCRC(unsigned char *);
bool getValues(unsigned char *);
bool getOpCode(unsigned char *);
bool getParameter(unsigned char *);
bool getStartCode(unsigned char *);

unsigned char getByte();

// ==========================================================
//                          Transmit
// ==========================================================
void setStartAndOpValues(char * , unsigned int );
void setParameterValue(char * , enum MainStateVariables , enum SyncMode );
void setValues(char * , enum MainStateVariables , enum SyncMode , enum GPIOFaultVariables , unsigned short );
void setCRCValues(char * , unsigned short );

#endif /* PARSEROUTINE_H_ */
