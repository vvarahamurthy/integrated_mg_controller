/*
 * PWMRect.h
 *
 *  Created on: Feb 19, 2019
 *      Author: Vishaal
 */

#ifndef HEADER_PWMRECT_H_
#define HEADER_PWMRECT_H_

//May want to move this somewhere else, but it didn't belong in the PWM defines
enum SwitchSide {
    High = 1,
    Low = 2
};

#endif /* HEADER_PWMRECT_H_ */
