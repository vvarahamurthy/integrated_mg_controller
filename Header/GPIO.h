/*
 * GPIO.h
 *
 *  Created on: December 9, 2016
 *      Author: MRicci
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "F2837S_files/F28x_Project.h"

struct debounced_signal
{
    uint16_t GPIO_filtered;       //!<Filtered GPIO input
    uint16_t schmitt_flag;        //!< Schmitt trigger flag
    uint16_t debounced;           //!< Debounced GPIO
    bool forced;             //!< Flag for forcing the input via software
    uint16_t  forceval;           //!< Value the GPIO debounced will be set to when software forced
};

#endif
