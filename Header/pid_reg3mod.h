/* =================================================================================
File name:       PID_REG3.H  (IQ version)                    
                    
Originator: Digital Control Systems Group
            Texas Instruments

Description: 
Header file containing constants, data type, and macro definitions for the PIDREG3.
=====================================================================================
 History:
-------------------------------------------------------------------------------------
 10-15-2009 Version 1.0
------------------------------------------------------------------------------*/
#ifndef __PIDREG3_MOD_H__
#define __PIDREG3_MOD_H__

    typedef struct {
                    float  Ref;   			// Input: Reference input
                    float  Fdb;   			// Input: Feedback input
                    float  Err;				// Variable: Error
                    float  Kp;				// Parameter: Proportional gain
                    float  Up;				// Variable: Proportional output
                    float  Ui;				// Variable: Integral output
                    float  Ud;				// Variable: Derivative output
                    float  OutPreSat; 		// Variable: Pre-saturated output
                    float  OutMax;		    // Parameter: Maximum output
                    float  OutMin;	    	// Parameter: Minimum output
                    float  Out;   			// Output: PID output
                    float  SatErr;			// Variable: Saturated difference
                    float  Ki;			    // Parameter: Integral gain
                    float  Kc;		     	// Parameter: Integral correction gain
                    float  Kd; 		        // Parameter: Derivative gain
                    float  Up1;		   	    // History: Previous proportional output
                    float  new_i;
                    float  u;
                    int int_ok;          //Check if OK to increase/decrease integrator
		 	 	} PIDREG3_MOD;

typedef PIDREG3_MOD *PIDREG3_MOD_handle;
/*-----------------------------------------------------------------------------
Default initalizer for the PIDREG3 object.
-----------------------------------------------------------------------------*/                     
#define PIDREG3MOD_DEFAULTS { 0,            \
                           0,           \
                           0,           \
                           _IQ(1.3),    \
                           0,           \
                           0,           \
                           0,           \
                           0,           \
                           _IQ(1),      \
                           _IQ(-1),     \
                           0,           \
                           0,           \
                           _IQ(0.02),   \
                           _IQ(0.5),    \
                           _IQ(1.05),   \
                           0,           \
                           0,           \
                           0,           \
                          0         \
                          }

/*------------------------------------------------------------------------------
    PID Macro Definition
------------------------------------------------------------------------------*/


#define PID_MOD_MACRO(v)                                                                               \
    v.Err = v.Ref - v.Fdb;                                  /* Compute the error */                    \
    v.Up= _IQmpy(v.Kp,v.Err);                               /* Compute the proportional output */       \
    v.new_i= v.Ui + _IQmpy(v.Ki,v.Err) + _IQmpy(v.Kc,v.SatErr);/*Compute the integral output*/          \
    v.u= v.Up + v.new_i;                        /* Compute the pre-saturated output */                  \
    v.int_ok = 1;                                                                                    \
    if(v.u>=v.OutMax)                                                                           \
    {                                                                                                   \
        if(v.Err > 0)                                                                                   \
        {                                                                                               \
            v.int_ok = 0;/*don't allow integrator update if saturated high and positive error*/     \
        }                                                                                               \
    }                                                                                                   \
    if(v.u<=v.OutMin)                                                                           \
    {                                                                                                   \
        if(v.Err < 0)                                                                                   \
        {                                                                                               \
            v.int_ok=0;/*don't allow Ui update if saturated low and negative error*/                \
        }                                                                                               \
    }                                                                                                   \
    if(v.int_ok==1)                                                                                    \
    {                                                                                                   \
        v.Ui = v.new_i;                                                                                 \
    }                                                                                                   \
    v.OutPreSat= v.Up + v.Ui;                        /* Compute the pre-saturated output */             \
    v.Out = _IQsat(v.OutPreSat, v.OutMax, v.OutMin);        /* Saturate the output */                   \
    v.SatErr = v.Out - v.OutPreSat;                         /* Compute the saturate difference */       \
    v.Up1 = v.Up;                                           /* Update the previous proportional output */
#endif
// Add the lines below if derivative output is needed following the integral update
// v.Ud = _IQmpy(v.Kd,(v.Up - v.Up1)); 
// v.OutPreSat = v.Up + v.Ui + v.Ud;

//D gain added for AAC 20170213
