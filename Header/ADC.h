/*
 * ADC.h
 *
 *  Created on: July 31, 2017
 *      Author: VVarahamurthy
 */

#ifndef ADC_H_
#define ADC_H_


#define FAKEA_D                     0          // Fakes the A/D inputs to good values to make faults go away with no hardware
#define DISABLEA_D                  0       // Just test code with ADC's disconnected -- use debugger to put current, voltage and make sure errors work properly

#define NUM_ADC_REGS 4  //A B C D

#define ADC_CNT_MAX                4096
#define ADC_PU_SCALE_FACTOR        0.000244140625     //1/2^12
#define ADC_PU_PPB_SCALE_FACTOR    0.000488281250     //1/2^11

#if 0
typedef struct {
	float Bus;		// Bus current
	float Us;      // phase A
	float Vs;      // phase B
	float Ws;      // phase C
} CURRENT_SENSOR;

typedef struct {
    float U;
    float V;
    float W;
} PHASE_VOLTAGE;

#endif

struct RMSFiltered_Signal{float RMS;
                        float Omega;
                        float RMS_old;
                       };

struct Analog_Signal {
    int16_t raw;    //!< Raw ADC register value (int16 because we use negative values when reading current sensors)
    uint16_t order; //!< Nth order polynomial to process this signal
    float coef[4];  //!< Vector of polynomial coefficients up to 4th order
    float lpf_alpha;//!< Alpha value for the low pass filter to be applied to this signal
    float filtered; //!< LPF'd raw PU value
    float value;    //!< Final, conditioned value in volts, amps, degC, etc.

    float warnh; //!< Warning level HIGH
    float warnl; //!< Warning level LOW
    float crith; //!< Critical (fault) level HIGH
    float critl; //!< Critical (fault) level LOW
    bool monitored; //!< Is this signal monitored to influence fault transitions (does fault detect care about this signal?)
    float lastfaultvalue; //!< If this signal caused a fault, what was its last value before the fault occurred?
    uint64_t numfaults;
};

#endif
