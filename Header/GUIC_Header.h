/*
 * GUIC_Header.h
 *
 *  Created on: August 2017
 *      Author: Greyson Miller
 */
#ifndef GUIC_HEADER_H_
#define GUIC_HEADER_H_

#include "F2837S_files/F28x_Project.h"
#include "Header/Globals.h"

#define HRD_TEST 0

//Defining these just for the build - likely not to use these ever
#define SMO_GAIN_SCALE 0
#define SMO_LOOP_SCALE 0

#define ADC_NUM_SOC 16

#define     PI      (3.14159265358979)

#define SwVersionMajor 0
#define SwVersionMinor 0
#define SwVersionBuild 1

enum QEP_MODE {
    qep_none=0,
    quadrature = 1,
    positionAndDirection
};

enum KnobSel{
    speedKnob = 1,
    angleErrorKnob,
    IQDisturbanceKnob,
};

enum Observers_Angles {
  SL_ORTEGA = 1,
  SL_SMOPOS = 1 << 1,
  SNS_ENC_QEP = 1 << 2,
  SNS_ENC_SSI = 1 << 3,
  SNS_HALLRAW = 1 << 4,
  SNS_HALLPLL = 1 << 5,
  SNS_ONCEAROUND = 1 << 6
};

enum ControlRefSel
{
    CRef_Analog = 1,
    CRef_CAN = 2,
    CRef_Serial = 3,
    CRef_Debugger = 4
};

//From "cleaner structs" version
enum Inverters
{
  LAUNCHPAD_GENSET,
  LAUNCHPAD_MC28377,
  MC15,
  MC40,
  ELMO_PMU,
  HONDA_LAUNCHPAD_DEBUG28377,
  HONDA_LAUNCHPAD_DEBUG28379,
  HPS400_SP
};

enum Motors
{
  STOCK=1,
  STOCK_LV,
  NASA_GEN,
  JOBY,
  HVJOBY,
  TURNIGY_G160,
  TURNIGY_DST700,
  JOBY_JM140,
  LP_DHA_4T3PY,
  LP_DHA_4T3PY_2,
  LP_DHA_2T3PY,
  LP_DHA_4T3PY_MagGear,
  LP_DHA_4T3PY_ArmyGear,
  LP_DHA_2T3PY_ArmyGear,
  LP_DHA_1T6PY,
  NASA_CRYO_6PY,
  KDE_7215XF,
  EMRAX_208,
  LP_DHA_1T6PY_CRYO,
  NEU_MOTOR_8057,
  LP_DHA_4T3PY_CAMIEM,
  HONDA_INVERTER_TEST,
  TMOTOR_U8,
  LP_DHA_120_6PWYE
};

enum SyncMode
{
    SOLO = 1,
    SERVO_SOLO,
    FOLLOWER_SOLO,
    GEN_SOLO,
    PARALLEL_MASTER,
    SERVO_MASTER,
    FOLLOWER_MASTER,
    GEN_MASTER,
    PARALLEL_SLAVE,
    SERVO_SLAVE,
    FOLLOWER_SLAVE,
    GEN_SLAVE,
    TORQUE_MODE
};

enum StartStopSrc
{
    StSel_GPIO,
    StSel_CAN,
    StSel_PWM,
    StSel_GPIOAndCAN
};

enum FaultOutputModes
{
    FltO_Disabled = 0,
    FltO_TripLow = 1,
    FltO_TripHigh = 2
};

enum AngleCommandSelections
{
    AngleCANCmnd = 0,
    AngleKnobCmnd
};


enum BuildLevels
{
    e_OpenLoop=1,
    e_CurrentLoopConstDandQ,
    e_CurrentLoopNoAngleSense,
    e_VoltageLoop,
    e_TorqueMode,
    e_SpeedLoop
};

enum ControlLoops {
    MC_PID_POS = 1,
    MC_PI_SPEED = 1 << 1,
    MC_PI_CURRENT= 1 << 2
};

struct CPU_timings { float System_freq;             // System Frequency
                  float  nanoSec;
                  float  microSec;
                  float  milliSec;
                  float  Sec;
               };                  // Changes based on which Board is selected

struct InverterParameters {
                  unsigned char StructVersion;
                  uint32_t FirmwareVersion;
                  uint16_t UniqueSerial;

                  enum Inverters InverterSel;
                  float DeadBand;

                  enum ElectronicsTypes Electronics;
                  enum ControllerTypes Controller;
                  enum GensetTypes  Genset;

//CPU Timers in Hz
                  uint32_t A_TASK_HZ;
                  uint32_t B_TASK_HZ;
                  uint32_t C_TASK_HZ;

//Voltage and current limits
                  float  BaseVoltage;
                  float  BaseCurrent;

//LIMits
                  float  LIM_I_DC_CRIT_H;
                  float  LIM_I_DC_CRIT_L;
                  float  LIM_I_DC_WARN_H;
                  float  LIM_I_DC_WARN_L;

                  float  LIM_V_DC_CRIT_H;
                  float  LIM_V_DC_CRIT_L;
                  float  LIM_V_DC_WARN_H;
                  float  LIM_V_DC_WARN_L;

                  float  LIM_T_CPU_CRIT_H;
                  float  LIM_T_CPU_CRIT_L;
                  float  LIM_T_CPU_WARN_H;
                  float  LIM_T_CPU_WARN_L;

                  float  LIM_T_DEV_CRIT_H;
                  float  LIM_T_DEV_CRIT_L;
                  float  LIM_T_DEV_WARN_H;
                  float  LIM_T_DEV_WARN_L;

                  float  LIM_T_PDB_CRIT_H;
                  float  LIM_T_PDB_CRIT_L;
                  float  LIM_T_PDB_WARN_H;
                  float  LIM_T_PDB_WARN_L;

                  float  LIM_T_HEATSINK_CRIT_H;
                  float  LIM_T_HEATSINK_CRIT_L;
                  float  LIM_T_HEATSINK_WARN_H;
                  float  LIM_T_HEATSINK_WARN_L;

                  float  LIM_T_LOGIC_CRIT_H;
                  float  LIM_T_LOGIC_CRIT_L;
                  float  LIM_T_LOGIC_WARN_H;
                  float  LIM_T_LOGIC_WARN_L;

                  float LIM_I_PHS;

                  float  Sense_Current_M;
                  float  Sense_Current_B;
                  float  Sense_Voltage_M;
                  float  Sense_Voltage_B;

                  float  Sense_VCCA_REF;
                  float  Sense_TempP2;
                  float  Sense_TempP1;
                  float  Sense_TempP0;
// PWMDAC
                int GPIO_PWMDAC_ANGLEERROR;
//Misc GPIO Numbers
                  int GPIO_LED1_NUM;
                  int GPIO_LED2_NUM;
                  int GPIO_ESTOP_NUM;
                  int GPIO_START_NUM;
//Desaturation fault GPIOs (only MC40 and MC15)
                  int GPIO_FLTHU_NUM;
                  int GPIO_FLTLU_NUM;
                  int GPIO_FLTHV_NUM;
                  int GPIO_FLTLV_NUM;
                  int GPIO_FLTHW_NUM;
                  int GPIO_FLTLW_NUM;
//Ready signals (VCC > UVLO) for gate drivers (MC15 and MC40 only)
                  int GPIO_RDYU_NUM;
                  int GPIO_RDYV_NUM;
                  int GPIO_RDYW_NUM;
//Reset signals to gate pre-driver ICs (MC15 and MC40 only)
                  int GPIO_NRST_NUM;
//Gate driver enable signal
                  int GPIO_ENGD_NUM;
//Communications GPIOs
                  //SCI (RS422)
                  int GPIO_RS422TX_NUM;
                  int GPIO_RS422RX_NUM;
                  int GPIO_RS422TXEN_NUM;
                  //I2C
                  int GPIO_I2C_SDA_NUM;
                  int GPIO_I2C_SCL_NUM;
                  //USB
                  int GPIO_USB_DP_NUM;
                  int GPIO_USB_DM_NUM;
                  int GPIO_USB_V_NUM;
                  //CAN
                  int GPIO_CAN_RX_NUM;
                  int GPIO_CAN_TX_NUM;
                  //SPI
                  int GPIO_SPI_CLK_NUM;
                  int GPIO_SPI_SOMI_NUM;
                  int GPIO_SPI_SIMO_NUM;
                  int GPIO_SPI_CS1_NUM;
                  int GPIO_SPI_CS2_NUM;

                  //Sensored commutation GPIO
                  //EQEP
                  int GPIO_QEPA_NUM;
                  int GPIO_QEPB_NUM;
                  int GPIO_QEPI_NUM;
                  int GPIO_QEPS_NUM;

                  //Hall
                  int GPIO_HALLAB_NUM;
                  int GPIO_HALLBC_NUM;
                  int GPIO_HALLCA_NUM;

                  //Speed pulse and once around
                  int GPIO_ONCEAROUND_NUM;
                  int GPIO_SPEEDPULSE_NUM;

                  //ECU GPIOs
                  int GPIO_THROTTLE_NUM;
                  int GPIO_EFI_EN_NUM;
                  int GPIO_TACH_NUM;

                  //Engine cooling
                  int GPIO_EDF_NUM;

                  //Starter GPIOs
                  int GPIO_ELMO_STO_NUM;
                  int GPIO_ELMO_START_NUM;

                  //SCI (RS232)
                  int GPIO_RS232TX_NUM;
                  int GPIO_RS232RX_NUM;

                  //Fault output for ESTOP bus
                  int GPIO_FLT_OUTPUT_NUM;

                  //Elmo PMU
                  int GPIO_ELMO_TC_1_NUM;
                  int GPIO_ELMO_EN_1_NUM;
                  int GPIO_ELMO_FLT_1_NUM;
                  int GPIO_ELMO_TC_2_NUM;
                  int GPIO_ELMO_EN_2_NUM;
                  int GPIO_ELMO_FLT_2_NUM;
//EPWM Selection
                  int PWM_U_NUM;
                  int PWM_V_NUM;
                  int PWM_W_NUM;

                  //40kW Smart Passive I/Os
                  int GPIO_PWM_RADF_NUM;    //!< Radiator fan control - EPWM2A
                  int GPIO_PWM_RECTF_NUM;   //!< Rectifier fan control -EPWM3B
                  int GPIO_ECAP_FUEL_L_NUM; //!< Fuel Level (L) - ECAP2
                  int GPIO_ECAP_FUEL_R_NUM; //!< Fuel level (R) - ECAP3
                                              //ECAP4 is engine tach
                  int GPIO_ECAP_BFRF_NUM;       //!< Rectifier fan feedback  ECAP5
                  int GPIO_ECAP_RADT1_NUM;      //!<Radiator fan feedback 1 ECAP6
                  int GPIO_ECAP_RADT2_NUM;      //!<Radiator fan feedback 2 ECAP7
                  int GPIO_OILLVL_NUM;       //!< Oil level sensor
                  int GPIO_24V_EN_NUM;      //<!24V bus enable
                  int GPIO_28V_EN_NUM;      //<!28V bus enable
                  int GPIO_24V_FLT_NUM;     //!< 24V bus fault output
                  int GPIO_28V_FLT_NUM;     //!< 28V bus fault output
                  int GPIO_FUELPUMP_EN_NUM; //!< Fuel pump enable;
                  int GPIO_SERVO_TXEN_NUM;  //!<RS485 servo control
                  int GPIO_SERVO_TX_NUM;  //!<RS485 servo control
                  int GPIO_SERVO_RX_NUM;  //!<RS485 servo control
                  int GPIO_TBUS_SDA_NUM;    //!< I2C BusBar sense
                  int GPIO_TBUS_SCL_NUM;    //!< I2C BusBar sense
                  int GPIO_ESTOP2_NUM;  //!< ESTOP #2
                  int GPIO_IGN1EN_NUM;  //!< Ignition 1 enable
                  int GPIO_IGN2EN_NUM;  //!< Ignition 2 enable


//ADC Defines
                 volatile uint16_t *ADC_REG_IU;
                 volatile uint16_t *ADC_REG_IV;
                 volatile uint16_t *ADC_REG_IW;
                 volatile uint16_t *ADC_REG_IBUS;
                 volatile uint16_t *ADC_REG_IBUS2;
                 volatile uint16_t *ADC_REG_VBUS;
                 volatile uint16_t *ADC_REG_VU;
                 volatile uint16_t *ADC_REG_VV;
                 volatile uint16_t *ADC_REG_VW;
                 volatile uint16_t *ADC_REG_TCPU;
                 volatile uint16_t *ADC_REG_TMODULE;
                 volatile uint16_t *ADC_REG_THSINK;
                 volatile uint16_t *ADC_REG_TPDB;
                 volatile uint16_t *ADC_REG_TBRD;
                 volatile uint16_t *ADC_REG_TSTATOR;
                 volatile uint16_t *ADC_REG_SPEEDKNOB;
                 volatile uint16_t *ADC_REG_SPARE1;
                 volatile uint16_t *ADC_REG_SPARE2;

                 //Below items are for 40kW smart passive
                 volatile uint16_t *ADC_REG_TAMB;
                 volatile uint16_t *ADC_REG_3V3;
                 volatile uint16_t *ADC_REG_5V;
                 volatile uint16_t *ADC_REG_1V2;
                 volatile uint16_t *ADC_REG_12VBAT;
                 volatile uint16_t *ADC_REG_T_COOLANT;
                 volatile uint16_t *ADC_REG_T_OIL;
                 volatile uint16_t *ADC_REG_IFP;
                 volatile uint16_t *ADC_REG_I12BAT;
                 volatile uint16_t *ADC_REG_I24V;
                 volatile uint16_t *ADC_REG_I28V;
                 volatile uint16_t *ADC_REG_24V;
                 volatile uint16_t *ADC_REG_28V;

#if(HRD_TEST==1)
                 volatile Uint16 *ADC_REG_TU1;
                 volatile Uint16 *ADC_REG_TU2;
                 volatile Uint16 *ADC_REG_TV1;
                 volatile Uint16 *ADC_REG_TV2;
                 volatile Uint16 *ADC_REG_TW1;
                 volatile Uint16 *ADC_REG_TW2;
                 volatile Uint16 *ADC_REG_TCOOL;

                 int ADC_CHAN_TU1;
                 int ADC_CHAN_TU2;
                 int ADC_CHAN_TV1;
                 int ADC_CHAN_TV2;
                 int ADC_CHAN_TW1;
                 int ADC_CHAN_TW2;
                 int ADC_CHAN_TCOOL;
#endif
                 int ADC_CHAN_IU;
                 int ADC_CHAN_IV;
                 int ADC_CHAN_IW;
                 int ADC_CHAN_IBUS;
                 int ADC_CHAN_IBUS2;
                 int ADC_CHAN_VU;
                 int ADC_CHAN_VV;
                 int ADC_CHAN_VW;
                 int ADC_CHAN_VBUS;
                 int ADC_CHAN_TCPU;
                 int ADC_CHAN_TMODULE;
                 int ADC_CHAN_THSINK;
                 int ADC_CHAN_SPEEDKNOB;
                 int ADC_CHAN_SPARE1;
                 int ADC_CHAN_SPARE2;
                 int ADC_CHAN_TPDB;
                 int ADC_CHAN_TBRD;
                 int ADC_CHAN_TSTATOR;

                 //Below items are for 40kW smart passive
                 int ADC_CHAN_TAMB;
                 int ADC_CHAN_3V3;
                 int ADC_CHAN_5V;
                 int ADC_CHAN_1V2;
                 int ADC_CHAN_12VBAT;
                 int ADC_CHAN_24V;
                 int ADC_CHAN_28V;
                 int ADC_CHAN_T_COOLANT;
                 int ADC_CHAN_T_OIL;
                 int ADC_CHAN_IFP;
                 int ADC_CHAN_I12BAT;
                 int ADC_CHAN_I24V;
                 int ADC_CHAN_I28V;

                 int ADC_SOCA_MAP[ADC_NUM_SOC];
                 int ADC_SOCB_MAP[ADC_NUM_SOC];
                 int ADC_SOCC_MAP[ADC_NUM_SOC];
                 int ADC_SOCD_MAP[ADC_NUM_SOC];

                 uint32_t CAN_BaseID;
                 uint32_t CAN_Bitrate;
                 uint32_t CAN_REG_BASE;

                 float LPF_AC_PoleHz;       //Pole for RMS measurement
                 float LPF_DC_PoleHz;       //Pole for DC measurement
                 float LPF_Input_PoleHz;

                 enum ControlRefSel ControlRefSelect;
                 enum KnobSel KnobSelect;
                 enum StartStopSrc StartStopSel;
                 float StartPWMHigh_us;
                 float StartPWMLow_us;
                 enum FaultOutputModes FaultOutputMode;
                 enum AngleCommandSelections AngleCommandSelect;
                 enum SyncMode SyncMode;
};

struct MotorParameters {
                 unsigned char  StructVersion;
                 enum Motors MotorSel;
                 float PWM_FREQ_KHZ;
                 float T_LOOP;
                 float LOOP_FREQ_KHZ;

                 int ObserverCommAndMechAngleSwitch;

                 int SPEED_LOOP_PRESCALE;
                 float T_SPDLOOP;
                 uint16_t MAINISR_PRESCALE;

                 float StatorResistance;                    //Phase to neutral, phase-phase/2
                 float StatorInductance;                    //Phase to neutral, phase-phase/2
                 float Kv;                                  //Motor back EMF in Volts/RPM
                 float Kt;                                  //Torque constant in Nm per A
                 float Psi;                                 // Motor Back EMF in peak volts per rad/sec
                 float Gamma;
                 float Poles;
                 uint32_t BaseFrequency;
                 uint32_t BaseSpeed;

//LIMits
                 float  LIM_I_BUS_HI;
                 float  LIM_I_BUS_LO;

                 float  LIM_V_BUS_HI;
                 float  LIM_V_BUS_LO;

                 float  LIM_SPD_HI;
                 float  LIM_SPD_LO;
                 int    LIM_SPD_DRV;

                 float  LIM_ID_HI;
                 float  LIM_ID_LO;

                 float  LIM_IQ_HI;
                 float  LIM_IQ_LO;

                 float  LIM_POS_HI;
                 float  LIM_POS_LO;

                 float LIM_I_DC_CRIT_H;
                 float LIM_I_DC_CRIT_L;
                 float LIM_V_DC_CRIT_H;
                 float LIM_V_DC_CRIT_L;

                 float spd_KP;
                 float spd_KI;
                 float spd_integrator;

                 float PositionSensorOffset;

                 float pos_KP;
                 float pos_KI;
                 float pos_KD;
                 float pos_integrator;
                 float pos_C1;
                 float pos_C2;

                 float BusV_Nominal;
                 float ILOOP_BW;
                 float id_KP;
                 float id_KI;
                 float id_integrator;

                 float iq_KP;
                 float iq_KI;
                 float iq_integrator;

                 float DQDC_FFGain;

                 float K_SLF;
                 float K_SLIDE;

                 float Start_VLockRotor;
                 float Start_Hold_Time;
                 float Start_IQOL;
                 float Start_IDOL;
                 float Start_LockRotorAngle;
                 float Start_TransitionRpm;
                 float Start_SpeedTol;
                 float Start_AngTol;
                 float Start_EnableControllerRef;

                 float NoStart_Timeout_Sec;

                 float RAMP_to_1_Secs;
                 float RAMP_down_Secs;

                 float slave_phase_shift;

                 bool FOC_DQ_Decouple;

                 enum ControlLoops LoopsEn;

                 enum Observers_Angles ObsEn;
                 enum Observers_Angles CommAng;
                 enum Observers_Angles MechAng;

                 enum QEP_MODE QepMode;
                 enum BuildLevels BuildLevel;

                 float LIM_T_STATOR_WARN_H;
                 float LIM_T_STATOR_CRIT_H;
                 float LIM_T_STATOR_WARN_L;
                 float LIM_T_STATOR_CRIT_L;

                 float LIM_I_PHS;

                 bool reverseEncoderAngle;
                 float SPI_Offset;
                 float speedErrorPEngage;
                 float maxAndMinAngleDistOffset;
                 float maxAndMinAngleError;

               };

extern volatile struct CPU_timings CPU;
extern volatile struct InverterParameters Inverter_Params;
extern volatile struct MotorParameters Motor_Params;

#endif

