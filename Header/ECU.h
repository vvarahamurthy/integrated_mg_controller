/*
 * ECU.h
 *
 *  Created on: Sep 11, 2018
 *      Author: vvarahamurthy
 */
#ifndef HEADER_ECU_H_
#define HEADER_ECU_H_

#include "F2837S_files/F28x_Project.h"
#include "Header/Globals.h"

#define THROTTLE_ENABLE 1
#define PWMREGEN 0

struct ECUParameters {
    unsigned char StructVersion;

    float LOOP_FREQ_KHZ;        //!<Frequency in kHz at which the ECU state machine and control loop are called in the main ISR
    float CoolingLoopFreqKHz;   //!<Frequency in kHz at which the engine cooling control loop runs

    float T_LOOP;               //!<Period in seconds of the ECU control loop

    //!<PID controller variables
    float ThrottleFreq;         //!<Frequency in Hz of the PWM signal to the EFI for throttle opening
    float FanPWMFreq;           //!<Frequency in Hz of the PWM signal to the ESCs controlling the EDFs

    int PWMDAC_NUM_THROTTLE;    //!<PWMDAC channel 1 (A) or 2 (B) for throttle signal
    int PWMDAC_NUM_EDF;         //!<PWMDAC channel 1 (A) or 2 (B) for ESC signal

    enum ECUControlModes ControlMode;   //!<ECU Control mode (manual, speed, current, voltage, power)

    float pid_speed_kp; //!<Proportional gain in speed control mode
    float pid_speed_ki; //!<Integral gain in speed control mode
    float pid_speed_kd; //!<Derivative gain in speed control mode

    float pid_current_kp; //!<Proportional gain in current control mode
    float pid_current_ki; //!<Integral gain in current control mode
    float pid_current_kd; //!<Derivative gain in current control mode

    float pid_voltage_kp; //!<Proportional gain in voltage control mode
    float pid_voltage_ki; //!<Integral gain in voltage control mode
    float pid_voltage_kd; //!<Derivative gain in voltage control mode

    float pid_power_kp; //!<Proportional gain in power control mode
    float pid_power_ki; //!<Integral gain in power control mode
    float pid_power_kd; //!<Derivative gain in power control mode

#if(NEWCONTROL_MODE)
    float pid_chgctrl_kp; //!<Proportional of the charge control loop that fine tunes RPM setpoint
    float pid_chgctrl_ki; //!<Integral of the charge control loop that fine tunes RPM setpoint
    float pid_chgctrl_kd; //!<Derivative of the charge control loop that fine tunes RPM setpoint
#endif

    float activepid_kp; //!<Proportional gain of whatever control mode is active (used when updating values via CAN)
    float activepid_ki; //!<Integral gain of whatever control mode is active (used when updating values via CAN)
    float activepid_kd; //!<Derivative gain of whatever control mode is active (used when updating values via CAN)

    float pid_cooling_kp; //!<Proportional gain of the engine head cooling loop
    float pid_cooling_ki; //!<Integral gain of the engine head cooling loop
    float pid_cooling_kd; //!<Derivative gain of the engine head cooling loop

    float CoolingThreshLoC;     //!<Temperature in degC at which cooling shuts off
    float CoolingThreshHiC;     //!<Temperature in degC at which cooling turns on
    float CoolingTargetC;       //!<Target CHT in degC
    enum ECUCoolModes CoolingMode;  //!<Cooling mode (manual or PID)
    float CoolingDuty;          //!<Manual cooling duty cycle setting [0,1.0]
    uint16_t LIM_CHT_HI;        //!<High CHT limit
    uint16_t LIM_CHT_LO;        //!<Low CHT limit

    float feedfw_int;   //!<Feedforward intercept
    float feedfw_mv;    //!<Feedforward voltage slope
    float feedfw_mi;    //!<Feedforward current slope
    float feedfw_gain;  //!<Feedforward overall gain

    float DCP_LPF_Hz;       //!<Cutoff frequency in Hz of the low-pass filter on DC bus power
    float DCP_LPF_Alpha;    //!<Calculated from above as 1 - exp(-2*pi*DCP_LPF_Hz/Loop_Hz)

    float FF_LPF_Hz;    //!<Cutoff frequency in Hz of the low-pass filter on the feedforward term
    float FF_LPF_Alpha; //!<Calculated from above as 1 - exp(-2*pi*DCP_LPF_Hz/Loop_Hz)

    float StartingThrottleHi; //!< Throttle opening at engine start/idle. [0,1.0]
    float StartingThrottleLo; //!< Throttle opening at engine idle after WUE. [0,1.0]
    uint16_t StartupEnrichTime; //!< Time in seconds to go from high starting throttle (achieve WUE) to actual idle throttle


    uint16_t MinBusPower_H;    //!<DC bus power demand in W at which control loops are enabled (avoids low-load high RPM situations for engine health)
    uint16_t MinBusPower_L;    //!<DC bus power demand in W at which control loops are disabled
    uint16_t MaxGenPower;   //!<Maximum genset power in W allowable - all power past this value is supplied by the battery
    float MinCHTC;          //!<Minimum CHT at which control loops are enabled (warm-up limit)

    uint16_t LIM_SPD_HI;    //!<Max commandable speed reference

    float PowerSpeedSlope;      //!<Slope of LUT for Power Demand -> Speed Reference
    float PowerSpeedIntercept;  //!<Intercept of LUT for Power Demand -> Speed Reference

    double PowerThrottleSlope_Lo;     //!<Slope of LUT for throttle feedforward Power -> Throttle LUT at powers below 5.3kW
    double SpeedThrottleSlope_Lo;
    double PowerThrottleIntercept_Lo; //!<Intercept  of LUT for throttle feedforward Power -> Throttle LUT below 5.3kW

    double PowerThrottleSlope_Hi;     //!<Slope of LUT for throttle feedforward Power -> Throttle LUT above 5.3kW
    double PowerThrottleIntercept_Hi; //!<Intercept  of LUT for throttle feedforward Power -> Throttle LUT above 5.3kW

    uint16_t MinSpeedRef;      //!<Minimum allowable RPM command in power control mode
    uint16_t MaxSpeedRef;      //!<Maximum allowable RPM command in power control mode

    float IdleSpeed;           //!<Speed at which engine should idle (unused)
};

extern struct ECUParameters ECU_Params;

float GetEcuPidReference(void);
float GetEcuPidFeedback(void);
float GetEcuPidError(void);
float GetEcuPidUp(void);
float GetEcuPidUi(void);
float GetEcuPidUd(void);
float GetEcuPidOutPreSat(void);

#endif /* HEADER_ECU_H_ */
