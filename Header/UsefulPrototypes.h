/*
 * UsefulPrototypes.h
 *
 *  Created on: Feb 8, 2021
 *      Author: VishaalVarahamurthy
 */

#ifndef HEADER_USEFULPROTOTYPES_H_
#define HEADER_USEFULPROTOTYPES_H_

#include "F2837S_files/F28x_Project.h"

extern void inc(uint32_t *);
extern void dec(uint32_t *);
extern bool inRange(float , float , float );
extern float saturate(float , float , float);
extern float convBytesToFloat(uint32_t );
extern uint32_t convFloatToBytes(float );

#define LPF_MACRO(adc_filter,raw,alpha)                                              \
    adc_filter = (float)(alpha*(raw) + (1.0-alpha)*(adc_filter));                           \

#endif /* HEADER_USEFULPROTOTYPES_H_ */
