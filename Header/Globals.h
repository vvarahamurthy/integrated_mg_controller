/*
 * Globals.h
 *
 *  Created on: Nov 24, 2020
 *      Author: KeithKolb
 */

#ifndef HEADER_GLOBALS_H_
#define HEADER_GLOBALS_H_

#include "F2837S_files/F28x_Project.h"
#include "IQMathLib.h"

enum GenControlStateVariables {
    GenControllerInit = -2,
    GenControllerWaitForMain = -1,
    GenControllerDisabled = 0,
    GenControllerEnabled = 1,
    GenControllerAlternator = 2,
    GenControllerCharge = 3,
    GenControllerBoost = 4,
    GenControllerStart = 5,
    GenControllerStartTimeout = 6,
    System_Reset = 0xA5
};

enum MainStateVariables
{
    Main_Init = -2,
    Main_Fault = -1,
    Main_Inactive = 0,
    Main_Active = 1,
};

enum MCLowLevelStateVariables
{
    MC_Init=-2,
    MC_ProcessGatesDisabled=-1,
    MC_WaitForParent=0,
    MC_GatesDisable=1,
    MC_ProcessGatesEnable=2,
    MC_PWMEnable=3
};

enum HighMotorControlVar
{
    e_MCWaitForMain = 0,
    e_MCInit = 1,
    e_MCDisabled = 2,
    e_MCCheckRotation = 3,
    e_MCLockRotor = 4,
    e_MCRamp = 5,
    e_MCCommutate = 6
};

enum ElmoStateVariables
{
    ElmoInit=-2,
    ElmoWaitForParent=-1,
    ElmoDis=0,
    ElmoEn=1
};

enum ECULowLevelStateVariables
{
    ECUInit=-2,
    ECUWaitForParent=-1,
    ECUDisable=0,
    ECUEnable=1
};

enum CritFaultVariables {
    FLT_CRIT_NONE = 0,
    FLT_CRIT_ADC = 1 << 0,     //Includes ESTOP
    FLT_CRIT_LOOP = 1 << 1,
    FLT_CRIT_GPIO = 1 << 2,
    FLT_CRIT_COMM_CAN = 1 << 3,
    FLT_CRIT_COMM_SER = 1 << 4
};

enum GPIOFaultVariables
{
    FLT_GPIO_NONE = 0,
    FLT_GPIO_ESTOP = 1 << 0,

    Utripflag = 1 << 1,
    Vtripflag = 1 << 2,
    Wtripflag = 1 << 3,

    FLT_GPIO_DESAT_HU = 1 << 4,
    FLT_GPIO_DESAT_LU = 1 << 5,
    FLT_GPIO_DESAT_HV = 1 << 6,
    FLT_GPIO_DESAT_LV = 1 << 7,

    FLT_GPIO_DESAT_HW = 1 << 8,
    FLT_GPIO_DESAT_LW = 1 << 9,
    FLT_GPIO_RDY_U = 1 << 10,
    FLT_GPIO_RDY_V = 1 << 11,
    FLT_GPIO_RDY_W = 1 << 12,

    FLT_GPIO_PWM_INPUT = 1 << 13
};

enum LoopFaultVariables
{
    FLT_LOOP_NONE = 0,
    FLT_LOOP_ID = 1 << 0,
    FLT_LOOP_IQ = 1 << 1,
    FLT_LOOP_SPEED = 1 << 2,    //Ovedrspeed, overaccel, underspeed all combined into this error code
    FLT_LOOP_ANG_EST = 1 << 3,  //Remove this and know that transitioning causes this, let IQ blow up?
    FLT_LOOP_CHG_CTRL = 1 << 4, //Charge controller loop fault
    FLT_LOOP_THT_CTRL = 1 << 5,     //ECU loop fault
    FLT_LOOP_ELMO_MAXFLT = 1 << 6,
    FLT_LOOP_MAX_STARTS = 1 << 7
};

enum AnalogFaultVariables
{
    FLT_ADC_NONE = 0,
    FLT_ADC_I_U=1,
    FLT_ADC_I_V=2,
    FLT_ADC_I_W=3,
    FLT_ADC_I_BUS=4,
    FLT_ADC_I_CU=5,
    FLT_ADC_I_CV=6,
    FLT_ADC_I_CW=7,
    FLT_ADC_I_CBUS=8,
    FLT_ADC_OV_BUS=9,
    FLT_ADC_UV_BUS=10,
    FLT_ADC_V_CBUS=11,
    FLT_ADC_T_CPU=12,
    FLT_ADC_T_HS=13,
    FLT_ADC_T_PDB=14,
    FLT_ADC_T_BRD=15,
    FLT_ADC_T_DEV=16,
    FLT_ADC_T_ENG=17,
    FLT_ADC_T_STAT=18,
    FLT_ADC_I_ESC = 19
};

enum CANFaultVariables {
    FLT_CRIT_CAN_NONE = 0,
    FLT_CRIT_CAN_ISR = 1,
    FLT_CRIT_CAN_MSG1 = 1 << 1,
    FLT_CRIT_CAN_MSG2 = 1 << 2,
    FLT_CRIT_CAN_MSG3 = 1 << 3,
    FLT_CRIT_CAN_MSG4 = 1 << 4,
    FLT_CRIT_CAN_MSG5 = 1 << 5,
    FLT_CRIT_CAN_MSG6 = 1 << 6,
    FLT_CRIT_CAN_MSG7 = 1 << 7,
    FLT_CRIT_CAN_MSG8 = 1 << 8,
    FLT_CRIT_CAN_MSG9 = 1 << 9,
    FLT_CRIT_CAN_MSG10 = 1 << 10
};

enum SerialFaultVariables {
    FLT_CRIT_SER_NONE = 0,
    FLT_CRIT_SER_ISR = 1,
    FLT_CRIT_SER_MSG1 = 1 << 1,
    FLT_CRIT_SER_MSG2 = 1 << 2,
    FLT_CRIT_SER_MSG3 = 1 << 3,
    FLT_CRIT_SER_MSG4 = 1 << 4,
    FLT_CRIT_SER_MSG5 = 1 << 5,
    FLT_CRIT_SER_MSG6 = 1 << 6,
    FLT_CRIT_SER_MSG7 = 1 << 7,
    FLT_CRIT_SER_MSG8 = 1 << 8,
    FLT_CRIT_SER_MSG9 = 1 << 9,
    FLT_CRIT_SER_MSG10 = 1 << 10
};

enum ECUControlModes {
    ECUCtrl_Manual = 0,         //Direct throttle command from either user (debug) or Generator controller
    ECUCtrl_Speed = 1,          //RPM command either from user(debug) or Generator controller
    ECUCtrl_Voltage = 2,          //RPM command either from user(debug) or Generator controller
    ECUCtrl_Current = 3,          //RPM command either from user(debug) or Generator controller
    ECUCtrl_Power = 4
};

enum ECUCoolModes {
    Cool_Manual = 0,
    Cool_PID = 1,
    Cool_OnOff = 2
};

enum ControllerTypes {
    MotorController = 0,
    GeneratorController = 1,
};

enum GensetTypes {
    GensetNone = 0,
    GensetActive = 1,
    GensetPassive = 2
};

enum ElectronicsTypes {
    DriveNone = 0,
    DriveInternal = 1,
    DriveExternal = 2,
};


enum adc_signal_id {
    adc_id_iu=0,
    adc_id_iv=1,
    adc_id_iw=2,
    adc_id_idc=3,
    adc_id_idcelmo1=4,
    adc_id_idcelmo2=5,
    adc_id_vdc=6,
    adc_id_pdc=7,
    adc_id_vu=8,
    adc_id_vv=9,
    adc_id_vw=10,
    adc_id_tcpu=11,
    adc_id_tstator=12,
    adc_id_tpdb=13,
    adc_id_tlogic=14,
    adc_id_thsink=15,
    adc_id_tdevice=16,
    adc_id_spare1=17,
    adc_id_spare2=18,
    adc_id_speedknob=19,
    adc_id_i24v=20,
    adc_id_i28v=21,
    adc_id_i12v=22,
    adc_id_ifp=23,
    adc_id_v3v3=24,
    adc_id_v1v2=25,
    adc_id_v5v0=26,
    adc_id_v24v0=27,
    adc_id_v28v0=28,
    adc_id_v12v0=29,
    adc_id_tamb=30,
    adc_id_toil=31,
    adc_id_tcool=32,
    adc_num_ids=33
};


static float g_BMSFdbSig;
static float g_PMU_ControlRefSig;
static uint16_t g_TimeSinceReset;
static uint32_t g_Faults;
static float g_FeedForwardTerm;
static float g_EcuControlRef;
static float g_FeedForwardFiltered;
static float g_actualPMUMode;

//Define global query-able ADC values
static float g_adc_currentbuselmo1;
static float g_adc_currentbuselmo2;
static float g_adc_currentu;
static float g_adc_currentv;
static float g_adc_currentw;
static float g_adc_currentbus;

static float g_adc_voltageu;
static float g_adc_voltagev;
static float g_adc_voltagew;
static float g_adc_voltagebus;

static float g_adc_powerbus;

static float g_adc_tempcpu;
static float g_adc_tempstator;
static float g_adc_tempdevice;
static float g_adc_tempheatsink;
static float g_adc_templogicboard;
static float g_adc_temppowerboard;

static float g_adc_spareinput1;
static float g_adc_spareinput2;
static float g_adc_speedknob;

float GetBMSFdbSig(void); // { return g_BMSFdbSig; }
float GetPMU_ControlRefSig(void); // { return g_PMU_ControlRefSig; }
uint16_t GetTimeSinceReset(void); // { return g_TimeSinceReset; }
uint32_t GetFaults(void); // { return g_Faults; }
float GetFeedForwardTerm(void); // { return g_FeedForwardTerm; }
float GetEcuControlRef(void); // { return g_EcuControlRef; }
float GetFilteredFeedForward(void); // { return g_FeedForwardFiltered; }

#endif /* HEADER_GLOBALS_H_ */

