/*
 * Observer.h
 *
 *  Created on: Sep 21, 2017
 *      Author: MRR
 */


#ifndef __OBSERVER_H__
#define __OBSERVER_H__


typedef struct {
    _iq  Rmotor;     // Alpha-Beta frame motor resistance in ohms
    _iq  Lmotor;     // Alpha-Beta frame motor inductance in henry
    _iq  PsiSquared;   // Alpha-Beta motor PM flux linkage squared

    _iq  Xhalpha;    // Alpha estimator state variable
    _iq  Xhbeta;     // Beta estimator state variable

    _iq  Ialpha;      // Input: Stationary alfa-axis stator current (measured)
    _iq  Ibeta;       // Input: Stationary beta-axis stator current (measured)
    _iq  Valpha;      // Input: Stationary alfa-axis stator voltage (Measured/commanded)
    _iq  Vbeta;       // Input: Stationary beta-axis stator voltage (measured/commanded)

    _iq  Yalpha;      // Observer input alpha
    _iq  Ybeta;       // Observer input beta

    _iq  ETAXhalpha;      // ETA of Xhat alpha
    _iq  ETAXhbeta;       // ETA of Xhat beta

    _iq  Gamma;      // Parameter: Observer gain
    _iq  Tloop;      // Update time of the the loop in seconds

    _iq  Theta;       // Output: Compensated rotor angle
    _iq	 VecDistance;  //Psi^2 - (ETAXhA^2 + ETAXhB^2)
           } OBSERV;

/*-----------------------------------------------------------------------------
Default initalizer for the SMOPOS object.
-----------------------------------------------------------------------------*/
#define OBSERV_DEFAULTS {  0,0,0,0,0,0,0,0, \
                           0,0,0,0,0,0,0,0,0 }


/*------------------------------------------------------------------------------
Prototypes for the functions in OBSERV.C
------------------------------------------------------------------------------*/

#define OBSERV_MACRO(v)                                                                                 \
                                                                                                        \
    /*  Y inputs to oberserver equation (4) in Ortega */                                                \
    v.Yalpha = v.Valpha - v.Rmotor*v.Ialpha;                                                            \
    v.Ybeta = v.Vbeta - v.Rmotor*v.Ibeta;                                                               \
                                                                                                        \
    /*  ETA   */                                                                                        \
    /* ETA(Xh) = Xh - L*Ialphabeta   equation (6)*/                                                     \
    v.ETAXhalpha =  v.Xhalpha - v.Lmotor*v.Ialpha;                                                      \
    v.ETAXhbeta =  v.Xhbeta - v.Lmotor*v.Ibeta;                                                         \
                                                                                                        \
    /* Observer -- equation (8)  */                                                                     \
    v.Xhalpha = v.Xhalpha + v.Tloop*(v.Yalpha+v.Gamma/2*v.ETAXhalpha*(v.Psimotor^2-(v.ETAXhalpha^2+v.ETAXhbeta^2)));   \
    v.Xhbeta = v.Xhbeta + v.Tloop*(v.Ybeta+v.Gamma/2*v.ETAXhbeta*(v.Psimotor^2-(v.ETAXhalpha^2+v.ETAXhbeta^2)));       \
                                                                                                        \
    /*  Rotor angle calculator -> Theta = atan( (xh2-Lib)/(xh1-Lia)) Eq (9) in Ortega  */               \
    v.Theta = _IQatan2PU( (v.Xhbeta-v.Lmotor*v.Ibeta),(v.Xhalpha-v.Lmotor*v.Ialpha));

#endif



