/*
 * RingBuffer.h
 *
 *  Created on: Jun 16, 2017
 *      Author: vvarahamurthy
 */
#include <stdbool.h>

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#define CMD_BUFFER_SIZE 100 //1 + 4 + 63*2 = 131
#define ECU_BUFFER_SIZE 100 //212 bytes in the stream

typedef struct {
	unsigned char *head;
	unsigned char *tail;
	unsigned char *read;
	unsigned char size;
	unsigned char count;
	unsigned char *data;
} RINGBUFFER;

unsigned char rb_pop(RINGBUFFER *);
unsigned char rb_read(RINGBUFFER *);

int rb_count(RINGBUFFER *);

void rb_rewind(RINGBUFFER *);
void rb_init(RINGBUFFER *, unsigned char );
void rb_flush(RINGBUFFER *);
void rb_push(RINGBUFFER *, unsigned char );
void rb_deleteHead(RINGBUFFER *);

bool isRingBufferEmpty( RINGBUFFER *);
bool isRingBufferFull( RINGBUFFER *);
bool isReadAtTail(RINGBUFFER *);


#endif /* RINGBUFFER_H_ */
