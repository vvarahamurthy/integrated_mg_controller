/*
 * ParameterMap.h
 *
 *  Created on: Feb 5, 2019
 *      Author: vvarahamurthy
 */

#ifndef HEADER_PARAMETERMAP_H_
#define HEADER_PARAMETERMAP_H_

#define NUM_PARAMS 150

enum datatype {
    type_char,
    type_uchar,
    type_int,
    type_uint,
    type_long,
    type_ulong,
    type_longlong,
    type_ulonglong,
    type_float,
    type_double
};


void InitParameterArray(void);
void SetParamMapElement(enum datatype, int , void * );

void SetParameterValue(int ,float );

void SetParam_int(float , void * );
void SetParam_uint(float , void * );

//8-bit numbers are actually "faked" in this CPU since all memory locations are 16-bit, so these may not even be necessary
void SetParam_char(float , void * );
void SetParam_uchar(float , void * );

void SetParam_long(float , void * );
void SetParam_ulong(float , void * );
void SetParam_longlong(float, void *);
void SetParam_ulonglong(float, void *);
void SetParam_float(float , void * );
void SetParam_double(float , void * );


void SetBroadcastValue(int , enum datatype , void * );

float GetParameterValue(int);

uint32_t GetRawParameterValue(int);

float GetBroadcastValue(int);
uint32_t GetRawBroadcastValue(int);

struct BroadcastEntries
{
    void * valueFunction;
    enum datatype dataType;
};

enum paramIds
{
    //Define inverter parameter IDs which can be set while moving
    ID_SW_VERSION = 0,
    ID_SERIAL_NUMBER = 1,

    //define motor parameter IDs which can be set while moving
    ID_SPD_KP = 2, //Motor_Params.spd_KP
    ID_SPD_KI = 3, //Motor_Params.spd_KI
    ID_POS_KP = 4,
    ID_POS_KI = 5,
    ID_ILOOP_GAIN = 6,
    ID_RAMPUP_SECS = 7,
    ID_RAMPDOWN_SECS = 8, //Motor_Params.RAMP_down_Secs

    //Define parameter IDs that can be set only while stopped
    ID_START_TLOCK = 9,
    ID_START_VLOCK = 10, //Motor_Params.Start_VLockRotor
    ID_START_IQOL = 11, //Motor_Params.Start_IDOL
    ID_START_IDOL = 12, //Motor_Params.Start_IQOL
    ID_START_TSPEED = 13, //Motor_Params.Start_TransitionSpeed
    ID_START_SPDTOL = 14,
    ID_START_ANGTOL = 15,
    ID_START_SPDEN = 16,
    ID_LIM_SPDH = 17,
    ID_LIM_SPDL = 18,
    ID_LIM_ACCH = 19,
    ID_LIM_POSH = 20,
    ID_LIM_POSL = 21,
    ID_LIM_IQL = 22,
    ID_LIM_MIBUSH = 23,
    ID_LIM_MIBUSL = 24,
    ID_LIM_MIPHSH = 25,
    ID_LIM_MVBUSH = 26,
    ID_LIM_MVBUSL = 27,
    ID_BUILDLVL = 28, //Motor_Params.BuildLevel
    ID_BUSV_NOM = 29, //Motor_Params.BusV_Nominal
    ID_BASESPD = 30,
    ID_OBSEN = 31,
    ID_COMMANG = 32,
    ID_MECHANG = 33,
    ID_FOCDCPL = 34,
    ID_LOOPSEN = 35, //Motor_Params.LoopsEn
    ID_QEPMODE = 36,
    ID_SLAVE_SHIFT = 37,
    ID_MOTOR_SEL = 38,
    ID_PWM_FREQ = 40, //Motor_Params.PWM_FREQ_KHZ
    ID_LOOP_FREQ = 41, //Motor_Params.LOOP_FREQ_KHZ
    ID_MAINISR_PSC = 42,
    ID_SPDLOOP_PSC = 43, //Motor_Params.SPEED_LOOP_PRESCALE
    ID_MOTOR_R = 44, //Motor_Params.StatorResistance
    ID_MOTOR_L = 45, //Motor_Params.StatorInductance
    ID_MOTOR_KV = 46, //Motor_Params.Kv
    ID_MOTOR_GAMMARPM = 47,
    ID_MOTOR_POLES = 48, //Motor_Params.Poles
    ID_MOTOR_KSLF = 49,
    ID_MOTOR_KSLIDE = 50,

    //Define parameter IDs that can be set only while stopped
    ID_LIM_IIBUSH = 51,
    ID_LIM_IIBUSL = 52,
    ID_LIM_IIPHSH = 53,
    ID_LIM_IVBUSH = 54,
    ID_LIM_TCPUH = 55,
    ID_LIM_TCPUL = 56,
    ID_LIM_TDEVH = 57,
    ID_LIM_TDEVL = 58,
    ID_LIM_TTC1H = 60,
    ID_LIM_TTC1L = 61,
    ID_LIM_TTC2H = 62,
    ID_LIM_TTC2L = 63,
    ID_LPFPOLE_AC = 64,
    ID_LPFPOLE_DC = 65,
    ID_LPFPOLE_IN = 66,
    ID_CANID = 67,
    ID_CANBITRATE = 68,
    ID_SEL_CTLREF = 69,
    ID_SEL_ANGREF = 70,
    ID_SEL_STSP = 71,
    ID_SEL_FLTO = 72,
    ID_SYNCMODE = 73,

    //Define ECU parameter IDs which can be set while moving
    ID_THRT_KP = 74,
    ID_THRT_KI = 75,
    ID_THRT_KD = 76,
    ID_COOL_KP = 77,
    ID_COOL_KI = 78,
    ID_COOL_KD = 79,
    ID_COOL_LO = 80,
    ID_COOL_HI = 81,
    ID_COOL_TARGET = 82,
    ID_COOL_MODE = 83,
    ID_COOL_DUTY = 84,
    ID_FFW_GAIN = 85,
    ID_IDLE_THRT = 86,
    ID_CHT_REV = 87,

    //Define ECU IDs that can be set only while stopped
    ID_LIM_TSTATOR = 88,
    ID_LIM_TCHT = 89,
    ID_LIM_THSNK = 90,
    ID_LIM_ESPDH = 91,
    ID_LIM_ESPDL = 92,
    ID_LIM_VVBUSL = 93,
    ID_LIM_IVBUSL = 94,
    ID_LIM_EVBUSH = 95,
    ID_LIM_EIBUSH = 96,
    ID_CTRL_MODE = 97,
    ID_IDLE_MS = 98,
    ID_REV_IBUSL = 99,
    ID_REV_IBUSH = 100,
    ID_IDLE_RPM = 101,
    ID_STARTLIM = 102,
    ID_STARTTYPE = 103,
    ID_ZEROSPD_MS = 104,
    ID_ICRANK = 105,
    ID_TESTENV = 106,
    ID_ECULOOP_FREQ = 107,
    ID_COOLLOOP_FREQ = 108,
    ID_ECULOOP_PSC = 109,
    ID_GC_TSTART = 110,
    ID_GC_KP = 111,
    ID_GC_KI = 112,
    ID_GC_KD = 113,
    ID_LPF_ECU_DC = 114,
    ID_LPF_ECU_FF = 115,
    ID_ECU_GAIN_FF = 116,
    ID_LPF_GC_DC = 117,
    ID_LPF_GC_FF = 118,
    ID_GC_FF_GAIN = 119,
    ID_ECU_CC_KP = 120,
    ID_ECU_CC_KI = 121,
    ID_ECU_CC_KD = 122,
    ID_ECU_IDLETHI = 123,
    ID_ECU_IDLETLO = 124,
    ID_LIM_STATOR_DERATE = 125,
    ID_GC_DERATE = 126,
    ID_GC_STARTRPM = 127,
    ID_GC_POWERTHRESH=128,
    ID_GC_MAXPWR = 129,
    ID_ECU_MINPWRH = 130,
    ID_ECU_MINPWRL = 131,
    ID_ECU_MAXPWR = 132,
    ID_ECU_TENRICH = 133,
    ID_GC_MAN = 134,
    ID_MAX_NUMBER_IDS = 135
};

enum broadcastIds
{
    Elmo1_State = 1,
    Elmo2_State = 2,
    Control_Reference = 3,
    BMS_Current_Value = 4,
    Genset1_Current = 5,
    Genset2_Current = 6,
    Bus_Voltage = 7,
    PID1_Reference = 8,
    PID1_Feedback = 9,
    PID1_Error = 10,
    PID1_Up = 11,
    PID1_Ui = 12,
    PID1_Ud = 13,
    PID1_OutPreSat = 14,
    PID2_Reference = 15,
    PID2_Feedback = 16,
    PID2_Error = 17,
    PID2_Up = 18,
    PID2_Ui = 19,
    PID2_Ud = 20,
    PID2_OutPreSat = 21,
    Stator_Temp = 22,
    Engine_Temp = 23,
    Engine_Speed = 24,
    Throttle = 25,
    Logic_Board_Temp = 26,
    Power_Board_Temp = 27,
    CPU_Temp = 28,
    Heatsink_Temp = 29,
    DC_Power = 30,
    ECU_State = 31,
    Time_Since_Reset = 32,
    Current_Faults = 33,
    ECU_Feedforward_Term = 34,
    ECU_PID_Reference = 35,
    ECU_PID_Feedback = 36,
    ECU_PID_Error = 37,
    ECU_PID_Up = 38,
    ECU_PID_Ui = 39,
    ECU_PID_Ud = 40,
    ECU_PID_OutPreSat = 41,
    ECU_Control_Reference = 42,
    GenControlCurrent_Filtered = 43,
    PMU_Mode = 44,
    Elmo1_NumFaults = 45,
    Elmo2_NumFaults = 46,
    ECU_CHG_PID_OutPreSat = 47,
    ECU_RPM_LUT = 48,
    ECU_DCPLPF = 49,
    GC_DCPLPF = 50,
    Number_Of_Broadcast_Opcodes = 51
};


float GetBroadcastValue(int);
uint32_t GetRawBroadcastValue(int);

#endif /* HEADER_PARAMETERMAP_H_ */
