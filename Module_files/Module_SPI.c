/*
 * Module_SPI.c
 *
 *  Created on: Sep 11, 2018
 *      Author: vvarahamurthy
 *
 *      This module handles SPI communication with the thermocouple
 *      reading chip
 */

#include "F2837S_files/F28x_Project.h"
#include "F2837xS_Pie_defines.h"
#include "Header/GUIC_Header.h"
#include "Header/Globals.h"

#define FixMeLater 1

uint16_t getSPIRxSSI(void);

void Init_SPI_Periph(void);
void Init_SPI_Interrupts(void);
interrupt void spiRxFifoIsr_TC(void);
interrupt void spiRxFifoIsr_SSI(void);
void SampleThermocouple(unsigned char);
void SPI_PingEncoder(void);
float SPI_GetSSIAngle(void);

void resetFirstRunSSI(void);
bool isInitialRunSSI(void);

float T_Stator=0.0;
float T_Head=0.0;
float SSIAngle = 0.0;
uint16_t SSItemp = 0.0;
float SSIAngleA[4];
bool isFirstRunSSI = 0;

void SPI_PingEncoder(void)
{
    // Transimit a clock signal to the encoder
    SpibRegs.SPITXBUF = (uint16_t)0xFF;
}


void Init_SPI_Periph(void)
{
    EALLOW;
    isFirstRunSSI = 0;

    // Initialize SPI FIFO registers
       SpibRegs.SPICCR.bit.SPISWRESET=0; // Reset SPI
       SpibRegs.SPICCR.all=0x000F;       //16-bit character, Loopback mode
       SpibRegs.SPICTL.all=0x0017;       //Interrupt enabled, Master/Slave XMIT enabled
       SpibRegs.SPISTS.all=0x0000;
#ifndef FixMeLater
       if(Inverter_Params.InverterSel==MGC2_12)
       {
           SpibRegs.SPIBRR.all=0x0063;       // baud = LSPLK/(BRR+1) = 500kHz, so LOSPCP = 4?
       }
       else if(Inverter_Params.InverterSel==MC40)
       {
           SpibRegs.SPIBRR.all=0x0031;       // 1MHz - SSI testing
       }
#endif
       SpibRegs.SPIBRR.all=0x0031;       // 1MHz - SSI testing MC40
       SpibRegs.SPIFFTX.all=0xC024;      // Enable FIFO's, set TX FIFO level to 4
       SpibRegs.SPIFFRX.all=0x0022;      // Set RX FIFO level to 4
       SpibRegs.SPIFFCT.all=0x00;
       SpibRegs.SPIPRI.all=0x0010;

       SpibRegs.SPIFFTX.bit.TXFFIENA=0;

       SpibRegs.SPIFFTX.bit.TXFIFO=1;
       SpibRegs.SPIFFRX.bit.RXFIFORESET=1;

       SpibRegs.SPICCR.bit.SPISWRESET=1;  // Enable SPI

//       SPI interrupts
#ifndef FixMeLater
       if(Inverter_Params.InverterSel==MGC2_12)
       {
           PieVectTable.SPIB_RX_INT = &spiRxFifoIsr_TC;
       }
       else if(Inverter_Params.InverterSel==MC40)
       {
           PieVectTable.SPIB_RX_INT = &spiRxFifoIsr_SSI;
       }
#endif
       PieVectTable.SPIB_RX_INT = &spiRxFifoIsr_SSI;
       PieCtrlRegs.PIEIER6.bit.INTx3=1;     // Enable PIE Group 6, INT 3
//       PieCtrlRegs.PIEIER6.bit.INTx4=1;    //Enable PIE Group 6, INT 4
       IER |= M_INT6;
      EDIS;
}

uint16_t tempspirx = 0;

interrupt void spiRxFifoIsr_SSI(void)
{
    uint16_t SpiRxData[]={0,0};
    //Read 32 bits out of SPI RX Buffer
    SpiRxData[0]=SpibRegs.SPIRXBUF;

    tempspirx = SpiRxData[0];

    // Masked out 3 bits on 3 different positions because RLS did not document it!
    SSIAngle = ( (( tempspirx & 0x7FFC) >> 2) * (0.04395/360.0));


    SpibRegs.SPIFFRX.bit.RXFFOVFCLR=1;  // Clear Overflow flag
    SpibRegs.SPIFFRX.bit.RXFFINTCLR=1;  // Clear Interrupt flag
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP6;      // Issue PIE ack
}

uint16_t getSPIRxSSI(void)
{
    return tempspirx;
}

float SPI_GetHeadTempC(void)
{
    return T_Head;
}

float SPI_GetStatorTempC(void)
{
    return T_Stator;
}

float prevThetaElect = 0.0;

bool isInitialRunSSI(void)
{
    return isFirstRunSSI;
}

void resetFirstRunSSI(void)
{
    isFirstRunSSI = 0;
}

float SPI_GetSSIAngle(void)
{
    return SSIAngle;
}
