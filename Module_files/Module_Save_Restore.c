/*
============================================================================
System Name:    LaunchPoint Motor Controller

File Name:      Module_Save_Restore.c

Target:         LP motor controllers

Author:         Mike Ricci

Description:    Saving and Restoring motor drive system parameters fromt the I2C-based EEPROM non-volatile storage
********************************************************************************* */

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "Header/GUIC_Header.h"
#include "Header/ECU.h"
#include "Header/Save_Restore.h"
#include "F2837S_files/F2837xS_I2c_defines.h"
#include "Header/I2C.h"

// ****************************************************************************
// Function Prototypes
// ****************************************************************************
bool  EEPROM_save_motor(struct MotorParameters *Motor_Params, uint32_t StructVersion);
bool  EEPROM_save_inverter(struct InverterParameters *Inverter_Params, uint32_t StructureVersion);
bool  EEPROM_load_motor(struct MotorParameters *Motor_Params, uint32_t StructVersion);
bool  EEPROM_load_inverter(struct InverterParameters *Inverter_Params, uint32_t StructureVersion);
bool  EEPROM_save_ECU(struct ECUParameters *, uint32_t);
bool  EEPROM_load_ECU(struct ECUParameters *, uint32_t);


void UpdateSuccessByte(enum SRSuccess, bool);
void IncrementCounter(unsigned char *);
unsigned char SR_GetSuccessStatus(void);

void LoadFromEEPROM(void);
// ****************************************************************************
// External Variables -- linkage to other modules
// ****************************************************************************

extern uint16_t I2C_Write(uint16_t *,uint16_t, uint32_t);
extern uint16_t I2C_Read(uint16_t *,uint16_t, uint32_t);
extern bool isGenerator(enum SyncMode);

// *****************************************************************************
//  Module resources - variables here //
// *****************************************************************************


static union MotorData MotorDataLoc;
static union InverterData InverterDataLoc;
static union ECUData ECUDataLoc;
static union StartOfRecord StartingData;

unsigned char LoadCnt_Motor=0;
unsigned char LoadCnt_Inv=0;
unsigned char LoadCnt_ECU = 0;
unsigned char SaveCnt_Motor=0;
unsigned char SaveCnt_Inv=0;
unsigned char SaveCnt_ECU=0;

enum SRSuccess LastRWStatus=SR_None;

// *****************************************************************************
//  Debug resources - variables here -- delete many of these once code works //
// *****************************************************************************

// *****************************************************************************
// Constants
// *****************************************************************************

// *****************************************************************************
// Modules - code here
// *****************************************************************************

bool EEPROM_save_motor(struct MotorParameters *p_Motor_Params, uint32_t StructVersion)
{
    bool returnvalue;

    StartingData.ProgrammedFlag = EEPROM_PROGRAMMED_FLAG;
    StartingData.Version = StructVersion;
    StartingData.WordCount = sizeof(struct MotorParameters);

    MotorDataLoc.Parameters = *p_Motor_Params;

    if (sizeof(union StartOfRecord) + sizeof(struct MotorParameters) >= INVERTERSTARTADDRESS )
    {
        return(false);
    }

    if (I2C_Write(StartingData.Data,sizeof(union StartOfRecord), MOTORSTARTADDRESS) == I2C_SUCCESS)
    {
        if( I2C_Write(MotorDataLoc.Data,sizeof(struct MotorParameters), MOTORSTARTADDRESS+sizeof(union StartOfRecord))== I2C_SUCCESS)
        {
            returnvalue = true;
            IncrementCounter(&SaveCnt_Motor);
        }
        else
        {
            returnvalue = false;
        }
    }
    else
    {
        returnvalue = false;
    }

    UpdateSuccessByte(MotorSave, returnvalue);

    return(returnvalue);
}


bool EEPROM_load_motor(struct MotorParameters *p_Motor_Params, uint32_t StructVersion)
{
    bool returnvalue;

    if (I2C_Read(StartingData.Data,sizeof(union StartOfRecord), MOTORSTARTADDRESS) == I2C_SUCCESS)
    {
        if( (StartingData.ProgrammedFlag == EEPROM_PROGRAMMED_FLAG) &&
                (StartingData.Version == StructVersion) &&
                (StartingData.WordCount == sizeof(struct MotorParameters) ))
        {
            if( I2C_Read(MotorDataLoc.Data ,sizeof(struct MotorParameters), MOTORSTARTADDRESS+sizeof(union StartOfRecord))== I2C_SUCCESS)
            {
                *p_Motor_Params = MotorDataLoc.Parameters;
                returnvalue = true;
                IncrementCounter(&LoadCnt_Motor);
            }
            else
            {
                returnvalue = false;
            }
        }
        else
        {
            returnvalue = false;
        }
    }
    else
    {
        returnvalue = false;
    }

    UpdateSuccessByte(MotorLoad, returnvalue);

    return(returnvalue);
}

bool EEPROM_save_inverter(struct InverterParameters *p_Inverter_Params, uint32_t StructVersion)
{
    bool returnvalue;

    StartingData.ProgrammedFlag = EEPROM_PROGRAMMED_FLAG;
    StartingData.Version = StructVersion;
    StartingData.WordCount = sizeof(struct InverterParameters);

    InverterDataLoc.Parameters = *p_Inverter_Params;

    if (sizeof(union StartOfRecord) + sizeof(struct InverterParameters) + INVERTERSTARTADDRESS  >= MAX_EEPROM_ADDRESS_BYTES/2 )
    {
        return(false);
    }

    if (I2C_Write(StartingData.Data,sizeof(union StartOfRecord), INVERTERSTARTADDRESS) == I2C_SUCCESS)
    {
        if( I2C_Write(InverterDataLoc.Data, sizeof(struct InverterParameters), INVERTERSTARTADDRESS+sizeof(union StartOfRecord))== I2C_SUCCESS)
        {
            returnvalue = true;
            IncrementCounter(&SaveCnt_Inv);
        }
        else
        {
            returnvalue = false;
        }
    }
    else
    {
        returnvalue = false;
    }

    UpdateSuccessByte(InverterSave, returnvalue);


    return(returnvalue);
}


bool EEPROM_load_inverter(struct InverterParameters *p_Inverter_Params, uint32_t StructVersion)
{
    bool returnvalue;

    if (I2C_Read(StartingData.Data,sizeof(union StartOfRecord), INVERTERSTARTADDRESS) == I2C_SUCCESS)
    {
        if( (StartingData.ProgrammedFlag == EEPROM_PROGRAMMED_FLAG) &&
                (StartingData.Version == StructVersion) &&
                (StartingData.WordCount == sizeof(struct InverterParameters) ))
        {
            if( I2C_Read(InverterDataLoc.Data ,sizeof(struct InverterParameters), INVERTERSTARTADDRESS+sizeof(union StartOfRecord))== I2C_SUCCESS)
            {
                *p_Inverter_Params = InverterDataLoc.Parameters;
                returnvalue = true;
                IncrementCounter(&LoadCnt_Inv);
            }
            else
            {
                returnvalue = false;
            }
        }
        else
        {
            returnvalue = false;
        }
    }
    else
    {
        returnvalue = false;
    }

    UpdateSuccessByte(InverterLoad, returnvalue);

    return(returnvalue);
}

bool EEPROM_save_ECU(struct ECUParameters *p_ECU_Params, uint32_t StructVersion)
{
    bool returnvalue;

    StartingData.ProgrammedFlag = EEPROM_PROGRAMMED_FLAG;
    StartingData.Version = StructVersion;
    StartingData.WordCount = sizeof(struct ECUParameters);

    ECUDataLoc.Parameters = *p_ECU_Params;

    if (sizeof(union StartOfRecord) + sizeof(struct ECUParameters) + ECUSTARTADDRESS  >= MAX_EEPROM_ADDRESS_BYTES/2 )
    {
        return(false);
    }

    if (I2C_Write(StartingData.Data,sizeof(union StartOfRecord), ECUSTARTADDRESS) == I2C_SUCCESS)
    {
        if( I2C_Write(ECUDataLoc.Data, sizeof(struct ECUParameters), ECUSTARTADDRESS+sizeof(union StartOfRecord))== I2C_SUCCESS)
        {
            returnvalue = true;
            IncrementCounter(&SaveCnt_ECU);
        }
        else
        {
            returnvalue = false;
        }
    }
    else
    {
        returnvalue = false;
    }

    UpdateSuccessByte(ECUSave, returnvalue);


    return(returnvalue);
}


bool EEPROM_load_ECU(struct ECUParameters *p_ECU_Params, uint32_t StructVersion)
{
    bool returnvalue;

    if (I2C_Read(StartingData.Data,sizeof(union StartOfRecord), ECUSTARTADDRESS) == I2C_SUCCESS)
    {
        if( (StartingData.ProgrammedFlag == EEPROM_PROGRAMMED_FLAG) &&
                (StartingData.Version == StructVersion) &&
                (StartingData.WordCount == sizeof(struct ECUParameters) ))
        {
            if( I2C_Read(ECUDataLoc.Data ,sizeof(struct ECUParameters), ECUSTARTADDRESS+sizeof(union StartOfRecord))== I2C_SUCCESS)
            {
                *p_ECU_Params = ECUDataLoc.Parameters;
                returnvalue = true;
                IncrementCounter(&LoadCnt_ECU);
            }
            else
            {
                returnvalue = false;
            }
        }
        else
        {
            returnvalue = false;
        }
    }
    else
    {
        returnvalue = false;
    }

    UpdateSuccessByte(ECULoad, returnvalue);

    return(returnvalue);
}



void IncrementCounter(unsigned char * a)
{
    if(*a>=255)
    {
        *a=1;
    }
    else
    {
        (*a)++;
    }
}

unsigned char SR_GetSuccessCtr(enum RWFlag flag)
{
    unsigned char returnvalue = 0;
    switch(flag)
    {
        case READmotor:
            returnvalue = LoadCnt_Motor;
            break;
        case WRITEmotor:
            returnvalue = SaveCnt_Motor;
            break;
        case READinverter:
            returnvalue = LoadCnt_Inv;
            break;
        case WRITEinverter:
            returnvalue = SaveCnt_Inv;
            break;
        case READECU:
            returnvalue = LoadCnt_ECU;
            break;
        case WRITEECU:
            returnvalue = SaveCnt_ECU;
        default:
            returnvalue = 0;
            break;
    }
    return returnvalue;
}

unsigned char SR_GetSuccessStatus(void)
{
    return (unsigned char)LastRWStatus;
}

void UpdateSuccessByte(enum SRSuccess successbit, bool success)
{
    if(success==true)
    {
        LastRWStatus |= successbit;
    }
    else
    {
        LastRWStatus &= (~successbit);
    }
}

void LoadFromEEPROM(void)
{
#if 0
    if(isGenerator(Inverter_Params.SyncMode))
    {
        EEPROM_load_ECU((struct ECUParameters *)&ECU_Params, ECU_Params.StructVersion);
    }
    else
    {
        EEPROM_load_motor((struct MotorParameters *)&Motor_Params, Motor_Params.StructVersion);
    }
#endif
    EEPROM_load_inverter((struct InverterParameters *)&Inverter_Params, Inverter_Params.StructVersion);
}
