/* ============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	PWM.C

Target:			Cryo controller board

Author:			Mike Ricci

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle the processor PWM peripherals and associated data structures
				FOR H-BRIDGE/COMMUTATION ONLY - see PWMRect for rectifier PWM control and PWMDAC for ECU control PWMs
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "IQMathLib.h"
#include "Header/GUIC_Header.h"
#include "F2837S_files/f28377pwm_drivers.h"


//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1


// Function prototypes
void Init_PWM(void);
void Reset_PWM_trip(void);
void Disable_PWM(void);
void PWM_Out(_iq a,_iq b,_iq c);
void TripZoneConfig(int );
bool CheckTripFlags(int );

// ****************************************************************************
// External Variables -- linkage to other modules
// ****************************************************************************

// ****************************************************************************
//  Module resources - variables here //
// ****************************************************************************

// Instance a PWM driver instance
PWMGEN pwm1 = PWMGEN_DEFAULTS;

// Used to indirectly access all EPWM modules -- used by PWM_MACROs below
volatile struct EPWM_REGS * ePWM[] = {
		&EPwm1Regs,			//intentional: (ePWM[0] not used)
		&EPwm1Regs, &EPwm2Regs, &EPwm3Regs, &EPwm4Regs, &EPwm5Regs, &EPwm6Regs, &EPwm7Regs,
		&EPwm8Regs, &EPwm9Regs, &EPwm10Regs, &EPwm11Regs, &EPwm12Regs};

// Function Definitions            //
void Init_PWM(void)
{
    unsigned int T_PWM = (uint16_t)(CPU.milliSec/Motor_Params.PWM_FREQ_KHZ);
	// ****************************************************************************
	// ****************************************************************************
	// PWM Configuration
	// ****************************************************************************
	// ****************************************************************************

	    // Initialize PWM module
		EALLOW;
		CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 0;	// Stop PWM clocks while setting up (?)
		ClkCfgRegs.PERCLKDIVSEL.bit.EPWMCLKDIV = 0;	// Set prescaler/divider for PWM clock from Sysclk to 1

		/* Initialize PWM module */  // is this repeated by some other stuff below -- at least the PWM_INIT_MACRO part?
		pwm1.PeriodMax = T_PWM/2;  // Prescaler X1 (T1), PWM period = T x 1
		pwm1.HalfPerMax= T_PWM/4;
		pwm1.Deadband  = (int16) ((float)Inverter_Params.DeadBand);								// setting = 10 gives ~67nS @ 150MHz ((GUIC))

		/*  Set up the three PWM modules  */
		// MC40 does not have current trips. Note, you may want to turn this off in Buildlevel 1.
		if(Motor_Params.BuildLevel != e_OpenLoop)
		{
	        TripZoneConfig(Inverter_Params.PWM_U_NUM);
	        TripZoneConfig(Inverter_Params.PWM_V_NUM);
	        TripZoneConfig(Inverter_Params.PWM_W_NUM);
		}

        /* Setup Sync*/
         (*ePWM[Inverter_Params.PWM_U_NUM]).TBCTL.bit.SYNCOSEL = 1;
		 (*ePWM[Inverter_Params.PWM_V_NUM]).TBCTL.bit.SYNCOSEL = 0;       /* Pass through*/
		 (*ePWM[Inverter_Params.PWM_W_NUM]).TBCTL.bit.SYNCOSEL = 0;       /* Pass through*/

        /* Configure each timer to sync*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).TBCTL.bit.PHSEN = 0;	/* First is master */
        (*ePWM[Inverter_Params.PWM_V_NUM]).TBCTL.bit.PHSEN = 0;
        (*ePWM[Inverter_Params.PWM_W_NUM]).TBCTL.bit.PHSEN = 0;

        /* Init Timer-Base Period Register for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).TBPRD = pwm1.PeriodMax;
        (*ePWM[Inverter_Params.PWM_V_NUM]).TBPRD = pwm1.PeriodMax;
        (*ePWM[Inverter_Params.PWM_W_NUM]).TBPRD = pwm1.PeriodMax;

        /* Init Timer-Base Phase Register for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).TBPHS.bit.TBPHS = 0;
        (*ePWM[Inverter_Params.PWM_V_NUM]).TBPHS.bit.TBPHS = 0;
        (*ePWM[Inverter_Params.PWM_W_NUM]).TBPHS.bit.TBPHS = 0;

        /* Init Timer-Base Control Register for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).TBCTL.all =  PWMTB_INIT_STATE;
		 (*ePWM[Inverter_Params.PWM_V_NUM]).TBCTL.all = PWMTB_INIT_STATE;
		 (*ePWM[Inverter_Params.PWM_W_NUM]).TBCTL.all = PWMTB_INIT_STATE;

        /* Init Compare Control Register for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).CMPCTL.all = CMPCTL_INIT_STATE;
        (*ePWM[Inverter_Params.PWM_V_NUM]).CMPCTL.all = CMPCTL_INIT_STATE;
        (*ePWM[Inverter_Params.PWM_W_NUM]).CMPCTL.all = CMPCTL_INIT_STATE;

       /* Init Action Qualifier Output A Register for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).AQCTLA.all = AQCTLA_INIT_STATE;
        (*ePWM[Inverter_Params.PWM_V_NUM]).AQCTLA.all = AQCTLA_INIT_STATE;
        (*ePWM[Inverter_Params.PWM_W_NUM]).AQCTLA.all = AQCTLA_INIT_STATE;

        /* Init Dead-Band Generator Control Register for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).DBCTL.all = DBCTL_INIT_STATE;
        (*ePWM[Inverter_Params.PWM_V_NUM]).DBCTL.all = DBCTL_INIT_STATE;
        (*ePWM[Inverter_Params.PWM_W_NUM]).DBCTL.all = DBCTL_INIT_STATE;

        /* Init Dead-Band Generator for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).DBFED.all = pwm1.Deadband;
        (*ePWM[Inverter_Params.PWM_U_NUM]).DBRED.all = pwm1.Deadband;
        (*ePWM[Inverter_Params.PWM_V_NUM]).DBFED.all = pwm1.Deadband;
        (*ePWM[Inverter_Params.PWM_V_NUM]).DBRED.all = pwm1.Deadband;
        (*ePWM[Inverter_Params.PWM_W_NUM]).DBFED.all = pwm1.Deadband;
        (*ePWM[Inverter_Params.PWM_W_NUM]).DBRED.all = pwm1.Deadband;

        /* Init PWM Chopper Control Register for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).PCCTL.all = PCCTL_INIT_STATE;
        (*ePWM[Inverter_Params.PWM_V_NUM]).PCCTL.all = PCCTL_INIT_STATE;
        (*ePWM[Inverter_Params.PWM_W_NUM]).PCCTL.all = PCCTL_INIT_STATE;
         																																				\
        EDIS;                         /* Disable EALLOW*/

		// Configure 7 to trigger the ADC start of conversion
        (*ePWM[Inverter_Params.PWM_U_NUM]).ETSEL.bit.SOCASEL   = ET_CTR_ZERO;			// Event trigger selection for SOC A from ePWM
        (*ePWM[Inverter_Params.PWM_U_NUM]).ETSEL.bit.SOCAEN	= 1;						// Enable SOC A trigger event generation
        (*ePWM[Inverter_Params.PWM_U_NUM]).ETPS.bit.SOCPSSEL = 1;						// use SOCAPRD2 and SOCACNT2 instead of SOCAPRD and SOCACNT
        (*ePWM[Inverter_Params.PWM_U_NUM]).ETSOCPS.bit.SOCAPRD2 = Motor_Params.MAINISR_PRESCALE;
//        (*ePWM[Inverter_Params.PWM_U_NUM]).ETPS.bit.SOCAPRD = ET_1ST;                                                                    // Event trigger SOC prescale - 0 is disable

		// configure 8 and 9 as slaves
        (*ePWM[Inverter_Params.PWM_V_NUM]).TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
//For 379D  (*ePWM[Inverter_Params.PWM_V_NUM]).TBCTL.bit.PHSEN    = TB_ENABLE;
        (*ePWM[Inverter_Params.PWM_V_NUM]).TBPHS.bit.TBPHS    = 0;
        (*ePWM[Inverter_Params.PWM_V_NUM]).TBCTL.bit.PHSDIR   = TB_UP;

        (*ePWM[Inverter_Params.PWM_W_NUM]).TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
//For 379D   (*ePWM[Inverter_Params.PWM_W_NUM]).TBCTL.bit.PHSEN    = TB_ENABLE;
        (*ePWM[Inverter_Params.PWM_W_NUM]).TBPHS.bit.TBPHS    = 0;
        (*ePWM[Inverter_Params.PWM_W_NUM]).TBCTL.bit.PHSDIR   = TB_UP;
#ifndef FixMeLater
		EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
		EPwm3Regs.TBCTL.bit.PHSEN    = TB_DISABLE;
		EPwm3Regs.TBPHS.bit.TBPHS    = 0;
		EPwm3Regs.TBCTL.bit.PHSDIR   = TB_UP;
#endif
		// ------------------------------------------------------------------------------
		//  INIT PWM period registers to something sane
		// ------------------------------------------------------------------------------
		pwm1.MfuncC1 = -1.0;
		pwm1.MfuncC2 = -1.0;
		pwm1.MfuncC3 = -1.0;		// Sets all low side gates on... i.e. braking (ideally we'd check for motor rotation/voltage first!)
		PWM_MACRO(Inverter_Params.PWM_U_NUM,Inverter_Params.PWM_V_NUM,Inverter_Params.PWM_W_NUM,pwm1)

	    //Set all TZ actions to bring these low in case active recitification had changed them
	    (*ePWM[Inverter_Params.PWM_U_NUM]).TZCTL.bit.TZA = 0x0002;
	    (*ePWM[Inverter_Params.PWM_V_NUM]).TZCTL.bit.TZA = 0x0002;
	    (*ePWM[Inverter_Params.PWM_W_NUM]).TZCTL.bit.TZA = 0x0002;
	    (*ePWM[Inverter_Params.PWM_U_NUM]).TZCTL.bit.TZB = 0x0002;
	    (*ePWM[Inverter_Params.PWM_V_NUM]).TZCTL.bit.TZB = 0x0002;
	    (*ePWM[Inverter_Params.PWM_W_NUM]).TZCTL.bit.TZB = 0x0002;

		EDIS;

}

void Enable_PWM(float Duty)	/*  This module assumes that PWM registers are set correctly before calling this */
{
	PWM_ENABLE_MACRO(Inverter_Params.PWM_U_NUM,Inverter_Params.PWM_V_NUM,Inverter_Params.PWM_W_NUM)
}

void Reset_PWM_trip(void)
{
	// Try to clear the hardware trip flag(CBC), if hardware trip is still set the bit will immediately reset...
	// Ideally we would modify the chunks of code that do the "enable" to first try to clear the flags, then check
	//  If they don't clear, then the HW trip still exists and the routine should not switch to enable
	//  Instead right now we just clear the flags all the time, and set ENABLE in serial comm interrupt routines (or debugger)
	//  then main loop will kick back out of ENABLE if it sees the flags set...

	EALLOW;                       /* Enable EALLOW */
	(*ePWM[Inverter_Params.PWM_V_NUM]).TZCLR.bit.CBC=1;
	(*ePWM[Inverter_Params.PWM_U_NUM]).TZCLR.bit.CBC=1;
	(*ePWM[Inverter_Params.PWM_W_NUM]).TZCLR.bit.CBC=1;
    EDIS;                         /* Disable EALLOW*/
}


void Disable_PWM(void)
{
    EALLOW;
    //Set all TZ actions to bring these low in case active recitification had changed them
    (*ePWM[Inverter_Params.PWM_U_NUM]).TZCTL.bit.TZA = 0x0002;
    (*ePWM[Inverter_Params.PWM_V_NUM]).TZCTL.bit.TZA = 0x0002;
    (*ePWM[Inverter_Params.PWM_W_NUM]).TZCTL.bit.TZA = 0x0002;
    (*ePWM[Inverter_Params.PWM_U_NUM]).TZCTL.bit.TZB = 0x0002;
    (*ePWM[Inverter_Params.PWM_V_NUM]).TZCTL.bit.TZB = 0x0002;
    (*ePWM[Inverter_Params.PWM_W_NUM]).TZCTL.bit.TZB = 0x0002;
    EDIS;
	PWM_DISABLE_MACRO(Inverter_Params.PWM_U_NUM,Inverter_Params.PWM_V_NUM,Inverter_Params.PWM_W_NUM)
}

void PWM_Out(_iq Ta,_iq Tb,_iq Tc)
{
	pwm1.MfuncC1 = Ta;
	pwm1.MfuncC2 = Tb;
	pwm1.MfuncC3 = Tc;
	PWM_MACRO(Inverter_Params.PWM_U_NUM,Inverter_Params.PWM_V_NUM,Inverter_Params.PWM_W_NUM,pwm1)							// Calculate the new PWM compare values
}
//GUIC function
void Set_PWM_freq(int freq_in_KHZ)
{
    unsigned int T_PWM = (uint16_t)(CPU.milliSec/Motor_Params.PWM_FREQ_KHZ);
    if (freq_in_KHZ != 0)
    {
        EALLOW;
        /* Initialize PWM module */  // is this repeated by some other stuff below -- at least the PWM_INIT_MACRO part?
        pwm1.PeriodMax = T_PWM/2;  // Prescaler X1 (T1), PWM period = T x 1
        pwm1.HalfPerMax= T_PWM/4;
        /* Init Timer-Base Period Register for EPWM1-EPWM3*/
        (*ePWM[Inverter_Params.PWM_U_NUM]).TBPRD = pwm1.PeriodMax;
        (*ePWM[Inverter_Params.PWM_V_NUM]).TBPRD = pwm1.PeriodMax;
        (*ePWM[Inverter_Params.PWM_W_NUM]).TBPRD = pwm1.PeriodMax;
        // PWM_MACRO(PWM_U_NUM,PWM_V_NUM,PWM_W_NUM,pwm1)    Done automatically within ISR
        /* Setup Sync Precautionary, to ensure that all the phases are lined up correctly */
        (*ePWM[Inverter_Params.PWM_U_NUM]).TBCTL.bit.SYNCOSEL = 0;       /* Pass through*/
        (*ePWM[Inverter_Params.PWM_V_NUM]).TBCTL.bit.SYNCOSEL = 0;       /* Pass through*/
        (*ePWM[Inverter_Params.PWM_W_NUM]).TBCTL.bit.SYNCOSEL = 0;       /* Pass through*/
        EDIS;
    }
}

void TripZoneConfig(int pwm_num)
{
    (*ePWM[pwm_num]).DCAHTRIPSEL.bit.TRIPINPUT4   = 0x01;
    (*ePWM[pwm_num]).DCALTRIPSEL.bit.TRIPINPUT4   = 0x01;

    (*ePWM[pwm_num]).DCTRIPSEL.bit.DCAHCOMPSEL    = 0x0F;
    (*ePWM[pwm_num]).DCTRIPSEL.bit.DCALCOMPSEL    = 0x0F;

    (*ePWM[pwm_num]).TZDCSEL.bit.DCAEVT1          = 0x02;
    (*ePWM[pwm_num]).TZDCSEL.bit.DCAEVT2          = 0x02;

    (*ePWM[pwm_num]).DCACTL.bit.EVT1SRCSEL        = 0x00;
    (*ePWM[pwm_num]).DCACTL.bit.EVT2SRCSEL        = 0x00;

    (*ePWM[pwm_num]).DCACTL.bit.EVT1FRCSYNCSEL    = 0x00;
    (*ePWM[pwm_num]).DCACTL.bit.EVT2FRCSYNCSEL    = 0x00;

    (*ePWM[pwm_num]).TZSEL.bit.DCAEVT1            = 0x01;
    (*ePWM[pwm_num]).TZSEL.bit.DCAEVT2            = 0x01;

    (*ePWM[pwm_num]).TZCTL.all = TZCTL_INIT_STATE;
    (*ePWM[pwm_num]).TZFRC.bit.OST = 1;
}

bool CheckTripFlags(int chan)
{
    return ((*ePWM[chan]).TZFLG.bit.OST==0x1);
}
