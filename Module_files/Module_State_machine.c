/* ============================================================================
System Name:    LaunchPoint Motor Controller

File Name:      State_machine.c

Target:         Cryo controller board

Author:         Mike Ricci and George Uy de Ong II

Description:    Refactoring motor controller software to be more modular and reduce linkage between sections
                This module will handle the processor PWM peripherals and associated data structures
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library
#include "F2837S_files/F28x_Project.h"
#include "Header/State_machine.h"
#include "Header/GUIC_Header.h"
#include "Header/Globals.h"
#include "Header/UsefulPrototypes.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1

// Function prototypes
enum MainStateVariables MainStateMachine(enum CritFaultVariables , uint32_t ,bool );

// ****************************************************************************
// External Function prototypes
// ****************************************************************************


// ****************************************************************************
//  Module resources - variables here //
// ****************************************************************************
enum MainStateVariables MainState = Main_Init;
enum CritFaultVariables LastCritFault = FLT_CRIT_NONE;
uint32_t LastErrorCode = 0;

uint32_t MainStateTicker=0;               //  Time how long since the last state transition
uint16_t Start_= 0;

/**Function: MainStateMachine
 * Runs in the B tasks with a 200Hz prescalar
 * Runs the top level state machine which takes incoming CAN commands (Todo - GPIO/PWM)
 * and transitions state based on whether the state is energized or not.
 *
 * Todo - SysDriveEnabledParam from a PWM duty cycle signal from the main wait loop
 *
 * @returns MainState - current state, sent to other state machines and interrupt as a signal
 *
 * @param CritFaultParam - Critical faults
 * @param ErrorCodeParam - Error code from a critical fault
 * @param SysDriveEnabledParam - Boolean whether or not the commanded state is energized
 *
 */
enum MainStateVariables MainStateMachine(enum CritFaultVariables CritFaultParam, uint32_t ErrorCodeParam, bool SysDriveEnabledParam)
{
    //Keep track of how long we've been in one state -- each state transition zeros this counter
     inc(&MainStateTicker);

     if(CritFaultParam == FLT_CRIT_NONE)
     {
         switch(MainState)
         {
             case Main_Init:
                 if( (MainStateTicker >= MAIN_SM_RATE/10L) && (SysDriveEnabledParam==0) )
                 {
                    MainState = Main_Inactive;
                    MainStateTicker=0;
                 }
                 break;

             case Main_Inactive:
                 if(SysDriveEnabledParam == true)
                 {
                    MainState = Main_Active;
                    MainStateTicker = 0;
                 }
                 break;

             case Main_Active:
                 if( SysDriveEnabledParam == false )
                 {
                    MainState = Main_Inactive;
                    MainStateTicker = 0;
                 }
                 break;

             case Main_Fault:
                 if(SysDriveEnabledParam == false)
                 {
                    MainState = Main_Inactive;
                    MainStateTicker = 0;
                 }
                 break;
             default:
                 break;
         }
     }
     else
     {
         if(MainState != Main_Fault)
         {
#if(!FAKE_FAULT_ERR)
             LastCritFault = CritFaultParam;
             LastErrorCode = ErrorCodeParam;
#endif
             MainState = Main_Fault;
             MainStateTicker = 0;
         }
     }

     return MainState;
}


enum MainStateVariables MainState_Get(void) {return MainState;}
enum CritFaultVariables GetLastCritFault(void) {return LastCritFault;}
uint32_t GetLastErrorCode(void) {return LastErrorCode;}
