/*
 * Module_SCI_ECU.c
 *
 *  Created on: Sep 26, 2018
 *      Author: vvarahamurthy
 */


#define FixMeLater 0
#define DeleteMe 0

#define ELMO_MANUAL_DEBUG 0
#define ELMO_MANUAL_FLT 0

#include "F2837S_files/F28x_Project.h"
#include "Header/GUIC_Header.h"
#include "Header/Elmo.h"
#include "Header/Globals.h"
#include "Header/State_Machine.h"

//Functions from other modules used in this one
extern void PWMDAC_InitHandle(struct PWMDACHandler *, int , int, float );
extern void PWMDAC_Disable(struct PWMDACHandler * );
extern void PWMDAC_SetOffset(struct PWMDACHandler *, float, float);
extern void PWMDAC_Enable(struct PWMDACHandler *);
extern void PWMDAC_Out(struct PWMDACHandler * , float );

extern void GPIO_ElmoEn(int, int );
extern void GPIO_ElmoSTO(int);
extern int GPIO_ElmoFlt(int);

//Prototype initializations for functions in this module
void Init_Elmos(void);
bool Elmo_FaultIO(int );
void ElmoStateMachine(enum MainStateVariables , enum CritFaultVariables , float );

elmo_handler_t Elmo[NUM_OF_ELMOS];

uint32_t gElmoStateTicker=0;

#if(ELMO_MANUAL_DEBUG)
int manualSTO = 0;
int manualE[] = {0,0};
extern uint16_t GetESTOP_Debounced(void);
#endif

#if(ELMO_MANUAL_FLT)
bool gdElmoFault = false;
#endif

volatile struct ElmoParams Elmo_Params;

/**Function: Init_Elmos
 * ------------------
 * Initializes the Elmo objects.
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void Init_Elmos(void)
{
    uint16_t i = 0;

    Elmo_Params.LOOP_FREQ_HZ = 1000;        //Frequency at which the elmo state machine runs

    Elmo_Params.av_gain = 100.0;    //Analog voltage gain in EASII - 100A/V
    Elmo_Params.av_max = 5.0;    //Maximum analog voltage command to Elmo
    Elmo_Params.av_offset = 2.5; //Analog offset in EASII to allow symmetric  torque command
    Elmo_Params.max_amps = Elmo_Params.av_gain * (Elmo_Params.av_max - Elmo_Params.av_offset);   //This will be used to scale an amps command to a % command

    Elmo_Params.pwmdac_hz = 20000.0; //PWMDAC Frequency in Hz
    Elmo_Params.lim_faults = 1;

    Elmo_Params.downtime_ms = 100;


    Elmo[0].GPIO_EN_NUM = Inverter_Params.GPIO_ELMO_EN_1_NUM;
    PWMDAC_InitHandle(&(Elmo[0].pwmdach),3,1,Elmo_Params.pwmdac_hz); //Elmo #0 is on Epwm3 channel A

    Elmo[1].GPIO_EN_NUM = Inverter_Params.GPIO_ELMO_EN_2_NUM;
    PWMDAC_InitHandle(&(Elmo[1].pwmdach),3,2,Elmo_Params.pwmdac_hz); //Elmo #0 is on Epwm3 channel B

    //Set up the states, PWMDAC offsets, and enable PWMDAC
    for(i=0;i<NUM_OF_ELMOS;i++)
    {
        Elmo[i].TC = 0;
        Elmo[i].state = ElmoInit;
        Elmo[i].stateticker=0;
        PWMDAC_SetOffset(&(Elmo[i].pwmdach), (Elmo_Params.av_offset/Elmo_Params.av_max)/Elmo_Params.pwmdac_hz, 1/Elmo_Params.pwmdac_hz);
        PWMDAC_Out(&(Elmo[i].pwmdach), Elmo[i].TC);
        PWMDAC_Enable(&(Elmo[i].pwmdach));
    }
}


/**Function: ElmoStateMachine
 * ------------------
 * Runs a state machine on each individual Elmo object to enable/disable/fault detect
 * This is similar to the controller state machine that lives inside Module_Controller.c
 *
 * Outputs:
 * None
 *
 * @param MainStateParam: Top level main state signal passed in as an arg
 * @param CritFaultParam: Critical faults from HiBW detect - causes fast shutdown
 * @param TorqueCommandParam: Torque command in amps
 */
void ElmoStateMachine(enum MainStateVariables MainStateParam, enum CritFaultVariables CritFaultParam, float TorqueCommandParam)
{
    int16_t i=0;    //Index for incrementing through the elmos array

    gElmoStateTicker++;

    for(i=0;i<NUM_OF_ELMOS;i++)
    {

#if(!ELMO_MANUAL_DEBUG)

        Elmo[i].stateticker++;

#if(ELMO_MANUAL_FLT)
        Elmo[i].FLT = gdElmoFault;
#else
        Elmo[i].FLT = GPIO_ElmoFlt(i);      //Update the FLT flag on this elmo
#endif

        if(CritFaultParam == FLT_CRIT_NONE)
        {
            switch(Elmo[i].state)
            {
                //Initial state of the Elmos, basically OFF with STO disabled. Never should enter this state again, unless CPU is reset
                case ElmoInit:
                    GPIO_ElmoSTO(0);                       //STO off on startup
                    GPIO_ElmoEn(Elmo[i].GPIO_EN_NUM, 0);   //Inhibit Elmos on startup
                    Elmo[i].numfaults = 0;                 //Reset number of faults this run

                    //Keep the Elmo in this state for a set downtime (kinda like Disable_No_Start)
                    if(Elmo[i].stateticker >= Elmo_Params.LOOP_FREQ_HZ * (Elmo_Params.downtime_ms/1000.0) )
                    {
                        if(MainStateParam == Main_Inactive)
                        {
                            Elmo[i].state = ElmoDis;
                            GPIO_ElmoSTO(1);                        //STO ON (both STOs are on the same GPIO)
                            GPIO_ElmoEn(Elmo[i].GPIO_EN_NUM, 0);    //INHIBIT this elmo
                            Elmo[i].stateticker = 0;
                        }
                        else
                        {
                            Elmo[i].state = ElmoInit;       //Stay in the init state until we receive the proper Disable command
                        }
                    }
                    break;
                case ElmoDis:
                    /* Code to check for an incoming re-enable signal.
                     * Will be internal for our bench testing but leaving open ended
                     * in case the use case for the FC to send the re-enable signal is
                     * desired
                     */
                    if(MainStateParam <= Main_Inactive)
                    {
                        Elmo[i].state = ElmoDis;    //Stay in this state until told otherwise.
                    }
                    else
                    {
                        if(Elmo[i].stateticker >= Elmo_Params.LOOP_FREQ_HZ * (Elmo_Params.downtime_ms/1000.0)) //Wait some time before re-enabling so it isn't instant
                        {
                            GPIO_ElmoEn(Elmo[i].GPIO_EN_NUM, 1);    //Enable this Elmo
                            PWMDAC_Enable(&(Elmo[i].pwmdach));      //Enable the PWMDAC output
                            Elmo[i].state = ElmoEn;                 //Transition to Elmo Enabled state
                            Elmo[i].stateticker = 0;
                        }
                    }
                    break;

                case ElmoEn:
                    if(MainStateParam == Main_Inactive)
                    {
                        GPIO_ElmoEn(Elmo[i].GPIO_EN_NUM, 0);    //INHIBIT this Elmo
                        Elmo[i].TC = 0.0;                       //Set torque command to 0
                        PWMDAC_Out(&(Elmo[i].pwmdach), 0.0);    //PWM output to 0% duty cycle
                        Elmo[i].state = ElmoDis;                //Transition to Elmo Disabled state
                        Elmo[i].stateticker = 0;
                        Elmo[i].numfaults = 0;                  //Reset number of faults this run if intentionally disabling
                    }
                    //Wait 100ms after setting enable state so we don't get stuck in a fault GPIO chasing loop
                    else if( (Elmo[i].stateticker >= Elmo_Params.LOOP_FREQ_HZ * (Elmo_Params.downtime_ms/1000.0)) && Elmo[i].FLT )
                    {
                        Elmo[i].numfaults++;                   //Increment # of faults
                        GPIO_ElmoEn(Elmo[i].GPIO_EN_NUM, 0);   //Disable this Elmo on fault

                        Elmo[i].state = ElmoDis;
                        Elmo[i].stateticker = 0;

                        //Elmo max faults is now considered a critical loop fault and will cause transition to WaitForParent
                    }
                    else
                    {
                        Elmo[i].TC = TorqueCommandParam/Elmo_Params.max_amps;
                        PWMDAC_Out( &(Elmo[i].pwmdach),Elmo[i].TC);
                        Elmo[i].state = ElmoEn;
                    }

                    break;
                case ElmoWaitForParent:
                    if( (MainStateParam == Main_Inactive) && (CritFaultParam == FLT_CRIT_NONE) )
                    {
                        GPIO_ElmoSTO(1);
                        Elmo[i].numfaults = 0;  //Reset # of faults
                        Elmo[i].state = ElmoDis;
                        Elmo[i].stateticker=0;
                    }
                    else
                    {
                        //Stay in this state
                    }
                    break;

                default:
                    break;
            }
        }
        else
        {
            if(Elmo[i].state != ElmoWaitForParent)
            {
                GPIO_ElmoSTO(0);                        //STO OFF
                GPIO_ElmoEn(Elmo[i].GPIO_EN_NUM, 0);    //INHIBIT this Elmo
                Elmo[i].TC = 0.0;                       //Set torque command to 0
                PWMDAC_Out(&(Elmo[i].pwmdach), 0.0);    //PWM output to 0% duty cycle

                Elmo[i].state = ElmoWaitForParent;
                Elmo[i].stateticker = 0;
            }
        }

#else
        GPIO_ElmoSTO(GetESTOP_Debounced());
        if(GenControlStateParam == PMUEnabled)
        {
            GPIO_ElmoEn(Elmo[i].GPIO_EN_NUM, 1);
        }
        else if(GenControlStateParam == PMUDisabled)
        {
            GPIO_ElmoEn(Elmo[i].GPIO_EN_NUM, 0);
        }

        Elmo[i].TC = TorqueCommandParam/ELMO_AV_GAIN/(ELMO_MAX_PWM_V-ELMO_AV_OFFSET);
        PWMDAC_Out( &(Elmo[i].pwmdach),Elmo[i].TC);
#endif
    }

#if(ELMO_MANUAL_FLT)
    gdElmoFault = false;
#endif
}

//Get methods for this file

/**Function: getElmoStatus
 * ------------------
 * Checks if either Elmo is faulted
 *
 * Outputs:
 * @returns 1 if neither elmo is faulted
 * @returns 0 if one of the elmos is faulted
 *
 * Inputs:
 * None
 */
bool getElmoStatus(void)
{
    if(Elmo[0].state == ElmoWaitForParent || Elmo[1].state == ElmoWaitForParent)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/**Function: getElmoExceededNumFaults
 * ------------------
 * Checks if either Elmo has exceeded the maximum # of faults allowable in one run
 *
 * @returns true if an Elmo has exceeded the maximum number of allowable faults
 * @returns false if neither elmo has exceeded the max number of allowable faults
 *
 * Inputs:
 * None
 */
bool getElmoExceededNumFaults(void)
{
    if( ((Elmo[0].numfaults >= Elmo_Params.lim_faults) && (Elmo[0].state != ElmoWaitForParent)) ||
            ((Elmo[1].numfaults >= Elmo_Params.lim_faults)) && Elmo[1].state != ElmoWaitForParent)
    {
        return true;
    }
    else
    {
        return false;
    }
}

float elmoTorqueCommand(void) { return Elmo[0].TC;}        //Returns the torque command in % to the Elmo
enum ElmoStateVariables GetElmo0State(void) {return Elmo[0].state;}
enum ElmoStateVariables GetElmo1State(void) {return Elmo[1].state;}
bool Elmo_FaultIO(int i) {return Elmo[i].FLT;}
float getElmoMaxAmps(void) {return Elmo_Params.max_amps;}
uint16_t getElmo1NumFaults(void) {return Elmo[0].numfaults;}
uint16_t getElmo2NumFaults(void) {return Elmo[1].numfaults;}


