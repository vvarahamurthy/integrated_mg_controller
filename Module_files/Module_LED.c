/* ============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	LED.C

Target:			Cryo controller board

Author:			Mike Ricci

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle the processor GPIO pins -- digital IO, PWM, and other peripherals that share GPIO
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "Header/LED.h"
#include "Header/GUIC_Header.h"
#include "F2837xS_GlobalPrototypes.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1


// Function prototypes

void Init_LED(void);
void Run_LED(void);
void SetLED1_BlinkRate(enum BlinkRate);
void LED2_ON(void);
void LED2_OFF(void);
extern void GPIO_TogglePin(uint16_t);


//  Module resources - variables here //

enum  BlinkRate LED1_BlinkRate;
int LED1_BlinkCounter;
int Ledtesting = 15;

// Function definitions

void Init_LED(void)
{
    Ledtesting = 14;
    GPIO_WritePin(Inverter_Params.GPIO_LED1_NUM,0);        // Turn on LED1
    GPIO_WritePin(Inverter_Params.GPIO_LED2_NUM,0);        // Turn on LED2

	LED1_BlinkCounter=0;
	LED1_BlinkRate=SLOW;

}

void Run_LED(void)
{
	LED1_BlinkCounter++;
	if(LED1_BlinkCounter>LED1_BlinkRate)
	{
		LED1_BlinkCounter = 0;
		GPIO_TogglePin(Inverter_Params.GPIO_LED1_NUM);//Inverter_Params.LED.LED1_GPIO_TOGGLE_REGISTER = 1;	   // Turn on/off LED1
	}

}


void SetLED1_BlinkRate(enum BlinkRate Rate)
{
	LED1_BlinkRate = Rate;
}


void LED2_ON()
{
	GPIO_WritePin(Inverter_Params.GPIO_LED2_NUM,0);	   // Turn on LED2
}


void LED2_OFF()
{
    GPIO_WritePin(Inverter_Params.GPIO_LED2_NUM,1);	   // Turn off LED2
}
