/*
 * Module_Gen_Control.c
 *
 *  Created on: Dec 10, 2020
 *      Author: KeithKolb
 */

#include "Header/pid_reg3mod.h"
#include "Header/Gen_Control.h"
#include "Header/GUIC_Header.h"
#include "Header/Elmo.h"
#include "Header/State_Machine.h"
#include "Header/Globals.h"
#include "Header/UsefulPrototypes.h"

enum GenControlStateVariables gGenControlState = GenControllerInit;
uint32_t gGenControlStateTicker = 0;

struct GenControl_Params  GenControlParams;
PIDREG3_MOD pid_gen_ctrl = PIDREG3MOD_DEFAULTS;

float GenControlCalc(enum GenControlStateVariables, float , float, float );
bool GenControlMotionStates(enum GenControlStateVariables );
void GenControl_ResetPID(void);

float EngineRPM = 0;
float gCurrentCommandAmps = 0;
float throttleCommand = 0.0;
float RPMCommand = 0.0;
float gLUTPower = 0.0;
#if(FAKE_GENCONTROL)
float gDebugRpm = 0;
#endif

uint16_t gFailedStartAttempts = 0;
float gGCPowerDemandLPF=0.0;

#if(NEGATIVE_BOOST)
float manualcurrentcommand=0.0;
#endif

extern float getElmoMaxAmps(void);
extern float adcGetSignalValue(enum adc_signal_id id);
extern float getSystemRPM(void);
extern float ECU_GetRPMLUT(void);
extern float ECU_GetThrottleSetting(void);
extern float GetEcuPidReference(void);
extern float CAN_GetBMSVolts(void);

/**Function: Init_GenController
 * ------------------
 * Initializes the Generator Controller module
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void Init_GenController(void)
{
    GenControlParams.LOOP_HZ = 200;
    GenControlParams.T_LOOP = 1.0/GenControlParams.LOOP_HZ;

#if(!NEWCONTROL_MODE)
    GenControlParams.FF_Gain = 1.0;
    GenControlParams.FF_LPF_Hz = 10.0;
    GenControlParams.FF_LPF_Alpha = (1.0-expf(-2.0*PI*GenControlParams.FF_LPF_Hz/GenControlParams.LOOP_HZ));
#else
    GenControlParams.FF_Gain = 1.0;
    GenControlParams.FF_LPF_Hz = 0.1;
    GenControlParams.FF_LPF_Alpha = (1.0-expf(-2.0*PI*GenControlParams.FF_LPF_Hz/GenControlParams.LOOP_HZ));
#endif

    GenControlParams.DCP_LPF_Hz = 1.0;
    GenControlParams.DCP_LPF_Alpha = (1.0-expf(-2.0*PI*GenControlParams.DCP_LPF_Hz/GenControlParams.LOOP_HZ));

    GenControlParams.pi_kp = 250.0E-7;
    GenControlParams.pi_ki= .25;
    GenControlParams.pi_kd = 0.0;

    pid_gen_ctrl.Kp = GenControlParams.pi_kp;
    pid_gen_ctrl.Ki = GenControlParams.pi_ki*GenControlParams.T_LOOP;
    pid_gen_ctrl.Kd = GenControlParams.pi_kd;

    GenControlParams.StartingAmps = 250.0;
    GenControlParams.StartingTime_ms = 1500.0;
#if(!COMPRESSIONTESTMODE)
    GenControlParams.StartRPMThresh = 3000;
#else
    GenControlParams.StartRPMThresh = 1000;
#endif
    GenControlParams.StartAttempts = 5;
    GenControlParams.StartAttempt_timeout_s = 2.0;

    if(Inverter_Params.Electronics == DriveExternal)
    {
        GenControlParams.MaxAmpsCmd = getElmoMaxAmps();
    }
    else
    {
        GenControlParams.MaxAmpsCmd = Inverter_Params.Sense_Current_M;
    }

    GenControlParams.MotionCheck_sec = 0.5;
    GenControlParams.MotionCheck_rpm = 100;

    GenControlParams.PowerTorqueLUT = -0.0290877294;
    GenControlParams.PSTIntLUT = -80.5735065701;

    GenControlParams.TorqueDemandDeratingFactor = 5;     //A/degC number to derate torque command by depending on stator temperature

    GenControlParams.MinPowerThresh = 1500;
    GenControlParams.MaxGenPower = 5400;

    GenControl_ResetPID();
}

/**Function: GenControl_ResetPID
 * ------------------
 * Resets all generator controller PID parameters
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void GenControl_ResetPID(void)
{
    //Zero all Elmo PID params
    pid_gen_ctrl.Err=0;
    pid_gen_ctrl.Fdb=0;
    pid_gen_ctrl.Ref=0;
    pid_gen_ctrl.Up=0;
    pid_gen_ctrl.Ud=0;
    pid_gen_ctrl.Out=0;
    pid_gen_ctrl.OutPreSat=0;
    pid_gen_ctrl.SatErr=0;
    pid_gen_ctrl.Up1=0;
    pid_gen_ctrl.u = 0;
    pid_gen_ctrl.Ui = 0;
    pid_gen_ctrl.new_i = 0;
    pid_gen_ctrl.int_ok = 0;
    pid_gen_ctrl.OutMax = 0;
    pid_gen_ctrl.OutMin = -1.0*GenControlParams.MaxAmpsCmd;
}


/**Function: GenControlCalc
 * ------------------
 * Runs the control loop that sets the torque command.
 * Called by GenControllerStateMachine
 *
 * @returns currentOutputAmps - torque command in amps
 *
 * @param MainState: State variable from the main state machine in the A tasks. Currently unused, but will be used if we want to merge the PMU modes back into the main state machine
 * @param ControlRefParam: Control reference taken from the CAN bus. In BOOST, this comes from the FC, and in CHARGE mode, comes from the BMS
 * @param BMSFdbParam: Battery current reading from the BMS. Positive is current OUT of the battery onto the DC bus
 * @param PMUModeParam: MODE command from the FC over CAN bus
 */
float GenControlCalc(enum GenControlStateVariables GenControlStateParam, float ControlRefParam, float BMSFdbParam, float RPMParam)
{
#if(NEWCONTROL_MODE)
    static float TorqueDemandDerating=0;
    static double TorqueDemand=0, TorqueDemand_LPF=0;
#else
    static float FeedForwardTerm, FeedForwardTerm_LPF;
#endif
    static float TotalPowerDemand=0, TotalPowerDemand_LPF=0, statorTemp=0;

#if(FAKE_GENCONTROL)
    float l_EngineRPM = gDebugRpm;
#else
    float l_EngineRPM = RPMParam;
#endif
    float currentOutputAmps = 0;

    statorTemp = adcGetSignalValue(adc_id_tstator); //Get current stator temperature (for thermal limiting behavior)

    //Calculate a feedforward term based on total power demand
    //Engine RPM 0 check so this doesn't blow up
    if( (GenControlStateParam == GenControllerCharge || GenControlStateParam == GenControllerAlternator) && l_EngineRPM >= 1000 )
    {
        //Total demanded power is the sum of power out of battery pack, power out of genset, and BMS charge request
        TotalPowerDemand = (adcGetSignalValue(adc_id_idc)+BMSFdbParam+ControlRefParam)*CAN_GetBMSVolts();
//        TotalPowerDemand = (adcGetSignalValue(adc_id_idc)+BMSFdbParam+ControlRefParam)*adcGetSignalValue(adc_id_vdc);
        TotalPowerDemand_LPF = (float)(GenControlParams.DCP_LPF_Alpha*(TotalPowerDemand) + (1.0-GenControlParams.DCP_LPF_Alpha)*(TotalPowerDemand_LPF));

        //Saturate to maximum allowable genset power (based on thermals) - power past this value comes out of battery
        TotalPowerDemand_LPF = saturate(0, GenControlParams.MaxGenPower, TotalPowerDemand_LPF);
        gGCPowerDemandLPF = TotalPowerDemand_LPF;

#if(!NEWCONTROL_MODE)

        gLUTPower = l_EngineRPM*1.17073 - 1795.12195;  //RPM -> Power LUT

        if(ECU_GetRPMLUT()>4000)
        {
            FeedForwardTerm = -1.0*((gLUTPower)/(l_EngineRPM*2*PI/60)/Motor_Params.Kt);  //Torque based on DC power in and speed in rad/s
            FeedForwardTerm_LPF = (float)(GenControlParams.FF_LPF_Alpha*(FeedForwardTerm) + (1.0-GenControlParams.FF_LPF_Alpha)*(FeedForwardTerm_LPF));
        }
        else
        {
            FeedForwardTerm = 0.0;
            FeedForwardTerm_LPF = 0.0;
        }


        g_FeedForwardFiltered = FeedForwardTerm_LPF;

        PID_MOD_MACRO(pid_gen_ctrl)
        currentOutputAmps = saturate(-1.0*GenControlParams.MaxAmpsCmd, 0.0, (GenControlParams.FF_Gain*FeedForwardTerm_LPF + pid_gen_ctrl.Out));
#else
        //Torque is based on engine speed - engine idles around 2kRPM. Don't command any torque until bus power is above a set threshold value
        if(TotalPowerDemand_LPF > GenControlParams.MinPowerThresh)
        {
            //Calculate a derating factor based on distance between stator temperature and the "warning" temperature which we
            //will use as our stator temperature limit before derating
            //Todo - derating based on heatsink, power board, engine temperature
            if(statorTemp >= Motor_Params.LIM_T_STATOR_WARN_H)
            {
                TorqueDemandDerating = (statorTemp - Motor_Params.LIM_T_STATOR_WARN_H)*GenControlParams.TorqueDemandDeratingFactor;
                //Drop torque command by at least 5% of the un-derated value, and up to 100% of the un-derated value
                TorqueDemandDerating = saturate(-1.0*TorqueDemand_LPF*0.05, -1.0*TorqueDemand_LPF, TorqueDemandDerating);
            }
            //Keep derating until we drop 5deg below our limit, for hysteresis
            else if(statorTemp <= (Motor_Params.LIM_T_STATOR_WARN_H - 5.0))
            {
                TorqueDemandDerating = 0;
            }
            TorqueDemand = (GenControlParams.PowerTorqueLUT*TotalPowerDemand_LPF + GenControlParams.PSTIntLUT)*saturate(0,1,(l_EngineRPM/GetEcuPidReference()));
            TorqueDemand_LPF = GenControlParams.FF_LPF_Alpha*(TorqueDemand) + (1.0-GenControlParams.FF_LPF_Alpha)*(TorqueDemand_LPF);
            currentOutputAmps = TorqueDemand_LPF + TorqueDemandDerating;
        }
        else
        {
            TorqueDemand = 0;
            TorqueDemand_LPF = 0;
            currentOutputAmps = 0;
        }
#endif
    }
    else
    {
#if(!NEWCONTROL_MODE)
        FeedForwardTerm = 0.0;
        FeedForwardTerm_LPF = 0.0;
        g_FeedForwardFiltered = FeedForwardTerm_LPF;
        GenControl_ResetPID();
#endif
#if(!NEGATIVE_BOOST)
        currentOutputAmps = 0.0;
#endif
    }

    switch(GenControlStateParam)
    {
        case GenControllerBoost:
#if(!NEGATIVE_BOOST)
            currentOutputAmps = saturate(0.0, GenControlParams.MaxAmpsCmd, (ControlRefParam/100.0)*GenControlParams.MaxAmpsCmd);
#else
            if(l_EngineRPM >= 1000)
            {
                currentOutputAmps = saturate(-1.0*GenControlParams.MaxAmpsCmd, 0, (manualcurrentcommand/100.0)*GenControlParams.MaxAmpsCmd);
            }
            else
            {
                currentOutputAmps = 0.0;
            }

#endif

            break;

        case GenControllerStart:
            currentOutputAmps = GenControlParams.StartingAmps;
            break;

        case GenControllerCharge:
            /* In this mode we want to charge the battery with whatever the BMS is requesting
             * BMS ControlRefParam is negative for discharge request, positive for charge request
             * BMSFdbParam is positive for current OUT of the battery, negative for current INTO the battery
             * Negative error causes negative output, negative output (lower PWM duty cycle) is negative torque
             */

            pid_gen_ctrl.Ref = -1.0*ControlRefParam;
            pid_gen_ctrl.Fdb = BMSFdbParam;
            break;

        case GenControllerAlternator:
            /*
             * In this mode we just want to keep battery current at 0
             * BMSFdbParam is positive for current OUT of the battery, negative for current INTO the battery
             * Negative error causes negative output, negative output (lower PWM duty cycle) is negative torque
             */
            pid_gen_ctrl.Ref = 0;
            pid_gen_ctrl.Fdb = BMSFdbParam;
            break;

        case GenControllerStartTimeout:
            currentOutputAmps = 0.0;
            break;

        default:
            //Should never happen
            break;
    }

    return currentOutputAmps;
}

/**Function: GenControllerStateMachine
 * ------------------
 * Runs the control loop that sets the torque command.
 * Calls genControlCalc
 * Called in the B tasks a 200Hz prescalar
 *
 * @returns gGenControlState - current state of the top level generator controller state machine
 *
 * @param MainStateParam - Main state from Main_State_Machine
 * @param GenControllerModeParam - Mode commanded over CAN bus, passes through to GenControlCalc
 * @param CritFaultParam - Critical fault parameter from HiBW Fault Detect
 * @param ElmoStatusParam - Boolean telling us whether an Elmo is faulted (unused right now..)
 * @param ControlRefParam - Control reference, passes through to GenControlCalc
 * @param BMSFdbParam - BMS Current - passes through to GenControlCalc
 *
 */
enum GenControlStateVariables GenControllerStateMachine(enum MainStateVariables MainStateParam, enum GenControlStateVariables GenControllerModeParam, enum CritFaultVariables CritFaultParam, bool ElmoStatusParam, float ControlRefParam, float BMSFdbParam, float RPMParam)
{
    gGenControlStateTicker++;

    if(CritFaultParam == FLT_CRIT_NONE)
    {
        switch(gGenControlState)
        {
            case GenControllerInit:
                if( (MainStateParam == Main_Inactive) && gGenControlStateTicker >= GenControlParams.LOOP_HZ/100L)
                {
                   gGenControlState = GenControllerDisabled;
                }
                break;

            case GenControllerDisabled:
                if(MainStateParam == Main_Active)
                {
                    //Going from disabled -> enabled, run once through the GenEnabled state regardless of whether the command was motion or not, in case we need an engine start

                    gGenControlState = GenControllerEnabled;
                    gGenControlStateTicker = 0;
                }
                else
                {
                    GenControl_ResetPID();
                    gGenControlState = GenControllerDisabled;
                    gGenControlStateTicker = 0;
                }
                break;

            case GenControllerEnabled:
                if(MainStateParam == Main_Active)
                {
                    //If the command is a motion command, start the engine if RPM is < 100
                    if(GenControlMotionStates(GenControllerModeParam) && EngineRPM < 100)
                    {
                        gGenControlState = GenControllerStart;
                        gGenControlStateTicker = 0;
                    }
                    //If already spinning, go straight to the commanded state
                    else
                    {
                        gGenControlState = GenControllerModeParam;
                        gGenControlStateTicker = 0;
                    }
                    //More of a status rather than doing anything...
                }
                else
                {
                    GenControl_ResetPID();
                    gGenControlState = GenControllerDisabled;
                    gGenControlStateTicker = 0;
                }
                break;

            case GenControllerStart:
                if(MainStateParam == Main_Active)
                {
                    if(gGenControlStateTicker >= GenControlParams.LOOP_HZ*GenControlParams.StartingTime_ms/1000.0)
                    {
                        if(GenControllerModeParam == GenControllerStart)
                        {
                            //Stay in this state, I guess...would be weird to command EngineStart continuously
                        }
                        else
                        {
                            if(RPMParam>=GenControlParams.StartRPMThresh)
                            {
                                gGenControlState = GenControllerModeParam; //Switch to whatever mode they were commanding after timeout
                            }
                            else
                            {
                                gGenControlState = GenControllerStartTimeout;
                                gFailedStartAttempts++;
                            }
                            gGenControlStateTicker = 0;
                        }
                    }
                    else if(RPMParam >= GenControlParams.StartRPMThresh)
                    {
                        gGenControlState = GenControllerModeParam;
                        gGenControlStateTicker=0;
                    }
                }
                else
                {
                    GenControl_ResetPID();
                    gGenControlState = GenControllerDisabled;
                    gGenControlStateTicker = 0;
                }
                break;
            case GenControllerStartTimeout:
                if(MainStateParam == Main_Active)
                {
                    //Timeout state before attempting another start
                    if(gGenControlStateTicker >= GenControlParams.LOOP_HZ*GenControlParams.StartAttempt_timeout_s)
                    {
                        gGenControlState = GenControllerStart;
                        gGenControlStateTicker = 0;
                    }
                    else
                    {
                        //Stay in this state
                    }
                }
                else
                {
                    GenControl_ResetPID();
                    gGenControlState = GenControllerDisabled;
                    gGenControlStateTicker = 0;
                }
                break;

            case GenControllerBoost:
                if(MainStateParam == Main_Active)
                {
                    //Switch states if we're receiving a commanded state different from the active state
                   if(GenControllerModeParam != gGenControlState)
                   {
                       gGenControlState = GenControllerModeParam;
                       gGenControlStateTicker = 0;
                   }
                }
                else
                {
                    GenControl_ResetPID();
                    gGenControlState = GenControllerDisabled;
                    gGenControlStateTicker = 0;
                }
                break;
            case GenControllerCharge:
                if(MainStateParam == Main_Active)
                {
                    //Switch states if we're receiving a commanded state different from the active state
                    if(GenControllerModeParam != gGenControlState)
                    {
                        gGenControlState = GenControllerModeParam;
                        gGenControlStateTicker = 0;
                    }
                }
                else
                {
                    GenControl_ResetPID();
                    gGenControlState = GenControllerDisabled;
                    gGenControlStateTicker = 0;
                }
                break;

            case GenControllerAlternator:
                if(MainStateParam == Main_Active)
                {
                    //Switch states if we're receiving a commanded state different from the active state
                    if(GenControllerModeParam != gGenControlState)
                    {
                        gGenControlState = GenControllerModeParam;
                        gGenControlStateTicker = 0;
                    }
                }
                else
                {
                    GenControl_ResetPID();
                    gGenControlState = GenControllerDisabled;
                    gGenControlStateTicker = 0;
                }
                break;

            case GenControllerWaitForMain:
                if( (CritFaultParam == FLT_CRIT_NONE) && MainStateParam == Main_Inactive)
                {
                    gGenControlState = GenControllerDisabled;
                    gGenControlStateTicker = 0;
                }
                break;
            default:
                break;
        }
    }
    else
    {
        if(gGenControlState!=GenControllerWaitForMain)
        {
            GenControl_ResetPID();
            gCurrentCommandAmps =   0;
#if(NEGATIVE_BOOST)
            manualcurrentcommand = 0;
#endif
            gGenControlState = GenControllerWaitForMain;
            gGenControlStateTicker = 0;
        }
    }

    if(Inverter_Params.Controller == GeneratorController)
    {
#if(!FAKE_GENCONTROL)
        if(GenControlMotionStates(gGenControlState))
        {
            gCurrentCommandAmps = GenControlCalc(gGenControlState, ControlRefParam, BMSFdbParam, RPMParam);
        }
        else
        {
            gCurrentCommandAmps = 0.0;
        }
#endif
    }

    return gGenControlState;
}


/**Function: GenControl_UpdateGains
 * ------------------
 * This function is called from the ProcessSetParameterCommand
 * function to update the PID gains of the generator controller
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void GenControl_UpdateGains(void)
{
    pid_gen_ctrl.Kp = GenControlParams.pi_kp;
    pid_gen_ctrl.Ki = GenControlParams.pi_ki*GenControlParams.T_LOOP;
    pid_gen_ctrl.Kd = 0.0;
}

/**Function: GenControlMotionOK
 * ------------------
 * This function is called from the getLoopCriticalFaults
 * function to check if the engine is spinning as expected in a motion state
 *
 * Outputs:
 * @returns false if the system should be spinning but is not
 * @returns true if the system is intentionally not spinning, or spinning while active
 *
 * Inputs:
 * None
 */
bool GenControlMotionOK(void)
{
    static uint16_t motionticker;
    //Check if the generator controller is in a state where it should be spinning (minus start attempt)
    if(GenControlMotionStates(gGenControlState) && gGenControlState != GenControllerStart && gGenControlState != GenControllerStartTimeout)
    {
        //If it's been half a second in charge, boost, or alternator and the system is still below threshold return false
        if( (gGenControlStateTicker>=GenControlParams.LOOP_HZ*GenControlParams.MotionCheck_sec) && getSystemRPM() < GenControlParams.MotionCheck_rpm)
        {
            motionticker++; //Increment a timer for every time this case is true
        }
        else
        {
            motionticker=0; //Zero out the timers
        }
    }
    else
    {
        motionticker=0;
    }

    if(motionticker>=Motor_Params.LOOP_FREQ_KHZ/2)
    {
        //If it has been half a second
        return false;
    }
    else
    {
        return true;
    }
}


/**Function: GenControlMotionStates
 * ------------------
 * This function is a check to see if a generator
 * controller state is one that should be in motion
 *
 * Used for fault checks and state transitions
 *
 * @returns false if the state is one in which the system should be halted
 * @returns true if the state is one in which the system should be in motion
 *
 * Inputs:
 * None
 */
bool GenControlMotionStates(enum GenControlStateVariables in)
{
    if(in == GenControllerAlternator || in == GenControllerStart || in == GenControllerBoost || in == GenControllerCharge)
    {
        return true;
    }

    return false;
}



/**Function: GenControlUpdateLPF
 * ------------------
 * This function is called from the ProcessSetParameterCommand
 * function to update the LPF alpha values when changed over CAN
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void GenControlUpdateLPF(void)
{
    GenControlParams.FF_LPF_Alpha = (1.0-expf(-2.0*PI*GenControlParams.FF_LPF_Hz/GenControlParams.LOOP_HZ));
    GenControlParams.DCP_LPF_Alpha = (1.0-expf(-2.0*PI*GenControlParams.DCP_LPF_Hz/GenControlParams.LOOP_HZ));
}



//Get methods for this file
bool GCEnergizedStates(enum GenControlStateVariables in) { return ( (in >= GenControllerEnabled) && (in != System_Reset));} //Returns true if the input state is "energized" or "enabled"
enum GenControlStateVariables GenControl_GetState(void) {return gGenControlState;}   //Returns the current generator controller state
float getGCAmpsCommand(void) {return gCurrentCommandAmps;}  //Returns the commanded amps from GenControlCalc
uint16_t getGenControlFailedStarts(void) {return gFailedStartAttempts;}
bool GenControlStartAttemptsOK(void) { return gFailedStartAttempts < GenControlParams.StartAttempts;}
float GenControl_GetDCPowerLPF(void) {return gGCPowerDemandLPF;}
uint32_t getGenControlStateTicker(void) {return gGenControlStateTicker;}

