/* ============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	Datalog.C

Target:			Cryo controller board

Author:			Mike Ricci

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle data logging variables and functions/routines
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "utility Src Files/DLOG_4CH_F.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1


// Function prototypes
void Init_Datalog(void);
void Run_Datalog(void);
float getDBuffCh1(unsigned int);
void setDBuffCh1(float, unsigned int);

//  Module resources - variables here //

// ****************************************************************************
// Variables for Datalog module
// ****************************************************************************
float DBUFF_4CH1[DLOG_SIZE],
      DBUFF_4CH2[DLOG_SIZE],
      DBUFF_4CH3[DLOG_SIZE],
      DBUFF_4CH4[DLOG_SIZE],
      DlogCh1,
      DlogCh2,
      DlogCh3,
      DlogCh4;

// Create an instance of DATALOG Module
DLOG_4CH_F dlog_4ch1;


// Function Definitions            //

void Init_Datalog(void)
{

    int i=0;

    for (i=0;i<DLOG_SIZE;i++)
    {
        DBUFF_4CH1[i]=0.0;
        DBUFF_4CH2[i]=0.0;
        DBUFF_4CH3[i]=0.0;
        DBUFF_4CH4[i]=0.0;
    }

	// ****************************************************
	// Initialize DATALOG module
	// ****************************************************
		DLOG_4CH_F_init(&dlog_4ch1);
		dlog_4ch1.input_ptr1 = &DlogCh1;	//data value
		dlog_4ch1.input_ptr2 = &DlogCh2;
		dlog_4ch1.input_ptr3 = &DlogCh3;
		dlog_4ch1.input_ptr4 = &DlogCh4;
		dlog_4ch1.output_ptr1 = &DBUFF_4CH1[0];
		dlog_4ch1.output_ptr2 = &DBUFF_4CH2[0];
		dlog_4ch1.output_ptr3 = &DBUFF_4CH3[0];
		dlog_4ch1.output_ptr4 = &DBUFF_4CH4[0];
		dlog_4ch1.size = DLOG_SIZE;
		dlog_4ch1.pre_scalar = DLOG_PRESCALAR;
		dlog_4ch1.trig_value = .1;
		dlog_4ch1.trig_mode = Normal;
		dlog_4ch1.trig_edge = Rising;
		dlog_4ch1.status = 3;
}

void Run_Datalog(void)
{
	DLOG_4CH_F_FUNC(&dlog_4ch1);
}

void setDBuffCh1(float c, unsigned int index)
{
    DBUFF_4CH1[index] = c;
}

// returns index value of dbuffch1
float getDBuffCh1(unsigned int index)
{
    return DBUFF_4CH1[index];
}

// returns index value of dbuffch2
float getDBuffCh2(unsigned int index)
{
    return DBUFF_4CH2[index];
}

// returns index value of dbuffch3
float getDBuffCh3(unsigned int index)
{
    return DBUFF_4CH3[index];
}

// returns index value of dbuffch4
float getDBuffCh4(unsigned int index)
{
    return DBUFF_4CH4[index];
}
