/*
 * Module_ECU.c
 *
 *  Created on: Aug 28, 2018
 *      Author: guydeong
 */
#include "F2837S_files/F28x_Project.h"
#include "IQMathLib.h"
#include "Header/ECU.h"
#include "Header/GUIC_Header.h"
//#include "Header/State_machine.h"
#include "Header/pid_reg3mod.h"
#include "Header/PWMDAC.h"
//#include "Header/Globals.h"
#include "Header/UsefulPrototypes.h"
#include "Header/Gen_Control.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1

#define MANUAL_DEBUG_ECU 1

void ECU_State_Machine(enum MainStateVariables , enum GenControlStateVariables , enum CritFaultVariables , float , float , enum ECUControlModes, float  , float);
void ThrottleControlCalc(enum GenControlStateVariables , float , float , enum ECUControlModes, float, float );

void Init_ECU(enum ECUControlModes );
void Reset_ECU_PID(enum ECUControlModes );

extern void Enable_EFI(void);
extern void Disable_EFI(void);

extern float adcGetSignalValue(enum adc_signal_id);

extern void PWMDAC_InitHandle(struct PWMDACHandler *, int, int, float );
extern void PWMDAC_Enable(struct PWMDACHandler *);
extern void PWMDAC_Disable(struct PWMDACHandler * );
extern void PWMDAC_SetFreq(struct PWMDACHandler * , float );
extern void PWMDAC_SetOffset(struct PWMDACHandler * , float , float );
extern void PWMDAC_Out(struct PWMDACHandler *, float);
extern uint32_t getGenControlStateTicker(void);
extern float CAN_GetBMSVolts(void);

extern float ECUSCI_GetGeadTempC(void);

extern struct ECUParameters ECU_Params;

extern enum ECUControlModes GetECUCtrlMode(void);
extern float getSystemRPM(void);

extern void enableFuelPump(void);
extern void disableFuelPump(void);
extern void enableIgnition(uint16_t);
extern void disableIgnition(uint16_t);

PIDREG3_MOD ecu_pid = PIDREG3MOD_DEFAULTS;
PIDREG3_MOD cooling_pid = PIDREG3MOD_DEFAULTS;
#if(NEWCONTROL_MODE)
PIDREG3_MOD chargecontrol_pid = PIDREG3MOD_DEFAULTS;
#endif

float gECU_LUT_Ref = 0.0;
float ThrottleDrive=0.0; //0 to 1.0 float for closed to full open throttle
enum ECULowLevelStateVariables ECUState = ECUInit;
uint32_t ECUStateTicker=0;

struct PWMDACHandler pwmdach_throttle;
struct PWMDACHandler pwmdach_edf;
struct PWMDACHandler pwmdach_radfan;
struct PWMDACHandler pwmdach_hsfan;

enum ECUCoolModes CoolMode = Cool_OnOff;
uint16_t CoolingLoopPrescalar = 1;
uint16_t CoolingLoopCount=1;
int CoolingOn=0;
int RevEnabled=0;

float gThrottleFFTerm=0.0;
float gECUDCPowerDemandLPF=0.0;

#if(MANUAL_DEBUG_ECU)
int gDebugEnable = 0;
float gDebugThrottle = 0;
float gDebugRadFan=0;
float gDebugHSFan = 0;
uint16_t gDebugFPEn=0;
uint16_t gDebugIgn1En=0;
uint16_t gDebugIgn2En=0;
#endif

/**Function: Init_ECU
 * ------------------
 * Initializes the ECU module, PIDs, sets up PWMDACs
 *
 * Outputs:
 * None
 *
 * @param ControlModeParam - Speed, voltage, current, or power control modes.
 */
void Init_ECU(enum ECUControlModes control)
{
    ECUState = ECUInit;

    //Set up ECU PID loop params
    ecu_pid.Kp = ECU_Params.activepid_kp;
    ecu_pid.Ki = ECU_Params.activepid_ki*ECU_Params.T_LOOP;
    ecu_pid.Kd = ECU_Params.activepid_kd;
    ecu_pid.Kc =     0.0;
    ecu_pid.OutMax = 1.0 - (ECU_Params.MinBusPower_H*ECU_Params.PowerThrottleSlope_Lo + ECU_Params.PowerThrottleIntercept_Lo);    //At most, 1.0 - the feedforward value for minimum power
    ecu_pid.OutMin = ECU_Params.StartingThrottleHi - 1.0; //Farthest negative it can go would take a throttle value of 100% to the idle value

    //Set up cooling PID loop params
    cooling_pid.Kp = ECU_Params.pid_cooling_kp;
    cooling_pid.Ki = ECU_Params.pid_cooling_ki*ECU_Params.T_LOOP*(ECU_Params.LOOP_FREQ_KHZ/ECU_Params.CoolingLoopFreqKHz);
    cooling_pid.Kd = ECU_Params.pid_cooling_kd;
    cooling_pid.Kc     = 0.0;
    cooling_pid.OutMax = 1.0;
    cooling_pid.OutMin = -1.0;

#if(NEWCONTROL_MODE)
    //Set up ECU PID loop params
    chargecontrol_pid.Kp = ECU_Params.pid_chgctrl_kp;
    chargecontrol_pid.Ki = ECU_Params.pid_chgctrl_ki*ECU_Params.T_LOOP;
    chargecontrol_pid.Kd = ECU_Params.pid_chgctrl_kd;
    chargecontrol_pid.Kc =     0.0;
    chargecontrol_pid.OutMax = 2000.0;
    chargecontrol_pid.OutMin = -2000.0;
#endif

    ThrottleDrive = 0.0;

    CoolingLoopPrescalar = (uint16_t)(ECU_Params.LOOP_FREQ_KHZ/ECU_Params.CoolingLoopFreqKHz);

    //Todo - get rid of magic numbers and add the 0-100% span into ECU_Params or figure out a way to assign these based on the type of servo we're turning
    //Temporary fix, make a check for the elmo PMU which talks to HFE

    if(Inverter_Params.InverterSel == ELMO_PMU)
    {
        PWMDAC_InitHandle(&pwmdach_throttle, 2, ECU_Params.PWMDAC_NUM_THROTTLE, ECU_Params.ThrottleFreq);
        PWMDAC_SetOffset(&pwmdach_throttle, (0.9/1000.0), (2.1/1000.0) );   //900us min, 2100us max, on CMPB
        PWMDAC_Out(&pwmdach_throttle, -0.7);
        PWMDAC_Enable(&pwmdach_throttle);

        PWMDAC_InitHandle(&pwmdach_edf, 2, ECU_Params.PWMDAC_NUM_EDF, ECU_Params.FanPWMFreq);
        PWMDAC_SetOffset(&pwmdach_edf, (0.8/1000.0),(1.213/1000.0));
        PWMDAC_Out(&pwmdach_edf, 0.0); //ESCs want something under 900us for "off"
        PWMDAC_Enable(&pwmdach_edf);
    }
    else if(Inverter_Params.InverterSel == HPS400_SP)
    {
        //Begin setup of PWMs for the 40kW genset

        //Set up the Throttle PWMDAC handler. EPWM10A
        PWMDAC_InitHandle(&pwmdach_throttle, 10, ECU_Params.PWMDAC_NUM_THROTTLE, ECU_Params.ThrottleFreq);
        PWMDAC_SetOffset(&pwmdach_throttle, (0.8/1000.0), (2.1/1000.0) );   //800us min, 2100us max
        PWMDAC_Out(&pwmdach_throttle, 0.0);
        PWMDAC_Enable(&pwmdach_throttle);

        //Set up the Radiator fan controller PWMDAC handler
        //Todo - lose magic numbers. This is tied to EPWM2A, 100Hz
        PWMDAC_InitHandle(&pwmdach_radfan, 2, 1, 100);
        PWMDAC_SetOffset(&pwmdach_radfan, (0.8/1000.0), (2.1/1000.0) );   //800us min, 2100us max
        PWMDAC_Out(&pwmdach_radfan, 0.0);
        PWMDAC_Enable(&pwmdach_radfan);

        //Set up the heatsink fan controller PWMDAC handler
        //Todo - lose magic numbers. This is tied to EPWM3B, 100Hz
        PWMDAC_InitHandle(&pwmdach_hsfan, 3, 2, 100);
        PWMDAC_SetOffset(&pwmdach_hsfan, (0.8/1000.0), (2.1/1000.0) );   //800us min, 2100us max
        PWMDAC_Out(&pwmdach_hsfan, 0.0);
        PWMDAC_Enable(&pwmdach_hsfan);
    }



}


/**Function: Reset_ECU_PID
 * ------------------
 * Zeroes out all PID terms for both throttle and cooling
 *
 * Outputs:
 * None
 *
 * @param ControlModeParam - Speed, voltage, current, or power control modes.
 */
void Reset_ECU_PID(enum ECUControlModes control)
{
    ecu_pid.Err=0;
    ecu_pid.Fdb=0;
    ecu_pid.Ref=0;
    ecu_pid.Ui=0;
    ecu_pid.Up=0;
    ecu_pid.Ud=0;
    ecu_pid.Out=0;
    ecu_pid.OutPreSat=0;
    ecu_pid.SatErr=0;
    ecu_pid.Up1=0;
    ecu_pid.u = 0;
    ecu_pid.new_i = 0;
    ecu_pid.int_ok = 0;

    cooling_pid.Err=0;
    cooling_pid.Fdb=0;
    cooling_pid.Ref=0;
    cooling_pid.Ui=0;
    cooling_pid.Up=0;
    cooling_pid.Ud=0;
    cooling_pid.Out=0;
    cooling_pid.OutPreSat=0;
    cooling_pid.SatErr=0;
    cooling_pid.Up1=0;
    cooling_pid.u = 0;
    cooling_pid.new_i = 0;
    cooling_pid.int_ok = 0;

#if(NEWCONTROL_MODE)
    chargecontrol_pid.Err=0;
    chargecontrol_pid.Fdb=0;
    chargecontrol_pid.Ref=0;
    chargecontrol_pid.Ui=0;
    chargecontrol_pid.Up=0;
    chargecontrol_pid.Ud=0;
    chargecontrol_pid.Out=0;
    chargecontrol_pid.OutPreSat=0;
    chargecontrol_pid.SatErr=0;
    chargecontrol_pid.Up1=0;
    chargecontrol_pid.u = 0;
    chargecontrol_pid.new_i = 0;
    chargecontrol_pid.int_ok = 0;
#endif
}


/**Function: ThrottleControlCalc
 * ------------------
 * Function that calculates throttle opening percentage to output to throttle servo
 * Called by the ECU_State_Machine in "Enabled" state
 *
 * Outputs:
 * None
 *
 * @param GenControlStateParam - Top level generator controller state.
 * @param ControlRefParam - Control reference - used only when running a passive genset
 * @param BMSFdbParam - Battery pack current - used to close a current loop when running as a passive genset, used to calculate total power for the Power->RPM LUT
 * @param ControlModeParam - Speed, voltage, current, or power control modes.
 */
void ThrottleControlCalc(enum GenControlStateVariables GenControlStateParam, float ControlRefParam, float BMSFdbParam, enum ECUControlModes ControlModeParam, float RPMParam, float BMSReqParam)
{
    static float ECUTotalCurrentDemand=0, ECUTotalPowerDemand=0, ECUTotalPowerDemand_LPF=0;
    static double ECUThrottleFFTerm_LPF=0, ECUThrottleFeedForwardTerm=0;

    /*Begin engine running code*/

    if(GenControlStateParam == GenControllerCharge || GenControlStateParam == GenControllerAlternator
#if(NEGATIVE_BOOST)
            ||GenControlStateParam == GenControllerBoost)
#else
        )
#endif
    {
        switch(ControlModeParam)
        {
            case ECUCtrl_Manual:
                ThrottleDrive = ControlRefParam;  //Throttle comes in as a 0-100% Command over CAN and is divided before being sent into this module
                break;

            case ECUCtrl_Speed:
                ecu_pid.Ref = ControlRefParam;   //Control reference in RPMs
                ecu_pid.Fdb = RPMParam;        //Engine RPMs feedback

                ECUThrottleFeedForwardTerm = 0.0;
                break;

            case ECUCtrl_Voltage:
                //ControlRefParam will be the reference in volts
                ecu_pid.Ref = ControlRefParam;
                ecu_pid.Fdb = adcGetSignalValue(adc_id_vdc);

                ECUThrottleFeedForwardTerm = ECU_Params.feedfw_int + ECU_Params.feedfw_mi*ECUTotalCurrentDemand + ECU_Params.feedfw_mv*ControlRefParam;
                break;

            case ECUCtrl_Current:
                //ControlRefParam will be in amps

                /***********************************************************************
                *BMS measured current (+) is OUT of the battery, (-) is INTO the battery
                *We want throttle to open when (desired current - pack current > 0),
                *since we don't want the battery outputting current.
                *
                *Positive Ref-Fdb error will cause pid.Out to be positive
                *BMS will send a positive "desired current" to ask for charge and
                *a negative "desired current" will be to ask for discharge
                *
                *So if we want throttledrive assignment to remain the same as above (no mess of 1 - Tdrive, etc)
                *ecu_pid.Fdb = -1.0 * (CURRENT OUT OF BATTERY)
                *ecu_pid.Ref = (DESIRED CHARGE CURRENT)
                *
                *example:
                *Current out of battery = 30A (Fdb)
                *Charge current requested = 0A (Ref)
                *
                * (0A) - (-1.0)*30A = 0A + 30A -> positive error/output, throttle opens
                * Requested charging current = 10A
                * (10.0A) - (-1.0)*(30A) = 40A error ->positive error/output, throttle opens
                * Requested charging current = -10A (discharge battery)
                * (-10A) - (-1.0)*(30A) = 20A error -> positive error/output, throttle opens but not as much as above
                *
                */
                if(GenControlStateParam == GenControllerAlternator)
                {
                    ECUTotalCurrentDemand = adcGetSignalValue(adc_id_idc)+BMSFdbParam;    //Always use this quantity so that a negative transient dumping a bunch of current into the battery shows 0
                }
                else if(GenControlStateParam == GenControllerCharge)
                {
                    ECUTotalCurrentDemand = adcGetSignalValue(adc_id_idc)+BMSFdbParam+BMSReqParam;    //Always use this quantity so that a negative transient dumping a bunch of current into the battery shows 0
                }

                ecu_pid.Ref = ControlRefParam;
                ecu_pid.Fdb = -1.0*BMSFdbParam;

                ECUThrottleFeedForwardTerm = ECU_Params.feedfw_int + ECU_Params.feedfw_mi*ECUTotalCurrentDemand + ECU_Params.feedfw_mv*adcGetSignalValue(adc_id_vdc);
                break;

            case ECUCtrl_Power:
                /* This control mode is used on an active system (PWM-able electronics - Elmo or internal drive)
                 * Lookup tables are created to map DC power demand on the bus to speed/throttle setpoints
                 * Ideally, at each power, the most fuel efficient speed is chosen as the target RPM for the system
                 * A throttle value for this speed and power output is fed forward and added to the output of A PI control loop
                 * that is closed around the target RPM. A second control loop is closed around battery current which
                 * fine tunes this target RPM setpoint. The regenerative torque controller (Module_Gen_Control.c)
                 * scales torque command proportionally with engine speed, so as the engine speeds up, it is loaded down more
                 */

                // 1. Calculate total current demand. In Alternator mode, we ignore the BMS charge request
                //      and only look at current out of the battery and current out of the genset
                if(GenControlStateParam == GenControllerAlternator)
                {
                    ECUTotalCurrentDemand = adcGetSignalValue(adc_id_idc)+BMSFdbParam;
                }
                else if(GenControlStateParam == GenControllerCharge)
                {
                    ECUTotalCurrentDemand = adcGetSignalValue(adc_id_idc)+BMSFdbParam+BMSReqParam;
                }

                // 2. Calculate total DC power demand by multiplying Vdc x Idc, and then LPF this value
                ECUTotalPowerDemand = ECUTotalCurrentDemand*CAN_GetBMSVolts();
//                ECUTotalPowerDemand = ECUTotalCurrentDemand*adcGetSignalValue(adc_id_vdc);
                ECUTotalPowerDemand_LPF = (float)(ECU_Params.DCP_LPF_Alpha*(ECUTotalPowerDemand) + (1.0-ECU_Params.DCP_LPF_Alpha)*(ECUTotalPowerDemand_LPF));
                gECUDCPowerDemandLPF = ECUTotalPowerDemand_LPF;

                // 3. Constrain this value to the maximum allowable power out of the genset. Power above ECU_Params.MaxGenPower will come out of the battery
                ECUTotalPowerDemand_LPF = saturate(0,ECU_Params.MaxGenPower, ECUTotalPowerDemand_LPF);

                // 4. Calculate the speed reference from the LUT that maps DC power demand to target RPM
                gECU_LUT_Ref = ECU_Params.PowerSpeedSlope*ECUTotalPowerDemand_LPF + ECU_Params.PowerSpeedIntercept;

#if(NEWCONTROL_MODE)
                // 5. Set the parameters for the charge control PI loop which fine tunes RPM setpoint to bring
                //    battery current to the target value (0 in alternator mode, BMS charge request in charge mode)
                if(GenControlStateParam == GenControllerAlternator)
                {
                    chargecontrol_pid.Ref = 0.0;
                }
                else if(GenControlStateParam == GenControllerCharge)
                {
                    chargecontrol_pid.Ref = BMSReqParam;
                }

                // 6. Constrain the fine-tuned RPM target change such that it does not command something past the maximum allowable engine speed
                chargecontrol_pid.OutMax = ECU_Params.MaxSpeedRef - gECU_LUT_Ref;

                // 7. Set the feedback value such that positive current out of the battery gives a positive change in RPM target
                // For example, in alternator mode, 10A out of the battery makes for an error of
                // Ref - Fdb = 0.0 - (-10A) = 10A -> positive error causes positive output, RPM target increases
                chargecontrol_pid.Fdb = -1.0*BMSFdbParam;
#endif
                // 8. Calculate the feedforward value for the throttle

                // If our RPM target based on power demand is below the minimum speed reference then the engine is essentially idling,
                // implying that the power demand is less than optimal for the engine to take on that load. If this is the case,
                // there is no need to feed forward the throttle
                if(gECU_LUT_Ref<=ECU_Params.MinSpeedRef)
                {
                    ECUThrottleFeedForwardTerm = 0;
                }
                // If the RPM target is above the minimum speed reference, then we are pulling enough power for the genset to begin doing work.
                // Here we calculate the throttle feedforward based on power and speed
                else
                {
                    //Below 5.2kW I found that the fit using a multivariable regression on power and speed was better than just on power
                    if(ECUTotalPowerDemand_LPF <= 5200)
                     {
                         ECUThrottleFeedForwardTerm = ECU_Params.PowerThrottleSlope_Lo*ECUTotalPowerDemand_LPF + ECU_Params.SpeedThrottleSlope_Lo*ecu_pid.Ref + ECU_Params.PowerThrottleIntercept_Lo;
                     }
                    //Above 5.2kW, the fit was pretty much perfectly linear with DC power demand
                     else
                     {
                         ECUThrottleFeedForwardTerm = ECU_Params.PowerThrottleSlope_Hi*ECUTotalPowerDemand_LPF + ECU_Params.PowerThrottleIntercept_Hi;
                     }
                }
#if(NEWCONTROL_MODE)
                // 9. Constrain the throttle controller's reference setpoint between min. and max. allowable speeds
                    ecu_pid.Ref = saturate(ECU_Params.MinSpeedRef,ECU_Params.MaxSpeedRef,gECU_LUT_Ref + chargecontrol_pid.Out);
#else
                    ecu_pid.Ref = saturate(ECU_Params.MinSpeedRef,ECU_Params.MaxSpeedRef,gECU_LUT_Ref);
#endif
                    /*
                }
                else
                {
                    gECU_LUT_Ref = 0.0;
                    ThrottleFeedForwardTerm = 0.0;
                    ecu_pid.Ref = ECU_Params.IdleSpeed;
                }
                    */
                 // 10. LPF the feedforward term, and constrain the control loop output such that it can't run away and open past 100%
                ECUThrottleFFTerm_LPF = (float)(ECU_Params.FF_LPF_Alpha)*(ECUThrottleFeedForwardTerm) + (1.0-ECU_Params.FF_LPF_Alpha)*ECUThrottleFFTerm_LPF;
                ecu_pid.OutMax = 1.0 - ECUThrottleFFTerm_LPF;   //Don't allow integrator to wind up and let throttle run past 100%
                ecu_pid.Fdb = RPMParam;        //Engine RPMs feedback

                break;

            default:
                break;
        }

        if(ControlModeParam != ECUCtrl_Manual)
        {
            if(ControlModeParam != ECUCtrl_Speed)
            {
                /*hysteresis for engine rev enable*/

                if( (ECUTotalPowerDemand_LPF <= ECU_Params.MinBusPower_L) && (RevEnabled==1) )
                {
                    RevEnabled = 0;
                    //Zero all ECU PID params
                    Reset_ECU_PID((enum ECUControlModes)ControlModeParam);
                    //Set throttle drive equal to idle throttle
                    ThrottleDrive = ECU_Params.StartingThrottleHi;
                    //Pre-load integrator of throttle controller so engine doesn't die when we reenable rev
                    ecu_pid.Ui = ECU_Params.StartingThrottleHi;
                }
                else if( (ECUTotalPowerDemand_LPF >= ECU_Params.MinBusPower_H) && (RevEnabled==0))
                {
                    RevEnabled = 1;
                    ecu_pid.Ui = ThrottleDrive; //Load integrator with last held value (idle)
                }
            }
            else
            {
                RevEnabled = 1; //Always rev enabled for speed control
            }

            if(RevEnabled==1)
            {
                if( (ECUTotalPowerDemand <= ECU_Params.MaxGenPower) && adcGetSignalValue(adc_id_tstator) < Motor_Params.LIM_T_STATOR_WARN_H)
                {
#if(NEWCONTROL_MODE)
                    //Only run charge control if stator is cool and we are not above max power
                    PID_MOD_MACRO(chargecontrol_pid)
#endif
                }
                PID_MOD_MACRO(ecu_pid)  //Always run the ECU PID
                ThrottleDrive = saturate(ECU_Params.StartingThrottleHi,1.0,ECU_Params.feedfw_gain*ECUThrottleFFTerm_LPF + ecu_pid.Out);
            }
            else
            {
                if(getGenControlStateTicker() <= (uint32_t)(ECU_Params.StartupEnrichTime*GenControlParams.LOOP_HZ))
                {
                    ThrottleDrive = ECU_Params.StartingThrottleHi + ((double)(ECU_Params.StartingThrottleLo - ECU_Params.StartingThrottleHi)/ECU_Params.StartupEnrichTime/GenControlParams.LOOP_HZ)*getGenControlStateTicker();
                }
                else
                {
                    ThrottleDrive = ECU_Params.StartingThrottleLo;
                }

                /*
                //Load integrator with starting throttle only if feedforward is disabled
                if(ECU_Params.feedfw_gain > 0.0)
                {
                    ecu_pid.Ui=0.0;
                }
                else
                {
                    ecu_pid.Ui = ECU_Params.StartingThrottle;
                }
                */
//                ecu_pid.Ui = ECU_Params.StartingThrottle;
            }
        }
    }
    else if(GenControlStateParam == GenControllerStart || GenControlStateParam == GenControllerStartTimeout)
    {
        ThrottleDrive = ECU_Params.StartingThrottleHi;
        ecu_pid.Ui = ECU_Params.StartingThrottleHi;
    }
    else
    {
        ThrottleDrive = 0.0;
    }

    PWMDAC_Out(&pwmdach_throttle, ThrottleDrive);

    gThrottleFFTerm = ECUThrottleFFTerm_LPF;
    /*End Throttle control code*/

}


/**Function: ECU_State_Machine
 * ------------------
 * State machine that transitions between enabled/disabled ECU states
 * In the Enabled state, runs a throttle control calculation function
 *
 * Outputs:
 * None
 *
 * @param MainStateParam - Top level main state from the "slow" main state machine.
 * @param GenControlStateParam - Top level generator controller state.
 * @param CritFaultParam - Critical faults
 * @param ControlRefParam - Control reference - used only when running a passive genset
 * @param BMSFdbParam - Battery pack current - used to close a current loop when running as a passive genset, used to calculate total power for the Power->RPM LUT
 * @param ControlModeParam - Speed, voltage, current, or power control modes.
 *
 */

void ECU_State_Machine(enum MainStateVariables MainStateParam, enum GenControlStateVariables GenControlStateParam, enum CritFaultVariables CritFaultParam, float ControlRefParam, float BMSFdbParam, enum ECUControlModes ControlModeParam, float RPMParam, float BMSReqParam )
{
    ECUStateTicker++;

#if(!MANUAL_DEBUG_ECU && !COMPRESSIONTESTMODE)

    /* Begin ECU state machine */
    if(CritFaultParam == FLT_CRIT_NONE)
    {
        switch(ECUState)
            {
                case ECUInit:
                    if(MainStateParam == Main_Inactive)
                    {
                        PWMDAC_Out(&pwmdach_throttle, -0.7);
                        Disable_EFI();
                        Reset_ECU_PID((enum ECUControlModes)ControlModeParam);
                        ThrottleDrive=0.0;

                        ECUState = ECUDisable;
                        CoolMode = Cool_OnOff;  //Set cooling mode to be whatever the user selected cooling mode is
                        RevEnabled=0;
                        ECUStateTicker=0;
                    }
                    else{} //Stay in init if we don't see main go Inactive

                    break;

                case ECUWaitForParent:
                    if(CritFaultParam == FLT_CRIT_NONE && MainStateParam == Main_Inactive)
                    {
                        ECUState = ECUDisable;
                        RevEnabled=0;
                        ECUStateTicker=0;
                    }
                    else
                    {
                        ECUState = ECUWaitForParent;
                    }
                    break;

                case ECUDisable:
                    if(MainStateParam == Main_Active)
                    {
                        PWMDAC_Enable(&pwmdach_throttle);
                        Enable_EFI();
                        RevEnabled=0;
                        ECUState = ECUEnable;
                        CoolMode = ECU_Params.CoolingMode;  //While running set cooling mode to whatever the user has selected
                        ECUStateTicker = 0;
                    }
                    else
                    {
                        //Do nothing
                    }

                    break;

                case ECUEnable:
                    if(MainStateParam == Main_Active)
                    {
                        ThrottleControlCalc(GenControlStateParam, ControlRefParam, BMSFdbParam, ControlModeParam, RPMParam, BMSReqParam);
                    }
                    else
                    {
                        PWMDAC_Out(&pwmdach_throttle, -0.7);
                        Disable_EFI();
                        Reset_ECU_PID((enum ECUControlModes)ControlModeParam);
                        ThrottleDrive=0.0;
                        RevEnabled=0;
                        ECUState = ECUDisable;
                        CoolMode = Cool_OnOff; //On/Off cooling when engine not running - if overheating it'll cool down
                        ECUStateTicker=0;
                    }

                    break;
                default:
                    break;
            }
    }
    else
    {
        if(ECUState!=ECUWaitForParent)
        {
            PWMDAC_Out(&pwmdach_throttle, 0.0);
            Disable_EFI();
            Reset_ECU_PID((enum ECUControlModes)ControlModeParam);
            RevEnabled=0;
            ThrottleDrive=0.0;
            CoolMode = Cool_OnOff; //On/Off cooling when engine not running - if overheating it'll cool down
            ECUState = ECUWaitForParent;
            ECUStateTicker=0;
        }
    }


    /* End ECU state machine */
#elif(MANUAL_DEBUG_ECU)
    PWMDAC_Enable(&pwmdach_throttle);
    PWMDAC_Out(&pwmdach_throttle,gDebugThrottle);

    if(Inverter_Params.InverterSel==ELMO_PMU)
    {
        if(gDebugEnable==0)
        {
            Disable_EFI();
        }
        else
        {
            Enable_EFI();
        }
    }
    else if(Inverter_Params.InverterSel==HPS400_SP)
    {
        PWMDAC_Enable(&pwmdach_radfan);
        PWMDAC_Out(&pwmdach_radfan, gDebugRadFan);

        PWMDAC_Enable(&pwmdach_hsfan);
        PWMDAC_Out(&pwmdach_hsfan, gDebugHSFan);

        if(gDebugFPEn==1)
        {
            enableFuelPump();
        }
        else
        {
            disableFuelPump();
        }

        if(gDebugIgn1En==1)
        {
            enableIgnition(1);
        }
        else
        {
            disableIgnition(1);
        }

        if(gDebugIgn2En==1)
        {
            enableIgnition(2);
        }
        else
        {
            disableIgnition(2);
        }
    }


#elif(COMPRESSIONTESTMODE)
    Enable_EFI();
    PWMDAC_Enable(&pwmdach_throttle);
    PWMDAC_Out(&pwmdach_throttle, 1.0);
#endif

    /*Start cooling code*/

    if(CoolMode == Cool_OnOff)
    {
        if( (ECUStateTicker <= ECU_Params.LOOP_FREQ_KHZ*3000L) && (ECUState == ECUWaitForParent))
        {
            //If EDFs failed, ESCs will want a signal reset.
            //This is a fault mode - state ticker should go to
            //zero when ECU state machine enters waitformain
            ECU_Params.CoolingDuty = 0.0;
        }
        else
        {
            /*hysteresis for cooling*/
            if(ECUSCI_GetGeadTempC()>=ECU_Params.CoolingThreshHiC)
            {
                ECU_Params.CoolingDuty = 0.85;
            }
            else if(ECUSCI_GetGeadTempC()<ECU_Params.CoolingThreshLoC)
            {
                ECU_Params.CoolingDuty = 0.0;
            }
        }
        PWMDAC_Out(&pwmdach_edf, ECU_Params.CoolingDuty);
    }
    else if(CoolMode==Cool_PID)
    {
        /*hysteresis for Cooling Loop*/
        if(ECUSCI_GetGeadTempC()>=ECU_Params.CoolingThreshHiC)
        {
            CoolingOn=1;
        }
        else if(ECUSCI_GetGeadTempC()<ECU_Params.CoolingThreshLoC)
        {
            CoolingOn=0;
        }

        /*Run cooling loop (prescaled)*/
        if(CoolingOn==1)
        {
            if(CoolingLoopCount>=CoolingLoopPrescalar)
            {
                cooling_pid.Ref = ECU_Params.CoolingTargetC;
                cooling_pid.Fdb = ECUSCI_GetGeadTempC();

                //Hold fan power steady if we're within +-7.5F of the target temp
                if(!inRange(cooling_pid.Fdb, cooling_pid.Ref+7.5, cooling_pid.Ref-7.5))
                {
                    PID_MOD_MACRO(cooling_pid)
                }

                PWMDAC_Out(&pwmdach_edf, -1.0*cooling_pid.Out);

                CoolingLoopCount=1;
            }
            else
            {
                CoolingLoopCount++;
            }
        }
        else
        {
            cooling_pid.Err=0;
            cooling_pid.Fdb=0;
            cooling_pid.Ref=0;
            cooling_pid.Ui=0;
            cooling_pid.Up=0;
            cooling_pid.Ud=0;
            cooling_pid.Out=0;
            cooling_pid.OutPreSat=0;
            cooling_pid.SatErr=0;
            cooling_pid.Up1=0;
            cooling_pid.u = 0;
            cooling_pid.new_i = 0;
            cooling_pid.int_ok = 0;

            PWMDAC_Out(&pwmdach_edf, 0.0); //ESCs want something under 900us for "off"

            CoolingLoopCount=1;
        }
    }
    else if(CoolMode == Cool_Manual)
    {
        if(adcGetSignalValue(adc_id_vdc)>=35.0)
        {
            PWMDAC_Out(&pwmdach_edf, ECU_Params.CoolingDuty);
        }
        else
        {
            PWMDAC_Out(&pwmdach_edf, 0.0);
        }

    }
}


/**Function: ECU_UpdateGains
 * ------------------
 * Updates the PID gains when their values are updated over CAN
 *
 * Outputs:
 * None
 *
 * @param modeswitch - boolean telling us whether we are changing control modes (unused right now)
 */
void ECU_UpdateGains(bool modeswitch)
{
    //ECU_Params.activepid_k* is the parameter that is changed via CAN

    switch(ECU_Params.ControlMode)
    {
        case ECUCtrl_Speed:
            ECU_Params.pid_speed_kp = ECU_Params.activepid_kp;
            ECU_Params.pid_speed_ki = ECU_Params.activepid_ki;
            ECU_Params.pid_speed_kd = ECU_Params.activepid_kd;
            break;
        case ECUCtrl_Voltage:
            ECU_Params.pid_voltage_kp = ECU_Params.activepid_kp;
            ECU_Params.pid_voltage_ki = ECU_Params.activepid_ki;
            ECU_Params.pid_voltage_kd = ECU_Params.activepid_kd;
            break;
        case ECUCtrl_Current:
            ECU_Params.pid_current_kp = ECU_Params.activepid_kp;
            ECU_Params.pid_current_ki = ECU_Params.activepid_ki;
            ECU_Params.pid_current_kd = ECU_Params.activepid_kd;
            break;
        case ECUCtrl_Power:
            ECU_Params.pid_power_kp = ECU_Params.activepid_kp;
            ECU_Params.pid_power_ki = ECU_Params.activepid_ki;
            ECU_Params.pid_power_kd = ECU_Params.activepid_kd;
            break;
    }

    ecu_pid.Kp = ECU_Params.activepid_kp;
    ecu_pid.Ki = ECU_Params.activepid_ki*ECU_Params.T_LOOP;
    ecu_pid.Kd = ECU_Params.activepid_kd;

#if(NEWCONTROL_MODE)
    chargecontrol_pid.Kp = ECU_Params.pid_chgctrl_kp;
    chargecontrol_pid.Ki = ECU_Params.pid_chgctrl_ki*ECU_Params.T_LOOP;;
    chargecontrol_pid.Kd = ECU_Params.pid_chgctrl_kd;
#endif

#if 0
    if(modeswitch==false)
        //If we're directly updating gains
        switch(GetECUCtrlMode())
        {
            case ECUCtrl_SpeedDirect:
                ECU_Params.pid_speed_kp = ECU_Params.activepid_kp;
                ECU_Params.pid_speed_ki = ECU_Params.activepid_ki;
                ECU_Params.pid_speed_kd = ECU_Params.activepid_kd;
                break;
            case Voltage_Control:
                ECU_Params.pid_voltage_kp = ECU_Params.activepid_kp;
                ECU_Params.pid_voltage_ki = ECU_Params.activepid_ki;
                ECU_Params.pid_voltage_kd = ECU_Params.activepid_kd;
                break;
            case ECUCtrl_GeneratorDirect:
                ECU_Params.pid_current_kp = ECU_Params.activepid_kp;
                ECU_Params.pid_current_ki = ECU_Params.activepid_ki;
                ECU_Params.pid_current_kd = ECU_Params.activepid_kd;
                break;
            default:
                break;
        }
    }
    else
    {
        //If we have changed control modes, load the currently stored PID gains for the corresponding control mode
        switch(ECU_Params.ControlMode)
        {
            break;
        }
    }

    ecu_pid.Kp = ECU_Params.activepid_kp;
    ecu_pid.Ki = ECU_Params.activepid_ki*ECU_Params.T_LOOP;
    ecu_pid.Kd = ECU_Params.activepid_kd;

    cooling_pid.Kp = ECU_Params.pid_cooling_kp;
    cooling_pid.Ki = ECU_Params.pid_cooling_ki*ECU_Params.T_LOOP*(ECU_Params.LOOP_FREQ_KHZ/ECU_Params.CoolingLoopFreqKHz);
    cooling_pid.Kd = ECU_Params.pid_cooling_kd;
#endif
}



/**Function: ECU_UpdateLPF
 * ------------------
 * Updates the alpha term of the low pass filters
 * when the parameter for the LPF cutoff frequency is changed
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void ECU_UpdateLPF(void)
{
    ECU_Params.DCP_LPF_Alpha = (1.0-expf(-2.0*PI*ECU_Params.DCP_LPF_Hz/ECU_Params.LOOP_FREQ_KHZ/1000.0));
    ECU_Params.FF_LPF_Alpha = (1.0-expf(-2.0*PI*ECU_Params.FF_LPF_Hz/ECU_Params.LOOP_FREQ_KHZ/1000.0));
}

/**Function: ECU_UpdateCoolingMode
 * ------------------
 * Updates the cooling mode when changed via CAN
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void ECU_UpdateCoolingMode(void)
{
    CoolMode = ECU_Params.CoolingMode;
}


float ECU_GetKI(void) {return ecu_pid.Ki/ECU_Params.T_LOOP;}
float ECU_GetKP(void) {return ecu_pid.Kp;}
float ECU_GetThrottleSetting(void) {return ThrottleDrive;}
enum ECULowLevelStateVariables ECU_GetState(void) {return ECUState;}
float GetEcuPidReference(void) {return ecu_pid.Ref;}
float GetEcuPidFeedback(void) {return ecu_pid.Fdb;}
float GetEcuPidError(void) {return ecu_pid.Err;}
float GetEcuPidUp(void) {return ecu_pid.Up;}
float GetEcuPidUi(void) {return ecu_pid.Ui;}
float GetEcuPidUd(void) {return ecu_pid.Ud;}
float GetEcuPidOutPreSat(void) {return ecu_pid.OutPreSat;}
float ECU_GetEDFDuty(void) {return ECU_Params.CoolingDuty;}
float ECU_GetPIDRef(void) {return ecu_pid.Ref;}
float ECU_GetRPMLUT(void) {return gECU_LUT_Ref;}
float ECU_GetFFTerm(void) {return gThrottleFFTerm;}
float ECU_GetDCPowerLPF(void) {return gECUDCPowerDemandLPF;}
#if(NEWCONTROL_MODE)
float ECU_GetChgPidOutPreSat(void) {return chargecontrol_pid.OutPreSat;}
#endif
