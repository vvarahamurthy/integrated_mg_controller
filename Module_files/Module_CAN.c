//###########################################################################
// FILE:   can_loopback_interrupts.c
// TITLE:  Example to demonstrate basic CAN setup and use.
//
//! \addtogroup cpu01_example_list
//! <h1>CAN External Loopback with Interrupts (can_loopback_interrupts)</h1>
//!
//! This example shows the basic setup of CAN in order to transmit and receive
//! messages on the CAN bus.  The CAN peripheral is configured to transmit
//! messages with a specific CAN ID.  A message is then transmitted once per
//! second, using a simple delay loop for timing.  The message that is sent is
//! a 4 byte message that contains an incrementing pattern.  A CAN interrupt
//! handler is used to confirm message transmission and count the number of
//! messages that have been sent.
//!
//! This example sets up the CAN controller in External Loopback test mode.
//! Data transmitted is visible on the CAN0TX pin and can be received with
//! an appropriate mailbox configuration.
//!
//! This example uses the following interrupt handlers:\n
//! - INT_CANA0 - CANIntHandler
//!
//
//###########################################################################
// $TI Release: F2837xS Support Library v180 $
// $Release Date: Fri Nov  6 16:27:58 CST 2015 $
// $Copyright: Copyright (C) 2014-2015 Texas Instruments Incorporated -
//             http://www.ti.com/ ALL RIGHTS RESERVED $
//###########################################################################

#include "F2837S_files/F28x_Project.h"
#include "F2837xS_Pie_defines.h"

#include "inc/hw_can.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"

#include "Header/LP_CAN.h"
#include "Header/GUIC_Header.h"
#include "Header/State_machine.h"
#include "Header/ParameterMap.h"
#include "Header/UsefulPrototypes.h"

/** README FOR SETTING UP CAN MESSAGES
 *
 * HINT: functions/structs named here can be jumped to by clicking
 * the function/struct name and pressing F3
 * Example: click on -> Init_CAN_ARBIDs <- and press F3 to jump to this function definition
 *
 * 1. Add a message handler to the struct CANMessageHandle instantiation list directly
 * below this readme as
 * <MSGNAME>_RxMessageHandle for an incoming message, and <MSGNAME>_TxMsgHandle for an
 * outgoing message.
 * Example: Temps_TxMsgHandle is the handler for the outgoing temperatures message
 *
 * 2. In LP_CAN.h add a member to struct CANIDs as CANID_<MSG>_TX or CANID_<MSG>_TX
 * Example: uint32_t CANID_TEMPS_TX;
 * (click here -> CANIDs <- and press F3 to jump to the struct)
 *
 * 3. In -> Init_CAN_ARBIDs <- assign the value of the above CANIDs member to its
 * desired value + the device ID (variable BaseID)
 * Example: CANIDList.CANID_TEMPS_TX = BaseID + 0x19;
 *
 * 4. In -> Init_CAN_Messages <- call the BuildMessage function on the message handler
 * with its member of the CANIDList and the type of message
 * MSG_OBJ_TYPE_TX for an outgoing message, MSG_OBJ_TYPE_RX for an incoming message
 * Example: CAN_BuildMessage(&Temps_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_TEMPS_TX, 0);
 *
 * 5. Define a function to update the data bytes of the CAN message
 * Input argument should be a pointer to a message handle
 * Call CAN_TransmitMessage(pMsgHandle) at the end of the function
 *
 * Example:
 * void CAN_Transmit_Temps(struct CANMessageHandle * pMsgHandle)
 * {
 *    pMsgHandle->MsgData[0] = (unsigned char)adcGetSignalValue(adc_id_tdevice) + 40;
 *    SetU16Bits(pMsgHandle,   (uint16_t)    (adcGetSignalValue(adc_id_tstator) + 40)*100,1);
 *    pMsgHandle->MsgData[3] = (unsigned char)adcGetSignalValue(adc_id_tpdb)    + 40;
 *    pMsgHandle->MsgData[4] = (unsigned char)adcGetSignalValue(adc_id_tlogic)  + 40;
 *    pMsgHandle->MsgData[5] = (unsigned char)adcGetSignalValue(adc_id_tcpu)    + 40;
 *    CAN_TransmitMessage(pMsgHandle);
 * }
 *
 * 6. Call this function with the message handler passed in, in one of the heartbeat
 * functions that are called out in the main IDDK.c file.
 * The current heartbeat functions are:
 * -> CAN_Heartbeat_10Hz <-
 * -> CAN_Heartbeat_100Hz <-
 * -> CAN_Heartbeat_6Hz <-
 *
 * Example:
 * //Called in C tasks
 * void CAN_Heartbeat_6Hz(void)
 * {
 *    CAN_Transmit_Temps(&Temps_TxMsgHandle);
 * }
 *
 */

struct CANMessageHandle BMSControl_RxMsgHandle,
                        BMSStatus_RxMsgHandle,
                        GenCtrl_RxMsgHandle,
                        SGQB_RxMsgHandle,
                        UniversalStates_TxMsgHandle,
                        UniversalRPM_TxMsgHandle,
                        UniversalFault_TxMsgHandle,
                        UniversalWarnings_TxMsgHandle,
                        UniversalTemps_TxMsgHandle,
                        UniversalIVP_TxMsgHandle,
                        UniversalCmdFbk_TxMsgHandle,
                        GensetInfo_TxMsgHandle,
                        GensetTemps_TxMsgHandle,
                        QueryResponse_TxMgsHandle;

#define FixMeLater 1
#define DeleteMe 1

/***********************************************/
/*  Function prototypes for functions in this module */
/***********************************************/
void Init_CAN_Periph(void);
void Init_CAN_Messages(void);
void Init_CAN_ARBIDs(void);

void CAN_BuildMessage(struct CANMessageHandle * , tMsgObjType , uint32_t, uint32_t , uint32_t);
void CAN_TransmitMessage(struct CANMessageHandle * );

uint32_t GetU32Bits(struct CANMessageHandle * , unsigned char);
uint16_t GetU16Bits(struct CANMessageHandle *, unsigned char);
int16_t GetI16Bits(struct CANMessageHandle * , unsigned char );
unsigned char GetByte(struct CANMessageHandle *, unsigned char);

void SetU16Bits(struct CANMessageHandle *, uint16_t , unsigned char );
void SetU32Bits(struct CANMessageHandle *, uint32_t , unsigned char );
void SetI16Bits(struct CANMessageHandle *, int16_t , unsigned char );
void SetI32Bits(struct CANMessageHandle *, int32_t , unsigned char );

/*Message processing function prototypes*/
void ProcessBMSCtrlMsg(struct CANMessageHandle *);
void ProcessBMSStatusMsg(struct CANMessageHandle *);
void ProcessGenCtrlMsg(struct CANMessageHandle *);
void ProcessSGQBMsg(struct CANMessageHandle *);

void CAN_MsgCheck_BMSCtrl(void);
void CAN_MsgCheck_BMSStatus(void);
void CAN_MsgCheck_SGQB(void);
void CAN_MsgCheck_GenCtrl(void);

void CAN_MessageCheck_10Hz(void);
void CAN_MessageCheck_100Hz(void);
void CAN_RunBroadcast(void);

uint32_t CAN_getErrorCode();

/*Interrupt handling function prototypes*/
interrupt void CANIntHandler(void);
void CAN_ReceiveIntClear(struct CANMessageHandle * );
void CAN_TransmitIntClear(struct CANMessageHandle * );

/***********************************************/
/*  External functions used in this module - linkage to other modules */
/***********************************************/
extern float adcGetSignalValue(enum adc_signal_id);

extern enum ECUControlModes GetECUCtrlMode(void);
extern float ECUSCI_GetGeadTempC(void);

extern enum MCLowLevelStateVariables GetControllerState(void);
extern enum ECULowLevelStateVariables ECU_GetState(void);
extern enum GenControlStateVariables GenControl_GetState(void);
extern enum ElmoStateVariables GetElmo0State(void);
extern enum ElmoStateVariables GetElmo1State(void);
extern float elmoTorqueCommand(void );
extern float getGCAmpsCommand(void);
extern float ECU_GetThrottleSetting(void);
extern float ECU_GetEDFDuty(void);
extern float getSystemRPM(void);
extern uint32_t getmainADCWarnings(void);
extern uint16_t getmainTimeSinceReset(void);
extern uint32_t GetLastErrorCode(void);
extern enum CritFaultVariables GetLastCritFault(void);
extern uint32_t ECAP1_GetPulseus(void);
extern float GetParameterValue(int16_t );
extern void ProcessSetParameterCommand(unsigned char , float );

extern void dec(uint32_t *);

// *****************************************************************************
//  Module resources - variables here //
// *****************************************************************************

uint32_t ObjIdCtr=0;  //This assigns the Object ID (not same as message ID) to the CAN messages
uint32_t CANISRTicker=0;
uint32_t CANISRError=1000;

tMsgObjType MsgTypes[30]={MSG_OBJ_TYPE_RX_REMOTE}; //Stores the type of message for interrupt handling. Initializing with one we aren't using

struct CANMessageHandle * MsgHandles[30];   //array of pointers to CAN message handles
struct CANIDs CANIDList;

ProcMsg_BMSCtrl_t CAN_BMSCtrl = PROC_BMSCTRL_DEFAULTS;
ProcMsg_BMSStatus_t CAN_BMSStatus = PROC_BMSSTATUS_DEFAULTS;
ProcMsg_GenCtrl_t  CAN_GenCtrlMsg = PROC_GENCTRL_DEFAULTS;
ProcMsg_SGQB_t CAN_SGQBMsg = PROC_SGQB_DEFAULTS;
struct Broadcast_t m_BroadcastMessages[NUM_OF_BROADCAST_MSG];

//*****************************************************************************
// A flag to indicate that some transmission error occurred.
//*****************************************************************************
volatile uint32_t g_bErrFlag = 0;
static char lastBroadcastIndex;

//---------------------------------------------//
//---------------------------------------------//
// BEGIN CAN MESSAGE BUILD FUNCTIONS           //
//---------------------------------------------//
//---------------------------------------------//
void Init_CAN_Messages(void)
{
    lastBroadcastIndex = 0;
    ObjIdCtr=0;


    CAN_BuildMessage(&QueryResponse_TxMgsHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_SGQB_RESPONSE_TX, 0, 8);

    CAN_BuildMessage(&UniversalIVP_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_IVP_TX, 0, 8);

    CAN_BuildMessage(&UniversalTemps_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_TEMPS_TX, 0, 8);

    CAN_BuildMessage(&UniversalRPM_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_UNIV_RPM_TX, 0, 6);

    CAN_BuildMessage(&UniversalStates_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_UNIV_STATES_TX, 0, 7);

    CAN_BuildMessage(&UniversalFault_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_UNIV_FLT_TX, 0, 8);

    CAN_BuildMessage(&UniversalWarnings_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_UNIV_WARN_TX, 0, 8);

    CAN_BuildMessage(&UniversalCmdFbk_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_UNIV_CTL_FBK, 0, 8);

    if(Inverter_Params.Controller != MotorController)
    {
        //These messages apply to both generator controller and genset
        CAN_BuildMessage(&GensetInfo_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_GEN_INFO_TX, 0, 8);
        CAN_BuildMessage(&GensetTemps_TxMsgHandle, MSG_OBJ_TYPE_TX, CANIDList.CANID_GEN_TEMPS_TX, 0, 6);

        CAN_BuildMessage(&BMSControl_RxMsgHandle, MSG_OBJ_TYPE_RX, CANIDList.CANID_GEN_BMSCTRL_RX, 0, 8);
        BMSControl_RxMsgHandle.process = &ProcessBMSCtrlMsg;

        CAN_BuildMessage(&BMSStatus_RxMsgHandle, MSG_OBJ_TYPE_RX, CANIDList.CANID_GEN_BMSSTATUS_RX, 0, 8);
        BMSStatus_RxMsgHandle.process = &ProcessBMSStatusMsg;

        if(Inverter_Params.StartStopSel==StSel_CAN && Inverter_Params.ControlRefSelect == CRef_CAN)
        {
            CAN_BuildMessage(&GenCtrl_RxMsgHandle, MSG_OBJ_TYPE_RX, CANIDList.CANID_UNIV_CTRL_RX, 0, 8);
            GenCtrl_RxMsgHandle.process = &ProcessGenCtrlMsg;
        }
    }

    if(Inverter_Params.StartStopSel==StSel_CAN && Inverter_Params.ControlRefSelect == CRef_CAN)
    {
        CAN_BuildMessage(&GenCtrl_RxMsgHandle, MSG_OBJ_TYPE_RX, CANIDList.CANID_UNIV_CTRL_RX, 0, 8);
        GenCtrl_RxMsgHandle.process = &ProcessGenCtrlMsg;
    }

    uint8_t index = 0;
    for ( index = 0; index < NUM_OF_BROADCAST_MSG; index++ )
    {
        m_BroadcastMessages[index].rate = -1.;
        m_BroadcastMessages[index].timer = 0;
    }

    CAN_BuildMessage(&SGQB_RxMsgHandle, MSG_OBJ_TYPE_RX, CANIDList.CANID_SGQB_RX, 0, 8);
    SGQB_RxMsgHandle.process = &ProcessSGQBMsg;
}

void Init_CAN_ARBIDs(void)
{
    uint32_t BaseID = 0x00;
    BaseID = Inverter_Params.CAN_BaseID;

    if(Inverter_Params.Controller == GeneratorController)
    {
        CANIDList.CANID_GEN_BMSCTRL_RX      = BMS_BASE_ID + BMS_CTRL_RX_ID;
        CANIDList.CANID_GEN_BMSSTATUS_RX    = BMS_BASE_ID + BMS_STATUS_RX_ID;
    }


    CANIDList.CANID_UNIV_CTRL_RX         = BaseID + GEN_CTRL_RX_ID;
    CANIDList.CANID_SGQB_RX             = BaseID + SGQB_RX_ID;

    CANIDList.CANID_IVP_TX              = BaseID + IVP_TX_ID;
    CANIDList.CANID_TEMPS_TX            = BaseID + TEMPS_TX_ID;
    CANIDList.CANID_SGQB_RESPONSE_TX    = BaseID + SGQB_RESPONSE_TX_ID;

    CANIDList.CANID_UNIV_STATES_TX      = BaseID + UNIV_STATES_ID;
    CANIDList.CANID_UNIV_RPM_TX      = BaseID + UNIV_RPM_ID;
    CANIDList.CANID_UNIV_FLT_TX      = BaseID + UNIV_FLT_ID;
    CANIDList.CANID_UNIV_WARN_TX      = BaseID + UNIV_WARN_ID;
    CANIDList.CANID_UNIV_CTL_FBK     = BaseID + UNIV_CMD_FBK;
    CANIDList.CANID_GEN_INFO_TX      = BaseID + GENSET_INFO_ID;
    CANIDList.CANID_GEN_TEMPS_TX      = BaseID + GENSET_TEMPS_ID;
}

//Builds a CAN Message Handle which contains the CAN message structure, the data array, and the object ID for interrupt clearing
void CAN_BuildMessage(struct CANMessageHandle * pMsgHandle, tMsgObjType type, uint32_t MsgId, uint32_t MsgFlags, uint32_t DLC)
{
    int i;
    ObjIdCtr++;        //This will start it at 1

    pMsgHandle->DLC = DLC;
    MsgTypes[ObjIdCtr]=type;
    MsgHandles[ObjIdCtr]=pMsgHandle;
    pMsgHandle->ObjID = ObjIdCtr;
    pMsgHandle->process=0;

    for(i=0;i<8;i++)
    {
        pMsgHandle->MsgData[i]=0x00;
    }
    tCANMsgObject *pMsgObject = &(pMsgHandle->MsgObj);
    unsigned char *pMsgData = (pMsgHandle->MsgData);

    *(uint32_t *)pMsgData = 0;
    pMsgObject->ui32MsgID = MsgId;
    pMsgObject->ui32MsgIDMask = 0;
    pMsgObject->ui32MsgLen = DLC;
    pMsgObject->pucMsgData = pMsgData;

    if(type==MSG_OBJ_TYPE_TX)
    {
        pMsgObject->ui32Flags = MSG_OBJ_TX_INT_ENABLE | MsgFlags;
        CANMessageSet(Inverter_Params.CAN_REG_BASE, pMsgHandle->ObjID, pMsgObject, MSG_OBJ_TYPE_TX);
    }
    else if(type==MSG_OBJ_TYPE_RX)
    {
        pMsgObject->ui32Flags = MSG_OBJ_RX_INT_ENABLE | MsgFlags;
        CANMessageSet(Inverter_Params.CAN_REG_BASE, pMsgHandle->ObjID, pMsgObject, MSG_OBJ_TYPE_RX);
    }
    pMsgHandle->ErrorCounter = 10;
}
//---------------------------------------------//
//---------------------------------------------//
// END CAN MESSAGE BUILD FUNCTIONS             //
//---------------------------------------------//
//---------------------------------------------//

//---------------------------------------------//
//---------------------------------------------//
// BEGIN CAN SETUP/INT FUNCTIONS               //
//---------------------------------------------//
//---------------------------------------------//

//*****************************************************************************
// Configure the CAN and enter a loop to transmit periodic CAN messages.
//*****************************************************************************
void Init_CAN_Periph(void)
{
    // Initialize the CAN controller
    CANInit(Inverter_Params.CAN_REG_BASE);

    // Setup CAN to be clocked off the PLL output clock
    CANClkSourceSelect(Inverter_Params.CAN_REG_BASE, 0);   /* 500kHz CAN-Clock */

    // Set up the bit rate for the CAN bus.  This function sets up the CAN
    // bus timing for a nominal configuration.  You can achieve more control
    // over the CAN bus timing by using the function CANBitTimingSet() instead
    // of this one, if needed.
    // In this example, the CAN bus is set to 500 kHz.  In the function below,
    // the call to SysCtlClockGet() is used to determine the clock rate that
    // is used for clocking the CAN peripheral.  This can be replaced with a
    // fixed value if you know the value of the system clock, saving the extra
    // function call.  For some parts, the CAN peripheral is clocked by a fixed
    // 8 MHz regardless of the system clock in which case the call to
    // SysCtlClockGet() should be replaced with 8000000.  Consult the data
    // sheet for more information about CAN peripheral clocking.
    CANBitRateSet(Inverter_Params.CAN_REG_BASE, CPU.Sec, Inverter_Params.CAN_Bitrate);

    // Enable interrupts on the CAN peripheral.  This example uses static
    // allocation of interrupt handlers which means the name of the handler
    // is in the vector table of startup code.  If you want to use dynamic
    // allocation of the vector table, then you must also call CANIntRegister()
    // here.
    CANIntEnable(Inverter_Params.CAN_REG_BASE, CAN_INT_MASTER | CAN_INT_ERROR | CAN_INT_STATUS);

    // Enable test mode and select external loopback
//    HWREG(Inverter_Params.CAN_REG_BASE + CAN_O_CTL) |= CAN_CTL_TEST
    HWREG(Inverter_Params.CAN_REG_BASE + CAN_O_CTL) |= CAN_CTL_INIT;  //added 20160219
//    HWREG(Inverter_Params.CAN_REG_BASE + CAN_O_TEST) = CAN_TEST_EXL;

    // Enable the CAN for operation.
    CANEnable(Inverter_Params.CAN_REG_BASE);

    CANGlobalIntEnable(Inverter_Params.CAN_REG_BASE, CAN_GLB_INT_CANINT0);

    // Initialize arbitration IDs based on controller position/type
    Init_CAN_ARBIDs();
    // Initialize the message handles and message objects
    Init_CAN_Messages();

    EALLOW;  // This is needed to write to EALLOW protected registers
    if(Inverter_Params.CAN_REG_BASE == CANA_BASE)
    {
        PieVectTable.CANA0_INT = &CANIntHandler;
        PieCtrlRegs.PIEIER9.bit.INTx5  = 1;     // PIE Group 9, INT3 (SCI-B RX)
    }
    else if(Inverter_Params.CAN_REG_BASE == CANB_BASE)
    {
        PieVectTable.CANB0_INT = &CANIntHandler;
        PieCtrlRegs.PIEIER9.bit.INTx7  = 1;     // PIE Group 9, INT3 (SCI-B RX)
    }
    IER |= M_INT9; // Enable group 9 interrupts (UART monitor example).
    EDIS;   // This is needed to disable write to EALLOW protected registers
}


//*****************************************************************************
// This function is the interrupt handler for the CAN peripheral.  It checks
// for the cause of the interrupt, and maintains a count of all messages that
// have been transmitted.
//*****************************************************************************
interrupt void CANIntHandler(void)
{
    uint32_t ulStatus;

    CANISRTicker++; //This will increment on either a TX or RX event

    // Read the CAN interrupt status to find the cause of the interrupt
    ulStatus = CANIntStatus(Inverter_Params.CAN_REG_BASE, CAN_INT_STS_CAUSE);

    if(ulStatus == CAN_INT_INT0ID_STATUS)
    {
        // Read the controller status.  This will return a field of status
        // error bits that can indicate various errors.  Error processing
        // is not done in this example for simplicity.  Refer to the
        // API documentation for details about the error status bits.
        // The act of reading this status will clear the interrupt.  If the
        // CAN peripheral is not connected to a CAN bus with other CAN devices
        // present, then errors will occur and will be indicated in the
        // controller status.
        ulStatus = CANStatusGet(Inverter_Params.CAN_REG_BASE, CAN_STS_CONTROL);

        //Check to see if an error occurred.
        if(((ulStatus  & ~(CAN_ES_TXOK | CAN_ES_RXOK)) != 7) &&         /*  7 is what the status register is set to when there is no bus event since last read -- "nothing to see here" */
                ((ulStatus  & ~(CAN_ES_TXOK | CAN_ES_RXOK)) != 0))
        {
            // Set a flag to indicate some errors may have occurred.
            g_bErrFlag = 1;
        }
    }
    else if(ulStatus>0 && ulStatus<=ObjIdCtr)
    {
        if(MsgTypes[ulStatus] == MSG_OBJ_TYPE_RX)
        {
            CAN_ReceiveIntClear(MsgHandles[ulStatus]);
            MsgHandles[ulStatus]->process(MsgHandles[ulStatus]);
        }
        else if(MsgTypes[ulStatus]==MSG_OBJ_TYPE_TX)
        {
            CAN_TransmitIntClear(MsgHandles[ulStatus]);
        }
    }
    else
    {
        //spurious interrupt handling
    }

    CANGlobalIntClear(Inverter_Params.CAN_REG_BASE, CAN_GLB_INT_CANINT0);
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;
}

void CAN_ReceiveIntClear(struct CANMessageHandle * pMsgHandle)
{
    CANMessageGet(Inverter_Params.CAN_REG_BASE,pMsgHandle->ObjID,&(pMsgHandle->MsgObj),true);
    CANIntClear(Inverter_Params.CAN_REG_BASE,pMsgHandle->ObjID);
}

void CAN_TransmitIntClear(struct CANMessageHandle * pMsgHandle)
{
    CANIntClear(Inverter_Params.CAN_REG_BASE,pMsgHandle->ObjID);
}

void CAN_TransmitMessage(struct CANMessageHandle * pMsgHandle)
{
    CANMessageSet(Inverter_Params.CAN_REG_BASE, pMsgHandle->ObjID, &(pMsgHandle->MsgObj), MSG_OBJ_TYPE_TX);
}
//---------------------------------------------//
//---------------------------------------------//
// END CAN SETUP/INT FUNCTIONS                 //
//---------------------------------------------//
//---------------------------------------------//


//-------------------------------------//
//-------------------------------------//
// BEGIN INTERNAL STATUS UPDATE FUNCTIONS//
//-------------------------------------//
//-------------------------------------//
void CAN_Transmit_IVP(struct CANMessageHandle * pMsgHandle)
{
    SetI16Bits(pMsgHandle, (int16)(adcGetSignalValue(adc_id_idc)*10.0), 0);
    SetI16Bits(pMsgHandle, (int16)(adcGetSignalValue(adc_id_vdc)*10.0), 2);
    SetI32Bits(pMsgHandle, (int32) adcGetSignalValue(adc_id_pdc), 4);

    CAN_TransmitMessage(pMsgHandle);
}

void CAN_Transmit_UniversalTemps(struct CANMessageHandle * pMsgHandle)
{
    pMsgHandle->MsgData[0] = (unsigned char)adcGetSignalValue(adc_id_thsink) + 40;
    pMsgHandle->MsgData[1] = (unsigned char)adcGetSignalValue(adc_id_tpdb)    + 40;
    pMsgHandle->MsgData[2] = (unsigned char)adcGetSignalValue(adc_id_tlogic)  + 40;
    pMsgHandle->MsgData[3] = (unsigned char)adcGetSignalValue(adc_id_tcpu)    + 40;
    SetU16Bits(pMsgHandle,   (uint16_t)    (adcGetSignalValue(adc_id_tstator) + 40)*100,4);
    SetU16Bits(pMsgHandle,   (uint16_t)    adcGetSignalValue(adc_id_spare1)*10, 6);             //DEBUG - Estimated Cooling Power
    CAN_TransmitMessage(pMsgHandle);
}

void CAN_Transmit_UniversalState(struct CANMessageHandle * pMsgHandle)
{
    pMsgHandle->MsgData[0] = (char)MainState_Get();
    pMsgHandle->MsgData[1] = (char)GenControl_GetState();
    pMsgHandle->MsgData[2] = (char)0;   //MC high level state
    pMsgHandle->MsgData[3] = (char)0;   //MC low level state
    pMsgHandle->MsgData[4] = (char)ECU_GetState();
    pMsgHandle->MsgData[5] = (char)GetElmo0State();
    pMsgHandle->MsgData[6] = (char)GetElmo1State();
    CAN_TransmitMessage(pMsgHandle);
}
void CAN_TransmitUniversalRPM(struct CANMessageHandle * pMsgHandle)
{
    SetU32Bits(pMsgHandle, getSystemRPM(), 0);
    SetU16Bits(pMsgHandle, getmainTimeSinceReset(), 4);
    CAN_TransmitMessage(pMsgHandle);
}

void CAN_TransmitUniversalFaults(struct CANMessageHandle * pMsgHandle)
{
    SetU32Bits(pMsgHandle, GetLastCritFault(), 0);
    SetU32Bits(pMsgHandle, GetLastErrorCode(), 4);
    CAN_TransmitMessage(pMsgHandle);
}

void CAN_TransmitUniversalWarnings(struct CANMessageHandle * pMsgHandle)
{
    SetU32Bits(pMsgHandle, (uint32_t)getmainADCWarnings(), 0);
    SetU32Bits(pMsgHandle, (uint32_t)0, 4);   //Unused, "other warnings"
    CAN_TransmitMessage(pMsgHandle);
}

void CAN_TransmitUniversalCmdFbk(struct CANMessageHandle * pMsgHandle)
{
    //Todo - send out applicable control reference if user is not using CAN to command system
    pMsgHandle->MsgData[0] = CAN_GenCtrlMsg.opcode;
    pMsgHandle->MsgData[1] = CAN_GenCtrlMsg.parameter;
    SetU32Bits(pMsgHandle, convFloatToBytes(CAN_GenCtrlMsg.value), 4);
    pMsgHandle->MsgData[2] = CAN_GenCtrlMsg.valid;
    pMsgHandle->MsgData[3] = GenCtrl_RxMsgHandle.ErrorCounter;
    CAN_TransmitMessage(pMsgHandle);
}

#if(READTHROTTLE)
float throttlesetting=0.0;
#endif

void CAN_TransmitGenInfo(struct CANMessageHandle * pMsgHandle)
{

    SetU16Bits(pMsgHandle, (int16_t)(getGCAmpsCommand()*100.0),0);
#if(!READTHROTTLE)
    SetU16Bits(pMsgHandle, (uint16_t)(ECU_GetThrottleSetting()*10000.0),2);
#else
    throttlesetting = saturate(0,1,0.000833333*(float)(ECAP1_GetPulseus()) -0.75);

    SetU16Bits(pMsgHandle, (uint16_t)(throttlesetting*10000.0),2);
#endif
    SetU16Bits(pMsgHandle, (int16_t)(ECU_GetEDFDuty()*100.0),4);
    SetU16Bits(pMsgHandle, (uint16_t)0,6); //Unused for now, maybe 2nd cooling fan duty
    CAN_TransmitMessage(pMsgHandle);
}

void CAN_TransmitGenTemps(struct CANMessageHandle * pMsgHandle)
{
    pMsgHandle->MsgData[0] = (unsigned char)(ECUSCI_GetGeadTempC() + 40);
    pMsgHandle->MsgData[1]= 0;  //future CHT2/coolant2
    pMsgHandle->MsgData[2]= 0;  //future intake
    pMsgHandle->MsgData[3]= 0;  //future radiator 1
    pMsgHandle->MsgData[4]= 0; //future radiator 2
    pMsgHandle->MsgData[5]= 0; //future oil temp
    CAN_TransmitMessage(pMsgHandle);
}


void CAN_Transmit_QueriedParameter(unsigned char param)
{
    float valuefoat=0.0;
    uint32_t valueint=0;
    if((param == ID_SW_VERSION) || (param == ID_SERIAL_NUMBER))
    {
        valueint = GetRawParameterValue(param);
        SetU32Bits(&QueryResponse_TxMgsHandle, valueint, 4);
    }
    else
    {
        valuefoat = convFloatToBytes(GetParameterValue(param));
        SetU32Bits(&QueryResponse_TxMgsHandle, valuefoat, 4);
    }
    QueryResponse_TxMgsHandle.MsgData[0] =  CAN_SGQBMsg.opcode;  //repeat opcode that was queried
    QueryResponse_TxMgsHandle.MsgData[1] =  param;  //repeat opcode that was queried
    QueryResponse_TxMgsHandle.MsgData[2] =  0;  //repeat opcode that was queried
    QueryResponse_TxMgsHandle.MsgData[3] =  0;  //repeat opcode that was queried

    CAN_TransmitMessage(&QueryResponse_TxMgsHandle);
}

void CAN_Transmit_Broadcast(unsigned char sysval, char rate)
{
    uint32_t val = 0;
    val = GetRawBroadcastValue(sysval);
    QueryResponse_TxMgsHandle.MsgData[0] =   2;  //"broadcast" opcode
    QueryResponse_TxMgsHandle.MsgData[1] =   sysval;
    SetI16Bits(&QueryResponse_TxMgsHandle, rate*10, 2);
    SetU32Bits(&QueryResponse_TxMgsHandle, val, 4);
    CAN_TransmitMessage(&QueryResponse_TxMgsHandle);
}

//-------------------------------------//
//-------------------------------------//
// END INTERNAL STATUS UPDATE FUNCTIONS//
//-------------------------------------//
//-------------------------------------//

//-------------------------------------//
//-------------------------------------//
// BEGIN EXTERNAL STATUS UPDATE FUNCTIONS//
//-------------------------------------//
//-------------------------------------//

//Called in C tasks
void CAN_Heartbeat_1Hz(void)
{
    CAN_TransmitUniversalWarnings(&UniversalWarnings_TxMsgHandle);
    CAN_TransmitUniversalFaults(&UniversalFault_TxMsgHandle);

    if(Inverter_Params.Genset != GensetNone)
    {
        CAN_TransmitGenTemps(&GensetTemps_TxMsgHandle);
    }

    CAN_TransmitUniversalCmdFbk(&UniversalCmdFbk_TxMsgHandle);

}

//Called in B tasks
void CAN_Heartbeat_10Hz(void)
{
    CAN_TransmitUniversalRPM(&UniversalRPM_TxMsgHandle);
}

//Called in B tasks
void CAN_Heartbeat_100Hz(void)
{
    CAN_Transmit_IVP(&UniversalIVP_TxMsgHandle);
}

//Called in C tasks
void CAN_Heartbeat_6Hz(void)
{
    CAN_Transmit_UniversalState(&UniversalStates_TxMsgHandle);
    CAN_Transmit_UniversalTemps(&UniversalTemps_TxMsgHandle);
    if(Inverter_Params.Controller == GeneratorController)
    {
        CAN_TransmitGenInfo(&GensetInfo_TxMsgHandle);
    }
}

//Called at 1200Hz in B tasks
void CAN_RunBroadcast(void)
{
    uint16_t i = 0;
    for( i=0; i<NUM_OF_BROADCAST_MSG; i++ )
    {
        if( m_BroadcastMessages[i].rate > 0 )
        {
            if( m_BroadcastMessages[i].timer >= (Inverter_Params.B_TASK_HZ / m_BroadcastMessages[i].rate) )
            {
                CAN_Transmit_Broadcast(m_BroadcastMessages[i].index, m_BroadcastMessages[i].rate);
                m_BroadcastMessages[i].timer = 0;
            }
            else
            {
                m_BroadcastMessages[i].timer++;
            }
        }
    }
}

//-------------------------------------//
//-------------------------------------//
// END EXTERNAL STATUS UPDATE FUNCTIONS//
//-------------------------------------//
//-------------------------------------//

//---------------------------------------------//
//---------------------------------------------//
// BEGIN CAN DATA MANIP FUNCTIONS              //
//---------------------------------------------//
//---------------------------------------------//
void SetU16Bits(struct CANMessageHandle * pMsgHandle, uint16_t val, unsigned char start)
{
    pMsgHandle->MsgData[start]= val & 0xff;
    pMsgHandle->MsgData[start+1]= (val>>8)&0xff;
}

void SetI16Bits(struct CANMessageHandle * pMsgHandle, int16_t val, unsigned char start)
{
    pMsgHandle->MsgData[start]= val & 0xff;
    pMsgHandle->MsgData[start+1]= (val>>8)&0xff;
}

void SetU32Bits(struct CANMessageHandle * pMsgHandle, uint32_t val, unsigned char start)
{
    pMsgHandle->MsgData[start]= val & 0xff;
    pMsgHandle->MsgData[start+1]= (val>>8)&0xff;
    pMsgHandle->MsgData[start+2]= (val>>16)&0xff;
    pMsgHandle->MsgData[start+3]= (val>>24)&0xff;
}

void SetI32Bits(struct CANMessageHandle * pMsgHandle, int32_t val, unsigned char start)
{
    pMsgHandle->MsgData[start]= val & 0xff;
    pMsgHandle->MsgData[start+1]= (val>>8)&0xff;
    pMsgHandle->MsgData[start+2]= (val>>16)&0xff;
    pMsgHandle->MsgData[start+3]= (val>>24)&0xff;
}


uint32_t GetU32Bits(struct CANMessageHandle * pMsgHandle, unsigned char ind)
{
    return (uint32_t) (   ((uint32_t)(pMsgHandle->MsgData[ind+3]) << 24) & 0xFF000000 |
                        ((uint32_t)(pMsgHandle->MsgData[ind+2]) << 16) & 0x00FF0000 |
                        ((uint32_t)(pMsgHandle->MsgData[ind+1]) << 8)  & 0x0000FF00 |
                        ((uint32_t)(pMsgHandle->MsgData[ind])   << 0)  & 0x000000FF );
}

uint32_t GetI32Bits(struct CANMessageHandle * pMsgHandle, unsigned char ind)
{
    return (int32_t) (   ((int32_t)(pMsgHandle->MsgData[ind+3]) << 24) & 0xFF000000 |
                        ((int32_t)(pMsgHandle->MsgData[ind+2]) << 16) & 0x00FF0000 |
                        ((int32_t)(pMsgHandle->MsgData[ind+1]) << 8)  & 0x0000FF00 |
                        ((int32_t)(pMsgHandle->MsgData[ind])   << 0)  & 0x000000FF );
}

uint16_t GetU16Bits(struct CANMessageHandle * pMsgHandle, unsigned char ind)
{
    return (uint16_t)((uint16_t) (pMsgHandle->MsgData[ind+1] << 8)  | (uint16_t) (pMsgHandle->MsgData[ind])  );
}

int GetI16Bits(struct CANMessageHandle * pMsgHandle, unsigned char ind)
{
    return (int16_t)((pMsgHandle->MsgData[ind+1] << 8)  | (pMsgHandle->MsgData[ind])  );
}

unsigned char GetByte(struct CANMessageHandle * pMsgHandle, unsigned char ind)
{
    return pMsgHandle->MsgData[ind];
}

//---------------------------------------------//
//---------------------------------------------//
// END   CAN DATA MANIP FUNCTIONS              //
//---------------------------------------------//
//---------------------------------------------//



//---------------------------------------------//
//---------------------------------------------//
// BEGIN CAN MSG PROCESSING FCNS               //
//---------------------------------------------//
//---------------------------------------------//
//Organizes BMS control message into its component struct
void ProcessBMSCtrlMsg(struct CANMessageHandle * pMsgHandle)
{
    CAN_BMSCtrl.PackVoltage = (float)(GetU16Bits(pMsgHandle, 0)/10.0);
    CAN_BMSCtrl.PackCurrent = (float)(GetI16Bits(pMsgHandle, 2)/10.0);
    CAN_BMSCtrl.DesiredCurrent = (float)(GetI16Bits(pMsgHandle, 4)/10.0);
    CAN_BMSCtrl.Fresh = true;
}

//Organizes BMS status message into its component struct - may not need at all for Nokia
void ProcessBMSStatusMsg(struct CANMessageHandle * pMsgHandle)
{
    CAN_BMSStatus.Monitors = (GetU32Bits(pMsgHandle,0) & 0x3FFFFFFF);    //Top 2 bits are STOP faults.
    if( (GetByte(pMsgHandle,3) & 0x20) == 0x00 )                    //5th bit of 3rd LSByte
    {
         CAN_BMSStatus.Relay_Status = RELAY_CLOSED;
    }
    else
    {
        CAN_BMSStatus.Relay_Status = RELAY_OPEN;
    }
    //Currently, relay is unused

    CAN_BMSStatus.RelayOpenTimer = GetU16Bits(pMsgHandle, 5);

    CAN_BMSStatus.Fresh = true;
}

void ProcessGenCtrlMsg(struct CANMessageHandle * pMsgHandle)
{
    CAN_GenCtrlMsg.opcode      =   pMsgHandle->MsgData[0];
    CAN_GenCtrlMsg.parameter   =   pMsgHandle->MsgData[1];
    CAN_GenCtrlMsg.value       =   convBytesToFloat(GetU32Bits(pMsgHandle,4));
    if( (CAN_GenCtrlMsg.parameter > 5) && (CAN_GenCtrlMsg.parameter != System_Reset) )
    {
        CAN_GenCtrlMsg.valid=false;
    }
    else
    {
        CAN_GenCtrlMsg.valid=true;
    }

    CAN_GenCtrlMsg.fresh       = true;
}

void ProcessSGQBMsg(struct CANMessageHandle * pMsgHandle)
{
    CAN_SGQBMsg.opcode      =   (enum CANOpCodes)pMsgHandle->MsgData[0];
    CAN_SGQBMsg.parameter   =   pMsgHandle->MsgData[1];
    CAN_SGQBMsg.value       =   convBytesToFloat(GetU32Bits(pMsgHandle,4));
    int16_t temp            =   GetI16Bits(pMsgHandle, 2);
    CAN_SGQBMsg.rate        =   temp/10; //Only applicable for opcode 2
    if(temp < 0 && temp >= -9)
    {
        CAN_SGQBMsg.rate    =   -1;
    }
    CAN_SGQBMsg.valid       =   true; //always true for now
    CAN_SGQBMsg.fresh       =   true;
}

void CAN_MsgCheck_BMSCtrl(void)
{
    if(CAN_BMSCtrl.Fresh==true)
    {
        BMSControl_RxMsgHandle.ErrorCounter=10;
        CAN_BMSCtrl.Fresh=false;
    }
    else
    {
        //Decrement error counter on invalid message
        dec(&(BMSControl_RxMsgHandle.ErrorCounter));
    }
}

void CAN_MsgCheck_BMSStatus(void)
{
    if(CAN_BMSStatus.Fresh==true)
    {
        BMSStatus_RxMsgHandle.ErrorCounter=10;
        CAN_BMSStatus.Fresh = false;
    }
    else
    {
        //Decrement error counter on invalid message
        dec(&(BMSStatus_RxMsgHandle.ErrorCounter));
    }
}

void CAN_MsgCheck_GenCtrl(void)
{
    if(CAN_GenCtrlMsg.fresh==true && CAN_GenCtrlMsg.valid==true)
    {
        if((enum GenControlStateVariables)CAN_GenCtrlMsg.parameter == System_Reset)
        {
            // go into infinite loop and watchdog will reset us
            for(;;){}
        }
        else if(CAN_GenCtrlMsg.mode != (enum GenControlStateVariables)CAN_GenCtrlMsg.parameter)
        {
            CAN_GenCtrlMsg.mode = (enum GenControlStateVariables)CAN_GenCtrlMsg.parameter;
            CAN_Transmit_UniversalState(&UniversalStates_TxMsgHandle);    //Asynchronously send if a state change appeared
        }
        CAN_GenCtrlMsg.ref = CAN_GenCtrlMsg.value;
        CAN_GenCtrlMsg.fresh = false;
        GenCtrl_RxMsgHandle.ErrorCounter = 10;
    }
    else
    {
        //Decrement error counter on invalid message
        dec(&(GenCtrl_RxMsgHandle.ErrorCounter));
    }
}

void CAN_MsgCheck_SGQB(void)
{
    int i = 0;
    if(CAN_SGQBMsg.fresh == 1)
    {
        CAN_SGQBMsg.fresh = 0;
        switch(CAN_SGQBMsg.opcode)
         {
             case opCANParamSet:
                 ProcessSetParameterCommand(CAN_SGQBMsg.parameter, CAN_SGQBMsg.value);
                 CAN_Transmit_QueriedParameter(CAN_SGQBMsg.parameter);
                 break;
             case opCANParamGet:
                 CAN_Transmit_QueriedParameter(CAN_SGQBMsg.parameter);
                 break;
             case opCANSysValBroadcast:
             {
                 int16_t index = CAN_SGQBMsg.parameter;
                 bool indexGreaterThan0 = (index >= 1);
                 bool indexLessThanOpcodes = (index < Number_Of_Broadcast_Opcodes);
                 if( indexGreaterThan0 && indexLessThanOpcodes )
                 {
                     if(CAN_SGQBMsg.rate == 0)
                     {
                         CAN_Transmit_Broadcast(CAN_SGQBMsg.parameter, CAN_SGQBMsg.rate);   //one-time query
                     }
                     else if(CAN_SGQBMsg.rate <= -1)
                     {
                         for(i = 0; i < NUM_OF_BROADCAST_MSG; i++)
                         {
                             if((uint32_t)CAN_SGQBMsg.parameter == m_BroadcastMessages[i].index)
                             {
                                 m_BroadcastMessages[i].rate = -1.;
                                 m_BroadcastMessages[i].timer = 0;
                             }
                         }
                     }
                     else
                     {
                         bool instertFail = true;
                         for(i = 0; i < NUM_OF_BROADCAST_MSG; i++)
                         {
                             uint32_t tempIndex = (uint32_t)CAN_SGQBMsg.parameter;
                             if(m_BroadcastMessages[i].index == tempIndex)
                             {
                                 instertFail = false;
                                 lastBroadcastIndex = i;
                                 if(CAN_SGQBMsg.rate >= MAX_BROADCAST_RATE)
                                 {
                                     m_BroadcastMessages[i].rate = MAX_BROADCAST_RATE;
                                 }
                                 else
                                 {
                                     m_BroadcastMessages[i].rate = CAN_SGQBMsg.rate;
                                 }
                                 break;
                             }
                         }
                         if(instertFail)
                         {

                              m_BroadcastMessages[lastBroadcastIndex].index = (uint32_t)CAN_SGQBMsg.parameter;
                              if(CAN_SGQBMsg.rate >= MAX_BROADCAST_RATE)
                              {
                                  m_BroadcastMessages[lastBroadcastIndex].rate = MAX_BROADCAST_RATE;
                              }
                              else
                              {
                                  m_BroadcastMessages[lastBroadcastIndex].rate = CAN_SGQBMsg.rate;
                              }
                              lastBroadcastIndex++;
                              if(lastBroadcastIndex >= 16)
                              {
                                  lastBroadcastIndex = 0;
                              }
                         }
                     }
                 }
                 break;
             }
             default:
                 break;
         }
    }

}

//Called w/a 10Hz prescaler in B tasks
void CAN_MessageCheck_10Hz(void)
{
    if(Inverter_Params.Controller == GeneratorController || Inverter_Params.Genset != GensetNone)
    {
        if(Inverter_Params.StartStopSel == StSel_CAN)
        {
            CAN_MsgCheck_GenCtrl();
        }
        CAN_MsgCheck_BMSStatus();
    }
    CAN_MsgCheck_GenCtrl();
    CAN_MsgCheck_SGQB();
}

//called with a 100Hz prescaler in B tasks
void CAN_MessageCheck_100Hz()
{
    if(Inverter_Params.Controller == GeneratorController || Inverter_Params.Genset != GensetNone)
    {
        CAN_MsgCheck_BMSCtrl();
    }
}

uint32_t CAN_getErrorCode()
{
    static uint32_t LastISRTick;
    uint32_t CANErrorCode = 0;
    int id=1;

    //Decrement the ISR error "timer" if the last ISRTick is the same as this one
    if(LastISRTick==CANISRTicker) {
        dec(&CANISRError);
    }
    else {
        CANISRError=Inverter_Params.A_TASK_HZ;    //Runs in A tasks - 1 sec with no CANISR and we ded. This resets the timer
    }

    LastISRTick=CANISRTicker;

    //Increment through the Message Handles array, and flip a bit for each message whose error counter has counted down to 0
    for(id=1;id<=ObjIdCtr;id++)
    {
        if(MsgHandles[id]->ErrorCounter==0)
        {
            CANErrorCode |= 1L << id;
        }
    }
    //An ISR error will take bit position 0
    if(CANISRError==0)
    {
        CANErrorCode |= 1L << 0;
    }

    return CANErrorCode;
}


unsigned char CAN_GetISRError(void) {return CANISRError;}

/*Get methods for processed CAN received data*/
float CAN_GetBMSReq() {return CAN_BMSCtrl.DesiredCurrent;}
float CAN_GetBMSFdb() {return CAN_BMSCtrl.PackCurrent;}
float CAN_GetBMSVolts() {return CAN_BMSCtrl.PackVoltage;}
enum RelayStatus CAN_GetBMSRelayStatus() {return CAN_BMSStatus.Relay_Status;}
float CAN_GetGenControlRef(void) {return CAN_GenCtrlMsg.ref;}
enum GenControlStateVariables CAN_GetGenCtlrModeCmd(void) {return CAN_GenCtrlMsg.mode;}
bool CAN_GetFCCommStatus(void)  {return (GenCtrl_RxMsgHandle.ErrorCounter>0);}
bool CAN_GetBMSCommStatus(void) {return (BMSControl_RxMsgHandle.ErrorCounter>0);}
