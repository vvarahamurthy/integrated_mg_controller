/*
 * Module_SCI_ECU.c
 *
 *  Created on: Sep 26, 2018
 *      Author: vvarahamurthy
 */


#define FixMeLater 0
#define DeleteMe 0

#include "F2837S_files/F28x_Project.h"
#include "F2837xS_Pie_defines.h"
#include "Header/GUIC_Header.h"
#include "Header/UART_Comm.h"
#include "Header/RingBuffer.h"
#include "Header/State_Machine.h"
#include "Header/Globals.h"
#include "Header/ECU.h"
#include "Header/UsefulPrototypes.h"

#define ECU_SCI_REGS ScibRegs
//Local function prototypes
void Init_ECU_SCI(void);
__interrupt void ECURXIsr(void);
__interrupt void ECUTXIsr(void);
void ECU_PingCHT(void);
void SCI_ECU_ProcessMessage(void);
bool Fault_Detect_ECUSCI(void);

extern enum MainStateVariables MainState_Get(void);
extern void dec(uint32_t *);


RINGBUFFER ECU_RxRingBuf;
RINGBUFFER ECU_TxRingBuf;
uint32_t ECUTxIsrCtr=0;
uint32_t ECURxIsrCtr=0;
float CHT=0.0;
float CHT_C = 0.0;
uint16_t BadMsgs=0;
uint16_t SWResetCtr=0;

enum ECU_ParseStates {PS_Wait, PS_Size, PS_Flag, PS_Vals, PS_CRC, PS_Fail} ECU_PState = PS_Wait;

extern int getCount(RINGBUFFER *);

/**Function: Init_ECU_SCI
 * ------------------
 * Initializes the SCI registers to talk to the HFE ECM
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void Init_ECU_SCI(void)
{
    // Note: Clocks were turned on to the SCIA peripheral
    // in the InitSysCtrl() function

    /* SCICCR: 1 stop bit, parity disabled, no loopback,
    *  async mode, idle-line protocol, 8-bits per char (same as 0x0007)
    */


    //Set up the SCI interrupts, point them to the ISR functions

    // Interrupts that are used in this example are re-mapped to
    // ISR functions found within this file.
    EALLOW;  // This is needed to write to EALLOW protected registers
    PieVectTable.SCIB_RX_INT = &ECURXIsr;
    PieCtrlRegs.PIEIER9.bit.INTx3  = 1;     // PIE Group 9, INT3 (SCI-B RX)
    PieVectTable.SCIB_TX_INT = &ECUTXIsr;
    PieCtrlRegs.PIEIER9.bit.INTx4 = 1;      // PIE Group 9, INT4 (SCI-B TX)
    IER |= M_INT9; // Enable group 9 interrupts (UART monitor example).
    EDIS;   // This is needed to disable write to EALLOW protected registers

    ECU_SCI_REGS.SCICCR.bit.ADDRIDLE_MODE = 0;       // Idle line mode - RS232 compatible
    ECU_SCI_REGS.SCICCR.bit.LOOPBKENA = 0;          // Loopback disabled
    ECU_SCI_REGS.SCICCR.bit.PARITY =    0;          // Even parity
    ECU_SCI_REGS.SCICCR.bit.SCICHAR = 7;            // 8 data bits
    ECU_SCI_REGS.SCICCR.bit.PARITYENA = 0;          // Parity disabled
    ECU_SCI_REGS.SCICCR.bit.STOPBITS = 0;           // 1 stop bit

    ECU_SCI_REGS.SCICTL1.bit.RXENA = 1;  // Enable RX
    ECU_SCI_REGS.SCICTL1.bit.TXENA = 1;  // Enable TX

    ECU_SCI_REGS.SCIPRI.bit.FREESOFT = 1; // Disable RX ERR, SLEEP, TXWAKE

    ECU_SCI_REGS.SCICTL2.bit.TXINTENA = 0;          // Single char TX interrupt not enabled
    ECU_SCI_REGS.SCICTL2.bit.RXBKINTENA = 1;        // Single char RX interrupt enabled (but over-ridden by RXFFIENA if FIFO enabled)

    ECU_SCI_REGS.SCIHBAUD.all = (((uint32_t)CPU.Sec/4L)/(SCI_ECU_BAUD_RATE * 8L) - 1)/256;
    ECU_SCI_REGS.SCILBAUD.all = (((uint32_t)CPU.Sec/4L)/(SCI_ECU_BAUD_RATE * 8L) - 1)%256;


    ECU_SCI_REGS.SCIFFTX.bit.SCIFFENA = 1;                  // Enable FIFO mode for the SCI

    ECU_SCI_REGS.SCIFFTX.bit.TXFIFORESET=0;
    ECU_SCI_REGS.SCIFFRX.bit.RXFIFORESET=0;                 // Clear TX and RX FIFO status bits (count bits) and hold reset

    ECU_SCI_REGS.SCIFFTX.bit.TXFFIL = TX_FIFO_LEVEL;        //.all Originally 0xC020 -- level TX FIFO interrupts at
    ECU_SCI_REGS.SCIFFTX.bit.TXFFINTCLR=1;                  // Clear any pending interrupts - write 1 to clear
    ECU_SCI_REGS.SCIFFTX.bit.TXFFIENA = 0;                  // Disable TX FIFO interrupt if com is in FIFO mode, code will enable when FIFO is loaded


    ECU_SCI_REGS.SCIFFRX.bit.RXFFIL = RX_FIFO_LEVEL;        //.all Originally 0x0021 - -level RX FIFO interrupts at
    ECU_SCI_REGS.SCIFFRX.bit.RXFFOVRCLR=1;                  // Clear Overflow flag
    ECU_SCI_REGS.SCIFFRX.bit.RXFFINTCLR=1;                  // Clear Interrupt flag - write 1 to clear
    ECU_SCI_REGS.SCIFFRX.bit.RXFFIENA = 1;                  // Enable receive FIFO interrupt

    ECU_SCI_REGS.SCIFFTX.bit.SCIRST = 1;                    // Remove reset from the SCI and SCI FIFO
    ECU_SCI_REGS.SCIFFCT.all=0x00;                          // No autobaud, no delay between transfers

    ECU_SCI_REGS.SCIFFTX.bit.TXFIFORESET=1;
    ECU_SCI_REGS.SCIFFRX.bit.RXFIFORESET=1;         // Allow RX, RX FIFO status bit (counters) to operate

    ECU_SCI_REGS.SCICTL1.all =0x0023;//0x0023;     // Relinquish SCI from Reset

    rb_init(&ECU_RxRingBuf, ECU_BUFFER_SIZE);
    rb_init(&ECU_TxRingBuf, ECU_BUFFER_SIZE);
}

/**Function: ECURXIsr
 * ------------------
 * Receive ISR for the serial comms with HFE ECM
 * Populates a ring buffer that is processed in SCI_ECU_ProcessMessage function
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
__interrupt void ECURXIsr(void)
{
    ECURxIsrCtr++;
    // Need to check for RX error (framing/overflow) here since that can cause interrupt as well
    // Need to check RX flags and see if it is RXRDY/RXFIFO or and error flag that caused interrupt
    // And also need to check that interrupt on error flag is enabled -- or find another way to poll
    // to deal with RX errors. --MR

    // while FIFO has characters copy them to the ring buffer, increment g_newChar
    while(ECU_SCI_REGS.SCIFFRX.bit.RXFFST > 0)
    {
        rb_push(&ECU_RxRingBuf,ECU_SCI_REGS.SCIRXBUF.all);
    }

    ECU_SCI_REGS.SCIFFRX.bit.RXFFOVRCLR=1;   // Clear Overflow flag
    ECU_SCI_REGS.SCIFFRX.bit.RXFFINTCLR=1;   // Clear Interrupt flag

    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;       // Issue PIE ack
    EINT;
}

/**Function: ECUTXIsr
 * ------------------
 * Transmit ISR for the serial comms with HFE ECM
 * This doesn't really do anything unless in FIFO mode
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
__interrupt void ECUTXIsr(void)
{
    EINT;
    ECU_SCI_REGS.SCIFFTX.bit.TXFFIENA = 0;
#if 0
// while ring buffer has characters and FIFO has space copy them to TX FIFO and increment TX counter
    while( (ECU_SCI_REGS.SCIFFTX.bit.TXFFST < 16) && (getCount(&ECU_TxRingBuf)>0) )
    {
        ECU_SCI_REGS.SCITXBUF.all=rb_pop(&ECU_TxRingBuf);
    }

    if(ECU_SCI_REGS.SCIFFTX.bit.TXFFST == 0)
    {
        ECU_SCI_REGS.SCIFFTX.bit.TXFFIENA = 0;                  // Disable TX FIFO interrupt if com is in FIFO mode, code will enable when FIFO is loaded
    }

    ECU_SCI_REGS.SCITXBUF.all = 0x61;
#endif
    ECUTxIsrCtr++;
    ECU_SCI_REGS.SCIFFTX.bit.TXFFINTCLR = 1;         // Clear interrupt flag
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;
}

/**Function: ECU_PingCHT
 * ------------------
 * Directly set the message sequence for CHT over serial to the ECM
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void ECU_PingCHT(void)
{
    //Head Temperature Request
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x00); //Size - 2 bytes
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x07);

    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x72); //"r"

    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x00); //CANID = 0

    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x07); //Table 7

    //This requests engine head temperature
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x00);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x16); //Offset - 22 bytes

    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x00);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x02); //Size - 2 bytes

    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0xA2);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x1E);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x7C);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x82); //CRC32
#if 0
    //This requests the "time on"
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x00);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x00); //Offset - 2 bytes

    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x00);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x02); //Length of data requested

    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0xBA);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0xB5);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0xA3);
    ECU_SCI_REGS.SCITXBUF.all = (uint16_t)(0x40); //CRC32
#endif

DINT;
    if(ECU_SCI_REGS.SCICTL1.bit.SWRESET==0)
    {
        ECU_SCI_REGS.SCICTL1.bit.SWRESET=1;
    }
    if( (ECU_SCI_REGS.SCIRXST.bit.RXERROR==1) && (ECU_SCI_REGS.SCICTL1.bit.SWRESET==1) )
    {
        SWResetCtr++;
        ECU_SCI_REGS.SCICTL1.bit.SWRESET=0;
    }
EINT;
}


/**Function: SCI_ECU_ProcessMessage
 * ------------------
 * Processes the receive ring buffer and parses out the CHT
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void SCI_ECU_ProcessMessage(void)
{
    RINGBUFFER *rb = &ECU_RxRingBuf;
    static int ind;
    static uint16_t cbyte;

    if(rb_count(rb)>0)
    {
        //Writing an unsophisticated, hard-coded, single-size parser
        //Assume we'll never be requesting more than 0xFF bytes of data, so the first byte will need to be a 0x00
        switch(ECU_PState)
        {
            case PS_Wait:
            {
                cbyte=0x0000;
                ind=0;
                if(rb_pop(rb)!=0x00)   //"Size of message" should always be 3 - 1 flag byte and 2 data bytes
                {
                    ECU_PState=PS_Wait;
                }
                else
                {
                    ECU_PState=PS_Size;
                }
                break;
            }
            case PS_Size:
            {
                if(rb_pop(rb)!=0x03)
                {
                    ECU_PState=PS_Wait;
                }
                else
                {
                    ECU_PState=PS_Flag;
                }
                break;
            }
            case PS_Flag:
            {
                if(rb_pop(rb)!=0x00) //Flag of 0x00 means OK
                {
                    ECU_PState=PS_Fail;
                }
                else
                {
                    ECU_PState=PS_Vals;
                }
                break;
            }
            case PS_Vals:
            {
                if(ind==0)
                {
                    cbyte = 256*rb_pop(rb);
                }
                else if(ind==1)
                {
                    cbyte += rb_pop(rb);
                }
                else
                {
                    CHT = (float)cbyte/10.0;
                    ECU_PState=PS_CRC;
                }
                ind++;
                break;
            }
            case PS_CRC:
            {
                ECU_PState=PS_Wait;
                //Ignoring CRC for now - it'll just pop them out until the next 0x0003
                break;
            }
            case PS_Fail:
            {
                BadMsgs++;
                ECU_PState=PS_Wait;
                break;
            }
        }
        return;
    }
    else
    {
        return;
    }
}


/**Function: ECUSCI_GetGeadTempC
 * ------------------
 * Converts the CHT from F to C and returns its value
 *
 * @returns CHT_C - cylinder head temperature in celsius
 *
 * Inputs:
 * None
 */
float ECUSCI_GetGeadTempC(void)
{
#if(!FAKE_ECU)
    CHT_C =(CHT - 32.0)*(5.0/9.0);
#endif
    return CHT_C;
}


//Todo - actually call this and OR it into the fault bits!
/**Function: Fault_Detect_ECUSCI
 * ------------------
 * Checks the RX ISR to see if it is properly firing
 *
 * @returns Flt - Critical serial fault bit flagged in enumeration
 *
 * Inputs:
 * None
 */
bool Fault_Detect_ECUSCI()
{
    //ECU is pinged at 2Hz - 1 second without any RX interrupts will trip CommTimeout
    //2 seconds with CommTimeout fault tripped will cause ECU SCI peripheral reset
    static uint32_t LastISRTick;
    static uint32_t ECUSCI_ISRError;

    if(LastISRTick==ECURxIsrCtr)
    {
        //Decrement ECU SCI ISR Error ticker
        dec(&ECUSCI_ISRError);
    }
    else
    {
        ECUSCI_ISRError= 50;    //Run in B tasks, 10Hz loop
    }

    LastISRTick=ECURxIsrCtr;

    if( ECUSCI_ISRError==0 )
    {
        return true;
    }

    return false;
}

bool gDebugCHT=true;

/**Function: checkCHTLims
 * ------------------
  * Checks head temp
 *
 * @returns true if the CHT is within acceptable range
 *
*/
bool checkCHTLims(void)
{
    static uint32_t falsecount_ecu;
    bool result = false;

    if(inRange(CHT_C, (float)ECU_Params.LIM_CHT_HI , (float)ECU_Params.LIM_CHT_LO))
    {
        falsecount_ecu = 0;
        result = true;
    }
    else
    {
        //2 seconds of overtemp/undertemp
        falsecount_ecu++;
        if(falsecount_ecu >= 2000L*Motor_Params.LOOP_FREQ_KHZ)
        {
            result = false;
        }
        else
        {
            result = true;
        }
    }
    gDebugCHT = result;
    return result;
}

//0007 7200 07 - MESSAGE BASE - 7 bytes, "r", CANID, Table #
//crc does not include 0007

//Message for rpm request:
//0007 7200 0700 0600 02BE 38DF F2

//Message for temperature request:
//0007 7200 0700 1600 02A2 1E7C 82

//Message for time on request - used for debugging;
//0007 7200 0700 0000 02BA B5A3 40
