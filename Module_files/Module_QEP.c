/* ============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	PQEP.C

Target:			Cryo controller board

Author:			Mike Ricci

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle encoder inputs and QEP encoder hardware module
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "Header/GUIC_Header.h"
#include "F2837S_files/F2837x_QEP_Module.h"

#include "F2837xS_eqep.h"
#include <math.h>


//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1


// Function prototypes
void Init_QEP(void);
void Run_QEP(void);

float getQEPMechAngle();
float getQEPElecAngle();

Uint16 getDirectionQEP();
Uint16 getQEPIndexSyncFlag();

void Reset_QEP(void);

//  Module resources - variables here //



// Used to indirectly access eQEP module
extern volatile struct EQEP_REGS *eQEP[] =
 				  {
 				  	&EQep1Regs,
					&EQep2Regs,
					&EQep3Regs,
				  };


// Instance a QEP interface driver
QEP qep3 = QEP_DEFAULTS;



// Function Definitions            //
float getQEPMechAngle(){
    return 1.0 - qep3.MechTheta;
}

float getQEPElecAngle(){
    return (float)qep3.ElecTheta;
}

Uint16 getDirectionQEP()
{
    return qep3.DirectionQep;
}

Uint16 getQEPIndexSyncFlag()
{
    return qep3.IndexSyncFlag;
}

void Reset_QEP(void)
{
    qep3.IndexSyncFlag = 0;
}
void Init_QEP(void)
{

	// ****************************************************************************
	// ****************************************************************************
	// Initialize QEP module
	// ****************************************************************************
	// ****************************************************************************


	// ****************************************************************************
	// ****************************************************************************
	// Parameter Initialisation
	// ****************************************************************************
	// ****************************************************************************

    // Init QEP parameters
    qep3.LineEncoder = 360; // these are the number of slots in the QEP encoder
    qep3.MechScaler  = 0.25/qep3.LineEncoder;
    qep3.PolePairs   = Motor_Params.Poles/2;
    qep3.CalibratedAngle = 0;

    // QEP_INIT_MACRO(QEP NUMBER, qep3), QEP NUMBER = n -1
    QEP_INIT_MACRO(2,qep3)
    EQep3Regs.QEPCTL.bit.IEI = 0;        // disable POSCNT=POSINIT @ Index


}

void Run_QEP(void)
{
	QEP_MACRO(2,qep3);
}

