/*
============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	HiBWFaults.C

Target:			Cryo controller board

Author:			Mike Ricci

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle faults detected within the hi bandwidth i.e. full interrupt rate processing
********************************************************************************* */

#define FixMeLater 1
#define DeleteMe 1

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "IQMathLib.h"

#include "Header/GUIC_Header.h"
#include "Header/State_machine.h"
#include "Header/ECU.h"
#include "Header/Globals.h"

//  Function prototypes
enum CritFaultVariables HiBWProtection(void);
enum LoopFaultVariables getLoopCriticalFaults(void);
/***********************************************/
/*  External functions used in this module - linkage to other modules */
/***********************************************/
extern enum GPIOFaultVariables getGPIOCriticalFaults(void);
extern enum AnalogFaultVariables getADCCriticalFaults(void);
extern bool getElmoExceededNumFaults(void);
extern bool MCTransitionOK(void);
extern float MCGetIPD(void);
extern float MCGetIPQ(void);
extern bool GenControlStartAttemptsOK(void);
extern bool GenControlMotionOK(void);


// ****************************************************************************
// External Variables -- linkage to other modules
// ****************************************************************************

extern bool checkPhaseUComparatorflags(void);
extern bool checkPhaseVComparatorflags(void);
extern bool checkPhaseWComparatorflags(void);

extern int16_t CMPSS_CURR_U;
extern int16_t CMPSS_CURR_V;
extern int16_t CMPSS_CURR_W;



// ****************************************************************************
// Local Variables -- variables used in this module
// ****************************************************************************
union {
    enum GPIOFaultVariables gpio;
    enum LoopFaultVariables loop;
    enum AnalogFaultVariables analog;
    uint32_t bytes;
} gErrorCode;

/*************************************************************/
/*  High bandwidth fault detection                           */
/*  This runs in the interrupt context                       */
/*************************************************************/

/**Function: HiBWProtection
 * ------------------
 * Detects critical faults from various categories
 * Also populates the global gErrorCode union with the appropriate set of bits
 *
 * Outputs:
 * @returns Whichever single critical fault group was first to trip.
 *
 * Inputs:
 * None
 */
enum CritFaultVariables HiBWProtection(void)
{
	enum CritFaultVariables Flt = FLT_CRIT_NONE;
	static uint64_t FaultCounter=0;
	
	gErrorCode.bytes = 0L;

#if(!DISABLELOOPFAULTS)
    if(getLoopCriticalFaults() != FLT_LOOP_NONE)
    {
        Flt |= FLT_CRIT_LOOP;
        gErrorCode.loop = getLoopCriticalFaults();
        FaultCounter++;
    }
#endif
#if(!DISABLE_AD_FAULTS)
    else if(getADCCriticalFaults() != FLT_ADC_NONE)
    {
        Flt |= FLT_CRIT_ADC;
        gErrorCode.analog = getADCCriticalFaults();
        FaultCounter++;
    }
#endif
#if(!DISABLEGPIOFAULTS)
    else if(getGPIOCriticalFaults()!=FLT_GPIO_NONE)
	{
	    Flt |= FLT_CRIT_GPIO;
	    gErrorCode.gpio = getGPIOCriticalFaults();
	    FaultCounter++;
	}
#endif

	//Todo - comms faults? Or should they go in the A or B tasks instead...
    //Tentatively checking CANbus faults in the A tasks (timers, ISR ticker, etc)]
    if(Flt == FLT_CRIT_NONE)
    {
        FaultCounter = 0;
    }

    if(FaultCounter < 2)
    {
        Flt = FLT_CRIT_NONE;    //Reset fault if we haven't seen at least two in a row
    }
    else
    {
        //leave fault as-is
    }
    /*
     *Todo - allow for more of these to appear simultaneously and figure out a different way to populate the error code?
     *Maybe Error code is an array of 5 uint32_t's, and we'd be able to populate more than one at once if more than one fault group tripped?
     */

	return(Flt);
}

enum LoopFaultVariables getLoopCriticalFaults(void)
{
    //Todo - add the loop faults in (critical overspeed goes here)
    //Todo - take a snapshot of all monitored values at the fault point

    //Tentatively Elmo exceeding its num faults will be considered a loop fault
    if(Inverter_Params.InverterSel == ELMO_PMU)
    {
        if(getElmoExceededNumFaults())
        {
            return FLT_LOOP_ELMO_MAXFLT;
        }
    }

    if(Inverter_Params.Controller == GeneratorController)
    {
        if(GenControlMotionOK() == false)
        {
            return FLT_LOOP_SPEED;
        }

        if(GenControlStartAttemptsOK()==false)
        {
            return FLT_LOOP_MAX_STARTS;
        }
    }

    if(Inverter_Params.Electronics == DriveInternal)
    {
        if( (MCGetIPD() >= Motor_Params.LIM_ID_HI) || (MCGetIPD() <= Motor_Params.LIM_ID_LO))
        {
            return FLT_LOOP_ID;
        }

        if( (MCGetIPQ() >= Motor_Params.LIM_IQ_HI) || (MCGetIPQ() <= Motor_Params.LIM_IQ_LO))
        {
            return FLT_LOOP_IQ;
        }

        if(MCTransitionOK() == false)
        {
            return FLT_LOOP_ANG_EST;
        }
    }


    return FLT_LOOP_NONE;
}

uint32_t HiBW_getErrorCode(void)
{
    return gErrorCode.bytes;
}
