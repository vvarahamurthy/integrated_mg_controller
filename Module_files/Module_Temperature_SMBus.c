/*
============================================================================
System Name:    LaunchPoint Integrated Motor-Generator Controller

File Name:      Module_Temperature_SMBus.c

Target:         HPS350 Smart Passive Controller

Author:

Description:    Talks to SMBus temperature sensors on the HPS350 busbar
********************************************************************************* */
