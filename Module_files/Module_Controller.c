/* ============================================================================
    System Name:    LaunchPoint Motor Controller

File Name:      Controller.C

Target:         Cryo controller board

Author:         Mike Ricci

Description:    Refactoring motor controller software to be more modular and reduce linkage between sections
                This module will handle the actual motor control algorithm -- the PI loops and state machine
**********************************************************************************/
// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "IQMathLib.h"
#include "Header/GUIC_Header.h"
#include "Header/PWMRect.h"
#include "Header/State_machine.h"
#include "Header/Controller.h"

#include "pi.h"                 // Include header for the PIDREG3 object
#include "Header/observer.h"               // Include header for the OBSERV object
#include "Header/LP_PID.h"
#include "Header/clarke_3.h"           // Include header for the CLARKE object
#include "Header/svgen_comp.h"         // Include header for the SVGENDQ object
#include "Header/ramp_cntl.h"          // Include header for the RAMPCNTL object (local modified version of library file with slight name change)
#include "speed_est.h"          // Include header for the SPEED_ESTIMATION object
#include "speed_fr.h"           // Include header for the SPEED_MEAS_QEP object
#include "Header/speed_pr_mr.h"
#include "Header/speed_est_deriv.h"            //  Definitions to calculate derivative of speed est ( look for errors/bad estimates)
#include "Header/Integrator.h"

#include "smopos.h"             // Include header for the SMOPOS object
#include "smopos_const.h"       // Include header for the SMOPOS object
#include "park.h"               // Include header for the PARK object
#include "ipark.h"              // Include header for the IPARK object
#include "rampgen.h"            // Include header for the RAMPGEN object
#include "volt_calc.h"          // Include header for the PHASEVOLTAGE object


#include "F2837S_files/F2837x_QEP_Module.h"
#include "Header/Globals.h"
#include "Header/UsefulPrototypes.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1

#ifndef FixMeLater
#include "Header/PWMDAC.h"
#endif

#define THRESH_IRECT 2.0

/***********************************************/
// Function Prototypes
/***********************************************/

void Init_Controller(void);
void Reset_Controller(void);

void MainControlLoop(enum BuildLevels, enum MainStateVariables, enum HighMotorControlVar MCStateParam, enum CritFaultVariables, float);
void motorCalculations(enum BuildLevels, enum HighMotorControlVar, float);
enum HighMotorControlVar MotorControlStateMachine(enum BuildLevels, enum MainStateVariables, enum CritFaultVariables );

void Rectify_Passive(void);
void Rectify_Active(void);
void PhaseRect(int , int *, float );

float getObsMechTheta(void);
float GetThetaElec(enum Observers_Angles);
float GetSpeedPU(enum Observers_Angles);
int32_t GetSpeedRPM(enum Observers_Angles);

bool isPIDPositionEnabled();
bool isObserverAngleSetTo(enum Observers_Angles );
bool isPISpeedEnabled();
bool isPICurrentEnabled();

void RunAngleEstimators(enum Observers_Angles, float);

//Todo - remove syncMode, replace MainState stuff
void RunSpeedEstimators(enum HighMotorControlVar, enum SyncMode, enum Observers_Angles, enum BuildLevels BuildLevel);
void Run_PID_Speed(enum SyncMode);

void Run_PID_Position(enum MainStateVariables, enum Observers_Angles);
void Run_PI_Current(enum BuildLevels, enum HighMotorControlVar, float, enum SyncMode, float );


void LoadIntegrators(void);
void IntegratorWatch(uint32_t);

float GenerateAngleError(enum SyncMode, float );
float SetRampDelay(float);
float getPiIqRef(void);

float getChirpSignal();
float getMechAngleScaled();
bool getSelectAngleOffsetSignal();
void SelectAngleOffsetSignal(bool);
void selectAngleOffset(bool);
float generateChirpSignal(float, float, float, float, float);
float getAngleDisturbanceOffset();
void resetAngleDisturbanceOffset();

float speedErrorPEngage = 0.001;
float gMaxNMinAngleError = 0.01;

float GetIPD(void);
float GetIPQ(void);
float GetAccel(void);

void UpdateCurrentGains_MC(void);
void UpdateSpeedLoopGains_MC(void);

float getDataLV1(void);

int CheckRampTransition(enum Observers_Angles);

/***********************************************/
/*  External functions used in this module - linkage to other modules */
/***********************************************/

// QEP
extern float getQEPMechAngle();
extern float getQEPElecAngle();
extern Uint16 getDirectionQEP();
extern Uint16 getQEPIndexSyncFlag();
extern void Reset_QEP(void);
extern bool CheckTripFlags(int );
extern Uint16 getSPIRxSSI(void);

// CAN Rotor Sync Mode
extern bool getRotorSyncMode(void);

// Reset CMPSS
extern void ResetCMPSSLimits(void);

// Debug conditions
extern bool isUberTest();

//From QEP Module
extern void Run_QEP(void);

//From Datalog Module
extern void Run_Datalog(void);

//From PWM Module
extern void PWM_Out(_iq a,_iq b,_iq c);
extern void Disable_PWM(void);
extern void Enable_PWM(void);

#ifndef FixMeLater
//From PWMDAC module
extern void PWMDAC_InitHandle(struct PWMDACHandler *, int, int, float );
extern void PWMDAC_Enable(struct PWMDACHandler *);
extern void PWMDAC_Disable(struct PWMDACHandler * );
extern void PWMDAC_SetFreq(struct PWMDACHandler * , float );
extern void PWMDAC_SetOffset(struct PWMDACHandler * , float , float );
extern void PWMDAC_Out(struct PWMDACHandler *, float);
#endif

//From GPIO Module
extern void Disable_gate_drive(void);
extern void Enable_gate_drive(void);
extern void Disable_gate_predriver(void);
extern void Enable_gate_predriver(void);
//From ADC Module
extern float adcGetSignalValue(enum adc_signal_id);

//Added external variables for GUI Composer
extern int SPEED_LOOP_PRESCALERG;

extern void Run_Hall(void);
extern float SPI_GetSSIAngle(void);
extern void SPI_PingEncoder(void);

extern void TurnOnFET(int , enum SwitchSide );
extern void Deadband(int );

// Get knob values
extern _iq getIQDisturbanceKnob(void);
extern _iq getAngleOffsetKnob(void);
extern _iq getSpeedKnob(void);

extern bool isMasterOrSolo(enum SyncMode);
extern bool isGenerator(enum SyncMode);

extern float CAN_GetRSAngOff(void);
extern float getADCAngleOffsetKnob(void);
extern Uint16 GPIO_ReadPin(Uint16);

extern bool isTorqueModeWithPositionSensor(enum BuildLevels, enum Observers_Angles );

// ****************************************************************************
// External Variables -- linkage to other modules
// ****************************************************************************
extern float
      DlogCh1,
      DlogCh2,
      DlogCh3,
      DlogCh4;              // Data logging channel "inputs" from Datalog module

extern float Hall_Ang;
extern float SSIAngle;

// ******************************************************************************
//  Module resources - variables here //
// ******************************************************************************

/**********************************************************/
enum SyncMode syncMode = SOLO;
float masterValue1 = 0, masterValue2 = 0; //Dummy vars
/************************************************************/

uint16_t SpeedLoopCount = 1;          // Speed loop counter


// Instance a position estimator
SMOPOS smo1 = SMOPOS_DEFAULTS;
// Instance a sliding-mode position observer constant Module
SMOPOS_CONST smo1_const = SMOPOS_CONST_DEFAULTS;
// Instance a position estimator
OBSERV obs1 = OBSERV_DEFAULTS;

// Instance a speed calculator based on sliding-mode position observer
SPEED_ESTIMATION speed3 = SPEED_ESTIMATION_DEFAULTS;

// Instance a speed calculator based on Encoder position
SPEED_MEAS_QEP speed1 = SPEED_MEAS_QEP_DEFAULTS;

//Instance a speed derivative estimator (used to look for bad angle estimates)
SPEED_D_ESTIMATION acceleration = SPEED_D_ESTIMATION_DEFAULTS;

// Instance a few transform objects
CLARKE clarke1 = CLARKE_DEFAULTS;
PARK   park1   = PARK_DEFAULTS;
IPARK  ipark1  = IPARK_DEFAULTS;
float Omega_e=0.0;  //Electrical frequency in rad/s

// Instance PI(D) regulators to regulate the d and q  axis currents, speed and position

PID_CONTROLLER  pid_spd = {PID_TERM_DEFAULTS, PID_PARAM_DEFAULTS, PID_DATA_DEFAULTS};
PI_CONTROLLER   pi_id   = PI_CONTROLLER_DEFAULTS;
PI_CONTROLLER   pi_iq   = PI_CONTROLLER_DEFAULTS;
INTEGRATOR      int_iq = INTEGRATOR_DEFAULTS;
INTEGRATOR      int_id = INTEGRATOR_DEFAULTS;

PID_CONTROLLER  pid_pos = {PID_TERM_DEFAULTS, PID_PARAM_DEFAULTS, PID_DATA_DEFAULTS};
//PI_CONTROLLER pi_spd = PI_CONTROLLER_DEFAULTS;

// Instance a Space Vector PWM modulator. This modulator generates a, b and c
// phases based on the d and q stationery reference frame inputs
SVGEN svgen1 = SVGEN_DEFAULTS;

// Instance a ramp controller to smoothly ramp the frequency
RMPCNTL rc1 = RMPCNTL_DEFAULTS;

//  Instance a ramp generator to simulate an Angle
RAMPGEN rg1 = RAMPGEN_DEFAULTS;

RAMPGEN rg2 = RAMPGEN_DEFAULTS;

//  Instance a phase voltage calculation
PHASEVOLTAGE volt1 = PHASEVOLTAGE_DEFAULTS;

// ****************************************************************************
// Variables for Field Oriented Control
// ****************************************************************************


static enum MCLowLevelStateVariables ControllerState;  //ControllerState is now local to this module and will change due to high bandwidth faults
// Instance variables for LEVEL1 test paraemeters

TESTPARAMETERS TestParams = TEST_DEFAULTS;

// Controller state variables

static enum OnceAroundTriggerStates OnceAroundTriggerState;


//1 if these loops are enabled, 0 if not.
bool SpeedLoopEn=0;
bool PosLoopEn = 0;
bool CurLoopEn = 0;
bool RotorSyncEn = 0;
// ===========================================
//              DEBUG
// ===========================================
float d_iqref=0.0;
float dataLV1Output = 0.0;

// Watch the iq and id values when ramping -G
float gWatchIQ[200];
float gWatchID[200];
float gWatchSpd[200];
float gWatchUAlpha[200];
float gWatchUBeta[200];
float avgIDIntegrator, avgIQIntegrator, avgSpdIntegrator;
float gNumID = 3.08;
float gNumIQ = 4.04;
float gNumSpeed = 0.001763951;
uint16_t element = 0;
uint16_t element3 = 0;


uint32_t MCHighLevelStateTicker=0;

bool PositionMode;

enum Observers_Angles obsEn;
enum Observers_Angles commAng;
enum Observers_Angles mechAng;

#if(GDEBUG == 0)
// UBER controller #1 is -0.88
    float spiOffset = -0.81;
    float spiOut = 0.0;
    float prevSpiOut = 0.0;
#endif

static int32_t m_pwmOnLoopCounts = -1;
int32_t GetPwmOnLoopCounts(void) { return m_pwmOnLoopCounts; }
void SetPwmOnLoopCounts( int32_t count ) { m_pwmOnLoopCounts = count; }

bool gPositionFlag;

bool FallingEdgeFlag;
bool uberTest = 0;

//Angle error/mechanical angle resolution variables
float gAngleError = 0.0;
float prevElecTheta;
float mechAccum = 0.0;
float encoderAngleDiff = 0.0;
float obsMechTheta = 0.0;
float angleDisturbanceOffset = 0.0;
typedef struct {
    float gain;
    float offset;
    float out;
}ANGLEERR;

ANGLEERR mechAngle;

bool m_hallSensor;
float m_lockRotorCount = 0;
bool TransitionFlagOK=true;

#ifndef FixMeLater
struct PWMDACHandler pwmdach_angerr;
#endif

// return pi_iq.ref
float getPiIqRef(void)
{
    return pi_iq.Ref;
}

float getDataLV1(void)
{
    return dataLV1Output;
}

// Select where angle offset gets from CAN (0), chirp (1), or ADC (2)
void selectAngleOffset(bool select)
{
#if(!SYNCMODE_FIX)
    if(select == 0)
    {
        // CAN_GetRSAngOff is normalized -0.5 to 0.5
        angleDisturbanceOffset = CAN_GetRSAngOff();
    }
    else
    {
        // getADCAngleOffsetKnob is normalized -0.5 to 0.5
        angleDisturbanceOffset = getADCAngleOffsetKnob() ;
    }
#endif
}

float getAngleDisturbanceOffset()
{
    return angleDisturbanceOffset;
}

void resetAngleDisturbanceOffset()
{
    angleDisturbanceOffset = 0.0;
}

float getMechAngleScaled()
{
    return mechAngle.out;
}

void InitMechAngleScale(void)
{
    mechAngle.gain = 1.0;
    mechAngle.offset = 0.0;
    mechAngle.out = 0.0;
}


#ifndef FixMeLater
void scaleAndOutput(float source, float offset, float gain, float max, float min)
{
    mechAngle.out = _IQsat( ( (source*gain) + offset ), max, min);
    PWMDAC_Out(&pwmdach_angerr, mechAngle.out);
}
#endif


// Returns T/F for FallingEdgeFlag status
bool isFallingEdgeFlagTrig()
{
    return FallingEdgeFlag;
}

// Set gPositionFlag status
void setFallingEdgeFlag(bool f)
{
    FallingEdgeFlag = f;
}

// Functions for gWatchID/IQ
#if 1
// Returns the average value of pi_id.i1 for the first 10 elements on commutate
float getAvgIDIntegrator(){
    return avgIDIntegrator;
}

float getAvgIQIntegrator()
{
    return avgIQIntegrator;
}

float getAvgSpdIntegrator()
{
    return avgSpdIntegrator;
}

void avgIntegrator(float * integrator, uint16_t n, short typeOfIntegrator)
{
    uint16_t i = 0;
    float sum = 0;

    // Proceed if n > 1
    if( n > 1)
    {
        // Start at one and add up to 10
        for( i = 1; i < (n+1); i++)
        {
            sum = sum + integrator[i];
        }

        switch(typeOfIntegrator)
        {
        case 0:
            avgIDIntegrator = sum/((float)n);
        break;

        case 1:
            avgIQIntegrator = sum/((float)n);
        break;

        case 2:
            avgSpdIntegrator = sum/((float)n);
        break;
        }
    }

}


// Add float in iq array
void addIQ(float fnum, uint16_t index)
{
    if( index < 200 )
    {
        gWatchIQ[index] = fnum;
    }
}

// Add U alpha
void addUAlpha(float fnum, uint16_t index)
{
    if( index < 200 )
    {
        gWatchUAlpha[index] = fnum;
    }

}

// Add U Beta
void addUBeta(float fnum, uint16_t index)
{
    if( index < 200 )
    {
        gWatchUBeta[index] = fnum;
    }

}

// Add ID
void addID(float fnum, uint16_t index)
{
    if( index < 200 )
    {
        gWatchID[index] = fnum;
    }
}

// Add spd
void addSpd(float fnum, uint16_t index)
{
    if( index < 200 )
    {
        gWatchSpd[index] = fnum;
    }
}


// ===========================================
//              CODE STARTS HERE
// ===========================================

// Save initialize observer angles
void saveObserverAngles(void)
{
    obsEn   = Motor_Params.ObsEn;
    commAng = Motor_Params.CommAng;
    mechAng = Motor_Params.MechAng;
}


// Initializing Module_Controller
void Init_Controller(void)
{

    saveObserverAngles();
    Motor_Params.ObserverCommAndMechAngleSwitch = 0;

    InitMechAngleScale();
    // Initialize gElecTheta
    mechAccum = 0.0;
    obsMechTheta = 0.0;
    prevElecTheta = 0.0;
    gPositionFlag = 0;
    FallingEdgeFlag = 0;
    gAngleError = 0.0;
    angleDisturbanceOffset = 0.0;

    // Initialize controller state
    ControllerState = MC_Init;

    // Initialize the Speed module for QEP based speed calculation
        speed1.K1 = _IQ21(1/(Motor_Params.BaseFrequency*Motor_Params.T_LOOP));
        speed1.K2 = _IQ(1/(1+Motor_Params.T_LOOP*2*PI*5));  // Low-pass cut-off frequency
        speed1.K3 = _IQ(1)-speed1.K2;
        speed1.BaseRpm = 2*60*(Motor_Params.BaseFrequency/Motor_Params.Poles);

    // Initialize the SPEED_EST module SMOPOS based speed calculation
        speed3.K1 = _IQ21(1/(Motor_Params.BaseFrequency*Motor_Params.T_LOOP));
        speed3.K2 = _IQ(1/(1+Motor_Params.T_LOOP*2*PI*5));  // Low-pass cut-off frequency
        speed3.K3 = _IQ(1)-speed3.K2;
        speed3.BaseRpm = 2*60*(Motor_Params.BaseFrequency/Motor_Params.Poles);  //  assumes speed = 1.0 is motor running at base frequency

    // Initialize the speed derivative (error detecting) module - parameters scaled by T_LOOP since speed estimator runs at full loop bandwidth (speed PI is slower)
        acceleration.K1 = _IQ21(1/(Motor_Params.T_LOOP));
        acceleration.K2 = _IQ(1/(1+Motor_Params.T_LOOP*2*PI*5));  // Low-pass cut-off frequency
        acceleration.K3 = _IQ(1)-acceleration.K2;

    // Initialize the RMPCnTL module
        //Set it to the lower ramp rate so we ramp up slowly before the transition
        rc1.RampDelayMax = (.0000305*(Motor_Params.LOOP_FREQ_KHZ*1000.0*Motor_Params.RAMP_to_1_Secs)/Motor_Params.SPEED_LOOP_PRESCALE);    //Set prescalar that tells how fast ramp can slew
        rc1.EqualFlag = 0;

    // Initialize the RAMPGEN module for open loop ramp
        rg1.StepAngleMax = _IQ(Motor_Params.BaseFrequency*Motor_Params.T_LOOP*(2.0/Motor_Params.Poles));

    //Initialize the RAMPGEN module for PLL
        rg2.StepAngleMax = (Motor_Params.BaseFrequency*Motor_Params.T_LOOP);


    // Initialize the OBSERVER module
        obs1.Rmotor = Motor_Params.StatorResistance;
        obs1.Lmotor = Motor_Params.StatorInductance;
        obs1.PsiSquared = powf(Motor_Params.Psi,2);
        obs1.Tloop = Motor_Params.T_LOOP;
        obs1.Gamma = Motor_Params.Gamma;

    // Initialize the SMOPOS constant module
        smo1_const.Rs = Motor_Params.StatorResistance*2.0;
        smo1_const.Ls = Motor_Params.StatorInductance*2.0;   //x2 because values are defined as phase-to-neutral
    //FixMe - bring the below back to real world scaling. Set BASE_CURRENT and BASE_VOLTAGE to 1 and Ts to T_LOOP without the weird scaling
        smo1_const.Ib = 1.0;
        smo1_const.Vb = 1.0;
        smo1_const.Ts = Motor_Params.T_LOOP;
        SMO_CONST_MACRO(smo1_const)

    // Initialize the SMOPOS module
        smo1.Fsmopos = _IQ(smo1_const.Fsmopos);
        smo1.Gsmopos = _IQ(smo1_const.Gsmopos);
        smo1.Kslide  = _IQ(Motor_Params.K_SLIDE);
        smo1.Kslf    = _IQ(Motor_Params.K_SLF);
        smo1.E0 =       0.50;          // Saturation value, shouldn't be used, was .5 in original "Per unit" code


        if(Motor_Params.BuildLevel != e_OpenLoop)
        {
            // Initialize the svgen module to put all low gates off
                    svgen1.Ta = -1.0;
                    svgen1.Tb = -1.0;
                    svgen1.Tc = -1.0;
        }

    // Initialize the PID module for speed

    pid_spd.param.Kp   = _IQ(Motor_Params.spd_KP);
    pid_spd.param.Ki   = _IQ(Motor_Params.T_SPDLOOP*Motor_Params.spd_KI);
    pid_spd.param.Kd   = _IQ(0.0);
    pid_spd.param.Kr   = _IQ(1.0);
    pid_spd.param.Umax = _IQ(Inverter_Params.LIM_I_DC_CRIT_H)/Inverter_Params.Sense_Current_M;   /* speed postion loop outputs in normalized units */
    pid_spd.param.Umin = _IQ(-1.0*Inverter_Params.LIM_I_DC_CRIT_H)/Inverter_Params.Sense_Current_M;  /*  speed position loop outputs in normalized units */


    // Initilaize the PID for position
    pid_pos.param.Kp    = _IQ(Motor_Params.pos_KP);
    pid_pos.param.Ki    = _IQ(Motor_Params.pos_KI);
    pid_pos.param.Kd    = _IQ(Motor_Params.pos_KD);
    pid_pos.term.c1    = _IQ(Motor_Params.pos_C1);
    pid_pos.term.c2    = _IQ(Motor_Params.pos_C2);
    pid_pos.param.Kr    = _IQ(1.0);
    pid_pos.param.Umax  = _IQ(Inverter_Params.LIM_I_DC_CRIT_H)/Inverter_Params.Sense_Current_M;   /* speed postion loop outputs in normalized units */
    pid_pos.param.Umin  = _IQ(-1.0*Inverter_Params.LIM_I_DC_CRIT_H)/Inverter_Params.Sense_Current_M;  /*  speed position loop outputs in normalized units */

    // Initialize the PI module for Id

    pi_id.Kp=_IQ(Motor_Params.id_KP);
    pi_id.Ki=_IQ(Motor_Params.T_LOOP*Motor_Params.id_KI);
    pi_id.Umax =_IQ(Motor_Params.LIM_ID_HI);
    pi_id.Umin =_IQ(Motor_Params.LIM_ID_LO);

    // Initialize the PI module for Iq
    pi_iq.Kp=_IQ(Motor_Params.iq_KP);
    pi_iq.Ki=_IQ(Motor_Params.T_LOOP*Motor_Params.iq_KI);
    pi_iq.Umax =_IQ(Motor_Params.LIM_IQ_HI);
    pi_iq.Umin =_IQ(Motor_Params.LIM_IQ_LO);

    rg2.Angle = 0;                  // Clear ramp gen angle accumulator
    rg2.Out = 0;                    // Clear ramp gen output angle

//  Note that the vectorial sum of d-q PI outputs should be less than 1.0 which
//  refers to maximum duty cycle for SVGEN. Another duty cycle limiting factor
//  is current sense through shunt resistors which depends on hardware/software
//  implementation. Depending on the application requirements 3,2 or a single
//  shunt resistor can be used for current waveform reconstruction. The higher
//  number of shunt resistors allow the higher duty cycle operation and better
//  dc bus utilization. The users should adjust the PI saturation levels
//  carefully during open loop tests (i.e pi_id.Umax, pi_iq.Umax and Umins) as
//  in project manuals. Violation of this procedure yields distorted  current
// waveforms and unstable closed loop operations which may damage the inverter.

    // Clear all output variables as well, make sure they are initialized properly when initialized.

    rc1.SetpointValue = 0;          // Clear ramp gen controller output back to 0 freq.
    rg1.Angle = 0;                  // Clear ramp gen angle accumulator
    rg1.Out = 0;                    // Clear ramp gen output angle

    speed3.EstimatedSpeed = 0;      // Clear estimated speed
    speed3.EstimatedSpeedRpm = 0;
    speed1.Speed = 0;       // Clear estimated speed from QEP if in use
    speed1.SpeedRpm = 0;    // Clear estimated RPM from QEP if in use

    pid_spd.term.Out = 0;                   // Clear output of speed controller PI
    pid_spd.data.i1 = 0;                    // Clear integrator storage of speed controller
    pid_spd.data.d1 = 0;
    pid_spd.data.d2 = 0;                    // Clear derivative terms
    pid_spd.data.w1 = 0;

    pi_id.Out = 0;                  // Clear output of ID PI controller
    pi_id.v1 = 0;
    pi_id.i1 = 0;
    pi_id.w1 = 0;

    pi_iq.Out = 0;                  // Clear output of IQ PI controller (What about internal states for integrator though? Is clearing output enough?)
    pi_iq.v1 = 0;
    pi_iq.i1 = 0;
    pi_iq.w1 = 0;

    int_iq.Out = 0.0;   //Reset DQ decoupling terms to zeroes
    int_iq.Err = 0.0;
    int_iq.Ki = 0.0;
    int_iq.ui = 0.0;
    int_iq.i1 = 0.0;

    int_id.Out = 0.0;
    int_id.Err = 0.0;
    int_id.Ki = 0.0;
    int_id.ui = 0.0;
    int_id.i1 = 0.0;

    ipark1.Qs = 0.0;                //Reset IPARK modules to 0
    ipark1.Ds = 0.0;

    avgIDIntegrator = 0;
    avgIQIntegrator = 0;
    avgSpdIntegrator = 0;
    Omega_e = 0.0;                  //Reset omega calculation

    m_hallSensor = (Motor_Params.CommAng == SNS_HALLRAW) || (Motor_Params.CommAng == SNS_HALLPLL);

    //Todo - rotor sync conditional here
#ifndef FixMeLater
    PWMDAC_InitHandle(&pwmdach_angerr, 2, Inverter_Params.GPIO_PWMDAC_ANGLEERROR, 60000);
    PWMDAC_Out(&pwmdach_angerr, 0.0);
    PWMDAC_Enable(&pwmdach_angerr);
#endif
}

// resetting to original angles observers
void resetCommObsAndMechAngles(void)
{
    Motor_Params.ObsEn = obsEn;
    Motor_Params.CommAng = commAng;
    Motor_Params.MechAng = mechAng;
    Motor_Params.ObserverCommAndMechAngleSwitch = 0;
}

// Resetting Module_Controller
void Reset_Controller(void)
{
#if(GDEBUG == 0)
    prevSpiOut = 0.0;
#endif

    resetCommObsAndMechAngles();

    ResetCMPSSLimits();
    prevElecTheta = 0.0;
    mechAccum = 0.0;
    obsMechTheta = 0.0;
    gPositionFlag= 0;
    gAngleError = 0.0;
    angleDisturbanceOffset = 0.0;

    rc1.SetpointValue = 0.0;          // Clear ramp gen controller output back to 0 freq.
    rc1.EqualFlag = 0.0;
    rc1.RampLowLimit = -1.0;
    rc1.RampDelayMax = (.0000305*(Motor_Params.LOOP_FREQ_KHZ*1000.0*Motor_Params.RAMP_to_1_Secs)/Motor_Params.SPEED_LOOP_PRESCALE);          // Set prescalar that tells how fast ramp can slew

    rg1.Angle = 0;                  // Clear ramp gen angle accumulator
    rg1.Out = 0;                    // Clear ramp gen output angle

    rg2.Angle = 0;                  // Clear ramp gen angle accumulator
    rg2.Out = 0;                    // Clear ramp gen output angle
    pid_pos.param.Kp    = _IQ(Motor_Params.pos_KP);
    pid_pos.param.Ki    = _IQ(Motor_Params.pos_KI);
    pid_pos.param.Kd    = _IQ(Motor_Params.pos_KD);

    pid_pos.term.Out=0;
    pid_pos.data.i1=0;
    pid_pos.data.d1=0;
    pid_pos.data.d2=0;
    pid_pos.data.w1=0;
    pid_pos.data.v1=0;

    speed3.EstimatedSpeed = 0;      // Clear estimated speed
    speed3.EstimatedSpeedRpm = 0;

    speed1.Speed = 0;       // Clear estimated speed from QEP if in use
    speed1.SpeedRpm = 0;    // Clear estimated RPM from QEP if in use

    pid_spd.param.Kp   = _IQ(Motor_Params.spd_KP);
    pid_spd.param.Ki   = _IQ(Motor_Params.T_SPDLOOP*Motor_Params.spd_KI);

    pid_spd.term.Out = 0;                   // Clear output of speed controller PI
    pid_spd.data.i1 = 0;                    // Clear integrator storage of speed controller
    pid_spd.data.d1 = 0;
    pid_spd.data.d2 = 0;                    // Clear derivative terms
    pid_spd.data.w1 = 0;

    pi_id.Out = 0;                  // Clear output of ID PI controller
    pi_id.v1 = 0;
    pi_id.i1 = 0;
    pi_id.w1 = 0;

    pi_iq.Out = 0;                  // Clear output of IQ PI controller
    pi_iq.v1 = 0;
    pi_iq.i1 = 0;
    pi_iq.w1 = 0;

    // Initialize the PI module for Id
    pi_id.Kp=_IQ(Motor_Params.id_KP);
    pi_id.Ki=_IQ(Motor_Params.T_LOOP*Motor_Params.id_KI);
    pi_id.Umax =_IQ(Motor_Params.LIM_ID_HI);
    pi_id.Umin =_IQ(Motor_Params.LIM_ID_LO);

    // Initialize the PI module for Iq
    pi_iq.Kp=_IQ(Motor_Params.iq_KP);
    pi_iq.Ki=_IQ(Motor_Params.T_LOOP*Motor_Params.iq_KI);
    pi_iq.Umax =_IQ(Motor_Params.LIM_IQ_HI);
    pi_iq.Umin =_IQ(Motor_Params.LIM_IQ_LO);

    int_iq.Out = 0.0;   //Reset DQ decoupling terms to zeroes
    int_iq.Err = 0.0;
    int_iq.Ki = 0.0;
    int_iq.ui = 0.0;
    int_iq.i1 = 0.0;

    int_id.Out = 0.0;   //Reset DQ decoupling terms to zeroes
    int_id.Err = 0.0;
    int_id.Ki = 0.0;
    int_id.ui = 0.0;
    int_id.i1 = 0.0;

    ipark1.Qs = 0.0;                //Reset IPARK modules to 0
    ipark1.Ds = 0.0;

    if(Motor_Params.BuildLevel != e_OpenLoop)
    {
        svgen1.Ta = -1.0;
        svgen1.Tb = -1.0;
        svgen1.Tc = -1.0;
    }

    svgen1.Ualpha = 0;
    svgen1.Ubeta = 0;
    svgen1.tmp1 = 0;
    svgen1.tmp2 = 0;
    svgen1.tmp3 = 0;


    Omega_e = 0.0;                  //Reset omega calculation

    smo1.Kslide  = _IQ(Motor_Params.K_SLIDE);

    Reset_QEP();

    m_pwmOnLoopCounts = TestParams.StepCycles;      // Reset counter for number of cycles to run step function test
}

// Functions to call from main to get the D and Q voltage, current, and angle.
float getAlphaVoltageCMD()
{
    return Motor_Params.Start_VLockRotor;
}

// get Beta Voltage
float getBetaVoltageCMD()
{
    //return svgen1.Ubeta;
    return 0;
}

// Get Ramp angle. For parallel mode
float getRampAngle()
{
    // Add the angle + slave + arctan(id/iq);
//    float temp = park1.Angle + Motor_Params.slave_phase_shift + _IQatan2PU(-pi_id.Ref,pi_iq.Ref);
    float temp   = park1.Angle + Motor_Params.slave_phase_shift + _IQatan2PU(-(ipark1.Ds),(ipark1.Qs));

    // if q and d are less than 0, add 180 degrees
    //if( (pi_id.Ref < 0) && (pi_iq.Ref < 0) )
    if( (ipark1.Qs < 0)  && (ipark1.Qs < 0) )
    {
        temp = temp + 0.5;
    }

    // change TEMP%1
    temp = fmodf(temp, 1.0);

    return temp;
}

// get ramp current magnitude
float getRampCurrent()
{
    return sqrt( powf(pi_iq.Ref,2) + powf(pi_id.Ref,2) );
}

// return Mech Angle
float getMechAngle()
{
    float polePairs = Motor_Params.Poles/2.0;

    return (park1.Angle/polePairs); // Theta_mech = Theta_elec / polePairs

}

//what is going on here? This is called in parseroutine.c but it returns the PI_IQ out? -VV
// get angle
float getAngle()
{
    //return park1.Angle;
    return pi_iq.Out;
}

// get q current
float getQCurrentCMD()
{
    return pi_iq.Ref;
}

// Radian units
float getObsMechTheta()
{
    return obsMechTheta;
}

// set observer mech theta
void setObsMechTheta(float o)
{
    obsMechTheta = o;
}

float getRampGenAngle()
{
    return rg1.Out;
}

void positionController(float ControllerRefParam, float speedErrorPEngage)
{

}

bool isOnceAroundTriggered(void){
    static bool onceAroundFlag    = 0;

    bool onceAroundSignal   = 0;
    bool output = 0;

    onceAroundSignal=  GPIO_ReadPin(Inverter_Params.GPIO_ONCEAROUND_NUM);

    switch(OnceAroundTriggerState)
    {
        /* At OnceEventState, if there is a once-around signal, go to SinglePulseState. Otherwise, stay in this state. */
        case OnceEventState:
            if( (onceAroundSignal == 1) && (onceAroundFlag == 0 ) )
            {
                OnceAroundTriggerState = OnceEventState;
            }
            else if( (onceAroundSignal == 0) && (onceAroundFlag == 0) )
            {
                OnceAroundTriggerState = SinglePulseState;
            }

            onceAroundFlag = 0;
            output = 0;
        break;

        /* This state only outputs true ONLY for one cycle. */
        case SinglePulseState:
            OnceAroundTriggerState = Wait;
            output = 1;
        break;

        /* This state waits until the hall sensor and magnet are not aligned anymore. Output must be 0*/
        case Wait:
            if( (onceAroundSignal == 0) && (onceAroundFlag == 1) )
            {
                OnceAroundTriggerState = SinglePulseState;
            }
            else if( (onceAroundSignal == 1) && (onceAroundFlag == 1) )
            {
                OnceAroundTriggerState = OnceEventState;
            }

            output = 0;
        break;
    }
    return output;

}

void runConvertElecToMech(float elecTheta)
{
    // Use later after finalizing this function
    float delta = 0.3;

    // if Z Pulse is detected and position counter is not equal to position max, then align them.
    if( isOnceAroundTriggered() == 1 )
    {
        mechAccum = -(elecTheta*(2/Motor_Params.Poles));
    }
    // Increasing
    else if( ( prevElecTheta >= (1.0 - delta) ) && (elecTheta <= delta))
    {
        // Increment mechAccum 1/PolePairs
        mechAccum += 1/(Motor_Params.Poles/2);

        // If mechAccum reached greater than one, than wrap
        if(mechAccum >= 1.0 )
        {
            mechAccum = 0.0;
        }
    }

    // If mechAccum reached greater than one, than wrap

    // Decreasing
    else if( (prevElecTheta <= delta) && ( elecTheta >= (1.0 - delta) ) )
    {
        // Decrement mechAccum by 1/PolePairs
        mechAccum -= elecTheta*(2/Motor_Params.Poles);
        // if mechAccum is less than 0, then mechAccum = (PolePairs - 1)/PolePairs
        if(mechAccum < 0.0 )
        {
            mechAccum = ((Motor_Params.Poles/2) - 1)/(Motor_Params.Poles/2);
        }
    }

    obsMechTheta = mechAccum + (elecTheta*(2/Motor_Params.Poles));

    if( obsMechTheta > 1.0)
    {
        obsMechTheta -= 1.0;
        mechAccum -= 1.0;
    }

    prevElecTheta = elecTheta;

}

// Convert mechanical to electric angle
float convertMechToElecTheta(float mechTheta)
{
    // MechTheta range is between 0 to 1
    if( mechTheta < 0.0 )
    {
        mechTheta = 0.0;
    }
    else if(mechTheta > 1.0)
    {
        mechTheta = 1.0;
    }

    float x = mechTheta*(Motor_Params.Poles/2.0);
    return (x - floor(x) );
}

bool isSensorlessTriggered()
{
    return Motor_Params.ObserverCommAndMechAngleSwitch;
}

// returns estimated speed (rpm)
int32 SpeedRPM()
{
    return speed3.EstimatedSpeedRpm;
}

// Set all phase to low. Does not PWM
void Rectify_Passive()
{
    Deadband(Inverter_Params.PWM_U_NUM);
    Deadband(Inverter_Params.PWM_V_NUM);
    Deadband(Inverter_Params.PWM_W_NUM);
}

// Begin rectify
void Rectify_Active()
{
    static int phasestatusU;
    static int phasestatusV;
    static int phasestatusW;

    PhaseRect(Inverter_Params.PWM_U_NUM, &phasestatusU, adcGetSignalValue(adc_id_iu));
    PhaseRect(Inverter_Params.PWM_V_NUM, &phasestatusV, adcGetSignalValue(adc_id_iv));
    PhaseRect(Inverter_Params.PWM_W_NUM, &phasestatusW, adcGetSignalValue(adc_id_iw));
}

void PhaseRect(int chan, int *status, float Iphase)
{
    /*
     * Status = 0 is both gates off
     * Status = 1 is low side on
     * Status = -1 is high side on
     */
    if((*status==0 || *status==1) && (Iphase > THRESH_IRECT))
    {
        TurnOnFET(chan, Low);
        *status = 1;
    }
    else if((*status==0|| *status==-1)&&(Iphase< -1.0*THRESH_IRECT))
    {
        TurnOnFET(chan, High);
        *status=-1;
    }
    else
    {
        Deadband(chan);
        *status=0;
    }
}

float getAngleError(void)
{
    return gAngleError;
}

float GetThetaElec(enum Observers_Angles src)
{
    float thetaelec = 0.0;

    if(src==SL_ORTEGA)
    {
        thetaelec =  obs1.Theta;
    }
    else if(src==SL_SMOPOS)
    {
        thetaelec =  smo1.Theta;
    }
    else if(src==SNS_ENC_QEP)
    {
        thetaelec =  getQEPElecAngle();
    }
    else if(src==SNS_ENC_SSI)
    {
        float SPI_AngleTemp = 0.0;

        if( Motor_Params.reverseEncoderAngle == 1)
        {
            SPI_AngleTemp = 1 - SPI_GetSSIAngle();
        }
        else
        {
            SPI_AngleTemp = SPI_GetSSIAngle();
        }

        thetaelec =  convertMechToElecTheta( SPI_AngleTemp ) - Motor_Params.SPI_Offset;

        // Wraps
        if( thetaelec >= 1.0)
        {
            thetaelec -= 1.0;
        }
        else if( thetaelec <= 0.0)
        {
            thetaelec += 1.0;
        }
    }
    else if(src==SNS_HALLPLL)
    {
        thetaelec =  rg2.Out;
    }
    else if(src==SNS_HALLRAW)
    {
        thetaelec =  Hall_Ang;
    }
    else
    {
        thetaelec =  0;
    }

    return thetaelec;
}

//Returns per-unit speed based on commutation angle
float GetSpeedPU(enum Observers_Angles src)
{
    float spu=0.0;
    if(src & (SL_ORTEGA | SL_SMOPOS | SNS_HALLRAW | SNS_HALLPLL | SNS_ENC_SSI) )
    {
        spu = (float)speed3.EstimatedSpeed;
    }
    else if(src==SNS_ENC_QEP)
    {
        spu = (float)speed1.Speed;
    }
    else
    {
        spu=0.0;
    }

    return spu;
}

int32_t GetSpeedRPM(enum Observers_Angles src)
{
    int32_t sprpm = 0;

    if(src &(SL_ORTEGA | SL_SMOPOS | SNS_HALLRAW | SNS_HALLPLL) )
    {
        sprpm = speed3.EstimatedSpeedRpm;
    }
    else if(src==SNS_ENC_QEP)
    {
        sprpm = speed1.SpeedRpm;
    }
    else if(src==SNS_ENC_SSI)
    {
        sprpm = 0;
    }
    else
    {
        sprpm = 0;
    }

    return sprpm;
}

float SetRampDelay(float acc)
{
    float retval = 0.0;
    if(acc>=0.0)
    {
        retval = (.0000305*(Motor_Params.LOOP_FREQ_KHZ*1000.0*Motor_Params.RAMP_to_1_Secs)/Motor_Params.SPEED_LOOP_PRESCALE);
    }
    else if(acc<0.0)
    {
        retval = (.0000305*(Motor_Params.LOOP_FREQ_KHZ*1000.0*Motor_Params.RAMP_down_Secs)/Motor_Params.SPEED_LOOP_PRESCALE);
    }
    return retval;
}

float GenerateAngleError(enum SyncMode syncMode, float masterValue1)
{
    float angErr=0.0;

    // Angle error
     if( syncMode == SERVO_MASTER || syncMode == SOLO )
     {
         angErr = getObsMechTheta() - rg1.Out;
     }
     else if( syncMode == FOLLOWER_MASTER )
     {
         angErr = getObsMechTheta() - rg1.Out;
     }
     else
     {
         angErr = 0.0;
     }

     // Both thetas are between 0 and 1. Therefore, the largest error is -0.5 and 0.5.
     // Anything that goes over that, is going to "the other way."
     if( angErr >= 0.5)
     {
         angErr -= 1.0;
     }
     else if( angErr <= -0.5)
     {
         angErr += 1.0;
     }

     return angErr;
}

void positionAndSpeedSelector(enum HighMotorControlVar StateMain, float ControllerRefParam)
{
    // Position control feature
    if(RotorSyncEn)
    {
        float absAngleErrorAndOffsetKnob = fabs(gAngleError - getAngleDisturbanceOffset() );
        float absSpdRefAndActualSpd = fabs(ControllerRefParam - GetSpeedPU(Motor_Params.CommAng));
        float const tolerance = 0.1;

        //  Relay / hysteresis function for enabling/disabling position loop based on speed error
        // Check if speed error is small enough to engage position control and we are not already in position control

        // Turn on position when speed reaches RPM set point
        if( (absSpdRefAndActualSpd < Motor_Params.speedErrorPEngage) && (gPositionFlag == 0) )
        {
          gPositionFlag = 1;
          // Switch speed integrator to position integrator
          pid_pos.data.i1 = pid_spd.data.i1*pid_spd.param.Kp/pid_pos.param.Kp;

          // Zero the speed integrator
          pid_spd.data.i1 = 0.0;
        }

        // Check if we are in position mode and the speed error is too large, then disengage position control

        // Turn off position when speed is far from RPM set point
        if( (absSpdRefAndActualSpd > Motor_Params.speedErrorPEngage) && (absAngleErrorAndOffsetKnob >= tolerance) &&(gPositionFlag == 1) )
        {
          gPositionFlag = 0;
          // Transfer position loop integrator to speed loop integrator
          pid_spd.data.i1 = pid_pos.data.i1*pid_pos.param.Kp/pid_spd.param.Kp;

          pid_pos.data.d1 = 0.0;
          pid_pos.data.d2 = 0.0;
          pid_pos.data.i1 = 0.0;
          pid_pos.data.ud = 0.0;
          pid_pos.data.ui = 0.0;
          pid_pos.data.up = 0.0;
          pid_pos.term.Out = 0.0;
        }
    }
    else if(gPositionFlag && ~RotorSyncEn)         // Transition out of sync mode
    {
        gPositionFlag = 0;
        // Transfer position loop integrator to speed loop integrator
        pid_spd.data.i1 = pid_pos.data.i1*pid_pos.param.Kp/pid_spd.param.Kp;

        pid_pos.data.d1 = 0.0;
        pid_pos.data.d2 = 0.0;
        pid_pos.data.i1 = 0.0;
        pid_pos.data.ud = 0.0;
        pid_pos.data.ui = 0.0;
        pid_pos.data.up = 0.0;
        pid_pos.term.Out = 0.0;
    }
}

void runPositionLoop(enum HighMotorControlVar MCStateParam, float ControllerRefParam)
{
    //NOTE - right now the pid reference is hard-set to the angle offset knob. Need to think through a "getOffset(offsetsrc)" function
    //because we're adding the CAN functionality now

    //Need some comments on this function to explain why each observer has a different ref/fdb source for the PID loop
    if( (Motor_Params.ObsEn & SL_ORTEGA) != 0)
    {
        if( (MCStateParam == e_MCCommutate) && gPositionFlag )
        {
           float satAngleOffset = _IQsat( getAngleDisturbanceOffset(), Motor_Params.maxAndMinAngleDistOffset, -Motor_Params.maxAndMinAngleDistOffset );      /* Saturate -0.49 to 0.49 */
           // check pid_macro if it minus out
           float satDiffAngleOffset = _IQsat( gAngleError - satAngleOffset, Motor_Params.maxAndMinAngleError, -Motor_Params.maxAndMinAngleError);
           // Select chirp or angle offset
           pid_pos.term.Ref = 0.0;
           pid_pos.term.Fbk = satDiffAngleOffset;

           // Run position mode
           PID_POS_MACRO(pid_pos)
        }
        else
        {
          pid_pos.data.d1 = 0.0;
          pid_pos.data.d2 = 0.0;
          pid_pos.data.i1 = 0.0;
          pid_pos.data.ud = 0.0;
          pid_pos.data.ui = 0.0;
          pid_pos.data.up = 0.0;
        }
    }
    // Using QEP
    else if( (Motor_Params.ObsEn & SNS_ENC_QEP) != 0)
    {
        if( (MCStateParam == e_MCRamp) || (MCStateParam == e_MCCommutate) && gPositionFlag )
        {
           pid_pos.term.Ref = park1.Angle;
           pid_pos.term.Fbk = getObsMechTheta();

           PID_POS_MACRO(pid_pos)
        }
        else
        {
          pid_pos.data.d1 = 0;
          pid_pos.data.d2 = 0;
          pid_pos.data.i1 = 0;
          pid_pos.data.ud = 0;
          pid_pos.data.ui = 0;
          pid_pos.data.up = 0;
        }
    }
    else if(Motor_Params.ObsEn & SNS_HALLPLL)
    {
        if((MCStateParam == e_MCRamp) && gPositionFlag )
        {
           pid_pos.term.Ref = Hall_Ang;
           if(Hall_Ang-rg2.Out > 0.5)
           {
               pid_pos.term.Fbk = (rg2.Out+1.0);
           }
           else if(Hall_Ang-rg2.Out < -0.5)
           {
               pid_pos.term.Fbk = (rg2.Out-1.0);
           }
           else
           {
               pid_pos.term.Fbk = rg2.Out;
           }
           PID_POS_MACRO(pid_pos)
           rg2.Freq = pid_pos.term.Out;
           RG_MACRO(rg2)
        }
        else
        {
           pid_pos.data.d1 = 0;
           pid_pos.data.d2 = 0;
           pid_pos.data.i1 = 0;
           pid_pos.data.ud = 0;
           pid_pos.data.ui = 0;
           pid_pos.data.up = 0;
        }
    }

}

void Run_PID_Speed(enum SyncMode syncMode)
{
#if(!SYNCMODE_FIX)
    if(isMasterOrSolo(syncMode))
    {
#endif
        if (SpeedLoopCount>=Motor_Params.SPEED_LOOP_PRESCALE)
        {
            if(isUberTest() == 1)
            {
                pid_spd.term.Ref = 0.0;
                // Saturate between 0.05 and -0.05, to avoid large changes in a short time.
                pid_spd.term.Fbk = _IQsat( (GetSpeedPU(Motor_Params.CommAng) - rc1.SetpointValue), 0.05, -0.05);
            }
            else
            {
                pid_spd.term.Ref = rc1.SetpointValue;
                pid_spd.term.Fbk = GetSpeedPU(Motor_Params.CommAng);
            }
            if(Motor_Params.FOC_DQ_Decouple==true)
            {
                Omega_e = rc1.SetpointValue*Motor_Params.BaseSpeed*(Motor_Params.Poles/2.0)*(2.0*PI/60.0);
            }
            PID_MACRO(pid_spd)
            SpeedLoopCount=1;
        }
        else
        {
            SpeedLoopCount++;
        }
#if(!SYNCMODE_FIX)
    }
#endif
}

void LoadIntegrators(void)
{
    pi_id.i1     = Motor_Params.id_integrator ;   // change
    pi_iq.i1     = Motor_Params.iq_integrator;   // change
    pid_spd.data.i1 = Motor_Params.spd_integrator; /// change later

    pi_id.Out    = 0.0;
    pi_id.v1     = 0.0;
    pi_id.w1     = 0;
    pi_id.ui     = 0.0;
    pi_id.up     = 0.0;

    pi_iq.Out    = 0.0;
    pi_iq.v1     = 0.0;
    pi_iq.w1     = 0;
    pi_iq.ui     = 0.0;
    pi_iq.up     = 0.0;
}

void IntegratorWatch(unsigned long state_ticker)
{
    if(state_ticker < 100)
    {
        if( element < 10 )
        {
            element++;
            addIQ(pi_iq.i1, element);
            addID(pi_id.i1, element);
            addSpd(pid_spd.data.i1, element); /// change later
        }
    }
    else if(state_ticker >= 100 && state_ticker <200)
    {
        // Calculate the average and send through CAN, SCI, or expressions window
        avgIntegrator(gWatchID, element, 0);
        avgIntegrator(gWatchIQ, element, 1);
        avgIntegrator(gWatchSpd, element, 2);
        element = 0;
    }
}

void Run_PI_Current(enum BuildLevels BuildLevel, enum HighMotorControlVar MCStateParam, float ControllerRefParam, enum SyncMode syncMode, float masterValue2)
{
    // ------------------------------------------------------------------------------
    //    Connect inputs of the PI IQ (quadrature) current loop and call PI macro
    // ------------------------------------------------------------------------------
#if(!SYNCMODE_FIX)
    if(isMasterOrSolo(syncMode))
    {
#endif
        if(MCStateParam==e_MCRamp)
        {
            pi_iq.Ref = Motor_Params.Start_IQOL;
        }
        else if(MCStateParam==e_MCCommutate)
        {
            if(BuildLevel==e_TorqueMode)
            {
                pi_iq.Ref = ControllerRefParam;
            }
            else if(BuildLevel==e_SpeedLoop)
            {
                //Possible cases:
                //  SpeedLoopEn = {0,1}
                //  PosLoopEn = {0,1}
                // gPositionFlag = {0,1}

                // Position Loop is running
                if (PosLoopEn || ( RotorSyncEn && gPositionFlag))
                {
                    //Todo - incorporate
                    pi_iq.Ref = pid_pos.term.Out * Inverter_Params.Sense_Current_M;
                }
                // Speed loop is running
                else if ( (SpeedLoopEn  && ~PosLoopEn && ~RotorSyncEn) || (RotorSyncEn && (~gPositionFlag)) )
                {
                    pi_iq.Ref = pid_spd.term.Out* Inverter_Params.Sense_Current_M;
                }
                // Must be level6 torque loop if neither speed nor torque enabled
                else
                {
                    pi_iq.Ref = ControllerRefParam;
                }
            }
        }
        else
        {
            pi_iq.Ref = 0.0;
        }
#if(!SYNCMODE_FIX)
    }
    else
    {
        pi_iq.Ref = masterValue2;
    }
#endif

    pi_iq.Fbk = park1.Qs;

    if(MCStateParam!=e_MCLockRotor)
    {
        PI_MACRO(pi_iq);

        if(Motor_Params.FOC_DQ_Decouple==true)
        {
            int_iq.Ki=Omega_e*Motor_Params.DQDC_FFGain*Motor_Params.T_LOOP;
            int_iq.Err=pi_iq.Ref - pi_iq.Fbk;
            INTEGRATOR_MACRO(int_iq)
        }
    }

    // ------------------------------------------------------------------------------
    //    Connect inputs of the PI ID (direct) current loop and call PI macro
    // ------------------------------------------------------------------------------
#if(!SYNCMODE_FIX)
    if(isMasterOrSolo(syncMode))
    {
#endif
        if(MCStateParam==e_MCRamp)
        {
            pi_id.Ref = Motor_Params.Start_IDOL;
        }
        else
        {
            pi_id.Ref = 0.0;
        }
#if(!SYNCMODE_FIX)
    }
    else
    {
        pi_id.Ref = 0.0;
    }
#endif
    pi_id.Fbk = park1.Ds;

    if(MCStateParam!=e_MCLockRotor)
    {
        PI_MACRO(pi_id)

        if(Motor_Params.FOC_DQ_Decouple==true)
        {
            int_id.Ki=int_iq.Ki;
            int_id.Err=pi_id.Ref - pi_id.Fbk;
            INTEGRATOR_MACRO(int_id)
        }
    }
}

void RunAngleEstimators(enum Observers_Angles ObsEn, float VoltageBusNorm)
{
    //20191115 VV - Moved Run_QEP and Run_Hall to main ISR loop below as they're always accessible
    if(ObsEn & SL_ORTEGA)
    {
        obs1.Ialpha = clarke1.Alpha;
        obs1.Ibeta  = clarke1.Beta;
        obs1.Valpha = ipark1.Alpha;
        obs1.Vbeta  = ipark1.Beta;

        /*  Y inputs to oberserver equation (4) in Ortega */
        obs1.Yalpha = obs1.Valpha - obs1.Rmotor*obs1.Ialpha;
        obs1.Ybeta = obs1.Vbeta - obs1.Rmotor*obs1.Ibeta;

        /*  ETA   */
        /* ETA(Xh) = Xh - L*Ialphabeta   equation (6)*/
        obs1.ETAXhalpha =  obs1.Xhalpha - obs1.Lmotor*obs1.Ialpha;
        obs1.ETAXhbeta =  obs1.Xhbeta - obs1.Lmotor*obs1.Ibeta;

        /* Observer -- equation (8)  */
        obs1.VecDistance = obs1.PsiSquared - (powf(obs1.ETAXhalpha,2)+powf(obs1.ETAXhbeta,2));

        obs1.Xhalpha = obs1.Xhalpha + obs1.Tloop*(obs1.Yalpha+(obs1.Gamma/2.0)*obs1.ETAXhalpha*obs1.VecDistance);
        obs1.Xhbeta = obs1.Xhbeta + obs1.Tloop*(obs1.Ybeta+(obs1.Gamma/2.0)*obs1.ETAXhbeta*obs1.VecDistance);

        /*  Rotor angle calculator -> Theta = atan( (xh2-Lib)/(xh1-Lia)) Eq (9) in Ortega  */
        obs1.Theta = _IQatan2PU( (obs1.Xhbeta-obs1.Lmotor*obs1.Ibeta),(obs1.Xhalpha-obs1.Lmotor*obs1.Ialpha));
    }

    if(ObsEn & SL_SMOPOS)
    {
        // -----------------------------------------------------------------------------
        //    Connect inputs of the VOLT_CALC module and call the phase voltage macro
        // ------------------------------------------------------------------------------
        volt1.DcBusVolt = VoltageBusNorm;

        volt1.MfuncV1 = svgen1.Ta;
        volt1.MfuncV2 = svgen1.Tb;
        volt1.MfuncV3 = svgen1.Tc;
        PHASEVOLT_MACRO(volt1)

        // ------------------------------------------------------------------------------
        //    Connect inputs of the SMO_POS module and call the sliding-mode observer macro
        // ------------------------------------------------------------------------------

        smo1.Ialpha = clarke1.Alpha;
        smo1.Ibeta  = clarke1.Beta;
        smo1.Valpha = volt1.Valpha;
        smo1.Vbeta  = volt1.Vbeta;
        SMO_MACRO(smo1)
    }
}

void RunSpeedEstimators(enum HighMotorControlVar MCStateParam, enum SyncMode syncMode, enum Observers_Angles ObsEn, enum BuildLevels BuildLevel)
{
    if(ObsEn & (SL_ORTEGA | SL_SMOPOS | SNS_HALLPLL | SNS_ENC_SSI) )
    {
        // ------------------------------------------------------------------------------
        //    Connect inputs of the SPEED_EST module and call the estimated speed macro
        //      speed3 is from the estimated FOC angle
        // ------------------------------------------------------------------------------

        speed3.EstimatedTheta = GetThetaElec(Motor_Params.CommAng);
        SE_MACRO(speed3)

        if( ( ( MCStateParam == e_MCRamp && rc1.SetpointValue <_IQ(Motor_Params.Start_TransitionRpm*(Motor_Params.Poles/2.0/60.0/Motor_Params.BaseFrequency)*0.85) ) || (MCStateParam == e_MCLockRotor) )
#if(!SYNCMODE_FIX)
                && (syncMode != PARALLEL_SLAVE)
#endif
                )
        {
            speed3.EstimatedSpeed = 0.0;        // Zero estimated speed when it doesn't make sense or during filter transients at startup
        }
        else if( ( (MCStateParam == e_MCRamp) || (MCStateParam == e_MCLockRotor) )
#if(!SYNCMODE_FIX)
                && syncMode == PARALLEL_SLAVE
#endif
        )
        {
            speed3.EstimatedSpeed = 0.0;        // Zero estimated speed when it doesn't make sense or during filter transients at startup
        }
        else if ( isTorqueModeWithPositionSensor(BuildLevel, Motor_Params.CommAng) )
        {
            speed3.EstimatedSpeed = 0.0;
        }

        // Call estimated speed derivative module (nominally acceleration, but if angle est fails then speed est will change rapidly)
        acceleration.EstimatedSpeed = speed3.EstimatedSpeed;
        SED_MACRO(acceleration)
    }

    if(ObsEn & SNS_ENC_QEP)
    {
        // ------------------------------------------------------------------------------
        //    Connect inputs of the SPEED_FR module and call the speed calculation macro
        //  speed1 is from the QEP module (which we don't have right now...)
        // ------------------------------------------------------------------------------
        speed1.ElecTheta = GetThetaElec(Motor_Params.CommAng);
        speed1.DirectionQep = (int32)(getDirectionQEP());
        SPEED_FR_MACRO(speed1);
    }
}

int CheckRampTransition(enum Observers_Angles src)
{
    if( fabs( rc1.SetpointValue - GetSpeedPU(Motor_Params.CommAng) ) <= Motor_Params.Start_SpeedTol )
    {
        if(src==SNS_ENC_QEP)
        {
            return (getQEPIndexSyncFlag() == 0xF0);
        }
        else if( src & (SL_ORTEGA | SL_SMOPOS | SNS_HALLPLL))
        {
            return ( fabs( rg1.Out - getObsMechTheta() ) <= Motor_Params.Start_AngTol);
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}

void MainControlLoop(enum BuildLevels BuildLevel, enum MainStateVariables StateMain, enum HighMotorControlVar MCStateParam, enum CritFaultVariables FaultParam, float SpeedRefParam) // rename speedref to control ref
{
    static int PWMEnableWait;

    //This is here because we want a good angle whether or not we are in a PWM state.
    //Might want to remove this and just run angle estimators before we run the current loops in the motorcalculations?
    switch(ControllerState)
    {
        case MC_Init:
            if( StateMain != Main_Active)
            {
                ControllerState = MC_ProcessGatesDisabled;
            }
            else
            {
                ControllerState = MC_Init;
            }

            //Disable PWM and reset controller
            //We should only be here for one interrupt cycle
            Reset_Controller();
            Disable_PWM();
        break;

        case MC_ProcessGatesDisabled:
            // Disable gate drive and go to WaitForMain state.
            Disable_gate_drive();
            ControllerState = MC_WaitForParent;
        break;

        case MC_WaitForParent:
            // At this state we wait for main to catch up.
            if( StateMain == Main_Inactive )
            {
                ControllerState = MC_GatesDisable;
            }
            else
            {
                ControllerState = MC_WaitForParent;
            }
        break;

        case MC_GatesDisable:
            // If main is at any of the pwm states then enable gate driver, otherwise stay off.
            if( StateMain == Main_Active )
            {
                //Todo - I put this under an inverter check but we may want a generic flag for 'single pulse testing mode' (VV)
                if(TestParams.active==true)
                {
                    PWM_Out(-1.0,-1.0,-1.0);        // For single pulse testing, want to enable with all high side off, all low side on to get first cycle clean
                    PWMEnableWait=0;
                    m_pwmOnLoopCounts = TestParams.StepCycles;      // Set up PWM cycle counter in case we are doing LEVEL1 testing
                }
                Enable_gate_drive();
                ControllerState = MC_ProcessGatesEnable;
            }
            else
            {
                ControllerState = MC_GatesDisable;
            }
        break;

        case MC_ProcessGatesEnable:
            // If not in pwm states OR there is a fault, then disable gate drive and go wait state.
            if( (StateMain != Main_Active ) || (FaultParam != FLT_CRIT_NONE) )
            {
                Disable_gate_drive();
                ControllerState = MC_WaitForParent;
            }
            // If pwm states, enable pwm
            else if( StateMain == Main_Active)
            {
                //Todo - I put this under an inverter check but we may want a generic flag for 'single pulse testing mode' (VV)
                if(TestParams.active==true)
                {
                    PWMEnableWait++;
                    if(PWMEnableWait>5)
                    {
                        PWMEnableWait = 0;
                        PWM_Out(-1.0,-1.0,-1.0);        // For single pulse testing, want to enable with all high side off, all low side on to get first cycle clean
                        Enable_PWM();
                        ControllerState = MC_PWMEnable;
                    }
                }
                else
                {
                    //Run motorCalculations once here to define PWMs before enabling
                    motorCalculations(BuildLevel, MCStateParam, SpeedRefParam);
                    Enable_PWM();
                    ControllerState = MC_PWMEnable;
                }
            }
            // Otherwise, stay in ProcessPWMEnable
            else
            {
                ControllerState = MC_ProcessGatesEnable;
            }
        break;

        case MC_PWMEnable:
            if( (StateMain != Main_Active) || (FaultParam != FLT_CRIT_NONE) )
            {
                Disable_PWM();
                Reset_Controller();
                ControllerState = MC_ProcessGatesDisabled;
            }
            else
            {
                //Todo - I put this under an inverter check but we may want a generic flag for 'single pulse testing mode' (VV)
                if(TestParams.active==true)
                {
                    motorCalculations(BuildLevel, MCStateParam, SpeedRefParam);
                    if(m_pwmOnLoopCounts > 0) // NOTE: doing >= might save a clock pulse or two if it comes to that
                    {
                        m_pwmOnLoopCounts--;
                    }

                    if( (StateMain != Main_Active) || (FaultParam != FLT_CRIT_NONE) || (m_pwmOnLoopCounts == 0) )
                    {
                        Disable_PWM();
                        Reset_Controller();
                        ControllerState = MC_ProcessGatesDisabled;
                    }
                }

                else
                {
                    // Do stuff when in LockRotor, Ramp, or e_MCCommutate state
                    //I moved this calculation after the check for faults so that we can save a potentially dangerous interrupt cycle of a PWM_Out call
                    //Since it previously was going through a control loop cycle & outputting before checking for faults (VV)
                    motorCalculations(BuildLevel, MCStateParam, SpeedRefParam);
                }
            }
        break;

        default:
            // Go to InitController
            // error(INVALID_STATE);
            ControllerState = MC_Init;
        break;
    }// End of switch
    // Let PrevState track Main state
}

// Checks if PID Position is enabled
bool isPIDPositionEnabled()
{
    return (Motor_Params.LoopsEn & MC_PID_POS);
}

// Check if Observer angle is set to the passing parameter
bool isObserverAngleSetTo(enum Observers_Angles ObserverAngle)
{
    return (Motor_Params.ObsEn & ObserverAngle);
}

// Checks if PI Speed is enabled
bool isPISpeedEnabled()
{
    return (Motor_Params.LoopsEn & MC_PI_SPEED);
}

// Checks if PI Current is enabled
bool isPICurrentEnabled()
{
    return (Motor_Params.LoopsEn & MC_PI_CURRENT);
}

//////////////////////////////////////////////////////////////////////
//
//  Main Controller code here
//
//////////////////////////////////////////////////////////////////////
void motorCalculations(enum BuildLevels BuildLevel, enum HighMotorControlVar MCStateParam, float ControllerRefParam)
{
    float VoltageBusNorm = 0;
    DINT;   // Capture configuration/settings for all loops "atomically" (CAN message could change these asynchronously)
    SpeedLoopEn = (isPISpeedEnabled() != 0);
    PosLoopEn = (isPIDPositionEnabled() != 0);
    CurLoopEn = (isPICurrentEnabled()!= 0);
#if(!SYNCMODE_FIX)
    RotorSyncEn = (bool) (getRotorSyncMode() );
#endif
    EINT;

    if(MCStateParam == e_MCLockRotor || MCStateParam == e_MCRamp || MCStateParam == e_MCCommutate)
    {
        VoltageBusNorm = adcGetSignalValue(adc_id_vdc)/sqrt(3.0);

        // Switch from sensored to sensorless
        if(isSensorlessTriggered())
        {
            obs1.Theta = GetThetaElec(Motor_Params.CommAng);
            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SL_ORTEGA;
        }

        if(BuildLevel==e_OpenLoop)
        {
            // ------------------------------------------------------------------------------
            //  In LEVEL1 with TestParams.SourceSel = STEP
            //  simply use the watch window to set TestParams.Ta,TestParams.Tb,TestParams.Tc
            //  to the  desired counter-timer values to get the PWM on the gates desired
            //  TestParams.StepU,TestParams.StepV,TestParams.StepW [-1,1] as floats
            //  Default on startup is Ta, Tb, Tc = -1 which gives 0% duty cycle on all phases = LV rail constant V on all phases
            //  TestParams.StepCycles sets the number of PWM cycles to run before disabling again.  If StepCyles is set negative
            //  step function PWM will run continuously until the "Start" button is released ("dead-person" functionality)
            //
            // Ta = 1.0 gives PWM GPIO 0 at 100% i.e. high gate on, Ta = -1.0 gives PWM Pin GPIO 0 at 0% i.e. low gate on
            // So initial test configuration should be Ta = -1.0, Tb = -1.0, Tc = -1.0 -- all low side gates ON.
            //  Then slowly increase Ta from -1.0 to get more and more "on" time (high side ON) for phase A
            //  Can also test negative polarity by starting with Ta, Tb = 1.0 (so A and B high side gates 100% on)
            //      Then decrease Ta slowly to get some of phase A low gate turned on and driving a negative current
            // Once phase A switching looks OK, go to phase B with Ta = -1.0 and Tb starting at -1.0 and increasing for positive phase B current (into phase A)
            //      Then Ta, Tb = 1.0 with Tb slowly decreased to test negative phase B current
            // Once phase A and B look OK then try symmetric waveforms
            //      Start with Ta = Tb = 0.0 which will make 50% duty cycle in-phase on both A and B (0 current should flow)
            //      Then increase (decrease) Ta while doing the opposite decreasing (increasing) Tb to make symmetric PWM waveforms
            // Then repeat but using A-C or B-C phases
            //   Also check that the A/D measured current matches what is expected (i.e. value from current probe) and is not too noisy;
            //   Also should check that gain and offset (zero) on ADC input is correct-- can be adjusted in the main ISR routine
            //  where the ADC reading is scaled and offset to make PhaseIA and PhaseIB
            // ------------------------------------------------------------------------------

            //Todo - I put this under an inverter check but we may want a generic flag for 'single pulse testing mode' (VV)
            if(TestParams.active==true)
            {
                if(TestParams.Source == Step)
                {
                    PWM_Out(TestParams.StepU,TestParams.StepV,TestParams.StepW);
                }
                else if( (TestParams.Source == SineDebugger) || (TestParams.Source == SineFromREF) )
                {
                    //  Run the ramp generator to generate a normalize angle "rotating" 0->1 at the desired frequency

                    rg1.Freq = TestParams.SineFreq;
                    RG_MACRO(rg1)

                    ipark1.Ds = 0.0;
                    if (TestParams.Source == SineDebugger)
                    {
                        ipark1.Qs = TestParams.SineAmplitude;
                    }
                    else if (TestParams.Source == SineFromREF)
                    {
                        ipark1.Qs = ControllerRefParam;
                    }

                    ipark1.Sine= __sinpuf32(rg1.Out);
                    ipark1.Cosine=__cospuf32(rg1.Out);
                    IPARK_MACRO(ipark1)

                    svgen1.Ualpha = ipark1.Alpha;
                    svgen1.Ubeta = ipark1.Beta;

                    // Ensure that we can't drive greater than 100 percent duty cycle
                    if( (powf(svgen1.Ualpha,2) + powf(svgen1.Ubeta,2)) > 1.0)
                    {
                        float temp_magnitude = sqrt( powf(svgen1.Ualpha,2) + powf(svgen1.Ubeta,2) );
                        svgen1.Ualpha = svgen1.Ualpha/temp_magnitude;
                        svgen1.Ubeta = svgen1.Ubeta/temp_magnitude;
                    }
                }

                SVGENDQ_MACRO(svgen1)

                // ------------------------------------------------------------------------------
                //  Connect inputs of the PWM_DRV module and call the PWM signal generation macro
                // ------------------------------------------------------------------------------

                PWM_Out(svgen1.Ta,svgen1.Tb,svgen1.Tc);

            }
        }

        // =============================== LEVEL 2 ======================================
        //    Level 2 verifies
        //    clarke/park transformations (CLARKE/PARK), phase voltage calculations,
        //   and verifies the dq-axis current regulation performed by PI current regulation loops
        // ==============================================================================

        else if(BuildLevel==e_CurrentLoopConstDandQ)
        {
            // ------------------------------------------------------------------------------
            //  Measure phase currents
            //  Connect inputs of the CLARKE module and call the clarke transformation macro
            //  Clarke transforms 3 inputs currents @ 120 degrees to two currents @ 90 degrees
            // ------------------------------------------------------------------------------
            clarke1.As= adcGetSignalValue(adc_id_iu); // Phase A curr.
            clarke1.Bs= adcGetSignalValue(adc_id_iv); // Phase B curr.
            clarke1.Cs= adcGetSignalValue(adc_id_iw); // Phase C curr.

            CLARKE_MACRO_3C(clarke1)

            // ------------------------------------------------------------------------------
            //  Connect inputs of the PARK module and call the park trans. macro
            //  Park goes from stator frame into (rotating) rotor frame
            // ------------------------------------------------------------------------------
            park1.Alpha = clarke1.Alpha;
            park1.Beta = clarke1.Beta;

            park1.Angle = Motor_Params.Start_LockRotorAngle;                    //Angle is 0 by definition in LockRotor state


            park1.Sine = __sinpuf32(park1.Angle);
            park1.Cosine = __cospuf32(park1.Angle);

            PARK_MACRO(park1)

            // ------------------------------------------------------------------------------
            //    Connect inputs of the PI module and call the PI IQ (Quadrature current) controller macro (MR - IQ controls torque in closed loop mode)
            // ------------------------------------------------------------------------------
            pi_iq.Ref = 0;                      // Set point for Q current is 0
            pi_iq.Fbk = park1.Qs;
            PI_MACRO(pi_iq)

            // ------------------------------------------------------------------------------
            //    Connect inputs of the PI module and call the PI ID (Direct current) controller macro - zero once spinning and angle loop closed (MR)
            // ------------------------------------------------------------------------------
            pi_id.Ref=Motor_Params.Start_IDOL;
            pi_id.Fbk = park1.Ds;
            PI_MACRO(pi_id)

            // ------------------------------------------------------------------------------
            //  Connect inputs of the INV_PARK module and call the inverse park trans. macro
            // ------------------------------------------------------------------------------
            //  Outputs of the PI for Q and D are the Q and D voltages that need to be applied
            //  So transform from D, Q axes to Alpha - Beta axes for voltages to be applied to motor to control currents
            ipark1.Ds = pi_id.Out;
            ipark1.Qs = pi_iq.Out;
            ipark1.Sine=park1.Sine;                         // Sine and cosine already computed when we did park, so simply re-use here
            ipark1.Cosine=park1.Cosine;
            IPARK_MACRO(ipark1)

            // ------------------------------------------------------------------------------
            //  Connect inputs of the SVGEN_DQ module and call the space-vector gen. macro
            // ------------------------------------------------------------------------------
            //  svgen1 will transform alpha and beta voltages into a, b, and c PWM times to generate the 3 terminal voltages
            svgen1.Ualpha = ipark1.Alpha / VoltageBusNorm;
            svgen1.Ubeta = ipark1.Beta / VoltageBusNorm;
            SVGENDQ_MACRO(svgen1)

            // ------------------------------------------------------------------------------
            //  Connect inputs of the PWM_DRV module and call the PWM signal generation macro
            // ------------------------------------------------------------------------------
            PWM_Out(svgen1.Ta,svgen1.Tb,svgen1.Tc);                         // Output the new PWM values
        }

        // =============================== LEVEL 3 ======================================
        //  Level 3 --  3a)Tune and set open loop commutation for startup (IdRef, IqRef, start timing, speed, delays)
        //              3b) Check that angle estimation is working well enough during open loop
        //              that we will be able to use estimated angle for commutation.
        // ==============================================================================
        // ------------------------------------------------------------------------------
        //   No Speed Controller at this build level
        //   Instead just set iq_ref and id_ref with debugger to set
        //   Desired torque loop current that will be commutated around the stator
        //
        // Use speed knob to change speed that open loop ramp goes up to, once found a good speed put that
        // into INITIAL_CL_SPEED and move to level 4

        else if(BuildLevel >= e_CurrentLoopNoAngleSense)
        {
            // ------------------------------------------------------------------------------
            //  Connect inputs of ramp controller and ramp generator modules
            // ------------------------------------------------------------------------------
#if(!SYNCMODE_FIX)
            if(isMasterOrSolo(syncMode) )
            {
#endif
                if(MCStateParam==e_MCRamp)
                {
                    if(BuildLevel==e_CurrentLoopNoAngleSense)
                    {
                        rc1.TargetValue = ControllerRefParam;
                    }
                    else
                    {
                        rc1.TargetValue = Motor_Params.Start_TransitionRpm*(Motor_Params.Poles/2.0/60.0/Motor_Params.BaseFrequency);
                    }
                }
                else if(MCStateParam==e_MCCommutate)
                {
                    rc1.TargetValue = ControllerRefParam;
                    rc1.RampDelayMax = SetRampDelay(acceleration.EstimatedSpeedDerivative);
                }
                else
                {
                    rc1.TargetValue=0.0;
                }
#if(!SYNCMODE_FIX)
            }
            else
            {
                rc1.TargetValue = 0.0;
            }
#endif
            RC_MACRO(rc1)

#if(!SYNCMODE_FIX)
            //Copied from latest UT Austin code (VV)
            if(syncMode == FOLLOWER_MASTER)
            {
                pid_pll.term.Ref = masterValue1;
                pid_pll.term.Fbk = rg1.Out;
                PID_POS_MACRO(pid_pll)
            }
#endif

            // ------------------------------------------------------------------------------
            //  Connect inputs of the RAMP GEN module and call the ramp generator macro
            // ------------------------------------------------------------------------------
            rg1.Freq = rc1.SetpointValue;
            RG_MACRO(rg1)

            // ------------------------------------------------------------------------------
            //  Measure phase currents, subtract the offset and normalize from (-0.5,+0.5) to (-1,+1).
            //  Connect inputs of the CLARKE module and call the clarke transformation macro
            //  Clarke transforms 3 inputs currents @ 120 degrees to two currents @ 90 degrees
            // ------------------------------------------------------------------------------
            clarke1.As= adcGetSignalValue(adc_id_iu); // Phase A curr.
            clarke1.Bs= adcGetSignalValue(adc_id_iv); // Phase B curr.
            clarke1.Cs= adcGetSignalValue(adc_id_iw); // Phase C curr.
            CLARKE_MACRO_3C(clarke1)

            // ------------------------------------------------------------------------------
            //  Connect inputs of the PARK module and call the park trans. macro
            //  Park goes from stator frame into (rotating) rotor frame
            // ------------------------------------------------------------------------------
            park1.Alpha = clarke1.Alpha;
            park1.Beta = clarke1.Beta;

            // -----------------------------------------------------
            // Select park1.Angle source based on state and syncMode
            // -----------------------------------------------------
            if(MCStateParam==e_MCRamp)
            {
                //In ramp mode, park1.Angle is open loop, from ramp generator.
#if(!SYNCMODE_FIX)
                //Master will generate this signal and send it to the slave
                if(isMasterOrSolo(syncMode))
                {
                    park1.Angle = convertMechToElecTheta(rg1.Out);
                }
                else
                {
                    park1.Angle = masterValue1; //Ramp angle + slave phase shift sent by master
                }
#endif
                park1.Angle = convertMechToElecTheta(rg1.Out);
            }
            else if(MCStateParam==e_MCCommutate)
            {
                park1.Angle = GetThetaElec(Motor_Params.CommAng);   //Master and slave use own sensored/sensorless angle
            }
            else
            {
                park1.Angle = 0.0;                 //LockRotor angle is 0 by definition
            }
            park1.Sine = __sinpuf32(park1.Angle);
            park1.Cosine = __cospuf32(park1.Angle);

            PARK_MACRO(park1)

            // Now run highest level control loop -- either position OR speed (not both) if one of them is enabled
            // If in current mode, then neither will be enabled


            if((MCStateParam==e_MCCommutate) && (BuildLevel == e_SpeedLoop))    // Highest level loops only run in e_MCCommutate && LEVEL6
            {
                // Check for Rotor Sync going in or out of sync mode
                positionAndSpeedSelector(MCStateParam, ControllerRefParam);

                gAngleError = GenerateAngleError(syncMode, masterValue1);       /* DEBUG --temporarily calculate position error all the time, even when not in rotor sync */

                // Note that PosLoopEn, SpeedLoopEn, CurLoopEn, and RotorSyncEn are not set up very well right now, there are too many
                // combinations of things being enabled that don't make sense and that the logic below won't capture
                // Use care with setting these presently
                if( PosLoopEn)
                {
                    // True servo loop positing mode would go here, not implemented yet
                    //  Just output something safe like 0 for right now
                    pid_pos.term.Out = 0.0;
                }
                else if (RotorSyncEn)
                {
                    // ------------------------------------------------------------------------------
                    //   If rotor sync control is enabled, generate angle error (should this be global?)
                     // ------------------------------------------------------------------------------
                     selectAngleOffset(Inverter_Params.AngleCommandSelect);

                     gAngleError = GenerateAngleError(syncMode, masterValue1);
     #ifndef FixMeLater
                     // PWMDAc out scaleAndOutput( source, offset, gain, max, min)
                     scaleAndOutput(getObsMechTheta(), mechAngle.offset, mechAngle.gain, 1.0, 0.0);
     #endif
                     // ------------------------------------------------------------------------------
                     // Check if rotors are within sync speed, set gPositionflag if they are
                     // If (gPositionFlag) then Run PID position loop based on enabled observers
                     // ------------------------------------------------------------------------------

                     runPositionLoop(MCStateParam, ControllerRefParam);    /*  Does not run PID_POS_MACRO if gPositionFlag = 0 */
                }

                if ( (SpeedLoopEn  && ~RotorSyncEn && ~PosLoopEn) || (RotorSyncEn && ~gPositionFlag ) )
                    // PosLoopEn takes pre5cendence over SpeedLoopEn, but gPositionFlag == 0 means PID_POSITION is not running so need to run speed loop
                {
                    // ------------------------------------------------------------------------------
                    //   Run PID speed loop (masters controller only)
                    // ------------------------------------------------------------------------------
                    Run_PID_Speed(syncMode);            //Function includes prescalar etc.
                }
                else
                {
                    // add torque mode later -George
                }
            }


            // =============================== LEVEL 4 ======================================
            //    Level 4 -- validate commuation sensorlessly -- in torque mode with current set from SpeedRef (speed knob)
            //      basically the same as level 3, only startup state machine
            //      continues transition to commutate off of estimated angle once open loop
            //      ramp has matched speed and one instant of estimated angle = open loop angle
            // ==============================================================================
            //   No Speed Controller at this build level
            // Speed knob ("SpeedRef") is used to set the desired quadrature current loop (so we are in torque control mode)
            // Right now speedref/6 is the value that will go into the IQ control loop command/reference for current to be commutated with estimated angle
            // ------------------------------------------------------------------------------
            //  *******Should add a speed set point above which the current will shut off to avoid a runaway
            //          condition... since torque mode with unloaded motor will likely result in uncontrolled speed/acceleration


            // Add a level 5 where the CL speed transition is set by the knob so that speed doesn't change when transitioning
            // to closed loop, just takes over closed loop commutation, then knob can change speed from there


                // =============================== LEVEL 6 ======================================
                //    Level 6 verifies the speed regulator performed by PI module.
                //    The system speed loop is closed by using the estimated speed as a feedback.
                // ==============================================================================
                //  StateSignal == LockRotor: lock the rotor of the motor
                //  StateSignal == Ramp: close the current loop
                //  StateSignal == e_MCCommutate: close the speed loop
                //  StateSignal anything else == set speed to zero, Estop()

            // ------------------------------------------------------------------------------
            //    Connect inputs of the PI current loops and call the PI macro
            // ------------------------------------------------------------------------------
            if(BuildLevel!=e_VoltageLoop)
            {
                Run_PI_Current(BuildLevel, MCStateParam, ControllerRefParam, syncMode, masterValue2);

                // ------------------------------------------------------------------------------
                //  Connect inputs of the INV_PARK module and call the inverse park trans. macro
                // ------------------------------------------------------------------------------
                ipark1.Ds = CurLoopEn*(pi_id.Out - Motor_Params.FOC_DQ_Decouple*int_iq.Out);
                ipark1.Qs = CurLoopEn*(pi_iq.Out + Motor_Params.FOC_DQ_Decouple*int_id.Out);
            }
            else    //LEVEL4 Does not run a current loop
            {
                // ------------------------------------------------------------------------------
                //  Connect inputs of the INV_PARK module and call the inverse park trans. macro
                // ------------------------------------------------------------------------------
#if(!SYNCMODE_FIX)
                if(isMasterOrSolo(syncMode))
                {
#endif
                    if(MCStateParam==e_MCRamp)
                    {
                        ipark1.Qs = Motor_Params.Start_IQOL;
                        ipark1.Ds = Motor_Params.Start_IDOL;
                    }
                    else if(MCStateParam==e_MCCommutate)
                    {
                        ipark1.Qs = rc1.SetpointValue*Inverter_Params.Sense_Voltage_M;
                        ipark1.Ds = 0.0;
                    }
#else
                }
                else
                {
                    ipark1.Ds = 0;
                    ipark1.Qs = masterValue2;
                }
#endif
            }

            ipark1.Sine=park1.Sine;
            ipark1.Cosine=park1.Cosine;
            IPARK_MACRO(ipark1)

            // --------------------------------------------------------------------------------------------
            //    Run all enabled angle estimators (Motor_Params.CommAng will be used for speed estimation)
            // --------------------------------------------------------------------------------------------
            RunAngleEstimators(Motor_Params.CommAng, VoltageBusNorm);

            // --------------------------------------------------------------------------------------------
            //    Convert electrical to mechanical
            // --------------------------------------------------------------------------------------------
            if( ( (Motor_Params.ObsEn & SL_SMOPOS) || (Motor_Params.ObsEn & SL_ORTEGA) ) || (Motor_Params.ObsEn & SNS_ONCEAROUND) )
            {
                runConvertElecToMech( GetThetaElec(Motor_Params.CommAng) );
            }

            // --------------------------------------------------------------------------------------------
            //    Run all enabled speed estimators (Motor_Params.CommAng will be used for speed estimation)
            // --------------------------------------------------------------------------------------------
            RunSpeedEstimators(MCStateParam, syncMode, Motor_Params.CommAng, BuildLevel);

            // ------------------------------------------------------------------------------
            //  Connect inputs of the SVGEN_DQ module and call the space-vector gen. macro
            // ------------------------------------------------------------------------------
            if(MCStateParam==e_MCLockRotor)
            {
#if(!SYNCMODE_FIX)
                if(isMasterOrSolo(syncMode))
                {
#endif
                    if(VoltageBusNorm != 0)
                    {
                        svgen1.Ualpha = Motor_Params.Start_VLockRotor / VoltageBusNorm;
                        svgen1.Ubeta = 0.0;
                    }
                    else
                    {
                        svgen1.Ualpha = 0.0;
                        svgen1.Ubeta = 0.0;
                        // error(DIVIDE_BY_ZERO);
                    }

                    if( element3 < 200 )
                    {
                        element3++;
                        addUAlpha(svgen1.Ualpha, element3);
                        addUBeta(svgen1.Ubeta, element3);
                    }
#if(!SYNCMODE_FIX)
                }
                else
                {
                    svgen1.Ualpha = masterValue1/ VoltageBusNorm;
                    svgen1.Ubeta  = masterValue2;

                    if( element3 < 200 )
                    {
                        element3++;
                        addUAlpha(svgen1.Ualpha, element3);
                        addUBeta(svgen1.Ubeta, element3);
                    }
                }
#endif
            }
            else
            {
                element3 = 0;
                if(VoltageBusNorm != 0)
                {
                    svgen1.Ualpha = ipark1.Alpha / VoltageBusNorm;
                    svgen1.Ubeta = ipark1.Beta / VoltageBusNorm;
                }
                else
                {
                    svgen1.Ualpha = 0.0;
                    svgen1.Ubeta = 0.0;
                    // error(DIVIDE_BY_ZERO);
                }
            }

            // Ensure that we can't drive greater than 100 percent duty cycle
            if( (powf(svgen1.Ualpha,2) + powf(svgen1.Ubeta,2)) > 1.0)
            {
                float temp_magnitude = sqrt( powf(svgen1.Ualpha,2) + powf(svgen1.Ubeta,2) );
                svgen1.Ualpha = svgen1.Ualpha/temp_magnitude;
                svgen1.Ubeta = svgen1.Ubeta/temp_magnitude;
            }

            SVGENDQ_MACRO(svgen1)

            // ------------------------------------------------------------------------------
            //  Connect inputs of the PWM_DRV module and call the PWM signal generation macro
            // ------------------------------------------------------------------------------

            PWM_Out(svgen1.Ta,svgen1.Tb,svgen1.Tc);
        }   //End of LEVEL3-6
    }   //End of MCStateParam == Lock/Ramp/Comm/Regen
    // ------------------------------------------------------------------------------
    //    Connect inputs of the DATALOG module
    // ------------------------------------------------------------------------------
#ifndef FixMeLater
    float encodervsSensorless =  obsMechTheta - getQEPMechAngle();
    // Both thetas are between 0 and 1. Therefore, the largest error is -0.5 and 0.5.
    // Anything that goes over that, is going to "the other way."
    if( encodervsSensorless >= 0.5)
    {
        encodervsSensorless -= 1.0;
    }
    else if( encodervsSensorless <= -0.5)
    {
        encodervsSensorless += 1.0;
    }
#endif

//    // dataLV1Output outputs through CAN to display data in LV
//    dataLV1Output =  convertMechToElecTheta( SPI_GetSSIAngle() ) - GetThetaElec(Motor_Params.CommAng) + spiOffset;
//    if( dataLV1Output >= 1.0)
//    {
//        dataLV1Output -= 1.0;
//    }
//    else if( dataLV1Output <= 0.0)
//    {
//        dataLV1Output += 1.0;
//    }

//    //These few code of lines is to get the offset when running buildlevel 6 sensorless.
//    dataLV1Output= convertMechToElecTheta(SPI_GetSSIAngle()) - GetThetaElec(Motor_Params.CommAng);
//
//    if( dataLV1Output >= 1)
//    {
//        dataLV1Output -= 1.0;
//    }
//    else if( dataLV1Output <= 0.0)
//    {
//        dataLV1Output += 1.0;
//    }


    float tempA = getObsMechTheta()-SPI_GetSSIAngle();
    if(tempA> 1.0)
    {
        tempA -=1.0;
    }
    else if(tempA <= 0.0)
    {
        tempA += 1.0;
    }


    DlogCh1 = adcGetSignalValue(adc_id_iu);
    DlogCh2 = adcGetSignalValue(adc_id_iv);;
    DlogCh3 = pi_iq.Out;
    DlogCh4 = pi_id.Out;

    Run_Datalog();

}// End of motorCalculations


//Leaving this in so we don't have to rewrite but everything is broken af
enum HighMotorControlVar MotorControlStateMachine(enum BuildLevels BuildLevel, enum MainStateVariables mainSate, enum CritFaultVariables critFault)
{
    static enum HighMotorControlVar m_motorState = e_MCInit;
    inc(&MCHighLevelStateTicker);

    //Run these so we always know our angle
    if(Motor_Params.ObsEn & (SNS_HALLRAW|SNS_HALLPLL))
    {
        Run_Hall();
    }

    if(Motor_Params.ObsEn & SNS_ENC_QEP)
    {
        Run_QEP();
    }

    if(critFault == FLT_CRIT_NONE)
    {
        switch(m_motorState)
        {
            case e_MCWaitForMain:
                if(mainSate == Main_Inactive)
                {
                    Disable_gate_predriver();
                    m_motorState = e_MCDisabled;
                }
                break;
            case e_MCInit:
                if(mainSate == Main_Inactive)
                {
                    Disable_gate_predriver();
                    m_motorState = e_MCDisabled;
                }
                break;
            case e_MCDisabled:
                if(mainSate == Main_Active)
                {
                    Enable_gate_predriver();
                    m_motorState = e_MCCheckRotation;
                }
                break;
            case e_MCCheckRotation:
                if(mainSate == Main_Inactive)
                {
                    Disable_gate_predriver();
                    m_motorState = e_MCDisabled;
                }
                else if(m_hallSensor && (BuildLevel > e_CurrentLoopNoAngleSense))
                {
                    m_motorState = e_MCCommutate;
                }
                else if((adcGetSignalValue(adc_id_vu) < 100) &&
                        (adcGetSignalValue(adc_id_vv) < 100) &&
                        (adcGetSignalValue(adc_id_vw) < 100))
                {
                    m_lockRotorCount = 0;
                    m_motorState = e_MCLockRotor;
                }
                else{}
                break;
            case e_MCLockRotor:
                if(mainSate == Main_Inactive)
                {
                    Disable_gate_predriver();
                    m_motorState = e_MCDisabled;
                }
                else if(m_lockRotorCount > Motor_Params.Start_Hold_Time && (BuildLevel > e_CurrentLoopConstDandQ))
                {
                    m_motorState = e_MCRamp;
                }
                else{}
                //at 200Hz each loop will take 5 ms
                m_lockRotorCount += 5.f;
                break;
            case e_MCRamp:
                if(mainSate == Main_Inactive)
                {
                    Disable_gate_predriver();
                    m_motorState = e_MCDisabled;
                }
                else if( (rc1.EqualFlag == 0x7FFFFFFF) && (BuildLevel > e_CurrentLoopNoAngleSense))
                {
#if 0
                    if(CheckRampTransition(Motor_Params.CommAng))
                    {
                        m_motorState = e_MCCommutate;
                    }
                    else
                    {
                        TransitionFlagOK = false;
                    }
#endif
                    m_motorState = e_MCCommutate;
                }
                else{}
                break;
            case e_MCCommutate:
                if(mainSate == Main_Inactive)
                {
                    Disable_gate_predriver();
                    m_motorState = e_MCDisabled;
                }

                break;
            default:
                m_motorState = e_MCDisabled;
                break;
        }
    }
    else
    {
        if(m_motorState!= e_MCWaitForMain)
        {
            m_motorState = e_MCWaitForMain;
            TransitionFlagOK = true;    //Reset transition flag
        }
    }

    return m_motorState;
}

void setTestParamAmp(float in)
{
    TestParams.SineAmplitude = in;
}

float MCGetIPD() {return ipark1.Ds;}
float MCGetIPQ() {return ipark1.Qs;}
bool MCTransitionOK() {return TransitionFlagOK;}
enum MCLowLevelStateVariables GetControllerState(void) {return ControllerState;}
float GetIPD(void) {return ipark1.Ds;}   // Using ipark1.Ds instead of pi_id.out since there is decoupling added beyond the pi_id.out
float GetIPQ(void) {return ipark1.Qs;}   // Using ipark1.Ds instead of pi_id.out since there is decoupling added beyond the pi_id.out
float GetAccel(void) {return acceleration.EstimatedSpeedDerivative;}

