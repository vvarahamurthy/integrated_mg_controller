/* ============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	Module_Hall.C

Target:			LaunchPoint Motor Controllers

Author:			Vishaal Varahamurty

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle hall sensor inputs
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "Header/GUIC_Header.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1

//Defines

// Function prototypes;
void Run_Hall(void);
//  Module resources - variables here //
uint16_t Hall_Angles[]={360, 240, 300, 120, 60, 180};
//uint16_t Hall_Angles[]={300, 180, 240, 60, 360, 120};
float Hall_Ang=0.0;
float Hall_AngCal=0.5;

// Function Definitions            //
extern bool isMasterOrSolo(enum SyncMode);
extern uint16_t GPIO_ReadPin(uint16_t);

void Run_Hall(void)
{
    unsigned char HallAB, HallBC, HallCA, Hall_Index;
    HallAB = GPIO_ReadPin(Inverter_Params.GPIO_HALLAB_NUM) << 2;
    HallBC = GPIO_ReadPin(Inverter_Params.GPIO_HALLBC_NUM) << 1;
    HallCA = GPIO_ReadPin(Inverter_Params.GPIO_HALLCA_NUM);

    Hall_Index = ((HallAB | HallBC | HallCA ) & 0x7);
    Hall_Ang=(float)Hall_Angles[Hall_Index-1]/360.0 + Hall_AngCal;
    if(Hall_Ang > 1.0)
    {
        Hall_Ang -= 1.0;
    }
    else if(Hall_Ang<0.0)
    {
        Hall_Ang += 1.0;
    }
}
