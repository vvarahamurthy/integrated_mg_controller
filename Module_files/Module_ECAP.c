/*
 * Module_ECAP.c
 *
 *  Created on: Jun 6, 2019
 *      Author: vvarahamurthy
 */

#include "F2837S_files/F28x_Project.h"
#include "F2837xS_Pie_defines.h"
#include "Header/GUIC_Header.h"
#include "Header/UsefulPrototypes.h"

#define DeleteMe 1
#define FixMeLater 1

#define ECAP_RPM_PSC (Motor_Params.Poles/2.0)   //Prescaler for RPM measurement. ex. 16 pole pairs if using a hall sensor

// Globals
uint32_t  ECap1IntCount;
uint32_t  ECap2IntCount;
uint32_t  ecap1_timeout=0;
uint32_t  ecap2_timeout=0;

// Function Prototypes
__interrupt void ecap1_isr(void);
__interrupt void ecap2_isr(void);
uint32_t ECAP2_GetPulseus(void);
float ECAP2_GetTachRpm(void);
extern void dec(uint32_t *);

float gmax_us = 2200.0;
float gmin_us = 800.0;

void Init_ECAP(void)
{
    // Interrupts that are used in this example are re-mapped to
    // ISR functions found within this file.
       EALLOW;  // This is needed to write to EALLOW protected registers
       PieVectTable.ECAP1_INT = &ecap1_isr;
       PieVectTable.ECAP2_INT = &ecap2_isr;
       EDIS;    // This is needed to disable write to EALLOW protected registers


       //Set up ECAP1 Peripheral
    // Step 4. Initialize the Device Peripherals:
       ECap1Regs.ECEINT.all = 0x0000;          // Disable all capture __interrupts
       ECap1Regs.ECCLR.all = 0xFFFF;           // Clear all CAP __interrupt flags
       ECap1Regs.ECCTL1.bit.CAPLDEN = 0;       // Disable CAP1-CAP4 register loads
       ECap1Regs.ECCTL2.bit.TSCTRSTOP = 0;     // Make sure the counter is stopped

       // Configure peripheral registers
       ECap1Regs.ECCTL2.bit.CONT_ONESHT = 1;   // One-shot
       ECap1Regs.ECCTL2.bit.STOP_WRAP = 1;     // Stop at 2 events
       ECap1Regs.ECCTL1.bit.CAP1POL = 0;       // Rising edge
       ECap1Regs.ECCTL1.bit.CAP2POL = 1;       // Falling edge
       ECap1Regs.ECCTL1.bit.CTRRST1 = 1;       // Reset counter after latch
       ECap1Regs.ECCTL1.bit.CTRRST2 = 0;       // Do not reset counter after
       ECap1Regs.ECCTL2.bit.SYNCI_EN = 0;      // Disable sync in
       ECap1Regs.ECCTL2.bit.SYNCO_SEL = 0;     // Pass through
       ECap1Regs.ECCTL1.bit.CAPLDEN = 1;       // Enable capture units

       ECap1Regs.ECCTL2.bit.TSCTRSTOP = 1;     // Start Counter
       ECap1Regs.ECCTL2.bit.REARM = 1;         // arm one-shot
       ECap1Regs.ECCTL1.bit.CAPLDEN = 1;       // Enable CAP1-CAP4 register loads
       ECap1Regs.ECEINT.bit.CEVT2 = 1;         // 4 events = __interrupt


       //Set up ECAP2 Peripheral
       ECap2Regs.ECEINT.all = 0x0000;          // Disable all capture __interrupts
       ECap2Regs.ECCLR.all = 0xFFFF;           // Clear all CAP __interrupt flags
       ECap2Regs.ECCTL1.bit.CAPLDEN = 0;       // Disable CAP1-CAP4 register loads
       ECap2Regs.ECCTL2.bit.TSCTRSTOP = 0;     // Make sure the counter is stopped

       // Configure peripheral registers
       ECap2Regs.ECCTL2.bit.CONT_ONESHT = 1;   // One-shot
       ECap2Regs.ECCTL2.bit.STOP_WRAP = 1;     // Stop at 2 events
       ECap2Regs.ECCTL1.bit.CAP1POL = 0;       // Rising edge
       ECap2Regs.ECCTL1.bit.CAP2POL = 1;       // Falling edge
       ECap2Regs.ECCTL1.bit.CTRRST1 = 1;       // Reset counter after latch
       ECap2Regs.ECCTL1.bit.CTRRST2 = 0;       // Do not reset counter after
       ECap2Regs.ECCTL2.bit.SYNCI_EN = 0;      // Disable sync in
       ECap2Regs.ECCTL2.bit.SYNCO_SEL = 0;     // Pass through
       ECap2Regs.ECCTL1.bit.CAPLDEN = 1;       // Enable capture units

       ECap2Regs.ECCTL2.bit.TSCTRSTOP = 1;     // Start Counter
       ECap2Regs.ECCTL2.bit.REARM = 1;         // arm one-shot
       ECap2Regs.ECCTL1.bit.CAPLDEN = 1;       // Enable CAP1-CAP4 register loads
       ECap2Regs.ECEINT.bit.CEVT2 = 1;         // 4 events = __interrupt

    // Initialize counters:
       ECap1IntCount = 0;
       ECap2IntCount = 0;

    // Enable CPU INT4 which is connected to ECAP1-4 INT:
       IER |= M_INT4;

    // Enable eCAP INTn in the PIE: Group 3 __interrupt 1-6
       PieCtrlRegs.PIEIER4.bit.INTx1 = 1;
       PieCtrlRegs.PIEIER4.bit.INTx2 = 1;
}

// ecap1_isr - ECAP1 ISR
//             ECAP runs on SYSCLK and EPWMCLK=SYSCLK/w
//             so multiply TBPRD by 2
//             ECAP accuracy is +/- 1 SYSCLK cycle
//             Due to compounded error, difference between two
//             events may be +/- 2 SYSCLK cycles
//
//             *NOTE* The accuracy of the ECAP is +/- 2 SYSCLK cycles
//             between events. However, we have increased the margin
//             to +/- 4 SYSCLK cycles because an emulator halt behaves
//             slightly differently between the EPWM and ECAP due to the
//             different operating frequencies. This may introduce a couple
//             cycle of deviation in the counter value captured the first
//             time after resuming from a breakpoint.
//
__interrupt void ecap1_isr(void)
{
   ecap1_timeout=10;
   ECap1IntCount++;

   // Clear interrupts and arm ECAP
   ECap1Regs.ECCLR.bit.CEVT2 = 1;
   ECap1Regs.ECCLR.bit.INT = 1;
   ECap1Regs.ECCTL2.bit.REARM = 1;

   // Acknowledge this __interrupt to receive more __interrupts from group 4
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP4;
}

__interrupt void ecap2_isr(void)
{
   ecap2_timeout=10;
   ECap2IntCount++;

   // Clear interrupts and arm ECAP
   ECap2Regs.ECCLR.bit.CEVT2 = 1;
   ECap2Regs.ECCLR.bit.INT = 1;
   ECap2Regs.ECCTL2.bit.REARM = 1;
   // Acknowledge this __interrupt to receive more __interrupts from group 4
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP4;
}

float ECAP1_GetTachRpm(void)
{
    float prd_sec = (float)(ECap1Regs.CAP1)/200000000.0;     //Period in seconds
    float rpm=0.0;
    static float prevrpm;

    if(prd_sec<=0) return 0;
    else
    {
        rpm=60.0/(prd_sec);
        if(rpm>10000) return prevrpm;
        else
        {
            prevrpm = 60.0/(prd_sec);
            return rpm;  //Returns rpm value
        }
    }
}

float ECAP2_GetTachRpm(void)
{
    float prd_sec = (float)(ECap2Regs.CAP1)/200000000.0;     //Period in seconds
    float rpm=0.0;
    static float prevrpm;

    if(prd_sec<=0) return 0;
    else
    {
        rpm=60.0/(prd_sec)/ECAP_RPM_PSC;
        if(rpm>10000) return prevrpm;
        else
        {
            prevrpm = 60.0/(prd_sec)/ECAP_RPM_PSC;
            return rpm;  //Returns rpm value
        }
    }
}

void Check_ECAP_ISR(void)
{
    dec(&ecap1_timeout);
    dec(&ecap2_timeout);

    if(ecap1_timeout<=0)
    {
        ECap1Regs.CAP2=0;
        ECap1Regs.CAP1=0;
    }

    if(ecap2_timeout<=0)
    {
        ECap2Regs.CAP2=0;
        ECap2Regs.CAP1=0;
    }
}

uint32_t ECAP2_GetPulseus(void) {return ECap2Regs.CAP2/200L;}    //Returns usec pulse width
uint32_t ECAP1_GetPulseus(void) {return ECap1Regs.CAP2/200L;}    //Returns usec pulse width
bool ECAP1_TimeoutOK(void) {return ecap1_timeout>0;}
bool ECAP2_TimeoutOK(void) {return ecap2_timeout>0;}
