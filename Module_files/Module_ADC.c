/*
============================================================================
System Name:    LaunchPoint Motor Controller

File Name:      ADC.C

Target:         Cryo controller board

Author:         Mike Ricci

Description:    Refactoring motor controller software to be more modular and reduce linkage between sections
                This module will handle the processor A/D pins -- analog IO
********************************************************************************* */

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "F2837xS_GlobalPrototypes.h"
#include "F2837xS_Adc_defines.h"              // Macros used for ADC examples.

#include "Header/ADC.h"
#include "Header/GUIC_Header.h"
#include "Header/svgen_comp.h"         // Include header for the SVGENDQ object
#include "F2837S_files/f2837xS_Delay.h"

#include "Header/UsefulPrototypes.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1


// For comparator
#define CTRIP_FILTER       2
#define NEGIN_DAC          0
#define REFERENCE_VDDA     0
#define CLEARLLATCH        1
/***********************************************/
/*  Function prototypes for functions in this module */
/***********************************************/
inline void ConfigureADC(void);

void Init_Analog(void);

void AnalogSensorSuite_Int(void);
void AnalogSensorSuite_100Hz(void);
void AnalogSensorSuite_10Hz(void);
bool checkPhaseUComparatorflags(void);
bool checkPhaseVComparatorflags(void);
bool checkPhaseWComparatorflags(void);
bool checkComparatorFlag(int );

void clearAllCOMPLatch(void);
void ResetCMPSSLimits(void);
Uint16 AmpstoRawAdc(float , int );
Uint16 ADC_GetRawNTC(void);

/***********************************************/
/*  External functions used in this module - linkage to other modules */
/***********************************************/

// ****************************************************************************
// External Variables -- linkage to other modules
// ****************************************************************************
extern SVGEN svgen1;        // Need to see svgen to determine duty to determine Bus current from phase currents

extern float ECU_GetEDFDuty();
extern bool checkCHTLims(void);

// *****************************************************************************
//  Module resources - variables here //
// *****************************************************************************

volatile struct ADC_REGS * ADC[] = {&AdcaRegs,&AdcbRegs, &AdccRegs, &AdcdRegs};
int * ADC_SOCMAP[] = { &Inverter_Params.ADC_SOCA_MAP,
                       &Inverter_Params.ADC_SOCB_MAP,
                       &Inverter_Params.ADC_SOCC_MAP,
                       &Inverter_Params.ADC_SOCD_MAP};

volatile struct CMPSS_REGS * Cmpps[] = {
        &Cmpss1Regs, &Cmpss1Regs, &Cmpss2Regs, &Cmpss3Regs, &Cmpss4Regs, &Cmpss5Regs, &Cmpss6Regs, &Cmpss7Regs,
        &Cmpss8Regs};

int CMPSS_CURR_U    = 0;
int CMPSS_CURR_V    = 0;
int CMPSS_CURR_W    = 0;
int CMPSS_VOLT_BUS  = 0;

struct Analog_Signal adc_currentu,
                     adc_currentv,
                     adc_currentw,
                     adc_currentbus,
                     adc_currentbuselmo1,
                     adc_currentbuselmo2,
                     adc_current24v0,
                     adc_current28v0,
                     adc_current12v0,
                     adc_currentfp,
                     adc_voltageu,
                     adc_voltagev,
                     adc_voltagew,
                     adc_voltagebus,
                     adc_voltage3v3,
                     adc_voltage1v2,
                     adc_voltage5v0,
                     adc_voltage24v0,
                     adc_voltage28v0,
                     adc_voltage12v0,
                     adc_powerbus,
                     adc_tempcpu,
                     adc_tempdevice,
                     adc_tempheatsink,
                     adc_temppowerboard,
                     adc_templogicboard,
                     adc_tempstator,
                     adc_tempambient,
                     adc_tempoil,
                     adc_tempcoolant,
                     adc_speedknob,
                     adc_spareinput1,
                     adc_spareinput2;

struct Analog_Signal *AnalogSignals[30];

float processAnalogSignalPoly(struct Analog_Signal *, int16_t);
float adcGetSignalValue(enum adc_signal_id );

float LpfInt_10kHz;
float LpfInt_5kHz;
float LpfInt_3kHz;
float LpfInt_1kHz;
float LpfInt_100Hz;
float LpfInt_50Hz;
float LpfInt_10Hz;
float LpfInt_2Hz;
float Lpf100Hz_50Hz= 0;
float Lpf100Hz_10Hz= 0;
float Lpf100Hz_2Hz= 0;
float Lpf10Hz_2Hz= 0;

// ****************************************************************************
// Variables for current measurement
// ****************************************************************************
// Offset calibration routine is run to calibrate for any offsets on the opamps
float offset_shntBUS,			// offset in shunt current BUS channel @ 0A
	  offset_shntV,        // offset in shunt current V fbk channel @ 0A
      offset_shntW,        // offset in shunt current W fbk channel @ 0A
	  offset_shntU;        // offset in shunt current U fbk channel @ 0A

int16 OffsetCalCounter=0;

// Initial zero/offset filter parameters for the ADC

float K1 = 0.95,		  // Offset filter coefficient K1: 0.05/(T+0.05);
      K2 = 0.05;	      // Offset filter coefficient K2: T/(T+0.05);


#define NTC0805_3800K_M -128.16246
#define NTC0805_3800K_B 128.6217

#define F2837X_CPU_M  837.87
#define F2837X_CPU_B -272.55407715

// This function assigns the corresponding cmpss respect to ADCINxx
// Keep in mind, it only checks the value and NOT the letter.
void configureCmppsRegs()
{
    // Setting Cmpps for current sensor phase U
    if( (Inverter_Params.ADC_REG_IU == &AdcaResultRegs.ADCRESULT0) && (Inverter_Params.ADC_CHAN_IU == 2) )
    {
        CMPSS_CURR_U = 1;
    }
    else if( (Inverter_Params.ADC_REG_IU == &AdcaResultRegs.ADCRESULT1) && (Inverter_Params.ADC_CHAN_IU == 2) )
    {
        CMPSS_CURR_U = 1;
    }

    // Setting Cmpps for current sensor phase V
    if( (Inverter_Params.ADC_REG_IV == &AdcaResultRegs.ADCRESULT1) && (Inverter_Params.ADC_CHAN_IV == 4) )
    {
        CMPSS_CURR_V = 2;
    }
    else if( (Inverter_Params.ADC_REG_IV == &AdcaResultRegs.ADCRESULT2) && (Inverter_Params.ADC_CHAN_IV == 4) )
    {
        CMPSS_CURR_V = 2;
    }

    // Setting Cmpps for current sensor phase W
    if( (Inverter_Params.ADC_REG_IW == &AdcaResultRegs.ADCRESULT2) && (Inverter_Params.ADC_CHAN_IW == 14) )
    {
        CMPSS_CURR_W = 4;
    }
    else if( (Inverter_Params.ADC_REG_IW == &AdcaResultRegs.ADCRESULT3) && (Inverter_Params.ADC_CHAN_IW == 14) )
    {
        CMPSS_CURR_W = 4;
    }

    // Setting Cmpps for voltage sensor for Bus
    if( (Inverter_Params.ADC_REG_VBUS == &AdcaResultRegs.ADCRESULT3) && (Inverter_Params.ADC_CHAN_VBUS == 5) )
    {
        CMPSS_VOLT_BUS = 3;
    }
    if( (Inverter_Params.ADC_REG_VBUS == &AdcaResultRegs.ADCRESULT0) && (Inverter_Params.ADC_CHAN_VBUS == 5) )
    {
        CMPSS_VOLT_BUS = 3;
    }

}

// Hardware phase U current trips
bool checkComparatorFlag(int chan)
{
    bool compHighLatch = (*Cmpps[chan]).COMPSTS.bit.COMPHLATCH;
    bool compLowLatch = (*Cmpps[chan]).COMPSTS.bit.COMPLLATCH;

    return (compHighLatch | compLowLatch);
}

// Reset all the phase comparator latch.
void clearAllCOMPLatch()
{
    EALLOW;
    (*Cmpps[CMPSS_CURR_U]).COMPSTSCLR.bit.HLATCHCLR = CLEARLLATCH;
    (*Cmpps[CMPSS_CURR_U]).COMPSTSCLR.bit.LLATCHCLR = CLEARLLATCH;
    (*Cmpps[CMPSS_CURR_V]).COMPSTSCLR.bit.HLATCHCLR = CLEARLLATCH;
    (*Cmpps[CMPSS_CURR_V]).COMPSTSCLR.bit.LLATCHCLR = CLEARLLATCH;
    (*Cmpps[CMPSS_CURR_W]).COMPSTSCLR.bit.HLATCHCLR = CLEARLLATCH;
    (*Cmpps[CMPSS_CURR_W]).COMPSTSCLR.bit.LLATCHCLR = CLEARLLATCH;
    EDIS;
}

void ResetCMPSSLimits(void)
{
    EALLOW;
    (*Cmpps[CMPSS_CURR_U]).DACHVALS.bit.DACVAL = AmpstoRawAdc(     Motor_Params.LIM_I_PHS, CMPSS_CURR_U);   // High
    (*Cmpps[CMPSS_CURR_U]).DACLVALS.bit.DACVAL = AmpstoRawAdc(-1.0*Motor_Params.LIM_I_PHS, CMPSS_CURR_U);   // Low
    (*Cmpps[CMPSS_CURR_V]).DACHVALS.bit.DACVAL = AmpstoRawAdc(     Motor_Params.LIM_I_PHS, CMPSS_CURR_V);   // High
    (*Cmpps[CMPSS_CURR_V]).DACLVALS.bit.DACVAL = AmpstoRawAdc(-1.0*Motor_Params.LIM_I_PHS, CMPSS_CURR_V);   // Low
    (*Cmpps[CMPSS_CURR_W]).DACHVALS.bit.DACVAL = AmpstoRawAdc(     Motor_Params.LIM_I_PHS, CMPSS_CURR_W);   // High
    (*Cmpps[CMPSS_CURR_W]).DACLVALS.bit.DACVAL = AmpstoRawAdc(-1.0*Motor_Params.LIM_I_PHS, CMPSS_CURR_W);   // Low
    EDIS;
}

void InitCMPSS(void)
{
    // Initialize CMPSS regs
    configureCmppsRegs();

    EALLOW;
    // CMPSS1, CMPSS2, CMPSS4, and CMPSS3 corresponds to ADCINA2 (I_U), ADCINA2 (I_V), ADCIN14 (I_W), and ADCINB2 (I_BUS) respectively.
    //Enable CMPSS
    (*Cmpps[CMPSS_CURR_U]).COMPCTL.bit.COMPDACE = 1;
    (*Cmpps[CMPSS_CURR_V]).COMPCTL.bit.COMPDACE = 1;
    (*Cmpps[CMPSS_CURR_W]).COMPCTL.bit.COMPDACE = 1;
//    (*Cmpps[CMPSS_VOLT_BUS]).COMPCTL.bit.COMPDACE = 1;

    //NEG signal comes from DAC
    (*Cmpps[CMPSS_CURR_U]).COMPCTL.bit.COMPHSOURCE = NEGIN_DAC;
    (*Cmpps[CMPSS_CURR_V]).COMPCTL.bit.COMPHSOURCE = NEGIN_DAC;
    (*Cmpps[CMPSS_CURR_W]).COMPCTL.bit.COMPHSOURCE = NEGIN_DAC;
//    (*Cmpps[CMPSS_VOLT_BUS]).COMPCTL.bit.COMPHSOURCE = NEGIN_DAC;

    //Use VDDA as the reference for DAC
    (*Cmpps[CMPSS_CURR_U]).COMPDACCTL.bit.SELREF = REFERENCE_VDDA;
    (*Cmpps[CMPSS_CURR_V]).COMPDACCTL.bit.SELREF = REFERENCE_VDDA;
    (*Cmpps[CMPSS_CURR_W]).COMPDACCTL.bit.SELREF = REFERENCE_VDDA;
//    (*Cmpps[CMPSS_VOLT_BUS]).COMPDACCTL.bit.SELREF = REFERENCE_VDDA;

    // Set DAC to midpoint for arbitrary reference
    // Start 2250 for now.
    (*Cmpps[CMPSS_CURR_U]).DACHVALS.bit.DACVAL = AmpstoRawAdc(     Motor_Params.LIM_I_PHS, CMPSS_CURR_U);   // High
    (*Cmpps[CMPSS_CURR_U]).DACLVALS.bit.DACVAL = AmpstoRawAdc(-1.0*Motor_Params.LIM_I_PHS, CMPSS_CURR_U);   // Low
    (*Cmpps[CMPSS_CURR_V]).DACHVALS.bit.DACVAL = AmpstoRawAdc(     Motor_Params.LIM_I_PHS, CMPSS_CURR_V);   // High
    (*Cmpps[CMPSS_CURR_V]).DACLVALS.bit.DACVAL = AmpstoRawAdc(-1.0*Motor_Params.LIM_I_PHS, CMPSS_CURR_V);   // Low
    (*Cmpps[CMPSS_CURR_W]).DACHVALS.bit.DACVAL = AmpstoRawAdc(     Motor_Params.LIM_I_PHS, CMPSS_CURR_W);   // High
    (*Cmpps[CMPSS_CURR_W]).DACLVALS.bit.DACVAL = AmpstoRawAdc(-1.0*Motor_Params.LIM_I_PHS, CMPSS_CURR_W);   // Low
//    (*Cmpps[CMPSS_VOLT_BUS]).DACHVALS.bit.DACVAL = 4096;   // High
//    (*Cmpps[CMPSS_VOLT_BUS]).DACLVALS.bit.DACVAL = 0;   // Low

    // Invert only Lowside
    (*Cmpps[CMPSS_CURR_U]).COMPCTL.bit.COMPLINV = 1;
    (*Cmpps[CMPSS_CURR_V]).COMPCTL.bit.COMPLINV = 1;
    (*Cmpps[CMPSS_CURR_W]).COMPCTL.bit.COMPLINV = 1;

    // Configure Digital Filter
    // Maximum CLKPRESCALE value provides the most time between samples
    // GUY- We need a window time of 300ns
    // 0x41 + 1 = 66 (dec) => 200MHz/66 = 3MHz => 330ns
    (*Cmpps[CMPSS_CURR_U]).CTRIPHFILCLKCTL.bit.CLKPRESCALE = 1;      // High
    (*Cmpps[CMPSS_CURR_U]).CTRIPLFILCLKCTL.bit.CLKPRESCALE = 1;      // Low
    (*Cmpps[CMPSS_CURR_V]).CTRIPHFILCLKCTL.bit.CLKPRESCALE = 1;      // High
    (*Cmpps[CMPSS_CURR_V]).CTRIPLFILCLKCTL.bit.CLKPRESCALE = 1;      // Low
    (*Cmpps[CMPSS_CURR_W]).CTRIPHFILCLKCTL.bit.CLKPRESCALE = 1;      // High
    (*Cmpps[CMPSS_CURR_W]).CTRIPLFILCLKCTL.bit.CLKPRESCALE = 1;      // Low
//    (*Cmpps[CMPSS_VOLT_BUS]).CTRIPHFILCLKCTL.bit.CLKPRESCALE = 1;      // High
//    (*Cmpps[CMPSS_VOLT_BUS]).CTRIPLFILCLKCTL.bit.CLKPRESCALE = 1;      // Low

    //Maximum SAMPWIN value provides largest number of samples
    // 0x1F + 1 = 32 (dec) samples
    (*Cmpps[CMPSS_CURR_U]).CTRIPHFILCTL.bit.SAMPWIN = 0x1F;             // High
    (*Cmpps[CMPSS_CURR_U]).CTRIPLFILCTL.bit.SAMPWIN = 0x1F;             // Low
    (*Cmpps[CMPSS_CURR_V]).CTRIPHFILCTL.bit.SAMPWIN = 0x1F;             // High
    (*Cmpps[CMPSS_CURR_V]).CTRIPLFILCTL.bit.SAMPWIN = 0x1F;             // Low
    (*Cmpps[CMPSS_CURR_W]).CTRIPHFILCTL.bit.SAMPWIN = 0x1F;             // High
    (*Cmpps[CMPSS_CURR_W]).CTRIPLFILCTL.bit.SAMPWIN = 0x1F;             // Low
//    (*Cmpps[CMPSS_VOLT_BUS]).CTRIPHFILCTL.bit.SAMPWIN = 0x1F;             // High
//    (*Cmpps[CMPSS_VOLT_BUS]).CTRIPLFILCTL.bit.SAMPWIN = 0x1F;             // Low

    //Maximum THRESH value requires static value for entire window
    //  THRESH should be GREATER than half of SAMPWIN
    // 0x1F + 1 = 32 (dec) samples
    (*Cmpps[CMPSS_CURR_U]).CTRIPHFILCTL.bit.THRESH = 0x1F;              // High
    (*Cmpps[CMPSS_CURR_U]).CTRIPLFILCTL.bit.THRESH = 0x1F;              // Low
    (*Cmpps[CMPSS_CURR_V]).CTRIPHFILCTL.bit.THRESH = 0x1F;              // High
    (*Cmpps[CMPSS_CURR_V]).CTRIPLFILCTL.bit.THRESH = 0x1F;              // Low
    (*Cmpps[CMPSS_CURR_W]).CTRIPHFILCTL.bit.THRESH = 0x1F;              // High
    (*Cmpps[CMPSS_CURR_W]).CTRIPLFILCTL.bit.THRESH = 0x1F;              // Low
//    (*Cmpps[CMPSS_VOLT_BUS]).CTRIPHFILCTL.bit.THRESH = 0x1F;              // High
//    (*Cmpps[CMPSS_VOLT_BUS]).CTRIPLFILCTL.bit.THRESH = 0x1F;              // Low

    //Reset filter logic & start filtering
    (*Cmpps[CMPSS_CURR_U]).CTRIPHFILCTL.bit.FILINIT = 1;                // High
    (*Cmpps[CMPSS_CURR_U]).CTRIPLFILCTL.bit.FILINIT = 1;                // Low
    (*Cmpps[CMPSS_CURR_V]).CTRIPHFILCTL.bit.FILINIT = 1;                // High
    (*Cmpps[CMPSS_CURR_V]).CTRIPLFILCTL.bit.FILINIT = 1;                // Low
    (*Cmpps[CMPSS_CURR_W]).CTRIPHFILCTL.bit.FILINIT = 1;                // High
    (*Cmpps[CMPSS_CURR_W]).CTRIPLFILCTL.bit.FILINIT = 1;                // Low
//    (*Cmpps[CMPSS_VOLT_BUS]).CTRIPHFILCTL.bit.FILINIT = 1;                // High
//    (*Cmpps[CMPSS_VOLT_BUS]).CTRIPLFILCTL.bit.FILINIT = 1;                // Low

    // Configure CTRIPOUT path
    // Digital filter output feeds CTRIPH
    (*Cmpps[CMPSS_CURR_U]).COMPCTL.bit.CTRIPHSEL = CTRIP_FILTER;        // High
    (*Cmpps[CMPSS_CURR_U]).COMPCTL.bit.CTRIPLSEL = CTRIP_FILTER;        // Low
    (*Cmpps[CMPSS_CURR_V]).COMPCTL.bit.CTRIPHSEL = CTRIP_FILTER;        // High
    (*Cmpps[CMPSS_CURR_V]).COMPCTL.bit.CTRIPLSEL = CTRIP_FILTER;        // Low
    (*Cmpps[CMPSS_CURR_W]).COMPCTL.bit.CTRIPHSEL = CTRIP_FILTER;        // High
    (*Cmpps[CMPSS_CURR_W]).COMPCTL.bit.CTRIPLSEL = CTRIP_FILTER;        // Low
//    (*Cmpps[CMPSS_VOLT_BUS]).COMPCTL.bit.CTRIPHSEL = CTRIP_FILTER;        // High
//    (*Cmpps[CMPSS_VOLT_BUS]).COMPCTL.bit.CTRIPLSEL = CTRIP_FILTER;        // Low

    // Configure CTRIPOUTH output pin
    // Configure OUTPUTXBAR3 to be CTRIPOUT1H
    // 0.1
    // MUX0, MUX2, MUX6, and MUX4 corresponds to CMPSS1, CMPSS2, CMPSS4, and CMPSS3 respectively.
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX0 = 0x01;          // 0 = CTRIPH, 1 = CTRIPH or CTRIPL
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX0 = 0x01;            // 1 to enable
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX2 = 0x01;          // 0 = CTRIPH, 1 = CTRIPH or CTRIPL
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX2 = 0x01;            // 1 to enable
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX6 = 0x01;          // 0 = CTRIPH, 1 = CTRIPH or CTRIPL
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX6 = 0x01;            // 1 to enable
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX4 = 0x01;          // 0 = CTRIPH, 1 = CTRIPH or CTRIPL
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX4 = 0x01;            // 1 to enable

    EPwmXbarRegs.TRIPOUTINV.bit.TRIP4 = 0x00;               // 1 to inverse
    EDIS;
}

void ConfigureADC(void)
{
	//Write ADC configurations and power up the ADC for both ADC A
    uint16_t i;

    EALLOW;

    //write configurations for ADC-A
    // External REFERENCE must be provided

	for(i=0;i<NUM_ADC_REGS;i++)
	{
	    if(*(ADC_SOCMAP[i]) >= 0) //only configure if this set of regs is used (default -1 on all indices of SOCX_MAP)
	    {
	        (*ADC[i]).ADCCTL2.bit.PRESCALE = 6;
	        AdcSetMode(i,ADC_RESOLUTION_12BIT,ADC_SIGNALMODE_SINGLE); //previously ADC_ADCA, ADC_ADCB, but these are 0-3
	        //Set pulse positions to late  - ADC interrupt happens after result0 should be valid
	        (*ADC[i]).ADCCTL1.bit.INTPULSEPOS = 1;

	        //Power up the ADC
	        (*ADC[i]).ADCCTL1.bit.ADCPWDNZ = 1;
	    }
	}
#if 0
	AdcaRegs.ADCCTL2.bit.PRESCALE   = 6; //set ADCCLK divider to /4 - max is 50 MHz from table 5.43 in www.ti.com/lit/ds/symlink/tms320f28377s.pdf
										// also see table 9-14 in http://www.ti.com/lit/ug/spruhx5c/spruhx5c.pdf for codes/values to get divisors
	AdcSetMode(ADC_ADCA,ADC_RESOLUTION_12BIT,ADC_SIGNALMODE_SINGLE);

    //Set pulse positions to late  - ADC interrupt happens after result0 should be valid
    AdcaRegs.ADCCTL1.bit.INTPULSEPOS = 1;

    //power up the ADC
    AdcaRegs.ADCCTL1.bit.ADCPWDNZ = 1;

    //write configurations for ADC-B
    // External REFERENCE must be provided

    AdcbRegs.ADCCTL2.bit.PRESCALE   = 6; //set ADCCLK divider to /4
    AdcSetMode(ADC_ADCB,ADC_RESOLUTION_12BIT,ADC_SIGNALMODE_SINGLE);

    //Set pulse positions to late - ADC interrupt happens after Result0 should be valid
    AdcbRegs.ADCCTL1.bit.INTPULSEPOS = 1;

    //power up the ADC
    AdcbRegs.ADCCTL1.bit.ADCPWDNZ = 1;
#endif

    //delay for > 1ms to allow ADC time to power up
    for(i = 0; i < 1000; i++){
        asm("   RPT#255 || NOP");
    }

    EDIS;
}

void Init_Analog()
{
    int i=0;
    int TriggerSel = 0x03 + 2*Inverter_Params.PWM_U_NUM;

    //Configure the ADC and power it up
    ConfigureADC();


    //Select the channels to convert and end of conversion flag
    //Current Measurements has multiple options

    EALLOW;

    for(i=0;i<NUM_ADC_REGS;i++)
    {
            if(*(ADC_SOCMAP[i]) >= 0) //this checks if element 0 of the SOCX_MAP is at least 0
            {
                (*ADC[i]).ADCSOC0CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 0) );
                (*ADC[i]).ADCSOC0CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC0CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
                // Configure PPB to eliminate subtraction related calculation
                (*ADC[i]).ADCPPB1CONFIG.bit.CONFIG = 0;    // a_PPB1 is associated with aSOC0
                (*ADC[i]).ADCPPB1OFFCAL.bit.OFFCAL = 0;    // Write zero to this for now till offset ISR is run

            if(*(ADC_SOCMAP[i] + 1) >=0 )
            {
                (*ADC[i]).ADCSOC1CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 1) );
                (*ADC[i]).ADCSOC1CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC1CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
                // Configure PPB to eliminate subtraction related calculation
                (*ADC[i]).ADCPPB2CONFIG.bit.CONFIG = 1;    // a_PPB2 is associated with aSOC1
                (*ADC[i]).ADCPPB2OFFCAL.bit.OFFCAL = 0;    // Write zero to this for now till offset ISR is run
            }

            if(*(ADC_SOCMAP[i] + 2) >=0 )
            {
                (*ADC[i]).ADCSOC2CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 2) );
                (*ADC[i]).ADCSOC2CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC2CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
                // Configure PPB to eliminate subtraction related calculation
                (*ADC[i]).ADCPPB3CONFIG.bit.CONFIG = 2;    // a_PPB3 is associated with aSOC2
                (*ADC[i]).ADCPPB3OFFCAL.bit.OFFCAL = 0;    // Write zero to this for now till offset ISR is run
            }

            if(*(ADC_SOCMAP[i] + 3) >=0 )
            {
                (*ADC[i]).ADCSOC3CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 3) );
                (*ADC[i]).ADCSOC3CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC3CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
                // Configure PPB to eliminate subtraction related calculation
                (*ADC[i]).ADCPPB4CONFIG.bit.CONFIG = 3;    // a_PPB4 is associated with aSOC3
                (*ADC[i]).ADCPPB4OFFCAL.bit.OFFCAL = 0;    // Write zero to this for now till offset ISR is run
            }

            if(*(ADC_SOCMAP[i] + 4) >=0 )
            {
                (*ADC[i]).ADCSOC4CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 4) );
                (*ADC[i]).ADCSOC4CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC4CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 5) >=0 )
            {
                (*ADC[i]).ADCSOC5CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 5) );
                (*ADC[i]).ADCSOC5CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC5CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 6) >=0 )
            {
                (*ADC[i]).ADCSOC6CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 6) );
                (*ADC[i]).ADCSOC6CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC6CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 7) >=0 )
            {
                (*ADC[i]).ADCSOC7CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 7) );
                (*ADC[i]).ADCSOC7CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC7CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 8) >=0 )
            {
                (*ADC[i]).ADCSOC8CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 8) );
                (*ADC[i]).ADCSOC8CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC8CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 9) >=0 )
            {
                (*ADC[i]).ADCSOC9CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 9) );
                (*ADC[i]).ADCSOC9CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC9CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 10) >=0 )
            {
                (*ADC[i]).ADCSOC10CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 10) );
                (*ADC[i]).ADCSOC10CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC10CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 11) >=0 )
            {
                (*ADC[i]).ADCSOC11CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 11) );
                (*ADC[i]).ADCSOC11CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC11CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 12) >=0 )
            {
                (*ADC[i]).ADCSOC12CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 12) );
                (*ADC[i]).ADCSOC12CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC12CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 13) >=0 )
            {
                (*ADC[i]).ADCSOC13CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 13) );
                (*ADC[i]).ADCSOC13CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC13CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 14) >=0 )
            {
                (*ADC[i]).ADCSOC14CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 14) );
                (*ADC[i]).ADCSOC14CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC14CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }

            if(*(ADC_SOCMAP[i] + 15) >=0 )
            {
                (*ADC[i]).ADCSOC15CTL.bit.CHSEL     = (*(ADC_SOCMAP[i] + 15) );
                (*ADC[i]).ADCSOC15CTL.bit.ACQPS     = 80;   // sample window in SYSCLK cycles (or ADCCLK cycles?)
                (*ADC[i]).ADCSOC15CTL.bit.TRIGSEL   = TriggerSel;   // trigger on ePWM10 SOCA
            }
        }
    }

    // ****************************************************************************
    // ****************************************************************************
    // Feedbacks OFFSET Calibration Routine
    // ****************************************************************************
    // ****************************************************************************
        EALLOW;
          CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;		// Start PWM clocks (?) and also ADC sampling
        EDIS;


        //Only run this zeroing process for the MC15/MC40
        if(Inverter_Params.InverterSel == MC40 || Inverter_Params.InverterSel == MC15)
        {
            offset_shntV = 0;
            offset_shntW = 0;
            offset_shntU = 0;
            offset_shntBUS = 0;     // Zero this with processor power, so there will be some offset from true zero...

            OffsetCalCounter=0;


            while ( OffsetCalCounter<2000 )
            {
                if(CpuTimer0Regs.TCR.bit.TIF == 1)
                {
                    if(OffsetCalCounter>500)        // Debug -- this loop needs to run faster and do more samples...
                    {

                        if(Inverter_Params.ADC_REG_IU != NULL)
                        {
                            offset_shntU = K1*offset_shntU + K2*(*Inverter_Params.ADC_REG_IU)*ADC_PU_SCALE_FACTOR*2.0;          //Phase U offset
                        }
                        if(Inverter_Params.ADC_REG_IV != NULL)
                        {
                            offset_shntV = K1*offset_shntV + K2*(*Inverter_Params.ADC_REG_IV)*ADC_PU_SCALE_FACTOR*2.0;          //Phase V offset
                        }

                        if(Inverter_Params.ADC_REG_IV != NULL)
                        {
                            offset_shntW = K1*offset_shntW + K2*(*Inverter_Params.ADC_REG_IW)*ADC_PU_SCALE_FACTOR*2.0;          //Phase W offset
                        }
                        if(Inverter_Params.ADC_REG_IBUS != NULL)
                        {
                            offset_shntBUS = K1*offset_shntBUS + K2*(*Inverter_Params.ADC_REG_IBUS)*ADC_PU_SCALE_FACTOR*2.0;            //Bus current offset, tare out processor/housekeeping current
                        }
                    }
                    CpuTimer0Regs.TCR.bit.TIF = 1;  // clear flag
                    OffsetCalCounter++;
                }
            }

            // ********************************************
            // Init OFFSET regs with identified values
            // ********************************************
            EALLOW;

            //On MC15 and MC40 controllers, ADCB inputs are bus voltage, not bus current.
            //Todo - figure out a better way to call or not call this offset stuff
            AdcbRegs.ADCPPB1OFFREF = (offset_shntBUS*4096.0);      // setting shunt Bus I offset
            AdcbRegs.ADCPPB2OFFREF = (offset_shntW*4096.0);      // setting shunt Iu offset
            AdcbRegs.ADCPPB3OFFREF = (offset_shntV*4096.0);      // setting shunt Iv offset
            AdcbRegs.ADCPPB4OFFREF = (offset_shntU*4096.0);      // setting shunt Iw offset
            EDIS;
        }

        EALLOW;
        //power up the the temperature sensor
        AnalogSubsysRegs.TSNSCTL.bit.ENABLE = 1;
        //delay to allow the sensor to power up
        DELAY_US(1000);
        EDIS;

#ifndef FixMeLater
        //Setting the Omegas for each RMS LP filtered signal
        currentUs.Omega = 0.00000636;
        currentUs.RMS_old = 0;
        currentVs.Omega = 0.00000636;
        currentVs.RMS_old = 0;
        currentWs.Omega = 0.00000636;
        currentWs.RMS_old = 0;
        BusVoltage.Omega = 0.00000636;
        BusVoltage.RMS_old = 0;
#endif
        /************************/
        /*Initialize ADC Signals*/
        /************************/

        //LPF alpha values for ADCs being sampled in the @ speed of interrupt
        LpfInt_10kHz = (1.0-expf(-2.0*PI*10000.0/(Motor_Params.LOOP_FREQ_KHZ*1000.0)));
        LpfInt_5kHz = (1.0-expf(-2.0*PI*5000.0/(Motor_Params.LOOP_FREQ_KHZ*1000.0)));
        LpfInt_3kHz = (1.0-expf(-2.0*PI*3000.0/(Motor_Params.LOOP_FREQ_KHZ*1000.0)));
        LpfInt_1kHz = (1.0-expf(-2.0*PI*1000.0/(Motor_Params.LOOP_FREQ_KHZ*1000.0)));
        LpfInt_100Hz = (1.0-expf(-2.0*PI*100.0/(Motor_Params.LOOP_FREQ_KHZ*1000.0)));
        LpfInt_50Hz = (1.0-expf(-2.0*PI*50.0/(Motor_Params.LOOP_FREQ_KHZ*1000.0)));
        LpfInt_10Hz = (1.0-expf(-2.0*PI*10.0/(Motor_Params.LOOP_FREQ_KHZ*1000.0)));
        LpfInt_2Hz = (1.0-expf(-2.0*PI*2.0/(Motor_Params.LOOP_FREQ_KHZ*1000.0)));

        //LPF alpha values for ADCs being sampled in a 100Hz task
        Lpf100Hz_50Hz = (1.0-expf(-2.0*PI*50.0/100.0));
        Lpf100Hz_10Hz = (1.0-expf(-2.0*PI*10.0/100.0));
        Lpf100Hz_2Hz = (1.0-expf(-2.0*PI*2.0/100.0));

        //LPF alpha values for ADCs being sampled in a 10Hz task
        Lpf10Hz_2Hz = (1.0-expf(-2.0*PI*2.0/10.0));

        //Todo- set warn and critical values for these
        if(Inverter_Params.Electronics == DriveInternal)
        {
            adc_currentu.lpf_alpha = LpfInt_5kHz;
            adc_currentu.coef[0] = Inverter_Params.Sense_Current_B;
            adc_currentu.coef[1] = Inverter_Params.Sense_Current_M;
            adc_currentu.crith = Motor_Params.LIM_I_PHS;
            adc_currentu.critl = -1.0*Motor_Params.LIM_I_PHS;
            adc_currentu.order   = 1;
            AnalogSignals[adc_id_iu] = &adc_currentu;
            adc_currentu.monitored = true;

            adc_currentv.lpf_alpha = LpfInt_5kHz;
            adc_currentv.coef[0] = Inverter_Params.Sense_Current_B;
            adc_currentv.coef[1] = Inverter_Params.Sense_Current_M;
            adc_currentv.order   = 1;
            adc_currentv.crith = Motor_Params.LIM_I_PHS;
            adc_currentv.critl = -1.0*Motor_Params.LIM_I_PHS;
            AnalogSignals[adc_id_iv] = &adc_currentv;
            adc_currentv.monitored = true;

            adc_currentw.lpf_alpha = LpfInt_5kHz;
            adc_currentw.coef[0] = Inverter_Params.Sense_Current_B;
            adc_currentw.coef[1] = Inverter_Params.Sense_Current_M;
            adc_currentw.order   = 1;
            adc_currentw.crith = Motor_Params.LIM_I_PHS;
            adc_currentw.critl = -1.0*Motor_Params.LIM_I_PHS;
            AnalogSignals[adc_id_iw] = &adc_currentw;
            adc_currentw.monitored = true;

            adc_voltageu.lpf_alpha = LpfInt_5kHz;
            adc_voltageu.coef[0] = Inverter_Params.Sense_Voltage_B;
            adc_voltageu.coef[1] = Inverter_Params.Sense_Voltage_M;
            adc_voltageu.order   = 1;
            AnalogSignals[adc_id_vu] = &adc_voltageu;
            adc_voltageu.monitored = false;

            adc_voltagev.lpf_alpha = LpfInt_5kHz;
            adc_voltagev.coef[0] = Inverter_Params.Sense_Voltage_B;
            adc_voltagev.coef[1] = Inverter_Params.Sense_Voltage_M;
            adc_voltagev.order   = 1;
            AnalogSignals[adc_id_vv] = &adc_voltagev;
            adc_voltagev.monitored = false;

            adc_voltagew.lpf_alpha = LpfInt_5kHz;
            adc_voltagew.coef[0] = Inverter_Params.Sense_Voltage_B;
            adc_voltagew.coef[1] = Inverter_Params.Sense_Voltage_M;
            adc_voltagew.order   = 1;
            AnalogSignals[adc_id_vw] = &adc_voltagew;
            adc_voltagew.monitored = false;

            adc_tempdevice.lpf_alpha = Lpf10Hz_2Hz;
            adc_tempdevice.coef[0] = Inverter_Params.Sense_TempP0;
            adc_tempdevice.coef[1] = Inverter_Params.Sense_TempP1;
            adc_tempdevice.coef[2] = Inverter_Params.Sense_TempP2;
            adc_tempdevice.order   = 2;
            adc_tempdevice.crith = Inverter_Params.LIM_T_DEV_CRIT_H;
            adc_tempdevice.critl = Inverter_Params.LIM_T_DEV_CRIT_L;
            AnalogSignals[adc_id_tdevice] = &adc_tempdevice;
            adc_tempdevice.monitored = true;
        }

        if(Inverter_Params.ADC_REG_VBUS != NULL)
        {
            adc_voltagebus.lpf_alpha = LpfInt_5kHz;
            adc_voltagebus.coef[0] = Inverter_Params.Sense_Voltage_B;
            adc_voltagebus.coef[1] = Inverter_Params.Sense_Voltage_M;
            adc_voltagebus.order   = 1;
            adc_voltagebus.critl   = Motor_Params.LIM_V_DC_CRIT_L;
            adc_voltagebus.crith   = Motor_Params.LIM_V_DC_CRIT_H;
            adc_voltagebus.warnh   = Inverter_Params.LIM_V_DC_WARN_H;
            adc_voltagebus.warnl   = Inverter_Params.LIM_V_DC_WARN_L;
            AnalogSignals[adc_id_vdc] = &adc_voltagebus;
            adc_voltagebus.monitored = true;
        }

        if(Inverter_Params.InverterSel == ELMO_PMU)
        {
            //Todo- remove magic numbers
            adc_currentbuselmo1.lpf_alpha = LpfInt_50Hz;
            adc_currentbuselmo1.coef[0] = 160.9405985;
            adc_currentbuselmo1.coef[1] = -225.4665687;
            adc_currentbuselmo1.order   = 1;
            adc_currentbuselmo1.crith = Inverter_Params.LIM_I_DC_CRIT_H;
            adc_currentbuselmo1.critl = Inverter_Params.LIM_I_DC_CRIT_L;
            adc_currentbuselmo1.warnh = Inverter_Params.LIM_I_DC_WARN_H;
            adc_currentbuselmo1.warnl = Inverter_Params.LIM_I_DC_WARN_L;
            AnalogSignals[adc_id_idcelmo1] = &adc_currentbuselmo1;
            adc_currentbuselmo1.monitored = false;

            //Todo- remove magic numbers
            adc_currentbuselmo2.lpf_alpha = LpfInt_50Hz;
            adc_currentbuselmo2.coef[0] = -160.0438727;
            adc_currentbuselmo2.coef[1] = 221.5791954;
            adc_currentbuselmo2.order   = 1;
            adc_currentbuselmo2.crith = Inverter_Params.LIM_I_DC_CRIT_H;
            adc_currentbuselmo2.critl = Inverter_Params.LIM_I_DC_CRIT_L;
            adc_currentbuselmo2.warnh = Inverter_Params.LIM_I_DC_WARN_H;
            adc_currentbuselmo2.warnl = Inverter_Params.LIM_I_DC_WARN_L;
            AnalogSignals[adc_id_idcelmo2] = &adc_currentbuselmo2;
            adc_currentbuselmo2.monitored = false;
        }

        //On a drive with Elmos, this item's value is set directly as the sum of elmo currents. ProcessAnalogSignalPoly is not called on this
        adc_currentbus.lpf_alpha = LpfInt_5kHz;
        adc_currentbus.coef[0] = Inverter_Params.Sense_Current_B;
        adc_currentbus.coef[1] = Inverter_Params.Sense_Current_M;
        adc_currentbus.order   = 1;
        adc_currentbus.crith = Motor_Params.LIM_I_DC_CRIT_H;
        adc_currentbus.critl = Motor_Params.LIM_I_DC_CRIT_L;
        adc_currentbus.warnh = Inverter_Params.LIM_I_DC_WARN_H;
        adc_currentbus.warnl = Inverter_Params.LIM_I_DC_WARN_L;
        AnalogSignals[adc_id_idc] = &adc_currentbus;
        if(Inverter_Params.ADC_REG_IBUS != NULL)
        {
            adc_currentbus.monitored = true;
        }
        else
        {
            adc_currentbus.monitored = false;
        }


        if(Inverter_Params.ADC_REG_TPDB!=NULL)
        {
            adc_temppowerboard.lpf_alpha = Lpf10Hz_2Hz;
            adc_temppowerboard.coef[0] = NTC0805_3800K_B;
            adc_temppowerboard.coef[1] = NTC0805_3800K_M;
            adc_temppowerboard.order   = 1;
            adc_temppowerboard.crith = Inverter_Params.LIM_T_PDB_CRIT_H;
            adc_temppowerboard.critl = Inverter_Params.LIM_T_PDB_CRIT_L;
            adc_temppowerboard.warnh = Inverter_Params.LIM_T_PDB_WARN_H;
            adc_temppowerboard.warnl = Inverter_Params.LIM_T_PDB_WARN_L;
            AnalogSignals[adc_id_tpdb] = &adc_temppowerboard;
            adc_temppowerboard.monitored = true;
        }

        if(Inverter_Params.ADC_REG_TBRD!=NULL)
        {
            adc_templogicboard.lpf_alpha = Lpf10Hz_2Hz;
            adc_templogicboard.coef[0] = NTC0805_3800K_B;
            adc_templogicboard.coef[1] = NTC0805_3800K_M;
            adc_templogicboard.order   = 1;
            adc_templogicboard.crith = Inverter_Params.LIM_T_LOGIC_CRIT_H;
            adc_templogicboard.critl = Inverter_Params.LIM_T_LOGIC_CRIT_L;
            adc_templogicboard.warnh = Inverter_Params.LIM_T_LOGIC_WARN_H;
            adc_templogicboard.warnl = Inverter_Params.LIM_T_LOGIC_WARN_L;
            AnalogSignals[adc_id_tlogic] = &adc_templogicboard;
            adc_templogicboard.monitored = true;
        }

        if(Inverter_Params.ADC_REG_THSINK!=NULL)
        {
            //y = -318.04182x3 + 489.33770x2 - 338.47588x + 161.62857R� = 0.99820
            //Todo - lose the magic numbers
            adc_tempheatsink.lpf_alpha = Lpf10Hz_2Hz;
            adc_tempheatsink.coef[0] = 161.62857;
            adc_tempheatsink.coef[1] = -338.47588;
            adc_tempheatsink.coef[2] = 489.33770;
            adc_tempheatsink.coef[3] = -318.04182;
            adc_tempheatsink.order   = 3;
            adc_tempheatsink.crith = Inverter_Params.LIM_T_HEATSINK_CRIT_H;
            adc_tempheatsink.critl = Inverter_Params.LIM_T_HEATSINK_CRIT_L;
            adc_tempheatsink.warnh = Inverter_Params.LIM_T_HEATSINK_WARN_H;
            adc_tempheatsink.warnl = Inverter_Params.LIM_T_HEATSINK_WARN_L;
            AnalogSignals[adc_id_thsink] = &adc_tempheatsink;
            adc_tempheatsink.monitored = true;
        }

        if(Inverter_Params.ADC_REG_TSTATOR!=NULL)
        {
            //Todo- remove magic numbers
            adc_tempstator.lpf_alpha = Lpf10Hz_2Hz;
            adc_tempstator.coef[0] = 1.15319E+03;
            adc_tempstator.coef[1] = -1.18491;
            adc_tempstator.coef[2] = 4.60180E-04;
            adc_tempstator.coef[3] = -6.14265E-08;
            adc_tempstator.order   = 3;
            adc_tempstator.crith = Motor_Params.LIM_T_STATOR_CRIT_H;
            adc_tempstator.critl = Motor_Params.LIM_T_STATOR_CRIT_L;
            adc_tempstator.warnh = Motor_Params.LIM_T_STATOR_WARN_H;
            adc_tempstator.warnl = Motor_Params.LIM_T_STATOR_WARN_L;
            AnalogSignals[adc_id_tstator] = &adc_tempstator;
            adc_tempstator.monitored = true;
        }

        if(Inverter_Params.ADC_REG_TCPU!=NULL)
        {
            //Todo - lose the magic numbers
            adc_tempcpu.lpf_alpha = Lpf10Hz_2Hz;
            adc_tempcpu.order = 1;
            adc_tempcpu.coef[0] = F2837X_CPU_B;
            adc_tempcpu.coef[1] = F2837X_CPU_M;
            adc_tempcpu.crith = Inverter_Params.LIM_T_CPU_CRIT_H;
            adc_tempcpu.critl = Inverter_Params.LIM_T_CPU_CRIT_L;
            adc_tempcpu.warnh = Inverter_Params.LIM_T_CPU_WARN_H;
            adc_tempcpu.warnl = Inverter_Params.LIM_T_CPU_WARN_L;
            AnalogSignals[adc_id_tcpu] = &adc_tempcpu;
            adc_tempcpu.monitored = true;
        }

        //Zeroed out the coefficient here since the value is calculated from IxV
        adc_powerbus.lpf_alpha = 1.0;
        adc_powerbus.coef[0] = 0.0;
        adc_powerbus.coef[1] = 0.0;
        adc_powerbus.order   = 1;
        AnalogSignals[adc_id_pdc] = &adc_powerbus;
        adc_powerbus.monitored = false;

        if(Inverter_Params.ADC_REG_SPARE1!=NULL)
        {
            if( (Inverter_Params.InverterSel == ELMO_PMU) && (Inverter_Params.Genset != GensetNone) )
            {
                //On the Elmo PMU, we are using this as the ESC current sensor
                //ACS712-ELC-30A is 66mV/A, powered at 5V (2.5V @ 0A)
                //ADC -> power LUT created 20210513
                adc_spareinput1.lpf_alpha = Lpf10Hz_2Hz;
                adc_spareinput1.coef[0] = 1001.435;
                adc_spareinput1.coef[1] = -1332.689;
                adc_spareinput1.order   = 1;
                AnalogSignals[adc_id_spare1] = &adc_spareinput1;
                adc_spareinput1.monitored = false;
            }
        }

        if(Inverter_Params.ADC_REG_SPARE2!=NULL)
        {
            //Not using this, zero out the coefs
            adc_spareinput2.lpf_alpha = 1.0;
            adc_spareinput2.coef[0] = 0.0;
            adc_spareinput2.coef[1] = 0.;
            adc_spareinput2.order   = 1;
            AnalogSignals[adc_id_spare2] = &adc_spareinput2;
            adc_spareinput2.monitored = false;
        }

        if(Inverter_Params.ADC_REG_SPEEDKNOB!=NULL)
        {
            //Not using this, zero out the coefs
            adc_speedknob.lpf_alpha = Lpf10Hz_2Hz;
            adc_speedknob.coef[0] = 0.0;
            adc_speedknob.coef[1] = 1.0;
            adc_speedknob.order   = 1;
            AnalogSignals[adc_id_speedknob] = &adc_speedknob;
            adc_speedknob.monitored = false;
        }


        //Begin setup of 40kW smart-passive ADCs

        //Ambient Temperature
        if(Inverter_Params.ADC_REG_TAMB!=NULL)
        {
            //Todo - add the coefficients
            adc_tempambient.lpf_alpha = Lpf10Hz_2Hz;
            adc_tempambient.coef[0] = 0.0;
            adc_tempambient.coef[1] = 1.0;
            adc_tempambient.order   = 1;
            AnalogSignals[adc_id_tamb] = &adc_tempambient;
            adc_tempambient.monitored = true;
        }

        //Oil Temperature
        if(Inverter_Params.ADC_REG_T_OIL!=NULL)
        {
            //Todo - add the coefficients
            adc_tempoil.lpf_alpha = Lpf10Hz_2Hz;
            adc_tempoil.coef[0] = 0.0;
            adc_tempoil.coef[1] = 1.0;
            adc_tempoil.order   = 1;
            AnalogSignals[adc_id_toil] = &adc_tempoil;
            adc_tempoil.monitored = true;
        }

        //Coolant Temperature
        if(Inverter_Params.ADC_REG_T_COOLANT!=NULL)
        {
            //Todo - add the coefficients
            adc_tempcoolant.lpf_alpha = Lpf10Hz_2Hz;
            adc_tempcoolant.coef[0] = 0.0;
            adc_tempcoolant.coef[1] = 1.0;
            adc_tempcoolant.order   = 1;
            AnalogSignals[adc_id_tcool] = &adc_tempcoolant;
            adc_tempcoolant.monitored = true;
        }

        //24V supply current
        if(Inverter_Params.ADC_REG_I24V!=NULL)
        {
            //Todo - add the coefficients
            adc_current24v0.lpf_alpha = Lpf10Hz_2Hz;
            adc_current24v0.coef[0] = 0.0;
            adc_current24v0.coef[1] = 1.0;
            adc_current24v0.order   = 1;
            AnalogSignals[adc_id_i24v] = &adc_current24v0;
            adc_current24v0.monitored = true;
        }

        //28V supply current
        if(Inverter_Params.ADC_REG_I28V!=NULL)
        {
            //Todo - add the coefficients
            adc_current28v0.lpf_alpha = Lpf10Hz_2Hz;
            adc_current28v0.coef[0] = 0.0;
            adc_current28v0.coef[1] = 1.0;
            adc_current28v0.order   = 1;
            AnalogSignals[adc_id_i28v] = &adc_current28v0;
            adc_current28v0.monitored = true;
        }

        //12V battery
        if(Inverter_Params.ADC_REG_I12BAT!=NULL)
        {
            //Todo - add the coefficients
            adc_current12v0.lpf_alpha = Lpf10Hz_2Hz;
            adc_current12v0.coef[0] = 0.0;
            adc_current12v0.coef[1] = 1.0;
            adc_current12v0.order   = 1;
            AnalogSignals[adc_id_i12v] = &adc_current12v0;
            adc_current12v0.monitored = true;
        }

        //Fuel pump current
        if(Inverter_Params.ADC_REG_IFP!=NULL)
        {
            //Todo - add the coefficients
            adc_currentfp.lpf_alpha = Lpf10Hz_2Hz;
            adc_currentfp.coef[0] = 0.0;
            adc_currentfp.coef[1] = 1.0;
            adc_currentfp.order   = 1;
            AnalogSignals[adc_id_ifp] = &adc_currentfp;
            adc_currentfp.monitored = true;
        }

        //3.3V supply
        if(Inverter_Params.ADC_REG_3V3!=NULL)
        {
            //Todo - add the coefficients
            adc_voltage3v3.lpf_alpha = Lpf10Hz_2Hz;
            adc_voltage3v3.coef[0] = 0.0;
            adc_voltage3v3.coef[1] = 1.0;
            adc_voltage3v3.order   = 1;
            AnalogSignals[adc_id_v3v3] = &adc_voltage3v3;
            adc_voltage3v3.monitored = true;
        }

        //1.2V supply
        if(Inverter_Params.ADC_REG_1V2!=NULL)
        {
            //Todo - add the coefficients
            adc_voltage1v2.lpf_alpha = Lpf10Hz_2Hz;
            adc_voltage1v2.coef[0] = 0.0;
            adc_voltage1v2.coef[1] = 1.0;
            adc_voltage1v2.order   = 1;
            AnalogSignals[adc_id_v1v2] = &adc_voltage1v2;
            adc_voltage1v2.monitored = true;
        }

        //5V supply
        if(Inverter_Params.ADC_REG_5V!=NULL)
        {
            //Todo - add the coefficients
            adc_voltage5v0.lpf_alpha = Lpf10Hz_2Hz;
            adc_voltage5v0.coef[0] = 0.0;
            adc_voltage5v0.coef[1] = 1.0;
            adc_voltage5v0.order   = 1;
            AnalogSignals[adc_id_v5v0] = &adc_voltage5v0;
            adc_voltage5v0.monitored = true;
        }

        //24V supply voltage
        if(Inverter_Params.ADC_REG_24V!=NULL)
        {
            //Todo - add the coefficients
            adc_voltage24v0.lpf_alpha = Lpf10Hz_2Hz;
            adc_voltage24v0.coef[0] = 0.0;
            adc_voltage24v0.coef[1] = 1.0;
            adc_voltage24v0.order   = 1;
            AnalogSignals[adc_id_v24v0] = &adc_voltage24v0;
            adc_voltage24v0.monitored = true;
        }

        //28V supply voltage
        if(Inverter_Params.ADC_REG_28V!=NULL)
        {
            //Todo - add the coefficients
            adc_voltage28v0.lpf_alpha = Lpf10Hz_2Hz;
            adc_voltage28v0.coef[0] = 0.0;
            adc_voltage28v0.coef[1] = 1.0;
            adc_voltage28v0.order   = 1;
            AnalogSignals[adc_id_v28v0] = &adc_voltage28v0;
            adc_voltage28v0.monitored = true;
        }

        //12V battery voltage
        if(Inverter_Params.ADC_REG_12VBAT!=NULL)
        {
            //Todo - add the coefficients
            adc_voltage12v0.lpf_alpha = Lpf10Hz_2Hz;
            adc_voltage12v0.coef[0] = 0.0;
            adc_voltage12v0.coef[1] = 1.0;
            adc_voltage12v0.order   = 1;
            AnalogSignals[adc_id_v12v0] = &adc_voltage12v0;
            adc_voltage12v0.monitored = true;
        }





#if FAKEA_D

    int ind=0;

    for(ind=0;ind<adc_num_ids;ind++)
    {
        AnalogSignals[ind]->value = (AnalogSignals[ind]->crith + AnalogSignals[ind]->critl)/2;
    }

#endif
}


void AnalogSensorSuite_Int()
{
#if ( ! DISABLEA_D)
    //Only check phase current sensors if the phase U current register is defined
    //Check and filter phase current signals
    if(Inverter_Params.ADC_REG_IU!=NULL)
    {
        g_adc_currentu = processAnalogSignalPoly(&adc_currentu, *Inverter_Params.ADC_REG_IU*2.0 - offset_shntU*ADC_CNT_MAX);
        g_adc_currentv = processAnalogSignalPoly(&adc_currentv, *Inverter_Params.ADC_REG_IV*2.0 - offset_shntV*ADC_CNT_MAX);
        g_adc_currentw = processAnalogSignalPoly(&adc_currentw, *Inverter_Params.ADC_REG_IW*2.0 - offset_shntW*ADC_CNT_MAX);
        /*
        current_sensor.Us = (float)((*Inverter_Params.ADC_REG_IU) * ADC_PU_SCALE_FACTOR*2.0 - offset_shntU)*Inverter_Params.Sense_Current_M + Inverter_Params.Sense_Current_B;
        current_sensor.Vs = (float)((*Inverter_Params.ADC_REG_IV) * ADC_PU_SCALE_FACTOR*2.0 - offset_shntV)*Inverter_Params.Sense_Current_M + Inverter_Params.Sense_Current_B;
        current_sensor.Ws = (float)((*Inverter_Params.ADC_REG_IW) * ADC_PU_SCALE_FACTOR*2.0 - offset_shntW)*Inverter_Params.Sense_Current_M + Inverter_Params.Sense_Current_B;

        //Software LPF the current signals
        LPF_MACRO(current_sensor_lpf.Us,current_sensor.Us,LpfInt_5kHz);
        LPF_MACRO(current_sensor_lpf.Vs,current_sensor.Vs,LpfInt_5kHz);
        LPF_MACRO(current_sensor_lpf.Ws,current_sensor.Ws,LpfInt_5kHz);
        */

    }

    //Check and filter phase current signals
    if(Inverter_Params.ADC_REG_IBUS!=NULL)                  //Only check bus current sensor directly if the bus current sensor register is defined
    {
        if(Inverter_Params.Electronics == DriveExternal)    //Check each current sensor on the power board
        {
            //Defining current OUT of the PMU to be positive
            g_adc_currentbuselmo1 = processAnalogSignalPoly(&adc_currentbuselmo1, *Inverter_Params.ADC_REG_IBUS);
            g_adc_currentbuselmo2 = processAnalogSignalPoly(&adc_currentbuselmo2, *Inverter_Params.ADC_REG_IBUS2);
            //Manually set the bus current equal to the sum of the two
            adc_currentbus.value = adc_currentbuselmo1.value + adc_currentbuselmo2.value;
            /*
            LPF_MACRO(ElmoCurrent[0],-1.0*((float)(*Inverter_Params.ADC_REG_IBUS)*ADC_PU_SCALE_FACTOR*225.4665687 - 160.9405985),LpfInt_50Hz);
            LPF_MACRO(ElmoCurrent[1],-1.0*((float)(*Inverter_Params.ADC_REG_IBUS2)*ADC_PU_SCALE_FACTOR*(-221.5791954) + 160.0438727),LpfInt_50Hz);
            LPF_MACRO(current_sensor_lpf.Bus,(ElmoCurrent[0]+ElmoCurrent[1]),LpfInt_2Hz);
            */
        }
        else                                                //Check the bus current sensor
        {
            g_adc_currentbus = processAnalogSignalPoly(&adc_currentbus, *Inverter_Params.ADC_REG_IBUS*2.0 - offset_shntBUS*ADC_CNT_MAX);
            /*
            current_sensor.Bus = (float)((*Inverter_Params.ADC_REG_IBUS) * ADC_PU_SCALE_FACTOR*2.0 - offset_shntBUS)*(-1.0)*Inverter_Params.Sense_Current_M + Inverter_Params.Sense_Current_B;
            LPF_MACRO(current_sensor_lpf.Bus,current_sensor.Bus,LpfInt_50Hz);
            */
        }
    }
    else
    {
        //Drives w/internal electronics calculate the bus current from the 3 phase currents and corresponding duty cycle
        if(Inverter_Params.Electronics==DriveInternal)
        {
            adc_currentbus.value = (adc_currentu.value*svgen1.Ta+adc_currentv.value*svgen1.Tb+adc_currentw.value*svgen1.Tc)/2.0;
//            current_sensor.Bus = (current_sensor.Us*svgen1.Ta+current_sensor.Vs*svgen1.Tb+current_sensor.Ws*svgen1.Tc)/2.0; //Calculate from phase currents if no bus current sensor
        }
    }

    g_adc_currentbus = adc_currentbus.value;

    //Check if phasae U voltage sensor register defined, and only sense phase voltage if true
    if(Inverter_Params.ADC_REG_VU!=NULL)
    {
        g_adc_voltageu = processAnalogSignalPoly(&adc_voltageu, *Inverter_Params.ADC_REG_VU);
        g_adc_voltagev = processAnalogSignalPoly(&adc_voltagev, *Inverter_Params.ADC_REG_VV);
        g_adc_voltagew = processAnalogSignalPoly(&adc_voltagew, *Inverter_Params.ADC_REG_VW);

        /*
        phase_voltage.U = (float)(*Inverter_Params.ADC_REG_VU)*ADC_PU_SCALE_FACTOR*Inverter_Params.Sense_Voltage_M + Inverter_Params.Sense_Voltage_B;
        phase_voltage.V = (float)(*Inverter_Params.ADC_REG_VV)*ADC_PU_SCALE_FACTOR*Inverter_Params.Sense_Voltage_M + Inverter_Params.Sense_Voltage_B;;
        phase_voltage.W = (float)(*Inverter_Params.ADC_REG_VW)*ADC_PU_SCALE_FACTOR*Inverter_Params.Sense_Voltage_M + Inverter_Params.Sense_Voltage_B;;
        */
    }

    //Only check bus voltage sensor if bus voltage sensor register is defined
    if(Inverter_Params.ADC_REG_VBUS!=NULL)
    {
        g_adc_voltagebus = processAnalogSignalPoly(&adc_voltagebus, *Inverter_Params.ADC_REG_VBUS);
        /*
        BusV = (float)(*Inverter_Params.ADC_REG_VBUS)*ADC_PU_SCALE_FACTOR*Inverter_Params.Sense_Voltage_M + Inverter_Params.Sense_Voltage_B;
        LPF_MACRO(BusVF,BusV,LpfInt_5kHz);
        */
    }
#endif
}

//100Hz Analog Signal Processing
void AnalogSensorSuite_100Hz()
{
#if ( ! DISABLEA_D)
    g_adc_powerbus = adc_voltagebus.value*adc_currentbus.value;
    adc_powerbus.value = g_adc_powerbus;
//    LPF_MACRO(DCPowerF,(BusVF*current_sensor_lpf.Bus), Lpf100Hz_2Hz);
#endif
}

//10Hz Analog Signal Processing
void AnalogSensorSuite_10Hz()
{
#if ( ! DISABLEA_D)
    // ************************************
    //  Begin Temp Sensor Signals
    // ************************************

    //Power board temp sensor
    if(Inverter_Params.ADC_REG_TPDB != NULL)
    {
        g_adc_temppowerboard = processAnalogSignalPoly(&adc_temppowerboard,*Inverter_Params.ADC_REG_TPDB);
    }

    //logic board temp sensor
    if(Inverter_Params.ADC_REG_TBRD!=NULL)
    {
        g_adc_templogicboard = processAnalogSignalPoly(&adc_templogicboard,*Inverter_Params.ADC_REG_TBRD);
    }

    //10K Pullup
    //y = -2.95347E-07x3 + 5.90605E-04x2 - 4.57858E-01x + 2.68341E+02 120c-250C
    //y = -1.00620E-08x3 + 5.25726E-05x2 - 1.27645E-01x + 2.00421E+02

    //3.48k Pullup
    //y = -6.14265E-08x3 + 4.60180E-04x2 - 1.18491E+00x + 1.15319E+03
    //y = -3.86031E-09x3 + 2.74442E-05x2 - 1.03530E-01x + 2.57168E+02

    if(Inverter_Params.ADC_REG_TSTATOR!=NULL)
    {
        //Todo - get rid of magic numbers
        if((*Inverter_Params.ADC_REG_TSTATOR) < 2500)
        {
            adc_tempstator.coef[3] = -265.2784832;
            adc_tempstator.coef[2] = 460.4372713;
            adc_tempstator.coef[1] = -424.05888;
            adc_tempstator.coef[0] = 2.57168E+02;
        }
        else
        {
            adc_tempstator.coef[3] = -4221.196938;
            adc_tempstator.coef[2] = 7720.539259;
            adc_tempstator.coef[1] = -4853.39136;
            adc_tempstator.coef[0] = 1.15319E+03;
        }

        g_adc_tempstator = processAnalogSignalPoly(&adc_tempstator, *Inverter_Params.ADC_REG_TSTATOR);
    }


    g_adc_tempcpu = processAnalogSignalPoly(&adc_tempcpu, *Inverter_Params.ADC_REG_TCPU);

    if(Inverter_Params.ADC_REG_TMODULE!=NULL)
    {
        g_adc_tempdevice = processAnalogSignalPoly(&adc_tempdevice,*Inverter_Params.ADC_REG_TMODULE);
    }

    if(Inverter_Params.ADC_REG_THSINK!=NULL)
    {
        g_adc_tempheatsink = processAnalogSignalPoly(&adc_tempheatsink,*Inverter_Params.ADC_REG_THSINK);
    }

    if(Inverter_Params.ADC_REG_SPARE1 != NULL)
    {
        g_adc_spareinput1 = processAnalogSignalPoly(&adc_spareinput1, *Inverter_Params.ADC_REG_SPARE1);
    }

    if(Inverter_Params.ADC_REG_SPARE2 != NULL)
    {
        g_adc_spareinput2 = processAnalogSignalPoly(&adc_spareinput2, *Inverter_Params.ADC_REG_SPARE2);
    }

    if(Inverter_Params.ADC_REG_SPEEDKNOB != NULL)
    {
        g_adc_speedknob = processAnalogSignalPoly(&adc_speedknob, *Inverter_Params.ADC_REG_SPEEDKNOB);
    }

    // ************************************
    //  End Temp Sensor Signals
    // ************************************

#if 0
    // ************************************
    //  Begin speedknob and misc signals
    // ************************************
    if(Inverter_Params.KnobSelect == angleErrorKnob)
    {
        AngleOffsetKnob = coerce(-0.5,0.5,(((float)((signed int)*Inverter_Params.ADC_REG_SPEEDKNOB))/4096.0 - 0.5));  /*  Range is [ -0.5, 0.5]  */;
    }
    else if (Inverter_Params.KnobSelect == IQDisturbanceKnob)
    {
        IQDistKnob = (((float)((signed int)*Inverter_Params.ADC_REG_SPEEDKNOB))/4096.0)*10;  /*  Range is [ 0.0,10.0]  */
    }
    else if (Inverter_Params.KnobSelect == speedKnob)
    {
        SpeedKnob = ((float)((signed int)*Inverter_Params.ADC_REG_SPEEDKNOB))/4096.0;  /*  Range is [ 0.0, 1.0]  */
    }
#endif

#endif
}

/**Function: processAnalogSignalPoly
 * ------------------
 * Filters an input and applies up to a 3rd order polynomial to it
 * ex. y = Ax^3 + Bx^2 + Cx + D
 *
 * Outputs:
 * Latest filtered and processed value of this Analog_Signal
 *
 * Inputs:
 * sgl:    pointer to an Analog_Signal struct for the desired measurement
 * regval:  ADC register value to process into this Analog_Signal
  */
float processAnalogSignalPoly(struct Analog_Signal *sgl, int16_t regval)
{
    uint16_t i;
    float sum=0;
    float raw_pu=0.0;

    sgl->raw = regval;
    raw_pu = regval*ADC_PU_SCALE_FACTOR;

    LPF_MACRO((sgl->filtered), raw_pu, (sgl->lpf_alpha))

    if((sgl->order)<=1)
    {
        //Don't want to deal with powf on a 1st order polynomial, just do mx+b
        sgl->value = (sgl->coef[1])*(sgl->filtered) + (sgl->coef[0]);
    }
    else
    {
        for(i=(sgl->order);i>0;i--)
        {
            sum= sum + sgl->coef[i]*powf(sgl->filtered, i);
        }
        sgl->value = sum + (sgl->coef[0]);    //Coef[0] is the zeroth order coefficient, or just offset.
    }

    return (sgl->value);
}

/**Function: checkEDFCurrentSensor
 * ------------------
 * Checks all analog signals in AnalogSignals struct array
 * against their high and low critical limits
 *
 * @returns true if the ESC current sensor is within a sensible range
 *
 * @param duty - 0-1.0 duty cycle of the ESC command, used in a LUT to get expected current
*/
bool checkEDFCurrentSensor(float duty)
{
    static uint32_t falsecount;
    bool result = false;

    float expectedCurrent = 199.01*powf(duty,2) + 110.06*duty - 3.2992;
    float tol = 0.3;    //% tolerance
    if(duty >= 0.2)
    {
        if(inRange(adcGetSignalValue(adc_id_spare1), expectedCurrent*(1.0+tol), expectedCurrent*(1.0-tol)))
        {
            falsecount = 0;
            result = true;
        }
        else
        {
            //3 seconds of mismatched cooling power
            falsecount++;
            if(falsecount >= 3000L*Motor_Params.LOOP_FREQ_KHZ)
            {
                result = false;
            }
            else
            {
                result = true;
            }
        }
    }
    else
    {
        result = true;  //Return true if the fans are supposed to be off anyway
    }
    return result;
}

uint16_t AmpstoRawAdc(float amps, int ch)
{
    float off=0.0;

    if      (ch==CMPSS_CURR_U) off=offset_shntU;
    else if (ch==CMPSS_CURR_V) off=offset_shntV;
    else if (ch==CMPSS_CURR_W) off=offset_shntW;
    return (Uint16)(((amps/(Inverter_Params.Sense_Current_M))+off)/(ADC_PU_SCALE_FACTOR*2.0));
}

/*Function: getADCCriticalFaults
 * ------------------
 * Checks all analog signals in AnalogSignals struct array
 * against their high and low critical limits
 *
 * Outputs:
 * Error code for the first out-of-bounds analog signal when compared against
 * its critical high and low limits
 *
 * Inputs:
 * None
*/
enum AnalogFaultVariables getADCCriticalFaults(void)
{
    //Todo - take a snapshot of all monitored values at the fault point
    uint16_t id = 0;
    uint16_t dir = 0;

    for(id=0;id<adc_num_ids;id++)
    {
        if(AnalogSignals[id]->monitored == true)
        {
            if( (AnalogSignals[id]->value) >= (AnalogSignals[id]->crith) )
            {
                AnalogSignals[id]->lastfaultvalue = AnalogSignals[id]->value;
                AnalogSignals[id]->numfaults++;
                dir = 1;
            }
            else if( (AnalogSignals[id]->value) <= (AnalogSignals[id]->critl) )
            {
                AnalogSignals[id]->lastfaultvalue = AnalogSignals[id]->value;
                AnalogSignals[id]->numfaults++;
                dir = 0;
            }
            else
            {
                AnalogSignals[id]->numfaults=0; //Reset fault counter
            }

            if(AnalogSignals[id]->numfaults > 2)
            {
                return (enum AnalogFaultVariables)id;
            }
        }
    }


#if 0
    switch(id)
    {
        case adc_id_iu:
            return FLT_ADC_I_U;
        case adc_id_iv:
            return FLT_ADC_I_V;
        case adc_id_iw:
            return FLT_ADC_I_W;
        case adc_id_idc:
            return FLT_ADC_I_BUS;
        case adc_id_idcelmo1:
            return FLT_ADC_I_BUS;
        case adc_id_idcelmo2:
            return FLT_ADC_I_BUS;
        case adc_id_vdc:
            if(dir == 1) {
                return FLT_ADC_OV_BUS;
            }
            else {
                return FLT_ADC_UV_BUS;
            }
        case adc_id_tcpu:
            return FLT_ADC_T_CPU;
        case adc_id_tstator:
            return FLT_ADC_T_STAT;
        case adc_id_tpdb:
            return FLT_ADC_T_PDB;
        case adc_id_tlogic:
            return FLT_ADC_T_BRD;
        case adc_id_thsink:
            return FLT_ADC_T_HS;
        case adc_id_tdevice:
            return FLT_ADC_T_DEV;
        default:
            break;
    }
#endif

    if(Inverter_Params.Genset != GensetNone)
    {
        if(!checkEDFCurrentSensor(ECU_GetEDFDuty()))
        {
            return FLT_ADC_I_ESC;
        }

        if(!checkCHTLims())
        {
            return FLT_ADC_T_ENG;
        }
    }

    return FLT_ADC_NONE;
}

/**Function: getADCWarnings
 * ------------------
 * Checks all analog signals in AnalogSignals struct array
 * against their high and low critical limits
 *
 * Outputs:
 * Bitfields that match the ID of ADC values that exceed their warning levels
 *
 * Inputs:
 * None
*/
uint32_t getADCWarnings(void)
{
    uint16_t id = 0;
    uint16_t dir = 0;
    uint32_t warnings = 0;

    //Todo - figure out if the high/low and dir even matter for warnings
    for(id=0;id<adc_num_ids;id++)
    {
        //Only check ones whose coef[1] is defined (if it's 0, then that ADC signal was never set up)
        if(AnalogSignals[id]->monitored == true)
        {
            if( (AnalogSignals[id]->value) >= (AnalogSignals[id]->warnh) )
            {
                dir = 1;
                warnings |= (1L << id);
            }
            else if( (AnalogSignals[id]->value) <= (AnalogSignals[id]->warnl) )
            {
                dir = 0;
                warnings |= (1L << id);
            }
        }
    }

    return warnings;
}

//Get methods for ADC values that will be externed and called in other modules
float adcGetSignalValue(enum adc_signal_id id) {return AnalogSignals[id]->value;}
float adcGetSignalRaw(enum adc_signal_id id) {return AnalogSignals[id]->raw;}
