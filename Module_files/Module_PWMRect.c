/* ============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	PWM.C

Target:			Cryo controller board

Author:			Mike Ricci

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle the processor PWM peripherals and associated data structures
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "F2837S_files/f28377pwm_drivers.h"
#include "Header/PWMRect.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1

extern volatile struct EPWM_REGS * ePWM[13];

// Function prototypes
void TurnOnFET(int chan, enum SwitchSide Side);
void Deadband(int chan);


// ****************************************************************************
//  Module resources - variables here //
// ****************************************************************************

// Function Definitions            //
void TurnOnFET(int chan, enum SwitchSide Side)
{
    //Still unclear whether TZCLR is needed, since we are exclusively using TZFRC
    EALLOW;
//    (*ePWM[chan]).TZCLR.bit.OST=1;
    if(Side==High)
    {
        (*ePWM[chan]).TZCTL.bit.TZA = 0x0001;   //Force high
        (*ePWM[chan]).TZCTL.bit.TZB = 0x0002;   //Force low
    }
    else if(Side==Low)
    {
        (*ePWM[chan]).TZCTL.bit.TZA = 0x0002;   //Force low
        (*ePWM[chan]).TZCTL.bit.TZB = 0x0001;   //Force high
    }
    (*ePWM[chan]).TZFRC.bit.OST = 1;
    EDIS;
}

void Deadband(int chan)
{
    EALLOW;
//    (*ePWM[chan]).TZCLR.bit.OST=1;
    (*ePWM[chan]).TZCTL.bit.TZA = 0x0002;   //Force low
    (*ePWM[chan]).TZCTL.bit.TZB = 0x0002;   //Force low
    (*ePWM[chan]).TZFRC.bit.OST = 1;
    EDIS;
}
