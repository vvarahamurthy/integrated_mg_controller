/* ============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	DIO.C

Target:			Cryo controller board

Author:			Mike Ricci

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle the processor GPIO pins -- digital IO, PWM, and other peripherals that share GPIO
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"

#include "Header/GPIO.h"
#include "Header/GUIC_Header.h"
#include "Header/State_machine.h"
#include "Header/ECU.h"

#include "F2837xS_GlobalPrototypes.h"
#include "F2837xS_Gpio_defines.h"
#include "Header/Globals.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1


extern bool ECAP1_TimeoutOK(void);
extern uint32_t ECAP1_GetPulseus(void);
extern bool inRange(float,float,float);

// Function prototypes
void Debounce_GPIO(void);
void Init_GPIO(void);
void SetupGPIO(uint16_t , uint16_t ,  uint16_t , uint16_t );
void Disable_gate_drive(void);
void Disable_gate_predriver(void);
void Enable_gate_drive(void);
void Clear_gate_driver(void);
void Reset_gate_driver(void);

void GPIO_FaultTrip(unsigned int );

void Debounce_Tach(void);

void SetupObsGPIO(void);
//Guic testing variable
int Localvartest = 12;
//  Module resources - variables here //


//  Module resources - variables here //
struct debounced_signal EstopGPIO;
struct debounced_signal StartGPIO;

struct debounced_signal FaultHU;	// U phase High gate driver fault
struct debounced_signal FaultLU;	// U phase Low gate driver fault
struct debounced_signal FaultHV;	// V phase High gate driver fault
struct debounced_signal FaultLV;	// V p hase Low gate driver fault
struct debounced_signal FaultHW;	// W phase High gate driver fault
struct debounced_signal FaultLW;	// W phase Low gate driver fault

// Function definitions

void Init_GPIO(void)
{
    if(Inverter_Params.Genset != GensetNone)
    {
        //ECU Interface GPIO Setup
        //**********************
        if(Inverter_Params.InverterSel == ELMO_PMU)
        {
            InitEPwm2Gpio();    // PWM for throttle signal to ECU
            SetupGPIO(Inverter_Params.GPIO_THROTTLE_NUM, GPIO_OUTPUT, 1, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_EDF_NUM, GPIO_OUTPUT, 1, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_RS232TX_NUM, GPIO_OUTPUT, 5, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_RS232RX_NUM, GPIO_INPUT, 5, (GPIO_ASYNC | GPIO_PULLUP) );

            SetupGPIO(Inverter_Params.GPIO_EFI_EN_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
            GPIO_WritePin(Inverter_Params.GPIO_EFI_EN_NUM,0);

            SetupGPIO(Inverter_Params.GPIO_TACH_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            EALLOW;
            InputXbarRegs.INPUT8SELECT = Inverter_Params.GPIO_TACH_NUM;    //input x-bar 8 is ECAP2
            EDIS;

#if(READTHROTTLE)
            SetupGPIO(85, GPIO_INPUT, 0, GPIO_QUAL6);

            EALLOW;
            InputXbarRegs.INPUT7SELECT = 85;    //ECAP1 is X-Bar input 7
            EDIS;
#else
            SetupGPIO(Inverter_Params.GPIO_START_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            StartGPIO.forced  = 0;
            StartGPIO.forceval = 0;

            if(Inverter_Params.StartStopSel==StSel_PWM) //Set up ECAP if we're watching the pulse width
            {
                EALLOW;
                InputXbarRegs.INPUT7SELECT = Inverter_Params.GPIO_START_NUM;    //ECAP1 is X-Bar input 7
                EDIS;
            }
#endif
        }
        else if(Inverter_Params.InverterSel==HPS400_SP)
        {
            InitEPwm2Gpio();
            SetupGPIO(Inverter_Params.GPIO_PWM_RADF_NUM, GPIO_OUTPUT, 1, GPIO_ASYNC);

            InitEPwm3Gpio();
            SetupGPIO(Inverter_Params.GPIO_PWM_RECTF_NUM, GPIO_OUTPUT, 1, GPIO_ASYNC);

            //Set up ECAP GPIOs
            SetupGPIO(Inverter_Params.GPIO_ECAP_FUEL_L_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_ECAP_FUEL_R_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_ECAP_RADT1_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_ECAP_RADT2_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_ECAP_BFRF_NUM, GPIO_INPUT, 0, GPIO_QUAL6);

            //2nd ESTOP setup
            SetupGPIO(Inverter_Params.GPIO_ESTOP2_NUM, GPIO_INPUT, 0, GPIO_QUAL6);

            //I2C GPIO initialization for SMBus
            SetupGPIO(Inverter_Params.GPIO_TBUS_SDA_NUM, GPIO_OUTPUT, 6, (GPIO_PULLUP | GPIO_OPENDRAIN | GPIO_ASYNC) );
            SetupGPIO(Inverter_Params.GPIO_TBUS_SCL_NUM, GPIO_OUTPUT, 6, (GPIO_PULLUP | GPIO_OPENDRAIN | GPIO_ASYNC) );

            //Set up Enable/disable outputs
            SetupGPIO(Inverter_Params.GPIO_24V_EN_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_28V_EN_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_IGN1EN_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_IGN2EN_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_FUELPUMP_EN_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);

            //Set up FLT inputs
            SetupGPIO(Inverter_Params.GPIO_24V_FLT_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_28V_FLT_NUM, GPIO_INPUT, 0, GPIO_QUAL6);

            //Todo - what kind of sensor is this? Just on/off if oil is above/below some level?
            SetupGPIO(Inverter_Params.GPIO_OILLVL_NUM, GPIO_INPUT, 0, GPIO_QUAL6);

            //Set up RS485 servo control (use PWM for now though)
            SetupGPIO(Inverter_Params.GPIO_SERVO_RX_NUM, GPIO_INPUT, 5, (GPIO_ASYNC | GPIO_PULLUP) );
            SetupGPIO(Inverter_Params.GPIO_SERVO_TX_NUM, GPIO_OUTPUT, 5, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_SERVO_TXEN_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
            GPIO_WritePin(Inverter_Params.GPIO_SERVO_TXEN_NUM, 1);



            EALLOW;
            InputXbarRegs.INPUT7SELECT = Inverter_Params.GPIO_ECAP_FUEL_L_NUM;    //ECAP1
            InputXbarRegs.INPUT8SELECT = Inverter_Params.GPIO_ECAP_FUEL_R_NUM;    //ECAP2
            InputXbarRegs.INPUT9SELECT = Inverter_Params.GPIO_TACH_NUM;             //ECAP3
            InputXbarRegs.INPUT10SELECT = Inverter_Params.GPIO_ECAP_BFRF_NUM;   //ECAP4
            InputXbarRegs.INPUT11SELECT = Inverter_Params.GPIO_ECAP_RADT1_NUM;   //ECAP5
            InputXbarRegs.INPUT12SELECT = Inverter_Params.GPIO_ECAP_RADT2_NUM;   //ECAP6
            EDIS;
        }
    }

    if(Inverter_Params.Electronics == DriveExternal)
    {
        SetupGPIO(Inverter_Params.GPIO_ELMO_STO_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
        GPIO_WritePin(Inverter_Params.GPIO_ELMO_STO_NUM, 0);

        SetupGPIO(Inverter_Params.GPIO_ELMO_EN_1_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
        SetupGPIO(Inverter_Params.GPIO_ELMO_EN_2_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
        SetupGPIO(Inverter_Params.GPIO_ELMO_FLT_1_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
        SetupGPIO(Inverter_Params.GPIO_ELMO_FLT_2_NUM, GPIO_INPUT, 0, GPIO_QUAL6);

        InitEPwm3Gpio();
        SetupGPIO(Inverter_Params.GPIO_ELMO_TC_1_NUM, GPIO_OUTPUT, 1, GPIO_ASYNC);
        SetupGPIO(Inverter_Params.GPIO_ELMO_TC_2_NUM, GPIO_OUTPUT, 1, GPIO_ASYNC);
    }


    //Inverters using the Infineon chips with the desat faults, ready signals, and nRST signals
    if(Inverter_Params.InverterSel==MC40 || Inverter_Params.InverterSel==MC15 || Inverter_Params.InverterSel==HONDA_LAUNCHPAD_DEBUG28379 || Inverter_Params.InverterSel == HONDA_LAUNCHPAD_DEBUG28377)
    {
        //Set up ready, fault and reset GPIOs
        if(Inverter_Params.Electronics==DriveInternal)
        {
            InitEPwm7Gpio();
            InitEPwm8Gpio();
            InitEPwm9Gpio();

            SetupGPIO(12, GPIO_OUTPUT, 1, GPIO_ASYNC);
            SetupGPIO(13, GPIO_OUTPUT, 1, GPIO_ASYNC);
            SetupGPIO(14, GPIO_OUTPUT, 1, GPIO_ASYNC);
            SetupGPIO(15, GPIO_OUTPUT, 1, GPIO_ASYNC);
            SetupGPIO(16, GPIO_OUTPUT, 5, GPIO_ASYNC);
            SetupGPIO(17, GPIO_OUTPUT, 5, GPIO_ASYNC);


            SetupGPIO(Inverter_Params.GPIO_FLTHU_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_FLTLU_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_FLTHV_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_FLTLV_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_FLTHW_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_FLTLW_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_RDYU_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_RDYV_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_RDYW_NUM, GPIO_INPUT, 0, GPIO_QUAL6);

            SetupGPIO(Inverter_Params.GPIO_NRST_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
            GPIO_WritePin(Inverter_Params.GPIO_NRST_NUM,0);

            SetupGPIO(Inverter_Params.GPIO_ENGD_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
            GPIO_WritePin(Inverter_Params.GPIO_ENGD_NUM,0);

            //Todo - add checks for drive internal etc here
            //Sensored commutation GPIO setup
            if(Motor_Params.ObsEn & SNS_ENC_SSI)
            {
                //SPI signals for SSI operation
                SetupGPIO(Inverter_Params.GPIO_SPI_CLK_NUM, GPIO_OUTPUT, 15, GPIO_ASYNC);
                SetupGPIO(Inverter_Params.GPIO_SPI_SOMI_NUM, GPIO_INPUT, 15, GPIO_ASYNC);
            }
            else if(Motor_Params.ObsEn & (SNS_HALLPLL | SNS_HALLRAW))
            {
                //Hall sensor setup
                SetupGPIO(Inverter_Params.GPIO_HALLAB_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
                SetupGPIO(Inverter_Params.GPIO_HALLBC_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
                SetupGPIO(Inverter_Params.GPIO_HALLCA_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
            }
            else if(Motor_Params.ObsEn & SNS_ENC_QEP)
            {
                //Just the uber test condition - need to confirm with george if these are correct
                if(Inverter_Params.InverterSel==MC40)
                {
                    //QEP A setup
                    GPIO_SetupPinOptions(Inverter_Params.GPIO_SPEEDPULSE_NUM, GPIO_INPUT, GPIO_QUAL6 );
                    GPIO_SetupPinMux(Inverter_Params.GPIO_SPEEDPULSE_NUM,0,5);

                    //QEP S setup (Unused)
                    GPIO_SetupPinOptions(64, GPIO_INPUT, GPIO_QUAL6 );
                    GPIO_SetupPinMux(64,0,5);

                    //Sensored commutation GPIO setup
                    if(Motor_Params.ObsEn & SNS_ENC_SSI)
                    {
                        //SPI signals for SSI operation
                        SetupGPIO(Inverter_Params.GPIO_SPI_CLK_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
                        SetupGPIO(Inverter_Params.GPIO_SPI_SOMI_NUM, GPIO_INPUT, 0, GPIO_ASYNC);
                    }
                    else if(Motor_Params.ObsEn & (SNS_HALLPLL | SNS_HALLRAW))
                    {
                        //Hall sensor setup
                        SetupGPIO(Inverter_Params.GPIO_HALLAB_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
                        SetupGPIO(Inverter_Params.GPIO_HALLBC_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
                        SetupGPIO(Inverter_Params.GPIO_HALLCA_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
                    }
                    else if(Motor_Params.ObsEn & SNS_ENC_QEP)
                    {
                        //Just the uber test condition - need to confirm with george if these are correct
                        if(Inverter_Params.InverterSel==MC40)
                        {
                #if 0
                            //QEP A setup
                            GPIO_SetupPinOptions(Inverter_Params.GPIO_SPEEDPULSE_NUM, GPIO_INPUT, GPIO_QUAL6 );
                            GPIO_SetupPinMux(Inverter_Params.GPIO_SPEEDPULSE_NUM,0,5);

                            //QEP S setup (Unused)
                            GPIO_SetupPinOptions(64, GPIO_INPUT, GPIO_QUAL6 );
                            GPIO_SetupPinMux(64,0,5);

                            //QEP I setup
                            GPIO_SetupPinOptions(Inverter_Params.GPIO_ONCEAROUND_NUM, GPIO_INPUT, GPIO_QUAL6);
                            GPIO_SetupPinMux(Inverter_Params.GPIO_ONCEAROUND_NUM,0,5);
                #endif
                        }
                        else
                        {
                            SetupGPIO(Inverter_Params.GPIO_QEPA_NUM, GPIO_INPUT, 5, GPIO_QUAL6);
                            SetupGPIO(Inverter_Params.GPIO_QEPB_NUM, GPIO_INPUT, 5, GPIO_QUAL6);
                            SetupGPIO(Inverter_Params.GPIO_QEPI_NUM, GPIO_INPUT, 5, GPIO_QUAL6);
                            /* Commenting out because this usually isn't used
                            GPIO_SetupPinOptions(Inverter_Params.GPIO_QEPS_NUM, GPIO_INPUT, GPIO_QUAL6);
                            GPIO_SetupPinMux(Inverter_Params.GPIO_QEPS_NUM, 0, 5);
                             */
                        }
                    }
                }
            }
        }
    }

        SetupGPIO(Inverter_Params.GPIO_LED1_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
        SetupGPIO(Inverter_Params.GPIO_LED2_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);

        SetupGPIO(Inverter_Params.GPIO_ESTOP_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
        EstopGPIO.forced = 0;
        EstopGPIO.forceval = 0;

        SetupGPIO(Inverter_Params.GPIO_START_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
        StartGPIO.forced  = 0;
        StartGPIO.forceval = 0;

        if(Inverter_Params.FaultOutputMode != FltO_Disabled)
        {
            SetupGPIO(Inverter_Params.GPIO_FLT_OUTPUT_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
        }

        //Set up CAN GPIO
        if(Inverter_Params.InverterSel == LAUNCHPAD_GENSET || Inverter_Params.InverterSel == LAUNCHPAD_MC28377)
        {
            SetupGPIO(Inverter_Params.GPIO_CAN_RX_NUM, GPIO_INPUT, 2, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_CAN_TX_NUM, GPIO_OUTPUT, 2, GPIO_PUSHPULL);
        }
        else
        {
            //We generally use 70 and 71 (CANA) on most controllers
            SetupGPIO(Inverter_Params.GPIO_CAN_RX_NUM, GPIO_INPUT, 5, GPIO_ASYNC);
            SetupGPIO(Inverter_Params.GPIO_CAN_TX_NUM, GPIO_OUTPUT, 5, GPIO_PUSHPULL);
        }

        //I2C GPIO initialization
        SetupGPIO(Inverter_Params.GPIO_I2C_SDA_NUM, GPIO_OUTPUT, 6, (GPIO_PULLUP | GPIO_OPENDRAIN | GPIO_ASYNC) );
        SetupGPIO(Inverter_Params.GPIO_I2C_SCL_NUM, GPIO_OUTPUT, 6, (GPIO_PULLUP | GPIO_OPENDRAIN | GPIO_ASYNC) );

        //RS422/485 Comm
        SetupGPIO(Inverter_Params.GPIO_RS422RX_NUM, GPIO_INPUT, 5, (GPIO_ASYNC | GPIO_PULLUP) );
        SetupGPIO(Inverter_Params.GPIO_RS422TX_NUM, GPIO_OUTPUT, 5, GPIO_ASYNC);
        SetupGPIO(Inverter_Params.GPIO_RS422TXEN_NUM, GPIO_OUTPUT, 0, GPIO_ASYNC);
        GPIO_WritePin(Inverter_Params.GPIO_RS422TXEN_NUM, 1);
}

/*Function: SetupGPIO
 * -----------------------
 * Combines the SetupPinOptions and SetupPinMux function calls
 *
 * Outputs:
 * None
 *
 * Inputs:
 * num:           GPIO number
 * dir:           Input/Output direction (GPIO_INPUT, GPIO_OUTPUT)
 * mux:           Mux position (refer to F2837xS datasheet)
 * flg:           Flags/Qualifiers defined in F2837xS_Gpio_defines.h
 */
void SetupGPIO(uint16_t num, uint16_t dir,  uint16_t mux, uint16_t flg)
{
    //Error out if the GPIO is not initialized
    if(num==0)
    {
//        asm ("      ESTOP0");
        return;
    }
    else
    {
        GPIO_SetupPinOptions(num, dir, flg);
        GPIO_SetupPinMux(num, GPIO_MUX_CPU1, mux);
    }
}

//  for digital filtering define 0xFFF as "1" and 0x000 as "0"																\
//  for 0xfff = 1, then .25 = 0x3FF, .75 = 0xC00; 0xF00 is schimtt trigger HIGH level, 0x0F0 is Schmitt trigger LOW level	\
//                  >>5 is divide by 32.  Filter is now 31/32 old value plus 1/32 new value = 0xfff>>5 = 0x07f				\
// Filters is :  filtered output = old filtered valued * .75 + physical input * .25											\

#define DEBOUNCE_MACRO(pin_num,debouncevar)                                                                                 \
                                                                                                                            \
if (debouncevar.forced == 1)                                               \
{                                                                              \
    debouncevar.debounced = debouncevar.forceval;                               \
}                                                                               \
else                                                                            \
{                                                                               \
    /*  Filter input here */                                                                                                            \
    debouncevar.GPIO_filtered = debouncevar.GPIO_filtered - (debouncevar.GPIO_filtered>>4);     /* Takes filtered output and subtracts filtered/4 = .75*filtered */ \
    if (GPIO_ReadPin(pin_num) == 1)  { debouncevar.GPIO_filtered = debouncevar.GPIO_filtered + 0xFF; }  /* Add 31/32 * physical input (Estop button GPIO60 ) */  \
    if (debouncevar.GPIO_filtered > 0xF00  && debouncevar.schmitt_flag == 0)    /* If filtered output greater than schmitt HIGH threshold  */   \
    {                                                                                                                                           \
        debouncevar.debounced = 1;                                                                                                              \
        debouncevar.schmitt_flag = 1;                                                                                                           \
    }                                                                                                                                           \
    if (debouncevar.GPIO_filtered < 0x0F0  && debouncevar.schmitt_flag == 1)    /* If filtered output lower than schmitt LOW threshold  */      \
    {                                                                                                                                           \
        debouncevar.debounced = 0;                                                                                                              \
        debouncevar.schmitt_flag = 0;                                                                                                           \
    }                                               \
}

/*
 * This routine debounces the Estop button and Start button GPIO inputs using a simple digital filter with a schmitt trigger on the output
 * It sets the Estop_debouced and Start_debounced variables appropriately for debounced state of input
 * This should be called at the speed the inputs want to be sampled to debounce
 */
void Debounce_GPIO(void)
{
    //  for digital filtering define 0xFFF as "1" and 0x000 as "0"
    //  for 0xfff = 1, then .25 = 0x3FF, .75 = 0xC00; 0xF00 is schimtt trigger HIGH level, 0x0F0 is Schmitt trigger LOW level
    //                  >>5 is divide by 32.  Filter is now 31/32 old value plus 1/32 new value = 0xfff>>5 = 0x07f
    // Filters is :  filtered output = old filtered valued * .75 + physical input * .25

    // Filter Estop input
    if(Inverter_Params.GPIO_ESTOP_NUM > 0 ) // this check only needs to be done once on inverter config for valid value, may not need to check all the time?
    {
        DEBOUNCE_MACRO(Inverter_Params.GPIO_ESTOP_NUM,EstopGPIO)
    }

    // Filter Start button input
    if(Inverter_Params.GPIO_START_NUM > 0 ) // this check only needs to be done once on inverter config for valid value, may not need to check all the time?
    {
        DEBOUNCE_MACRO(Inverter_Params.GPIO_START_NUM,StartGPIO)
    }


    if(Inverter_Params.Electronics==DriveInternal)
    {
        // Gate Driver FAULT U, V, & W inputs
        DEBOUNCE_MACRO(Inverter_Params.GPIO_FLTHU_NUM,FaultHU)
        DEBOUNCE_MACRO(Inverter_Params.GPIO_FLTLU_NUM,FaultLU)
        DEBOUNCE_MACRO(Inverter_Params.GPIO_FLTHV_NUM,FaultHV)
        DEBOUNCE_MACRO(Inverter_Params.GPIO_FLTLV_NUM,FaultLV)
        DEBOUNCE_MACRO(Inverter_Params.GPIO_FLTHW_NUM,FaultHW)
        DEBOUNCE_MACRO(Inverter_Params.GPIO_FLTLW_NUM,FaultLW)
    }
}


/*  Set GPIOs to disable gate drivers  */
void Disable_gate_drive(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_ENGD_NUM,0);         // Clear the "enable" line to the hi-amp gate driver ICs
    GPIO_WritePin(Inverter_Params.GPIO_NRST_NUM,0);         // Clear the "not-reset" line to the pre-driver gate drives
}

/*  Set GPIOs to enable gate drivers  */
void Enable_gate_drive(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_ENGD_NUM,1);         // Set the "enable" line to the hi-amp gate driver ICs
    GPIO_WritePin(Inverter_Params.GPIO_NRST_NUM,1);         // Set the "not-reset" line to the pre-driver gate drives
}

void Enable_gate_predriver(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_NRST_NUM,1);         // Set the "not-reset" line to the pre-driver gate drives
}

void Disable_gate_predriver(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_NRST_NUM,0);         // Clear the "not-reset" line to the pre-driver gate drives
}

void Disable_EFI(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_EFI_EN_NUM, 0);
}

void Enable_EFI(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_EFI_EN_NUM, 1);
}



void enableFuelPump(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_FUELPUMP_EN_NUM, 1);
}

void disableFuelPump(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_FUELPUMP_EN_NUM, 0);
}

void enableIgnition(uint16_t num)
{
    if(num==0)
    {
        GPIO_WritePin(Inverter_Params.GPIO_IGN1EN_NUM, 1);
        GPIO_WritePin(Inverter_Params.GPIO_IGN2EN_NUM, 1);
    }
    else if(num==1)
    {
        GPIO_WritePin(Inverter_Params.GPIO_IGN1EN_NUM, 1);
    }
    else if(num==2)
    {
        GPIO_WritePin(Inverter_Params.GPIO_IGN2EN_NUM, 1);
    }
}

void disableIgnition(uint16_t num)
{
    if(num==0)
    {
        GPIO_WritePin(Inverter_Params.GPIO_IGN1EN_NUM, 0);
        GPIO_WritePin(Inverter_Params.GPIO_IGN2EN_NUM, 0);
    }
    else if(num==1)
    {
        GPIO_WritePin(Inverter_Params.GPIO_IGN1EN_NUM, 0);
    }
    else if(num==2)
    {
        GPIO_WritePin(Inverter_Params.GPIO_IGN2EN_NUM, 0);
    }
}

void enable24VSupply(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_24V_EN_NUM,1);
}

void disable24VSupply(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_24V_EN_NUM,0);
}

void enable28VSupply(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_28V_EN_NUM,1);
}

void disable28VSupply(void)
{
    GPIO_WritePin(Inverter_Params.GPIO_28V_EN_NUM,0);
}



/**Function: GPIO_ElmoEn
 * ------------------
 * Toggles the INHIBIT input on each Elmo
 *
 * Outputs:
 * GPIO Status 1 or 0
 *
 * Inputs:
 * num: Which Elmo are we querying for the FAULT status?
 * val: 1 = Enable - GPIO HIGH
 *      0 = Inhibit - GPIO LOW
 */
void GPIO_ElmoEn(int num, int val)
{
    GPIO_WritePin(num, val);
}

/**Function: GPIO_ElmoSTO
 * ------------------
 * Actuates the STO line on the Elmos. Both of the STO signals come from the same GPIO
 *
 * Outputs:
 * None
 *
 * Inputs:
 * val: 1 = bring STO line HIGH
 *      0 = bring STO line LOW
 */
void GPIO_ElmoSTO(int val)
{
    GPIO_WritePin(Inverter_Params.GPIO_ELMO_STO_NUM, val);
}


//true is "faulted, active"
//false is "unfaulted, inactive"
void GPIO_FaultTrip(unsigned int active)
{
    if(Inverter_Params.FaultOutputMode==FltO_TripHigh)
    {
        GPIO_WritePin(Inverter_Params.GPIO_FLT_OUTPUT_NUM, active);
    }
    else if(Inverter_Params.FaultOutputMode==FltO_TripLow)
    {
        GPIO_WritePin(Inverter_Params.GPIO_FLT_OUTPUT_NUM, !(active));
    }
}

/*************************************************************/
/*  Fault Detection code                                     */
/*  This runs in the foreground (main wait loop)             */
/*************************************************************/


enum GPIOFaultVariables getGPIOCriticalFaults(void)
{
	enum GPIOFaultVariables Flt = FLT_GPIO_NONE;
	static uint32_t PWMFltCtr;

    if(EstopGPIO.debounced == 0)
    {
        Flt |= FLT_GPIO_ESTOP;
    }

#if 0
	if ((*ePWM[Inverter_Params.PWM_U_NUM]).TZFLG.bit.OST==0x1 || (*ePWM[Inverter_Params.PWM_U_NUM]).TZFLG.bit.CBC==0x1 )
	{
		Flt |= Utripflag;
	}
	if ((*ePWM[Inverter_Params.PWM_V_NUM]).TZFLG.bit.OST==0x1 || (*ePWM[Inverter_Params.PWM_V_NUM]).TZFLG.bit.CBC==0x1)
	{
		Flt |= Vtripflag;
	}
	if ((*ePWM[Inverter_Params.PWM_W_NUM]).TZFLG.bit.OST==0x1 || (*ePWM[Inverter_Params.PWM_W_NUM]).TZFLG.bit.CBC==0x1)
	{
		Flt |= Wtripflag;
	}
#endif
	if(Inverter_Params.Electronics == DriveInternal)
	{
        if (FaultHU.debounced==0)
        {
            Flt |= FLT_GPIO_DESAT_HU;
        }
        if (FaultLU.debounced==0)
        {
            Flt |= FLT_GPIO_DESAT_LU;
        }
        if (FaultHV.debounced==0)
        {
            Flt |= FLT_GPIO_DESAT_HV;
        }
        if (FaultLV.debounced==0)
        {
            Flt |= FLT_GPIO_DESAT_LV;
        }
        if (FaultHW.debounced==0)
        {
            Flt |= FLT_GPIO_DESAT_HW;
        }
        if (FaultLW.debounced==0)
        {
            Flt |= FLT_GPIO_DESAT_LW;
        }
        if(!GPIO_ReadPin(Inverter_Params.GPIO_RDYU_NUM))
        {
            Flt |= FLT_GPIO_RDY_U;
        }
        if(!GPIO_ReadPin(Inverter_Params.GPIO_RDYV_NUM))
        {
            Flt |= FLT_GPIO_RDY_V;
        }
        if(!GPIO_ReadPin(Inverter_Params.GPIO_RDYW_NUM))
        {
            Flt |= FLT_GPIO_RDY_W;
        }
	}

	if(Inverter_Params.StartStopSel == StSel_PWM)
	{
	    //Making sure the PWM input is within the correct range
	    if( !ECAP1_TimeoutOK() || !inRange(ECAP1_GetPulseus(), Inverter_Params.StartPWMHigh_us*1.05, Inverter_Params.StartPWMLow_us*0.95 ) )
	    {
	        PWMFltCtr++;
	    }
	    else
	    {
	        PWMFltCtr=0;
	    }
	    //FLT out if out of range for more than a second
	    if(PWMFltCtr>=Motor_Params.LOOP_FREQ_KHZ*1000)
	    {
	        Flt |= FLT_GPIO_PWM_INPUT;
	    }
	}

	return(Flt);
}

/*Function: GPIO_ElmoFlt
 * ------------------
 * Runs a state machine on each individual Elmo object to enable/disable/fault detect
 * This is similar to the controller state machine that lives inside Module_Controller.c
 *
 * Outputs:
 * GPIO Status 1 or 0
 *
 * Inputs:
 * num: Which Elmo are we querying for the FAULT status?
 */
uint16_t GPIO_ElmoFlt(int num)
{
    if(num==0)
    {
        return GPIO_ReadPin(Inverter_Params.GPIO_ELMO_FLT_1_NUM);
    }
    if(num==1)
    {
        return GPIO_ReadPin(Inverter_Params.GPIO_ELMO_FLT_2_NUM);
    }
    else
    {
        return 0;
    }
}

void SetupObsGPIO(void)
{
    //Observer-specific GPIO setup

    //Sensored commutation GPIO setup
    if(Motor_Params.ObsEn & SNS_ENC_SSI)
    {
        //SPI signals for SSI operation
        SetupGPIO(Inverter_Params.GPIO_SPI_CLK_NUM, GPIO_OUTPUT, 15, GPIO_ASYNC);
        SetupGPIO(Inverter_Params.GPIO_SPI_SOMI_NUM, GPIO_INPUT, 15, GPIO_ASYNC);
    }
    else if(Motor_Params.ObsEn & (SNS_HALLPLL | SNS_HALLRAW))
    {
        //Hall sensor setup
        SetupGPIO(Inverter_Params.GPIO_HALLAB_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
        SetupGPIO(Inverter_Params.GPIO_HALLBC_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
        SetupGPIO(Inverter_Params.GPIO_HALLCA_NUM, GPIO_INPUT, 0, GPIO_QUAL6);
    }
    else if(Motor_Params.ObsEn & SNS_ENC_QEP)
    {
        //Just the uber test condition - need to confirm with george if these are correct
        if(Inverter_Params.InverterSel==MC40)
        {
            //QEP A setup
            GPIO_SetupPinOptions(Inverter_Params.GPIO_SPEEDPULSE_NUM, GPIO_INPUT, GPIO_QUAL6 );
            GPIO_SetupPinMux(Inverter_Params.GPIO_SPEEDPULSE_NUM,0,5);

            //QEP S setup (Unused)
            GPIO_SetupPinOptions(64, GPIO_INPUT, GPIO_QUAL6 );
            GPIO_SetupPinMux(64,0,5);

            //QEP I setup
            GPIO_SetupPinOptions(Inverter_Params.GPIO_ONCEAROUND_NUM, GPIO_INPUT, GPIO_QUAL6);
            GPIO_SetupPinMux(Inverter_Params.GPIO_ONCEAROUND_NUM,0,5);
        }
        else
        {
            SetupGPIO(Inverter_Params.GPIO_QEPA_NUM, GPIO_INPUT, 5, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_QEPB_NUM, GPIO_INPUT, 5, GPIO_QUAL6);
            SetupGPIO(Inverter_Params.GPIO_QEPI_NUM, GPIO_INPUT, 5, GPIO_QUAL6);
            /* Commenting out because this usually isn't used
            GPIO_SetupPinOptions(Inverter_Params.GPIO_QEPS_NUM, GPIO_INPUT, GPIO_QUAL6);
            GPIO_SetupPinMux(Inverter_Params.GPIO_QEPS_NUM, 0, 5);
             */
        }
    }
}

uint16_t GetESTOP_Debounced(void) {return EstopGPIO.debounced;}
uint16_t GetStart_Debounced(void) {return StartGPIO.debounced;}
