/* ============================================================================
System Name:  	LaunchPoint Motor Controller

File Name:	  	PWM.C

Target:			Cryo controller board

Author:			Mike Ricci

Description:	Refactoring motor controller software to be more modular and reduce linkage between sections
				This module will handle the processor PWM peripherals and associated data structures
**********************************************************************************/

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"
#include "Header/GUIC_Header.h"
#include "Header/PWMDAC.h"
#include "F2837S_files/f28377pwm_drivers.h"             // Driver macros adapted from HVPM project by MRR
#include "F2837S_files/f2837x_pwmdac.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1

// Function prototypes
void PWMDAC_InitHandle(struct PWMDACHandler *, int , int, float );
void PWMDAC_Disable(struct PWMDACHandler * );
void PWMDAC_Enable(struct PWMDACHandler *);
void PWMDAC_SetFreq(struct PWMDACHandler * , float );
void PWMDAC_Out(struct PWMDACHandler * , float );
void PWMDAC_SetOffset(struct PWMDACHandler * , float , float );

//External resources
extern volatile struct EPWM_REGS * ePWM[13];

/* Function: Init_PWMDAC_Handle - Initializes a PWMDAC handler
 * Outputs: None
 *
 * Inputs:
 * *pwmdh - pointer to PWMDAC handler struct
 * npwm - which EPWM register to use (1-9)
 * chan - 1 or 2 for CMPA or CMPB (GPIO dependent)
 * freq - frequency in Hz
 *
 */
void PWMDAC_InitHandle(struct PWMDACHandler *pwmdh, int npwm, int chan, float freq)
{
    pwmdh->pwm_num = npwm;

    if( (chan<1) || (chan>2) )
    {
        pwmdh->chan = 1;
    }
    else
    {
        pwmdh->chan = chan;
    }
    pwmdh->freq = freq;
    pwmdh->PeriodMax = (CPU.Sec/freq/32/2);
    pwmdh->duty_offset_min=0.0;
    pwmdh->duty_offset_max=1.0;
    pwmdh->PwmDacIn = 0;

    EALLOW;
    /* Setup Sync*/
    (*ePWM[npwm]).TBCTL.bit.SYNCOSEL = 0;     /* Pass through*/

    /* Allow each timer to be sync'ed*/
    (*ePWM[npwm]).TBCTL.bit.PHSEN = 1;

    /* Init Timer-Base Period Register for EPWM4*/
    (*ePWM[npwm]).TBPRD = pwmdh->PeriodMax;

    /* Init Timer-Base Phase Register for EPWM4*/
    (*ePWM[npwm]).TBPHS.bit.TBPHS = 0;

    /* Init Timer-Base Control Register for EPWM4*/
    (*ePWM[npwm]).TBCTL.all = PWMDAC_INIT_STATE;

    /* Init Compare Control Register for EPWM4*/
    (*ePWM[npwm]).CMPCTL.all = PWMDAC_CMPCTL_INIT_STATE;

    /* Init Action Qualifier Output A Register for EPWM4*/
    (*ePWM[npwm]).AQCTLA.all = PWMDAC_AQCTLA_INIT_STATE;
    (*ePWM[npwm]).AQCTLB.all = PWMDAC_AQCTLB_INIT_STATE;

    /* Init Dead-Band Generator Control Register for EPWM4*/
    (*ePWM[npwm]).DBCTL.all = PWMDAC_DBCTL_INIT_STATE;

    /* Init PWM Chopper Control Register for EPWM4*/
    (*ePWM[npwm]).PCCTL.all = PWMDAC_PCCTL_INIT_STATE;


    /* Init Trip Zone Select Register*/
    (*ePWM[npwm]).TZSEL.all = PWMDAC_TZSEL_INIT_STATE;

    /* Init Trip Zone Control Register*/
    (*ePWM[npwm]).TZCTL.all = PWMDAC_TZCTL_INIT_STATE_OSTLO;

    EDIS;                             /* Disable EALLOW*/
}

/* Function: PWMDAC_Disable - Disables the PWM output (sets OST bit)
 * Outputs: None
 *
 * Inputs:
 * *pwmdh - pointer to PWMDAC handler struct.
 */
void PWMDAC_Disable(struct PWMDACHandler * pwmdh)
{
    EALLOW;
    (*ePWM[pwmdh->pwm_num]).TZFRC.bit.OST=1;
    EDIS;
}

/* Function: PWMDAC_Enable - Enables the PWM output (clears OST bit)
 * Outputs: None
 *
 * Inputs:
 * *pwmdh - pointer to PWMDAC handler struct.
 */
void PWMDAC_Enable(struct PWMDACHandler * pwmdh)
{
    EALLOW;
    (*ePWM[pwmdh->pwm_num]).TZCLR.bit.OST=1;
    EDIS;
}

/* Function: PWMDAC_SetFreq - Sets PWMDAC output frequency
 * Outputs: None
 *
 * Inputs:
 * *pwmdh - pointer to PWMDAC handler struct.
 * freq_in_Hz - desired frequency in Hz
 */
void PWMDAC_SetFreq(struct PWMDACHandler * pwmdh, float freq_in_Hz)
{
    pwmdh->freq = freq_in_Hz;
    pwmdh->PeriodMax = (CPU.Sec/freq_in_Hz/32/2);
}

/* Function: PWMDAC_Out - Sets PWMDAC output duty cycle
 * Outputs: None
 *
 * Inputs:
 * *pwmdh - pointer to PWMDAC handler struct.
 * duty - desired duty cycle [0.0 to 1.0 representing 0% or 100%]
 */
void PWMDAC_Out(struct PWMDACHandler * pwmdh, float duty)
{
    int32 TmpD2;
    float fm, fb;

    fb = (float)(32767.0*(2.0*(pwmdh->duty_offset_min) - 1.0)) ;
    fm = (float)(2.0*32767.0*(pwmdh->duty_offset_max - pwmdh->duty_offset_min));

    /*
     Insert calculation for throttle [0,1] -> PwmDacInPointer units, whatever they are
     *
     *-32767 = 0%, 32767 = 100% duty cycle
     *So calculation is Input = -32767 + 32767*2.0*duty cycle*/

    /* Update Timer-Base period Registers*/
    (*ePWM[pwmdh->pwm_num]).TBPRD = pwmdh->PeriodMax;

    pwmdh->PwmDacIn = (int16)(fm*duty + fb);
    TmpD2 = (int32)(pwmdh->PeriodMax)*(int32)(pwmdh->PwmDacIn);                 /* Q15 = Q0*Q15*/


   /* Compute the compare A (Q0) from the EPWM4 A duty cycle ratio (Q15)*/
    if(pwmdh->chan==1)
    {
        (*ePWM[pwmdh->pwm_num]).CMPA.bit.CMPA = (int16)(TmpD2>>16) + (int16)((pwmdh->PeriodMax)>>1); /* Q0 = (Q15->Q0)/2 + (Q0/2)*/
    }
    else if(pwmdh->chan==2)
    {
        (*ePWM[pwmdh->pwm_num]).CMPB.bit.CMPB = (int16)(TmpD2>>16) + (int16)((pwmdh->PeriodMax)>>1); /* Q0 = (Q15->Q0)/2 + (Q0/2)*/
    }
}

/* Function: PWMDAC_SetOffset - Sets the duty cycles for 0% and 100% commands
 * Outputs: None
 *
 * Inputs:
 * *pwmdh - pointer to PWMDAC handler struct.
 * min - pulse width in seconds that a command of 0.0 will translate to
 * max - pulse width in seconds that a command of 1.0 will translate to
 */
void PWMDAC_SetOffset(struct PWMDACHandler * pwmdh, float min, float max)
{
    if(max>=1.0 || max<=min)
    {
        max=1.0;
    }
    if(min<=0.0 || max<=min)
    {
        min=0.0;
    }

    pwmdh->duty_offset_min = min*(pwmdh->freq);
    pwmdh->duty_offset_max = max*(pwmdh->freq);
}
