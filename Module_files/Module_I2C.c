/*
============================================================================
System Name:    LaunchPoint Motor Controller

File Name:      Module_I2C.c

Target:         LP motor controllers

Author:         Mike Ricci

Description:    Refactoring motor controller software to be more modular and reduce linkage between sections
                This module will handle the processor I2C pins for writing to an EEPROM
                Based off of TI demo code
                I2CA_WriteData and I2CReadData are essentially take from demo code with minor modifications
                I2C_Read() and I2CWrite() are new functions designed to do a 'memcopy" to/from EEPROM and RAM
********************************************************************************* */

// Include header files used in the main function
// define float maths and then include IQmath library
#include "F2837S_files/F28x_Project.h"
#include "F2837S_files/f2837xS_Delay.h"
#include "F2837S_files/F2837xS_I2c_defines.h"

#include "F2837xS_Pie_defines.h"
#include "F2837xS_GlobalPrototypes.h"

#include "Header/PLL_Defines.h"
#include "Header/I2C.h"

//  Used to mark code for deletion later once port it working
#define DeleteMe 1
#define FixMeLater 1

// ****************************************************************************
// Function Prototypes
// ****************************************************************************
void   Init_I2C(void);
uint16_t I2CA_WriteData(struct I2CMSG *msg);
uint16_t I2CA_ReadData(struct I2CMSG *msg);

uint16_t I2C_Read(uint16_t *,uint16_t, uint32_t);
uint16_t I2C_Write(uint16_t *,uint16_t, uint32_t);

void I2C_Reset(void);

__interrupt void i2c_int1a_isr(void);

// ****************************************************************************
// External Variables -- linkage to other modules
// ****************************************************************************



// *****************************************************************************
//  Module resources - variables here //
// *****************************************************************************

struct I2CMSG I2cMsgOut1={I2C_MSGSTAT_INACTIVE,
                          I2C_SLAVE_ADDR,
                          I2C_NUMBYTES,
                          I2C_EEPROM_HIGH_ADDR,
                          I2C_EEPROM_LOW_ADDR};

struct I2CMSG I2cMsgIn1={ I2C_MSGSTAT_INACTIVE,
                          I2C_SLAVE_ADDR,
                          I2C_NUMBYTES,
                          I2C_EEPROM_HIGH_ADDR,
                          I2C_EEPROM_LOW_ADDR};

struct I2CMSG I2cMsgACKpoll={ I2C_MSGSTAT_INACTIVE,
                          I2C_SLAVE_ADDR,
                          1,
                          I2C_EEPROM_HIGH_ADDR,
                          I2C_EEPROM_LOW_ADDR};


struct I2CMSG *CurrentMsgPtr;               // Used in __interrupts


uint16_t Error=0;
uint16_t ReadFailures=0;
uint16_t I2C_FaultFlag=0;
uint16_t i2cisrctr=0;

// *****************************************************************************
//  Debug resources - variables here -- delete many of these once code works //
// *****************************************************************************
uint16_t I2C_Writing=0;
uint16_t I2C_Reading=0;
int16_t I2C_WLoopCount=0;
int16_t I2C_RLoopCount=0;
int16_t WSuccessCount=0;
int16_t RSuccessCount=0;
uint32_t I2C_Ctr=0;
uint32_t ReadDataCounter=0;
uint32_t ReadBusyCounter=0;

// *****************************************************************************
// Constants
// *****************************************************************************

#define I2C_maxwait 100            // Number of times looping to wait for write operation to complete in ms

// *****************************************************************************
// Modules - code here
// *****************************************************************************

void Init_I2C(void)
{
    uint16_t i;

    // Interrupts that are used in this example are re-mapped to
    // ISR functions found within this file.
   I2caRegs.I2CMDR.bit.IRS = 0;         //Reset I2C module for configuration

   // Initialize I2C
   I2caRegs.I2CSAR.all = I2C_SLAVE_ADDR;        // Slave address - EEPROM control code

   I2caRegs.I2CPSC.all = 6;     // Prescaler - need 7-12 Mhz on module clk
   I2caRegs.I2CCLKL = 10;           // NOTE: must be non zero
   I2caRegs.I2CCLKH = 5;            // NOTE: must be non zero

   I2caRegs.I2CIER.bit.ARDY = 1;    //Enable ARDY __interrupt (register ready -- means byte sent counter dec to 0)
   I2caRegs.I2CIER.bit.SCD  = 1;    //Enable SCD __interrupt  (stop condition -- interrupts when stop condition is sent/detected)
   I2caRegs.I2CIER.bit.NACK  = 1;   // Enable interrupt on NACK

   I2caRegs.I2CMDR.all = 0x0000;    // Clear I2C modes and status

   I2caRegs.I2CMDR.bit.XA = 0;      // 7 bit slave device addressing mode
   I2caRegs.I2CMDR.bit.MST = 1;     // Master Mode

   I2caRegs.I2CFFTX.bit.TXFFRST = 1;    // Enable FIFO mode and TXFIFO
   I2caRegs.I2CFFTX.bit.I2CFFEN = 1;

   I2caRegs.I2CFFRX.bit.RXFFINTCLR = 1;
   I2caRegs.I2CFFRX.bit.RXFFRST = 1;    // Enable RXFIFO, clear RXFFINT

   // Clear incoming/outgoing message buffers
   for (i = 0; i < I2C_MAX_BUFFER_SIZE; i++)
   {
       I2cMsgIn1.MsgBuffer[i] = 0x0000;
       I2cMsgOut1.MsgBuffer[i] = 0x0000;
   }

   EALLOW;
   PieVectTable.I2CA_INT = &i2c_int1a_isr;
   // Enable I2C __interrupt 1 in the PIE: Group 8 __interrupt 1
   PieCtrlRegs.PIEIER8.bit.INTx1 = 1;
   // Enable CPU INT8 which is connected to PIE group 8
   IER |= M_INT8;
   EDIS;

   return;
}


//  addr is the EEPROM starting address (in words, so multiply by 2 to get EEPROM byte address) for the memory copy
// p_data is a pointer to the data in DSP memory that should be copied to the EEPROM (word address pointer)
// size16 is the number of 16 bit words that should be copied (so bytes into EEPROM is 2X the # of words)
// This routine blocks until the entire memory copy is completed, or it returns with an error
// This routine will try to reset the I2C bus and clear/deal with errors, but after too many tries/too much time
//      it will exit with an error
// uses the message structure I2cMsgOut1 as the temporary storage buffer for transmitting messages to copy
// entire block of memory
// The function right now only works properly starting on 16 byte boundaries in the EEPROM


uint16_t I2C_Write(uint16_t *p_data, uint16_t size16, uint32_t addr)
{

    uint16_t *p_buf = I2cMsgOut1.MsgBuffer;
    uint32_t curAddr= addr*2;     // Convert word address to byte address

    uint16_t WriteTryCtr = 0;

    while(curAddr < addr*2 + 2*size16)
    {
        uint16_t nBytes = (I2C_NUMBYTES < (addr*2 + 2*size16)-curAddr) ? I2C_NUMBYTES : (addr*2 + 2*size16)-curAddr;     // nBytes is smaller of Max Msg Size or Bytes to end of entire copy op
        nBytes = (nBytes < I2C_EEPROM_PAGE-(curAddr % I2C_EEPROM_PAGE) ) ? nBytes : I2C_EEPROM_PAGE-(curAddr % I2C_EEPROM_PAGE);     // nBytes is smaller of 1) Max Msg Size  2) Bytes to end of entire copy op  3) Bytes to end of EEPROM memory page
        I2cMsgOut1.NumOfBytes = nBytes;
        I2cMsgOut1.MsgStatus = I2C_MSGSTAT_SEND_WITHSTOP;   // Probably don't need this?  But maybe need to do this to clear message structure in case failure earlier left it in messy state

        I2cMsgOut1.MemoryHighAddr = ((curAddr >> 8) & 0x00FF); //Split into high and low bytes
        I2cMsgOut1.MemoryLowAddr  = ( curAddr        & 0x00FF);

// this has been changed to pull out hi-low bytes into separate words in buffer
        int i;
        for(i=0;i<nBytes;i++)
        {
            if(i % 2 == 0)
            {
                *(p_buf+i) = *(p_data+i/2) & 0x00ff;
            }
            else
            {
                *(p_buf+i) = *(p_data+i/2) >> 8;
            }
        }

 //       memcpy(p_buf, p_data, nBytes);                      //Copy into local buffer the bytes (or words??) to be written

        ///////////////////////////////////
        // Write data to EEPROM section  //
        ///////////////////////////////////

        //   try to write and loop and re-try if there is a NACK since EEPROM is still be processing last write
        //  So we try and wait until self-timed internal programming is done
        // device will NACK to subsequent writes until it is done programming

        WriteTryCtr = 0;

        do
        {
            Error = I2CA_WriteData(&I2cMsgOut1);

            if(Error != I2C_SUCCESS)        // NACK will not be seen yet, this is just checking if I2C wasn't even able to write to TX FIFO for some other error, success here does not mean write to EERPOM was success yet
            {
                // what are the possible errors (including I2Cfault from interrupt?)
                // May want to have a loop that allows some retries for errors -- things like bus busy are possible errors
                return(Error);
            }

            I2C_Ctr = 0;
            while(I2cMsgOut1.MsgStatus == I2C_MSGSTAT_WRITE_BUSY)  // Interrupt should change status once message is written or a NACK received, wait while "busy" and trying to write
            {
                I2C_Ctr++;
                if (I2C_Ctr>I2C_maxwait) return(I2C_MAX_ATTEMPT_ERROR);     // Return with an error if we wait too long for interrupt to set status inactive
                DELAY_US(1);
            }

            // if we get a NACK then we need to flush FIFO before looping and trying again since the write_data attempt put the address and data into the FIFO
            if(I2cMsgOut1.MsgStatus != I2C_MSGSTAT_INACTIVE)        // I2C interrupt will set MsgStatus to "INACTIVE" if write was completed successfully, otherwise we likely got a NACK
            {
                I2caRegs.I2CFFTX.bit.TXFFRST = 0;  // Reset and flush the TX FIFO since we got a NACK on the attempted write operation
            }

            DELAY_US(10);
            WriteTryCtr++;
            if (WriteTryCtr> I2C_MAX_ATTEMPTS ) return(I2C_NACK_ERROR);

        } while(I2cMsgOut1.MsgStatus != I2C_MSGSTAT_INACTIVE); // When interrupt sees completion of write successfully it sets status to INACTIVE
                                                                // If there was a NACK because EEPROM is still busy the status will be something else
        curAddr += nBytes;
        p_data  += nBytes/2;
    }

    return(I2C_SUCCESS);
}



/*  Writes a I2CMSG message to the I2C port */
/*  I2CMSG structure has a 16 byte buffer in it to match the FIFO size */
/*  So this command can write up to 16 bytes to the I2C */

uint16_t I2CA_WriteData(struct I2CMSG *msg)       // Writes a message structure length of data to the I2C EEPROM  -limited by FIFO-2 in size
{
    uint16_t i;

   // Need to check that number of bytes to send is smaller than FIFO buffer or else
   // return an error here

   if (msg->NumOfBytes>I2C_MAX_BUFFER_SIZE-2)
   {
       return I2C_MESSAGE_TOO_LONG_ERROR;
   }

   // Wait until the STP bit is cleared from any previous master communication.
   // Clearing of this bit by the module is delayed until after the SCD bit is
   // set. If this bit is not checked prior to initiating a new message, the
   // I2C could get confused.
   if (I2caRegs.I2CMDR.bit.STP == 1)
   {
      return I2C_STP_NOT_READY_ERROR;
   }

   // Setup slave address
   I2caRegs.I2CSAR.all = msg->SlaveAddress | msg->MemoryHighAddr;

   // Check if bus busy
   if (I2caRegs.I2CSTR.bit.BB == 1)
   {
      return I2C_BUS_BUSY_ERROR;
   }

   // Set signal / status flag for  interrupt routine
   msg->MsgStatus = I2C_MSGSTAT_WRITE_BUSY;
   CurrentMsgPtr = msg;                    // this pointer is used by ISR to know what is being read

   // Send start as master transmitter
   // I2caRegs.I2CMDR.all = 0x6E20;

   I2caRegs.I2CMDR.bit.STT = 1;      // Start Condition -- generate a start condition on the bus when enabled
   I2caRegs.I2CMDR.bit.STP = 1;     // Generate a stop when chars are sent
   I2caRegs.I2CMDR.bit.MST = 1;     // Master Mode
   I2caRegs.I2CMDR.bit.TRX = 1;     // Transmit mode
   I2caRegs.I2CMDR.bit.XA = 0;      // 7 bit addresssing
   I2caRegs.I2CMDR.bit.RM = 0;      // Repeat mode:  0 means STP will be set when # of chars in counter is sent
   I2caRegs.I2CMDR.bit.FDF = 0;     // Not free data mode (use addresses)
   I2caRegs.I2CMDR.bit.BC = 0;     // Bit count - 8 bits per byte

   I2caRegs.I2CFFTX.bit.TXFFRST = 1;  // Enable the TX FIFO operation

   I2caRegs.I2CMDR.bit.IRS = 1;     // Take I2C out of reset

   // Setup number of bytes to send
   // MsgBuffer + Address
   // I2caRegs.I2CCNT = msg->NumOfBytes+2;
   I2caRegs.I2CCNT = msg->NumOfBytes+1;

   // Setup data to send
   // I2caRegs.I2CDXR.all = msg->MemoryHighAddr;
   I2caRegs.I2CDXR.all = msg->MemoryLowAddr;        // Push address into TX FIFO

   for (i=0; i<msg->NumOfBytes; i++)

   {
      I2caRegs.I2CDXR.all = *(msg->MsgBuffer+i);        // Push message payload into TX FIFO
   }


   return I2C_SUCCESS;
}



//  I2C multiple packet read function
//  addr is the EEPROM starting address (in 2 byte words) for the memory copy from EEPROM to DSP memory
//          addr will be converted to byte address for use in EEPROM addressing by multiplying by 2
// p_data is a pointer to the data in DSP memory where the EEPROM data should be copied
//  Size16 is the number of 16 bit words to read (two bytes at a time from the EEPROM byte memory)
// This routine blocks until the entire memory copy is completed, or it returns with an error
// This routine will try to reset the I2C bus and clear/deal with errors, but after too many tries/too much time
//      it will exit with an error
// uses the message structure I2cMsgIn1 as the temporary storage buffer for transmitting messages to copy
// entire block of memory



uint16_t I2C_Read(uint16_t *p_data, uint16_t size16, uint32_t addr)
{

/*    uint32_t nBytes = I2cMsgIn1.NumOfBytes; */
    uint16_t *p_buf = I2cMsgIn1.MsgBuffer;
    uint32_t curAddr = addr*2;            //  curAddr is byte address in EEPROM, addr is word address in function call

    uint16_t ReadError = 0;
    uint16_t I2C_Ctr=0;
    uint16_t ReadAttemptCtr = 0;

    while(curAddr < addr*2 + (uint32_t) size16*2)
    {

        uint32_t nBytes = (I2C_NUMBYTES < (addr*2 + 2*size16)-curAddr) ? I2C_NUMBYTES : (addr*2 + 2*size16)-curAddr;     // nBytes is smaller of Max Msg Size or Bytes to end of entire copy op
        nBytes = (nBytes < I2C_EEPROM_PAGE-(curAddr % I2C_EEPROM_PAGE) ) ? nBytes : I2C_EEPROM_PAGE-(curAddr % I2C_EEPROM_PAGE);     // nBytes is smaller of 1) Max Msg Size  2) Bytes to end of entire copy op  3) Bytes to end of EEPROM memory page
        I2cMsgIn1.NumOfBytes = nBytes;

        I2cMsgIn1.MsgStatus = I2C_MSGSTAT_SEND_NOSTOP;   // Probably don't need this?  But maybe need to do this to clear message structure in case failure earlier left it in messy state
                                                         // or do it in I2C_ReadData()
        I2cMsgIn1.MemoryHighAddr = ( (uint16_t) (curAddr >> 8) & 0x00FF);    //Split into high and low bytes
        I2cMsgIn1.MemoryLowAddr  = ( (uint16_t) curAddr        & 0x00FF);

        ReadAttemptCtr = 0;           // Zero counter for failures attempting to read this block

        while(I2cMsgIn1.MsgStatus == I2C_MSGSTAT_SEND_NOSTOP)
            // Status starts == SEND_NOSTOP, if read is successful status will be "INACTIVE", if not successful status will return to "SEND_NOSTOP, so we try again)
        {
            I2C_Ctr = 0;                                        // Clear the "try" counter
            ReadError = I2CA_ReadData(&I2cMsgIn1);
            while( ReadError != I2C_SUCCESS)             // Try to start reading process, might fail so do a couple of tries if needed
            {
                I2C_Ctr++;
               // Maybe setup an attempt counter to break an infinite while
               // loop. The EEPROM will send back a NACK while it is performing
               // a write operation. Even though the write communique is
               // complete at this point, the EEPROM could still be busy
               // programming the data. Therefore, multiple attempts are
               // necessary.
                if (I2C_Ctr>I2C_MAX_ATTEMPTS) return(ReadError);     // Return with an error if we try too many times for the read initiation to succeed
                DELAY_US(1);
                ReadError = I2CA_ReadData(&I2cMsgIn1);
            }
            // Now we wait for the interrupt to tell us the read is complete or we had a NACK/failure
            I2C_Ctr = 0;                                        // Clear the "try" counter
            while( (I2cMsgIn1.MsgStatus == I2C_MSGSTAT_SEND_NOSTOP_BUSY) || (I2cMsgIn1.MsgStatus == I2C_MSGSTAT_READ_BUSY) )
                    // Wait during these conditions - means I2C and interrupt is still writing address and reading bytes
                    // Interrupt should set status to INACTIVE if read successful, or SEND_NOSTOP if a NACK is received to address setup
            {
                I2C_Ctr++;
                if (I2C_Ctr>I2C_maxwait) return(I2C_MAX_ATTEMPT_ERROR);     // Return with an error if we wait too long for interrupt to set status inactive
                DELAY_US(1);
            }
 /*           if (ReadFailures > I2C_MAX_ATTEMPTS) return(I2C_NACK_ERROR);  Not sure what this was/is doing, removed it  */
            ReadAttemptCtr++;
            if (ReadAttemptCtr> I2C_MAX_ATTEMPTS ) return(I2C_MAX_ATTEMPT_ERROR);
        }
        if (I2cMsgIn1.MsgStatus != I2C_MSGSTAT_INACTIVE ) return(I2C_ERROR );

//        memcpy(p_data,p_buf,nBytes);            // *dst, *src, nbytes
// Changed to read bytes into hi/low words
        uint16_t i;
        for(i=0;i<nBytes;i++)
        {
            if(i % 2 == 0)
            {
                *(p_data+i/2)= *(p_buf+i);
            }
            else
            {
                *(p_data+i/2)+= (*(p_buf+i) << 8);
            }
        }

        p_data += nBytes/2;
        curAddr += (uint32_t) nBytes;
    }
    return(I2C_SUCCESS);
}



//
// Sets up read operation and status, etc for interrupt routine
//
// Actual transfer of bytes from FIFO to MSG structure happens in
// interrupt routine after the read address has been sent to the EEPROM
//  When read of bytes into message structure is completed, interrupt will set
// msg->MsgStatus = I2C_MSGSTAT_INACTIVE


uint16_t I2CA_ReadData(struct I2CMSG *msg)
{
   // Wait until the STP bit is cleared from any previous master communication.
   // Clearing of this bit by the module is delayed until after the SCD bit is
   // set. If this bit is not checked prior to initiating a new message, the
   // I2C could get confused.
   if (I2caRegs.I2CMDR.bit.STP == 1)
   {
      return I2C_STP_NOT_READY_ERROR;
   }

   I2caRegs.I2CSAR.all = msg->SlaveAddress | msg->MemoryHighAddr;

  // Check if bus busy
  if (I2caRegs.I2CSTR.bit.BB == 1)
  {
     return I2C_BUS_BUSY_ERROR;
  }

  CurrentMsgPtr = msg;                    // this pointer is used by ISR to know what is being read
  msg->MsgStatus = I2C_MSGSTAT_SEND_NOSTOP_BUSY;

  I2caRegs.I2CMDR.all = 0x0000;         // Clear I2C command-mode register, temporarily disable I2C, clear status when CMDR.IRS=0

  //  I2caRegs.I2CMDR.all = 0x2620;
  // Setup I2c modes and then enable
  I2caRegs.I2CMDR.bit.STT = 1;      // Start Condition -- generate a start condition on the bus when enabled
  I2caRegs.I2CMDR.bit.STP = 0;     // Don't generate a stop when chars are sent
  I2caRegs.I2CMDR.bit.MST = 1;     // Master Mode
  I2caRegs.I2CMDR.bit.TRX = 1;     // Transmit mode
  I2caRegs.I2CMDR.bit.XA = 0;      // 7 bit addresssing
  I2caRegs.I2CMDR.bit.RM = 0;      // Repeat mode:  0 means STP will be set when something happens - but next step in interrupt is with ARDY int, so don't want an STP in this step
  I2caRegs.I2CMDR.bit.FDF = 0;     // Not free data mode (use addresses)
  I2caRegs.I2CMDR.bit.BC = 0;     // Bit count - 8 bits per byte

  I2caRegs.I2CMDR.bit.IRS = 1;     // Take I2C out of reset

  // I2caRegs.I2CCNT = 2;
 I2caRegs.I2CCNT = 1;
  // I2caRegs.I2CDXR.all = msg->MemoryHighAddr;
 I2caRegs.I2CDXR.all = msg->MemoryLowAddr;                 // Push into TX FIFO

   return I2C_SUCCESS;
}


//
// I2C Interrupt Handler
//
//  Note that not all conditions handled -- there are if conditions that don't have any else associated,
//     so there are conditions where the "else" just drops through -- not sure if this is OK in weird
//      fault conditions/failures or not.--MR


__interrupt void i2c_int1a_isr(void)     // I2C-A
{
    uint16_t IntSource, i;
   i2cisrctr++;
   // Read __interrupt source
   IntSource = I2caRegs.I2CISRC.bit.INTCODE;

   // Interrupt source = stop condition detected
   if(IntSource == I2C_SCD_ISRC)
   {
      // If completed message was writing data, reset msg to inactive state or NACK depending on why we got a stop
      if (CurrentMsgPtr->MsgStatus == I2C_MSGSTAT_WRITE_BUSY)
      {
          if (I2caRegs.I2CSTR.bit.NACK == 1)
          {
              CurrentMsgPtr->MsgStatus = I2C_NACK_ERROR;
          }
          else
          {
              CurrentMsgPtr->MsgStatus = I2C_MSGSTAT_INACTIVE;
          }
      }
       // If a message receives a NACK during the address setup portion of the
          // EEPROM read, the code further below included in the register access ready
          // __interrupt source code will generate a stop condition. After the stop
          // condition is received (here), set the message status to try again.
          // User may want to limit the number of retries before generating an error.
      else if(CurrentMsgPtr->MsgStatus == I2C_MSGSTAT_SEND_NOSTOP_BUSY)
      {
         CurrentMsgPtr->MsgStatus = I2C_MSGSTAT_SEND_NOSTOP;
         ReadFailures++;
      }
      // If completed message was reading EEPROM data, reset msg to inactive state
      // and read data from FIFO.
      else if (CurrentMsgPtr->MsgStatus == I2C_MSGSTAT_READ_BUSY)
      {
         for(i=0; i < CurrentMsgPtr->NumOfBytes; i++)
         {
           CurrentMsgPtr->MsgBuffer[i] = I2caRegs.I2CDRR.all;
         }
         CurrentMsgPtr->MsgStatus = I2C_MSGSTAT_INACTIVE;
      }
      else
      {
          // what to do if we got a stop condition with an unexpected message status?
      }

   }  // end of stop condition detected

   // Interrupt source = Register Access Ready
   // This __interrupt is used to determine when the EEPROM address setup portion of the
   // read data communication is complete. Since no stop bit is commanded, this flag
   // tells us when the message has been sent instead of the SCD (stop condition) flag. If a NACK is
   // received, clear the NACK bit and command a stop (which will then trigger the stop interrupt above once stop is completed).
   //  Otherwise, move on to the read
   // data portion of the communication.
   else if(IntSource == I2C_ARDY_ISRC)
   {
      if(I2caRegs.I2CSTR.bit.NACK == 1)
      {
         I2caRegs.I2CMDR.bit.STP = 1;                   // Set stop bit generation, once stop generated ISR will catch it and set flags to stop this try and allow higher level code to try again
         I2caRegs.I2CSTR.all = I2C_CLR_NACK_BIT;        // Clear NACK by writing to the NACK bit in STR register
      }
      else if(CurrentMsgPtr->MsgStatus == I2C_MSGSTAT_SEND_NOSTOP_BUSY)
      {
          CurrentMsgPtr->MsgStatus = I2C_MSGSTAT_READ_BUSY;

 //         I2caRegs.I2CMDR.all = 0x2C20;         // Send restart as master receiver
 //       Do not want to do an I2C reset here (IRS = 0) since we are relying on BB=1 to create a "repeated start" condition
// Send restart as master receiver now
          I2caRegs.I2CMDR.bit.STT = 1;       // Start Bit -- generate a start condition - should be "repeated start" since BB should be set from address write that just happened with no stop
          I2caRegs.I2CMDR.bit.STP = 1;       // Generate stop bit when internal data counter = 0
          I2caRegs.I2CMDR.bit.MST = 1;     // Master Mode

          I2caRegs.I2CMDR.bit.TRX = 0;     // Receive mode
          I2caRegs.I2CMDR.bit.XA = 0;      // 7 bit addresssing

          I2caRegs.I2CCNT = CurrentMsgPtr->NumOfBytes;   // Setup how many bytes to expect

      }
      else
      {
          // ????? what to do here for default condition
      }
   }  // end of register access ready
   // Interrupt source == NACK
   // should come here if write data operation gets a NACK because EEPROM is still busy with internal programming for a previous write
   else if (IntSource == I2C_NACK_ISRC)          // Should come here if only NACK set, if NACK & ARDY is set it will get caught above (during write address portion of Read operation)
                                               // should only come here if get NACK during Write data to EEPROM operation
   {
       I2caRegs.I2CMDR.bit.IRS = 0;             // Reset I2C to deal with NACK
//       I2caRegs.I2CCNT = 0;                     //  Got a NACK, so don't send any more characters
                                               // Setting to zero should cause a "stop" if the STP bit is set (during write data operation)
                                               // Stop will cause another interrupt that gets caught above
                                               // but setting zero also casues ARDY condition which is higher priority than stop
       CurrentMsgPtr->MsgStatus = I2C_NACK_ERROR;       // Set status to inducate NACK error happened in write
       i++;
   }
   else
   {
      // Generate some error due to invalid __interrupt source
     // asm("   ESTOP0");
       i++;
   }

   // Enable future I2C (PIE Group 8) __interrupts
   PieCtrlRegs.PIEACK.all |= PIEACK_GROUP8;
}



//
//  Reset the I2C module at start, or if something is drastically wrong
//

void I2C_Reset()
{
    uint16_t i;
    DINT;                                           //Disable interrupts

    I2caRegs.I2CMDR.bit.IRS = 0;                    //Disable the I2C module

    I2cMsgOut1.MsgStatus = I2C_MSGSTAT_INACTIVE;    //Set message structures to INACTIVE
    I2cMsgIn1.MsgStatus = I2C_MSGSTAT_INACTIVE;     //and clear the RAM buffers
    for(i=0; i<I2C_MAX_BUFFER_SIZE; i++)
    {
        I2cMsgOut1.MsgBuffer[i] = 0;
        I2cMsgIn1.MsgBuffer[i] = 0;
    }

    DELAY_US(1000);
    I2caRegs.I2CMDR.bit.IRS = 1;                    //Take I2C module out of reset

    ReadFailures = 0;

    EINT;                                           //Re-enable interrupts
}


