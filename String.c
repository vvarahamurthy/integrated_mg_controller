/*
 * String.c
 *
 *  Created on: Oct 4, 2018
 *      Author: guydeong
 */
#include "F2837S_files/F28x_Project.h"
#include "Header/String.h"
#include "Header/UART_Comm.h"

// reverses a string 'str' of length 'len'
void reverse(char *str, int len)
{
    int i=0, j=len-1, temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}

 // Converts a given integer x to string str[].  d is the number
 // of digits required in output. If d is more than the number
 // of digits in x, then 0s are added at the beginning.
int intToStr(int x, char str[], int d)
{
    int i = 0;
    while (x)
    {
        str[i++] = (x%10) + '0';
        x = x/10;
    }

    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}

// Converts a floating point number to string.
void ftoa(float n, char * res, int afterpoint)
{
    // Power int
    unsigned int power = 1.0;

    // Extract integer part
    int ipart = (int)n;

    // Extract floating part
    float fpart = n - (float)ipart;

    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0)
    {
        res[i] = '.';  // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        unsigned pIndex;

        // pow() was not working will so I just made this
        for(pIndex = 0; pIndex < afterpoint; pIndex++)
        {
            if( power )
            power = power*10;
        }

        fpart = fpart * power;

        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}

// At Fault state when start button is pressed datalog will be sent
void graphDatalog()
{
    // Temp string array
    char string[20];

    unsigned int i;

    for(i = 0; i < 3; i++)
    {
        ftoa(i, string, 4);
        printCom(string);
        printCom(",");

        ftoa(1, string, 4);
        printCom(string);
        printCom(",");

        ftoa(0, string, 4);
        printCom(string);
        printCom(",");

        ftoa(0, string, 4);
        printCom(string);
        printCom("\n");
    }
}

// Transmit datalog to terminal (I personal use putty or Serial Port Monitor)
void transmitDatalog()
{
    // Temp string array
    char string[20];

    printCom("Datalog 1   Datalog 2    Datalog 3    Datalog 4\n");

    unsigned int i;

    for(i = 0; i < 3; i++)
    {
        ftoa(i, string, 4);
        printCom(string);
        printCom("    ");

        ftoa(1, string, 4);
        printCom(string);
        printCom("     ");

        ftoa(0, string, 4);
        printCom(string);
        //printCom(",");

        ftoa(0, string, 4);
        printCom(string);
        printCom("\n");
    }
}
