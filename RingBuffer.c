/*
 * RingBuffer.c
 *
 *  Created on: Jun 16, 2017
 *      Author: George Uy de Ong II, ssnyder
 *
 *      1) The head is wherre chars gets permenently deleted when popped out
 *      2) The tail is where new charse gets pushed in
 *      3) The read can index from the head to the tail as chars are popped out, but chars stay
 *
 */
#include "F2837S_files/F28x_Project.h"
#include "Header/RingBuffer.h"
#include <stdlib.h>

// Returns true if buffer is empty
bool isRingBufferEmpty( RINGBUFFER *pRingBuffer)
{
    bool ringBufferIsEmpty = 0;     // Set false

    // If head and tail are at same place, it is empty.
    if(pRingBuffer->head == pRingBuffer->tail)
    {
        ringBufferIsEmpty = 1;
        // error(EMPTY_BUFFER);
    }

    return ringBufferIsEmpty;
}

// Returns true if buffer is full
bool isRingBufferFull( RINGBUFFER *pRingBuffer)
{
    bool ringBufferIsFull = 0;  // Set false
    if( (isRingBufferEmpty(pRingBuffer) == 0) && (pRingBuffer->head == pRingBuffer->tail) )
    {
        // Ring buffer is full
        ringBufferIsFull = 1;
    }

    return ringBufferIsFull;
}

// Returns true when read pointer reaches to tail
bool isReadAtTail(RINGBUFFER *prb)
{
    bool readPntIsAtTail = 0;  // set false
    if( prb->read == prb->tail )
    {
        readPntIsAtTail = 1;
    }
    return readPntIsAtTail;
}

// Get the number of new chars in the buffer
int rb_count(RINGBUFFER *prb)
{
    return prb->count;
}

// Return count
int getCount(RINGBUFFER *prb)
{
	return prb->count;
}

// Initialize ring buffer
void rb_init(RINGBUFFER *prb, unsigned char size)
{
	prb->count = 0;
	prb->data = (unsigned char *)calloc(size, sizeof(unsigned char));
	prb->head = prb->data;
    prb->read = prb->data;
	prb->tail = prb->data;
	prb->size = size;
}

// Updates the number of new chars in the buffer.
void updateCount(RINGBUFFER *prb)
{
    if( prb->head > prb->tail )
    {
        prb->count = prb->size - (prb->head - prb->tail);
    }
    else
    {
        prb->count = prb->tail - prb->head;
    }

    if(prb->count == prb->size - 1)
    {
        rb_flush(prb);
    }
}

// Pushed one byte into the rear of the ring buffer
void rb_push(RINGBUFFER *prb, unsigned char d)
{
	*(prb->tail) = d;

	// If tail is at the end of the ring buffer, go back to the beginning.
	if(prb->tail == prb->data + prb->size - 1)
	{
		prb->tail = prb->data;
	}
	else
	{
		(prb->tail)++;
	}

	updateCount(prb);

}


// Deletes the values of the head by setting to NULL. Should be used inside discard, right before popping
void rb_deleteHead(RINGBUFFER *prb)
{
    *(prb->head) = NULL;
}


// Pops the head
unsigned char rb_pop(RINGBUFFER *prb)
{
	unsigned char *ptmp = prb->head;

	// Checks if head reaches at the end of the buffer
	if(prb->head == prb->data + prb->size - 1)
	{
        prb->head = prb->data;
	}
	else
	{
        (prb->head)++;
	}

	// If previous head and read were pointing at the same addr, read points the current head.
	if( ptmp == prb->read)
	{
	    prb->read = prb->head;
	}

	updateCount(prb);

	return *ptmp;
}

// Read what is pointer is pointing to
unsigned char rb_read(RINGBUFFER *prb)
{
    unsigned char *ptmp = prb->read;

    // If read reach end of the buffer then go back to head
    if(prb->read == prb->data + prb->size - 1)
    {
        prb->read = prb->data;
    }

    // If read pointer reaches to tail, save error
    else if(prb->read == prb->tail)
    {
        prb->read = prb->tail;
        // error(READ_AT_END_OF_BUFFER);
    }
    // Otherwise, point to the next index
    else
    {
        (prb->read)++;
    }

    return *ptmp;
}

// Read pointer points back to the head
void rb_rewind(RINGBUFFER *prb)
{
    prb->read = prb->head;
}

// Reset the buffer
void rb_flush(RINGBUFFER *prb)
{
	prb->head = prb->data;
	prb->read = prb->data;
	prb->tail = prb->data;
	prb->count = 0;
}
