/*
 * ErrorHandler.c
 *
 *  Created on: Oct 22, 2018
 *      Author: guydeong
 */

#include "F2837S_files/F28x_Project.h"
#include "Header/ErrorHandler.h"

#if 0
// Returns true, if all files initialize correctly.
bool verifyAllInit()
{
    bool isTrue = 0; // set false

    if(
         checkInitParseRoutine()

         == 1
    )
    {
        isTrue = 1;
    }

    return isTrue;
}

#endif
