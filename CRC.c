/*
 * CRC.c
 *
 *  Created on: Jun 26, 2018
 *      Author: guydeong
 *
 *  Description: This program does a CRC calculation by utilizing the VCU. The CRC
 *  struct contains the start marker, op code, parameter, values, and crc result.
 *  Also, this can handle 8, 16, 32 bit calculation.
 *
 */
#include "F2837S_files/F28x_Project.h"
#include "Header/CRC.h"

// CRC number of Bytes
#define		NUMBYTES			(128)

// CRC object
CRC_Obj CRC_Object;

// Handle CRC
CRC_Handle crcHandler;
// Updates the values in the CRC object
void updateCRCObject(unsigned short numberOfBytes)
{
    CRC_Object.seedValue   = 0x00;
    CRC_Object.nMsgBytes   = numberOfBytes;
    CRC_Object.parity      = CRC_parity_even;
    CRC_Object.crcResult   = 0;
}

void updateCRCCANObject(CRC_Obj *obj)
{
    obj->seedValue   = 0x00;
    obj->nMsgBytes   = 12;
    /*even though the data array is unsigned char, nMsgBytes = 2 will only read the first byte.
     * For example for unsigned char array[] = 0xAA,0xBB,0xCC,0xDD
     * CRC is actually calculated with AA00 BB00 CC00 DD00 and nBytes = 8!
     */

    obj->parity      = CRC_parity_even;
    obj->crcResult   = 0;
}


// A function that calculates the 32-bit CRC VCU
uint32_t run32CrcVcu(unsigned short numberOfBytes, Parse_Object *ParseMessage)
{
    // update CRC struct paramters
    updateCRCObject(numberOfBytes);

    // Save message to pointer
    CRC_Object.pMsgBuffer   =  &ParseMessage;
    CRC_Object.init         =  (void (*)(void *))CRC_init32Bit;
    CRC_Object.run          =  (void (*)(void *))CRC_run32BitPoly1;

    // Initialize Handler
    crcHandler       =  &CRC_Object;

    // Run the VCU 32-bit CRC routine
    CRC_Object.init(crcHandler);
    CRC_Object.run(crcHandler);

    return CRC_Object.crcResult;
}

//! \brief A function that calculates the 16-bit CRC VCU
unsigned int run16CrcVcu(unsigned short numberOfBytes, Parse_Object *ParseMessage)
{
    // update CRC struct paramters
    updateCRCObject(numberOfBytes);

    // This is the ONLY part of the code is different from the function.
    CRC_Object.pMsgBuffer   =  ParseMessage;
    CRC_Object.init         =  (void (*)(void *))CRC_init16Bit;
    CRC_Object.run          =  (void (*)(void *))CRC_run16BitPoly1;

    // Initialize Handler
    crcHandler       =  &CRC_Object;

    // Run the VCU 32-bit CRC routine
    CRC_Object.init(crcHandler);
    CRC_Object.run(crcHandler);

    return (unsigned int)CRC_Object.crcResult;
}

unsigned int run16CrcCANVcu(unsigned char *msg)
{
    //CRC-16/XMODEM
    CRC_Obj CANCRCObject;
    CRC_Handle crc_handler;
    // update CRC struct paramters
    updateCRCCANObject(&CANCRCObject);

    // This is the ONLY part of the code is different from the function.
    CANCRCObject.pMsgBuffer   =  msg;
    CANCRCObject.init         =  (void (*)(void *))CRC_init16Bit;
    CANCRCObject.run          =  (void (*)(void *))CRC_run16BitPoly2;

    // Initialize Handler
    crc_handler       =  &CANCRCObject;

    // Run the VCU 32-bit CRC routine
    CANCRCObject.init(crc_handler);
    CANCRCObject.run(crc_handler);

    return (uint16_t)CANCRCObject.crcResult;
}


//! \brief A function that calculates the 8-bit CRC VCU
uint8_t run8CrcVcu(unsigned short numberOfBytes, Parse_Object *ParseMessage)
{
    // update CRC struct paramters
    updateCRCObject(numberOfBytes);

    // This is the ONLY part of the code is different from the function.
    CRC_Object.init         =  (void (*)(void *))CRC_init8Bit;
    CRC_Object.run          =  (void (*)(void *))CRC_run8Bit;

    // Initialize Handler
    crcHandler       =  &CRC_Object;

    // Run the VCU 32-bit CRC routine
    CRC_Object.init(crcHandler);
    CRC_Object.run(crcHandler);

    return CRC_Object.crcResult;
}
