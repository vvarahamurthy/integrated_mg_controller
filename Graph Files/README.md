# README #

This repository contains the source code for the LaunchPoint Motor Controller.  The controller may support more than one motor and/or motor controller.

Presently supported controllers are mainly the 400V, 120A (" 40 kW") controller and the 400V, 46A ("15 kW", "HEIST") controller, but also should be able to run on the 28V, 75A ("AF controller") prototype boards.

### Who do I talk to? ###

* Team leader: Mike Ricci
* Developers: Mike Ricci, Vishaal Varahamurthy
* Repository Administrator: John Underhill