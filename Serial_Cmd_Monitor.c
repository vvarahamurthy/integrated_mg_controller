#include "F2837S_files/F28x_Project.h"
#include "Header/Serial_Cmd_Monitor.h"
#include "Header/UART_Comm.h"
#include "Header/RingBuffer.h"

#define     TEMP_THRESHOLD      4

#define		RW_CMD 			0x80

#define		EXTENSION_BYTE		0x07

// Originally a TI file
// VV added SSCI_MODE== FIFOINT in summer 2017
// Enabled hardware FIFO on both RX and TX, added software ring buffer for TX messages
// 2017-09-16  MR editing VV code to finish FIFO TX and RX
//

//RW CMD TYPE
#define 	READ 				1
#define 	WRITE 				0
//ENDIANNESS
#define 	BIG_ENDIAN 			1
#define		LITTLE_ENDIAN 		0
//ADDRESSIBLE SIZE
#define 	ADDRESSIBLE_32_BIT 	1
#define 	ADDRESSIBLE_16_BIT 	0
// override these depends on target


unsigned char gInCmdBuffer[CMD_BUFFER_SIZE];
unsigned short gInCmdBufferIdx = 0;
volatile unsigned short gInCmdSkipCount;

unsigned int g_COMCmdCtr=0;
int g_MAUsToRead=0;
int g_MAUSize=0;

#if(SCI_MODE==FIFOINT)

extern int getCount(RINGBUFFER *);

extern void sci_start_xmit(void);

#else

extern void sci_xmit(int );

#endif


void ClearBufferRelatedParam();

// override these depends on target
int GetTargetEndianness()
{
	return LITTLE_ENDIAN;
}
// override these depends on target
void Write8bitByteToCOM(unsigned char c)
{
	sci_xmit(c & 0xff);         // this blocks if TX buff is full in INDCHAR mode, so we will wait here and always transmit the single character
	                            // In FIFO this always returns (maybe unless ring buffer is full??)
}

int GetSizeOfMAUIn8bitByte()
{

	unsigned char maxMAUValue = (unsigned char)(-1);
	switch (maxMAUValue)
	{
	case 0xff:
		return 1;
	case 0xffff:
		return 2;
	default:
		return 0;
	}
}

int WriteToCmdBuffer(unsigned char* buf, unsigned short* bufIdx, unsigned char d)
{
	if ( (*bufIdx) < CMD_BUFFER_SIZE )
	{
		buf[*bufIdx] = d & 0xff;
		(*bufIdx)++;
		return 0;
	}

	return 1;
}

void ResetInCmdBuffer()
{
	gInCmdBufferIdx = 0;
}

int WriteByteToInCmdBuffer(unsigned char d)
{
	return WriteToCmdBuffer(gInCmdBuffer, &gInCmdBufferIdx, d);
}

int GetTransferSizeInMAU() //transfer size refer to the words to read/write of a given cmd, not the number of bytes for the whole cmd packet
{
	return (gInCmdBuffer[0] & 0x3f);
}

int VerifyInputCmdHeaders()
{
	return ((gInCmdBuffer[0] & 0x80) == 0x80) ? 0 : 1;
}

int GetInputCmdType()
{
	return (gInCmdBuffer[0] & 0x80);
}

int GetRWFlag()//equivalent to endianness on the MAU in transmission
{
	int ret = ((gInCmdBuffer[0] >> 6) & 0x1); //
	return ret;
}

unsigned char* GetInCmdAddress()
{
	unsigned char* addr = 0;
	uint32_t addr_value = 0;
	int i = 0;
	int addressSize = 4;
	for (; i < addressSize; i++)
	{
		addr_value |= (uint32_t)( gInCmdBuffer[1 + i] << 8 * (addressSize - 1 - i) ); //big endian
	}

	addr = (unsigned char*) addr_value;
	return addr;
}

void WriteMAUToCOM(unsigned char d)
{
	int MAUSize = GetSizeOfMAUIn8bitByte();
	g_MAUSize = MAUSize;
	switch (MAUSize)
	{
	case 1:
		Write8bitByteToCOM(d);
		break;
	case 2:
	{
		unsigned char MAU[2];
		MAU[0] = (unsigned char)(d & 0xff);
		MAU[1] = (unsigned char)(d >> 8);
		if (GetTargetEndianness() == LITTLE_ENDIAN)
		{
			Write8bitByteToCOM(MAU[0]);
			Write8bitByteToCOM(MAU[1]);
			g_COMCmdCtr+=2;
		} else {
			Write8bitByteToCOM(MAU[1]);
			Write8bitByteToCOM(MAU[0]);
		}
	}
	break;
	default://only handles 8bit, 16bit MAU
		break;
	}
}

unsigned char GetWriteCmdDataMAU(int idx)
{
	unsigned char startIdx = 1 + 4;

	unsigned char val = 0;
	int MAUSize = GetSizeOfMAUIn8bitByte();
	int byteOffset = idx*MAUSize;

	switch (MAUSize)
	{
	case 1:
		val = gInCmdBuffer[startIdx + byteOffset];
		break;
	case 2:
		if (GetTargetEndianness() == LITTLE_ENDIAN)
		{
			val = ( gInCmdBuffer[startIdx + byteOffset + 1] << 8 ) | gInCmdBuffer[startIdx + byteOffset];
		} else {
			val = ( gInCmdBuffer[startIdx + byteOffset] | gInCmdBuffer[startIdx + byteOffset + 1] << 8 );
		}
		break;
	default://only handles 8bit, 16bit MAU
		break;
	}

	return val;
}

void ClearBufferRelatedParam()
{
	gInCmdSkipCount = 0;
	gInCmdBufferIdx	= 0;
}

void MemAccessCmd(int RW)
{
	unsigned short MAUsToRead = 0;
	unsigned char dataChar = 0;
	unsigned char* addr = GetInCmdAddress();
	unsigned short i, j;

	//Changing this function to stuff every byte that it originally wrote to COM
	//into the TX memory buffer instead.
	for ( j = 0; j < 1; j++ )               // This writes the 1 byte reply message header - just a copy of the first byte of the 5 byte header *to* target from host
	{
		Write8bitByteToCOM(gInCmdBuffer[j]);
		g_COMCmdCtr++;
	}

	MAUsToRead = GetTransferSizeInMAU();
	g_MAUsToRead = MAUsToRead;
	for ( i = 0; i < MAUsToRead; i++ )
	{
		if (RW == READ)
		{
			dataChar = *(addr + i);
			WriteMAUToCOM(dataChar);
		} else { //WRITE
			dataChar = GetWriteCmdDataMAU(i);
			*(addr + i) = dataChar;
		}
	}
    sci_start_xmit();           // Start of transmission sequence that we just loaded into buffers via the WriteMAUToCom() calls
}

int ProcessCommand()
{
	//Not sure it verifies the first byte again...
	if ( VerifyInputCmdHeaders() )
	{
		return 1;
	}

	switch ( GetInputCmdType() )	//and a third time...?
	{
	case RW_CMD:
		MemAccessCmd(GetRWFlag());
		break;
	default:
		return 1;
	}

	return 0;
}


void receivedDataCommand(unsigned char d) // only lower byte will be used even if MAU is bigger than 1 byte
{
	WriteByteToInCmdBuffer(d);

	if (gInCmdSkipCount > 0)        // skip count is zero when first character of header is read so we skip this on the first byte
	{
		gInCmdSkipCount--;
		return;
	}

	if (gInCmdBufferIdx > 0 && gInCmdSkipCount == 0)
	{
		if ( VerifyInputCmdHeaders() )      //wrong input header, clear cmd buffer
		{
			ClearBufferRelatedParam();
			return;
		}

		if (gInCmdBufferIdx == 1) {         // If we just processed the first byte of the header then set gInCmdSkipCount to tell how many more bytes to read before coming back here to process full command
			if (GetRWFlag() == WRITE)       // write means external device is writing into our target memory
			{
				gInCmdSkipCount = 4 - 1 + GetTransferSizeInMAU() * GetSizeOfMAUIn8bitByte();
			} else {
				gInCmdSkipCount = 4 - 1 ;
			}
		} else {
		    ProcessCommand();
			ClearBufferRelatedParam();
		}
		return;
	}

}

