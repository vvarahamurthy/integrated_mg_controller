/*
 * ParameterMap.c
 *
 *  Created on: Feb 5, 2019
 *      Author: vvarahamurthy
 */


#include "F2837S_files/F28x_Project.h"
#include "Header/ParameterMap.h"
#include "Header/GUIC_Header.h"
#include "Header/ECU.h"
#include "Header/UID_UNIQUE.h"
#include "inc/hw_types.h"
#include "Header/Elmo.h"
#include "Header/ADC.h"
#include "Header/Globals.h"
#include "Header/Gen_Control.h"

void *pParameterMap[100]={(void *)NULL};        //Array of pointers to each parameter
void (*pSetFuncs[100])(float, void *)={NULL};   //Array of pointers to functions
enum datatype ParameterTypes[100]={(enum datatype)0};                  //Array of data types
struct BroadcastEntries pBroadcastFunctionsMap[52] = {(void *)NULL};

extern enum ECUVariableStates ECU_GetState(void);
extern float ECUSCI_GetGeadTempC(void);
extern float ECU_GetThrottleSetting(void);
extern uint16_t getElmo2NumFaults(void);
extern uint16_t getElmo2NumFaults(void);
extern float getGCAmpsCommand(void);
extern float GetEcuPidOutPreSat(void);
extern float GetEcuPidReference(void);
extern float ECU_GetRPMLUT(void);
extern float ECU_GetChgPidOutPreSat(void);
extern float ECU_GetFFTerm(void);
extern float GenControl_GetDCPowerLPF(void);
extern float ECU_GetDCPowerLPF(void);

extern struct GenControl_Params  GenControlParams;
extern struct ECUParameters ECU_Params;

#if(ENGINE_TUNING_MAPPING)
extern float manualcurrentcommand;
#endif

uint32_t sw_version;
uint32_t sw_serial_number;// = *uid_unique;

float GetBMSFdbSig(void) { return g_BMSFdbSig; }
float GetPMU_ControlRefSig(void) { return g_PMU_ControlRefSig; }
uint16_t GetTimeSinceReset(void) { return g_TimeSinceReset; }
uint32_t GetFaults(void) { return g_Faults; }
float GetFeedForwardTerm(void) { return g_FeedForwardTerm; }
float GetEcuControlRef(void) { return g_EcuControlRef; }
float GetFilteredFeedForward(void) { return g_FeedForwardFiltered; }
//enum GenControlStateVariables GenControl_GetMode(void) { return (enum GenControlStateVariables)g_actualPMUMode; }

void InitParameterArray(void)
{
    uint32_t swVersionMajor = SwVersionMajor;
    uint32_t swVersionMinor = SwVersionMinor;
    uint32_t swVersionBuild = SwVersionBuild;
    sw_version = (swVersionMajor << 16) | (swVersionMinor << 8) | swVersionBuild;
    EALLOW;
    sw_serial_number = *uid_unique;
    EDIS;

    SetParamMapElement(type_ulong,  (int)ID_SW_VERSION,  (void *)&sw_version);
    SetParamMapElement(type_ulong,  (int)ID_SERIAL_NUMBER,  (void *)&sw_serial_number);
    SetParamMapElement(type_float,  (int)ID_COOL_DUTY,  (void *)&ECU_Params.CoolingDuty);
    SetParamMapElement(type_int,  (int)ID_COOL_MODE,  (void *)&ECU_Params.CoolingMode);
    SetParamMapElement(type_float,  (int)ID_THRT_KP,  (void *)&ECU_Params.activepid_kp);
    SetParamMapElement(type_float,  (int)ID_THRT_KI,  (void *)&ECU_Params.activepid_ki);
    SetParamMapElement(type_float,  (int)ID_ICRANK,  (void *)&GenControlParams.StartingAmps);
    SetParamMapElement(type_float,  (int)ID_GC_TSTART,  (void *)&GenControlParams.StartingTime_ms);
    SetParamMapElement(type_uint,  (int)ID_GC_STARTRPM,  (void *)&GenControlParams.StartRPMThresh);
    SetParamMapElement(type_float,  (int)ID_GC_KP,  (void *)&GenControlParams.pi_kp);
    SetParamMapElement(type_float,  (int)ID_GC_KI,  (void *)&GenControlParams.pi_ki);
    SetParamMapElement(type_float,  (int)ID_GC_KD,  (void *)&GenControlParams.pi_kd);
    SetParamMapElement(type_float,  (int)ID_LPF_ECU_DC,  (void *)&ECU_Params.DCP_LPF_Hz);
    SetParamMapElement(type_float,  (int)ID_LPF_ECU_FF,  (void *)&ECU_Params.FF_LPF_Hz);
    SetParamMapElement(type_float,  (int)ID_ECU_GAIN_FF,  (void *)&ECU_Params.feedfw_gain);
    SetParamMapElement(type_float,  (int)ID_LPF_GC_DC,  (void *)&GenControlParams.DCP_LPF_Hz);
    SetParamMapElement(type_float,  (int)ID_LPF_GC_FF,  (void *)&GenControlParams.FF_LPF_Hz);
    SetParamMapElement(type_float,  (int)ID_GC_FF_GAIN,  (void *)&GenControlParams.FF_Gain);
    SetParamMapElement(type_float,  (int)ID_ECU_IDLETHI,  (void *)&ECU_Params.StartingThrottleHi);
    SetParamMapElement(type_float,  (int)ID_ECU_IDLETLO,  (void *)&ECU_Params.StartingThrottleLo);
    SetParamMapElement(type_float,  (int)ID_LIM_STATOR_DERATE, (void *)&Motor_Params.LIM_T_STATOR_WARN_H);
    SetParamMapElement(type_float,  (int)ID_GC_DERATE, (void *)&GenControlParams.TorqueDemandDeratingFactor);
    SetParamMapElement(type_uint,  (int)ID_GC_MAXPWR, (void *)&GenControlParams.MaxGenPower);
    SetParamMapElement(type_uint,  (int)ID_GC_POWERTHRESH, (void *)&GenControlParams.MinPowerThresh);
    SetParamMapElement(type_uint,  (int)ID_ECU_MINPWRH, (void *)&ECU_Params.MinBusPower_H);
    SetParamMapElement(type_uint,  (int)ID_ECU_MINPWRL, (void *)&ECU_Params.MinBusPower_L);
    SetParamMapElement(type_uint,  (int)ID_ECU_MAXPWR, (void *)&ECU_Params.MaxGenPower);
    SetParamMapElement(type_uint,  (int)ID_ECU_TENRICH, (void *)&ECU_Params.StartupEnrichTime);

#if(NEWCONTROL_MODE)
    SetParamMapElement(type_float,  (int)ID_ECU_CC_KP,  (void *)&ECU_Params.pid_chgctrl_kp);
    SetParamMapElement(type_float,  (int)ID_ECU_CC_KI,  (void *)&ECU_Params.pid_chgctrl_ki);
    SetParamMapElement(type_float,  (int)ID_ECU_CC_KD,  (void *)&ECU_Params.pid_chgctrl_kd);
#endif

#if(ENGINE_TUNING_MAPPING)
    SetParamMapElement(type_float,  (int)ID_GC_MAN,  (void *)&manualcurrentcommand);
#endif



    SetBroadcastValue(Elmo1_NumFaults, type_int, (void *)&getElmo2NumFaults);
    SetBroadcastValue(Elmo2_NumFaults, type_int, (void *)&getElmo2NumFaults);
    SetBroadcastValue(ECU_PID_OutPreSat, type_float, (void *)&GetEcuPidOutPreSat);
    SetBroadcastValue(ECU_PID_Reference, type_float, (void *)&GetEcuPidReference);
    SetBroadcastValue(ECU_RPM_LUT, type_float, (void *)&ECU_GetRPMLUT);

#if(NEWCONTROL_MODE)
    SetBroadcastValue(ECU_CHG_PID_OutPreSat, type_float, (void *)&ECU_GetChgPidOutPreSat);
#endif
    SetBroadcastValue(ECU_Feedforward_Term, type_float, (void *)&ECU_GetFFTerm);
    SetBroadcastValue(ECU_DCPLPF, type_float, (void *)&ECU_GetDCPowerLPF);
    SetBroadcastValue(GC_DCPLPF, type_float, (void *)&GenControl_GetDCPowerLPF);

#if 0
    SetParamMapElement(type_ulong,  (int)ID_SW_VERSION,  (void *)&sw_version);
    SetParamMapElement(type_ulong,  (int)ID_SERIAL_NUMBER,  (void *)&sw_serial_number);
    SetParamMapElement(type_float,  (int)ID_LOOP_FREQ,  (void *)&Motor_Params.LOOP_FREQ_KHZ);
    SetParamMapElement(type_float,  (int)ID_PWM_FREQ,  (void *)&Motor_Params.PWM_FREQ_KHZ);
    SetParamMapElement(type_int,    (int)ID_SPDLOOP_PSC,  (void *)&Motor_Params.SPEED_LOOP_PRESCALE);
    SetParamMapElement(type_float,  (int)ID_MOTOR_POLES,  (void *)&Motor_Params.Poles);
    SetParamMapElement(type_float,  (int)ID_MOTOR_KV,  (void *)&Motor_Params.Kv);
    SetParamMapElement(type_float,  (int)ID_MOTOR_L,  (void *)&Motor_Params.StatorInductance);
    SetParamMapElement(type_float,  (int)ID_MOTOR_R,  (void *)&Motor_Params.StatorResistance);
    SetParamMapElement(type_float,  (int)ID_SPD_KP, (void *)&Motor_Params.spd_KP);
    SetParamMapElement(type_float,  (int)ID_SPD_KI, (void *)&Motor_Params.spd_KI);
    SetParamMapElement(type_float,  (int)ID_BUSV_NOM, (void *)&Motor_Params.BusV_Nominal);
    SetParamMapElement(type_float,  (int)ID_START_VLOCK, (void *)&Motor_Params.Start_VLockRotor);
    SetParamMapElement(type_float,  (int)ID_START_IQOL, (void *)&Motor_Params.Start_IDOL);
    SetParamMapElement(type_float,  (int)ID_START_IDOL, (void *)&Motor_Params.Start_IQOL);
    SetParamMapElement(type_float,  (int)ID_START_TSPEED, (void *)&Motor_Params.Start_TransitionSpeed);
    SetParamMapElement(type_float,  (int)ID_RAMPDOWN_SECS, (void *)&Motor_Params.RAMP_down_Secs);
    SetParamMapElement(type_int,    (int)ID_LOOPSEN, (void *)&Motor_Params.LoopsEn);
    SetParamMapElement(type_int,    (int)ID_BUILDLVL, (void *)&Motor_Params.BuildLevel);


    SetBroadcastValue(PMU_Mode, type_char, (void *)&GenControl_GetMode);
    SetBroadcastValue(Elmo1_State, type_float, (void *)&GetElmo0State );
    SetBroadcastValue(Elmo2_State, type_float, (void *)&GetElmo1State );
    SetBroadcastValue(Control_Reference, type_float, (void *)&GetPMU_ControlRefSig );
    SetBroadcastValue(BMS_Current_Value, type_float, (void *)&GetBMSFdbSig );
    SetBroadcastValue(Genset1_Current, type_float, (void *)&GetElmo0Current );
    SetBroadcastValue(Genset2_Current, type_float, (void *)&GetElmo1Current );
    SetBroadcastValue(Bus_Voltage, type_float, (void *)&ADC_V_Bus );
    SetBroadcastValue(Stator_Temp, type_float, (void *)&ADC_StatorNTC );
    SetBroadcastValue(Engine_Temp, type_float, (void *)&ECUSCI_GetGeadTempC );
    SetBroadcastValue(Engine_Speed, type_float, (void *)&ECU_GetSpeedRPM );
    SetBroadcastValue(Throttle, type_int, (void *)&ECU_GetThrottleSetting );
    SetBroadcastValue(Logic_Board_Temp, type_float, (void *)&ADC_T_BoardF );
    SetBroadcastValue(Power_Board_Temp, type_float, (void *)&ADC_T_PowerBoardF );
    SetBroadcastValue(CPU_Temp, type_int, (void *)&ADC_T_CPU );
    SetBroadcastValue(Heatsink_Temp, type_float, (void *)&ADC_T_DeviceF );
    SetBroadcastValue(DC_Power, type_float, (void *)&ADC_P_DCF );
    SetBroadcastValue(ECU_State, type_int, (void *)&ECU_GetState );
    SetBroadcastValue(Time_Since_Reset, type_int, (void *)&GetTimeSinceReset );
    SetBroadcastValue(Current_Faults, type_long, (void *)&GetFaults );
    SetBroadcastValue(ECU_Feedforward_Term, type_float, (void *)&GetFeedForwardTerm );
    SetBroadcastValue(ECU_PID_Reference, type_float, (void *)&GetEcuPidReference );
    SetBroadcastValue(ECU_PID_Feedback, type_float, (void *)&GetEcuPidFeedback );
    SetBroadcastValue(ECU_PID_Error, type_float, (void *)&GetEcuPidError );
    SetBroadcastValue(ECU_PID_Up, type_float, (void *)&GetEcuPidUp );
    SetBroadcastValue(ECU_PID_Ui, type_float, (void *)&GetEcuPidUi );
    SetBroadcastValue(ECU_PID_Ud, type_float, (void *)&GetEcuPidUd );
    SetBroadcastValue(ECU_PID_OutPreSat, type_float, (void *)&GetEcuPidOutPreSat );
    SetBroadcastValue(ECU_Control_Reference, type_float, (void *)&GetEcuControlRef );
    SetBroadcastValue(Elmo_Feedforward_Term_Filtered, type_float,(void *)&GetFilteredFeedForward );
#endif
}


/*Function: SetParamMapElement
 * ------------------
 * Populates the parameter array with a pointer to a parameter. The element is
 * typecast based on the data type and a corresponding function is selected in
 * to cast the data appropriately while setting the parameter value.
 *
 * Outputs:
 * None
 *
 * Inputs:
 * type:    enumerated data type, decides what pointer type and function are used
 * index:   index for both parameter ptr array and function ptr array
 * pItem:   pointer to desired struct element
 */
void SetParamMapElement(enum datatype type, int16_t index, void * pItem )
{
    ParameterTypes[index]=type;
    switch(type)
    {
        case type_char:
            pParameterMap[index]=(char *)pItem;
            pSetFuncs[index]=&SetParam_char;
            break;
        case type_uchar:
            pParameterMap[index]=(unsigned char *)pItem;
            pSetFuncs[index]=&SetParam_uchar;
            break;
        case type_int:
            pParameterMap[index]=(int *)pItem;
            pSetFuncs[index]=&SetParam_int;
            break;
        case type_uint:
            pParameterMap[index]=(uint16_t *)pItem;
            pSetFuncs[index]=&SetParam_uint;
            break;
        case type_long:
            pParameterMap[index]=(int32_t *)pItem;
            pSetFuncs[index]=&SetParam_long;
            break;
        case type_ulong:
            pParameterMap[index]=(uint32_t *)pItem;
            pSetFuncs[index]=&SetParam_ulong;
            break;
        case type_longlong:
            pParameterMap[index]=(uint32_t *)pItem;
            pSetFuncs[index]=&SetParam_longlong;
            break;
        case type_ulonglong:
            pParameterMap[index]=(uint32_t *)pItem;
            pSetFuncs[index]=&SetParam_ulonglong;
            break;
        case type_float:
            pParameterMap[index]=(float *)pItem;
            pSetFuncs[index]=&SetParam_float;
            break;
        case type_double:
            pParameterMap[index]=(double *)pItem;
            pSetFuncs[index]=&SetParam_float;
            break;
        default:
            asm ("      ESTOP0");
    }
}


/*Function: SetParameterValue
 * -----------------------
 * Calls the appropriate function for the data type and copies the value
 * inputted to the value at the address
 *
 * Outputs:
 * None
 *
 * Inputs:
 * param_number:    parameter number/index of parameter pointer array
 * value:           float value to send to the parameter.
 */
void SetParameterValue(int16_t param_number,float value)
{
    pSetFuncs[param_number](value, (void *)pParameterMap[param_number]);
}

void SetParam_char(float val, void * dest)
{
    *(char *)dest = (char)val;
}

void SetParam_uchar(float val, void * dest)
{
    *(unsigned char *)dest = (unsigned char)val;
}

void SetParam_int(float val, void * dest)
{
    *(int16_t *)dest = (int16_t)val;
}

void SetParam_uint(float val, void * dest)
{
    *(uint16_t *)dest= (uint16_t)val;
}

void SetParam_long(float val, void * dest)
{
    *(int32_t *)dest=(int32_t)val;
}

void SetParam_ulong(float val, void * dest)
{
    *(uint32_t *)dest = (uint32_t)val;
}

void SetParam_longlong(float val, void * dest)
{
    *(int64_t *)dest=(int64_t)val;
}

void SetParam_ulonglong(float val, void * dest)
{
    *(uint64_t *)dest=(uint64_t)val;
}


void SetParam_float(float val, void * dest)
{
    *(float *)dest=val;
}

void SetParam_double(float val, void * dest)
{
    *(double *)dest=val;
}


/*Function: GetParameterValue
 * -----------------------
 * Returns a copy of the value at a certain parameter
 * map index
 *
 * Outputs:
 * float retval: value at the requested parameter number
 *
 * Inputs:
 * param_number:    parameter number/index of parameter pointer array
 */
float GetParameterValue(int16_t param_number)
{
    float retval=0.0;

    //Pointer to the desired parameter array element
    void *pParam = (void *)pParameterMap[param_number];

    union{
        unsigned char ui8;
        uint16_t ui16;
        uint32_t ui32;
        uint64_t ui64;
        char i8;
        int16_t i16;
        int32_t i32;
        int64_t i64;
        float vflt;
    }get_val;

    switch(ParameterTypes[param_number])
    {
        case type_char:
            get_val.i8 = *((char *)pParam);
            retval = (float)get_val.i8;
            break;
        case type_uchar:
            get_val.ui8 = *((unsigned char *)pParam);
            retval = (float)get_val.ui8;
            break;
        case type_int:
            get_val.i16 = *((int16_t *)pParam);
            retval = (float)get_val.i16;
            break;
        case type_uint:
            get_val.ui16 = *((uint16_t *)pParam);
            retval = (float)get_val.ui16;
            break;
        case type_long:
            get_val.i32 = *((int32_t *)pParam);
            retval = (float)get_val.i32;
            break;
        case type_ulong:
            get_val.ui32 = *((uint32_t *)pParam);
            retval = (float)get_val.ui32;
            break;
        case type_longlong:
            get_val.i64 = *((int64_t *)pParam);
            retval = (float)get_val.i64;
            break;
        case type_ulonglong:
            get_val.ui64 = *((uint64_t *)pParam);
            retval = (float)get_val.ui64;
            break;
        case type_float:
            get_val.vflt = *((float *)pParam);
            retval = (float)get_val.vflt;
            break;
        default:
            break;
    }

    return retval;
}

uint32_t GetRawParameterValue(int16_t param_number)
{
    uint32_t retval = 0;
    void *tempPnt = pParameterMap[param_number];
    uint32_t *pTempVal = (uint32_t*)tempPnt;
    retval = *pTempVal;
    return retval;
}

void SetBroadcastValue(int16_t index, enum datatype type, void * func)
{
    bool indexGreaterThan0 = (index >= 1);
    bool indexLessThanOpcodes = (index < Number_Of_Broadcast_Opcodes);
    if( indexGreaterThan0 && indexLessThanOpcodes )
    {
        pBroadcastFunctionsMap[index].valueFunction = func;
        pBroadcastFunctionsMap[index].dataType = type;
    }
}

/* This function takes an index of the pBroadcastFunctionsMap
 * and returns the raw bytes of the floating point number
 * of the values of the function in that index
 * */

uint32_t GetRawBroadcastValue(int index)
{
    uint32_t retval = 0;
    bool indexGreaterThan0 = (index >= 1);
    bool indexLessThanOpcodes = (index < Number_Of_Broadcast_Opcodes);
    if( indexGreaterThan0 && indexLessThanOpcodes )
    {
        float tempFloat = 0;
        switch( pBroadcastFunctionsMap[index].dataType )
        {
            case type_char:
            {
                char tempChar = 0;
                char (*f)(void);
                f = (char(*)(void))pBroadcastFunctionsMap[index].valueFunction;
                tempChar = f();
                tempFloat = (float)tempChar;
                break;
            }
            case type_uchar:
            {
                unsigned char tempUchar = 0;
                unsigned char (*f)(void);
                f = (unsigned char(*)(void))pBroadcastFunctionsMap[index].valueFunction;
                tempUchar = f();
                tempFloat = (float)tempUchar;
                break;
            }
            case type_int:
            {
                int16_t tempInt = 0;
                int16_t (*f)(void);
                f = (int16_t(*)(void))pBroadcastFunctionsMap[index].valueFunction;
                tempInt = f();
                tempFloat = (float)tempInt;
                break;
            }
            case type_uint:
            {
                uint16_t tempUint = 0;
                uint16_t (*f)(void);
                f = (uint16_t(*)(void))pBroadcastFunctionsMap[index].valueFunction;
                tempUint = f();
                tempFloat = (float)tempUint;
                break;
            }
            case type_long:
            {
                long tempLong = 0;
                long (*f)(void);
                f = (long(*)(void))pBroadcastFunctionsMap[index].valueFunction;
                tempLong = f();
                tempFloat = (float)tempLong;
                break;
            }
            case type_ulong:
            {
                uint32_t tempUlong = 0;
                uint32_t (*f)(void);
                f = (uint32_t(*)(void))pBroadcastFunctionsMap[index].valueFunction;
                tempUlong = f();
                tempFloat = (float)tempUlong;
                break;
            }
            case type_float:
            {

                float (*f)(void);
                f = (float(*)(void))pBroadcastFunctionsMap[index].valueFunction;
                tempFloat = f();
                break;
            }
            case type_longlong: // fall through
            case type_ulonglong: // fall through
            default:
                break;
        }
        union ufloat
        {
            float fl;
            uint32_t ut;
        };
        union ufloat tempUfloat;
        tempUfloat.fl = tempFloat;
        retval = tempUfloat.ut;
    }

    return retval;
}
