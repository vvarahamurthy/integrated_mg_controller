
#include "F2837S_files/F28x_Project.h"
#include "DLOG_4CH_F.h"

#define FixMeLater 1

/*  Originally from C:\ti\controlSUITE\development_kits\TMDSIDDK_v1.0\IDDK_OM_Servo_F2837x\utility Src Files */

/*  Triggers on Channel 1, rising edge across trigger value */

//*********** Structure Init Function *****//

void DLOG_4CH_F_init(DLOG_4CH_F *v)
{
	v->input_ptr1=0;
	v->input_ptr2=0;
	v->input_ptr3=0;
	v->input_ptr4=0;
	v->output_ptr1=0;
	v->output_ptr2=0;
	v->output_ptr3=0;
	v->output_ptr4=0;
	v->prev_value=0;
	v->trig_value=0;
	v->status=0;
	v->pre_scalar=0;
	v->skip_count=0;
	v->size=0;
	v->count=0;
	v->trig_mode = Auto;
	v->trig_edge = Rising;
}

//*********** Function Definition ********//
void DLOG_4CH_F_FUNC(DLOG_4CH_F *v)
{
	switch(v->status)
	{
	case 1: /* wait for trigger*/
		if(*(v->input_ptr1)>(v->trig_value) && (v->prev_value)<(v->trig_value) )
		{
			/* rising edge detected start logging data*/
			v->status=3;
		}
		break;
	case 2: /*wait for trigger*/
	    if(*(v->input_ptr1)<(v->trig_value) && (v->prev_value)>(v->trig_value) )
	    {
	        //Falling edge detection
	        v->status=3;
	    }
		break;
	case 3:
		v->skip_count++;
		if(v->skip_count>=v->pre_scalar)
		{
			v->skip_count=0;
			v->output_ptr1[v->count]=*(v->input_ptr1);
			v->output_ptr2[v->count]=*(v->input_ptr2);
			v->output_ptr3[v->count]=*(v->input_ptr3);
			v->output_ptr4[v->count]=*(v->input_ptr4);
			v->count++;
			if(v->count==v->size)
			{
				v->count=0;
				switch(v->trig_mode)
				{
				    case Auto:
				        v->status = 3;
				        break;
				    case Normal:
				        if(v->trig_edge==Rising)
				        {
				            v->status = 1;
				        }
				        else if(v->trig_edge==Falling)
				        {
				            v->status = 2;
				        }
				        break;
				    case Manual:
				        v->status=0;
				        break;
				    default:
				        v->status=0;
				        break;
				}
			}
		}
		break;
	default:
		break;
	}
	v->prev_value=*(v->input_ptr1);
}

