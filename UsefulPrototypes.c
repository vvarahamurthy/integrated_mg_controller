/*
 * UsefulPrototypes.c
 *
 *  Created on: Feb 8, 2021
 *      Author: VishaalVarahamurthy
 */

#include "Header/UsefulPrototypes.h"
#include <limits.h>

/**Function: saturate
 * ------------------
 * Saturates a float input between two bounds
 *
 * @returns Input value if between [min,max]. Saturated value if out of range
 *
 * @param min: Lower bound for saturation
 * @param max: Upper bound for saturation
 * @param in:  input value
 */
float saturate(float min, float max, float in)
{
    if(in>=max)
    {
        return max;
    }
    else if(in<=min)
    {
        return min;
    }
    else return in;
}

/**Function: dec
 * ------------------
 * Decrements the value at an address. Stops at 0 to prevent overflow.
 *
 * Outputs:
 * None
 *
 * @param *in - Pointer to a uint32_t value to decrement
 */
void dec(uint32_t * in)
{
    if(*in > 0 )
    {
        (*in)--;
    }
    else
    {
        *in = 0;
    }
}

/**Function: inc
 * ------------------
 * Increments the value at an address. Resets @ ULONG_MAX to prevent overflow
 *
 * Outputs:
 * None
 *
 * Inputs:
 * @param *in - Pointer to a uint32_t value to increment.
 */
void inc(uint32_t * in)
{
    if(*in < ULONG_MAX )
    {
        (*in)++;
    }
    else
    {
        //Resets if you've hit ULONG_MAX
        *in = 0;
    }
}


/**Function: inRange
 * ------------------
 * Increments the value at an address. Resets @ ULONG_MAX to prevent overflow
 *
 * @returns true if in is within range [min,max]
 * @returns false if not

 * @param in - value to check
 * @param max - upper bound on range check
 * @param min - lower bound on range check
 */
bool inRange(float in, float max, float min)
{
    bool retval = false;
    if( (in<=max) && (in>=min) )
    {
        retval = true;
    }
    return retval;
}

float convBytesToFloat(uint32_t src)
{
    union{
        uint32_t bytes;
        float val;
    }value;

    value.bytes=src;

    return value.val;
}

uint32_t convFloatToBytes(float src)
{
    union{
        uint32_t bytes;
        float flt;
    }value;

    value.flt=src;

    return value.bytes;
}
