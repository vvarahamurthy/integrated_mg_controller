/* ============================================================================
System Name:    IDDK Sevo Control

File Name:      IDDK_Servo.C

Target:         Rev 1.1 of the F2837x control card

Author:         C2000 Systems Lab, 05th March 2015

Description:	Project uses two ISRs
				1. Resolver ISR triggered by ADC INT, this is defined as the
				     high priority interrupt and runs at 160Khz
				2. Inverter ISR rate is 10Khz and is triggered by EPWM11,
				     reason to choose EPWM11 is because of the SD reading
				     EPWM11 provides periodic sync resets to the SDFM module,
				     after each reset the SDFM data is ready after 3 OSRs and
				     needs to be read immediately.
*/

//  Used to mark code for deletion later once port it working
#define DeleteMe    1
#define FixMeLater  1

#define UBER_TEST   1

// Include header files used in the main function
// define float maths and then include IQmath library

#include "F2837S_files/F28x_Project.h"

#include "Header/GUIC_Header.h"
#include "Header/State_machine.h"
#include "Header/LP_CAN.h"
#include "Header/ECU.h"
#include "Header/LED.h"
#include "Header/Elmo.h"
#include "Header/Globals.h"
#include "Header/GPIO.h"
#include "Header/UsefulPrototypes.h"

#include "F2837xS_Pie_defines.h"
#include "F2837xS_GlobalPrototypes.h"


#ifdef FLASH
#pragma CODE_SECTION(MotorControlISR,"ramfuncs");
#endif

#pragma INTERRUPT (MotorControlISR, LPI)

#define RPM_LPF_ALPHA ((1.0-expf(-2.0*PI*500.0/Inverter_Params.B_TASK_HZ)))  //10Hz filter for RPM

static uint32_t interruptOnTime = 0;
static uint32_t interruptOffTime = 0;
static uint32_t interruptEntryTime = 0;
static uint32_t interruptExitTime = 0;

// Prototype statements for functions found within this file.
interrupt void MotorControlISR(void);
void ResetByInfiniteLoop(void);

/***********************************************/
/*  External functions used by the main module */
/***********************************************/

//******************************
// CAN
//******************************


//******************************
//Functions externed from ADC.c
//******************************
extern void Init_Analog(void);
extern void AnalogSensorSuite_Int();
extern void AnalogSensorSuite_100Hz();
extern void AnalogSensorSuite_10Hz();
extern uint32_t getADCWarnings(void);
extern float adcGetSignalValue(enum adc_signal_id);


// ******************************
// GPIO Functions
// ******************************
extern void Init_GPIO(void);
extern void Debounce_GPIO(void);
extern uint16_t GetESTOP_Debounced(void);
extern uint16_t GetStart_Debounced(void);

// *************************************
// PWM functions
// *************************************
extern void Init_PWM(void);

// *************************************
// ECU Functions
// *************************************
extern void Init_ECU(enum ECUControlModes);
extern void InitECUParams(enum ECUControlModes);
extern void Init_ECU_SCI(void);
extern enum ECUControlModes GetECUCtrlMode(void);
extern void ECU_PingCHT();
extern bool Fault_Detect_ECUSCI(void);
extern void SCI_ECU_ProcessMessage(void);
extern void ECU_State_Machine(enum MainStateVariables , enum GenControlStateVariables , enum CritFaultVariables , float , float , enum ECUControlModes, float , float );


// *************************************
// ECAP Functions
// *************************************
extern void Init_ECAP(void);
extern float ECAP2_GetTachRpm(void);
extern uint32_t ECAP1_GetPulseus(void);

// *************************************
// Functions externed from Module_Controller
// *************************************
extern void Init_Controller(void);
extern void MainControlLoop(enum BuildLevels, enum MainStateVariables, enum HighMotorControlVar MCStateParam, enum CritFaultVariables, float);

// *************************************
// Serial comm functions
// *************************************
extern void Init_SCI(void);
extern void InitParseRoutine();
extern void ClearBufferRelatedParam(void);
extern void checkForMessages(void);
extern unsigned char getRxState(void);
extern enum CritFaultVariables Fault_Detect_UART(void);
extern float getMasterValue1_sig();
extern float getMasterValue2_sig();
extern void transmitMessage(enum MainStateVariables, enum SyncMode, enum CritFaultVariables);

// *************************************
// LED Functions
// *************************************
extern void Init_LED();
extern void Run_LED();
extern void LED2_ON(void);
extern void LED2_OFF(void);
extern void SetLED1_BlinkRate();

// *******************************
// Externed CAN Functions
// *******************************
extern void Init_CAN_Periph(void);

//"status update" functions that send info out
extern void CAN_Heartbeat_1Hz(void);
extern void CAN_Heartbeat_6Hz(void);
extern void CAN_Heartbeat_10Hz(void);
extern void CAN_Heartbeat_100Hz(void);

extern void CAN_RunBroadcast(void);

extern void CAN_AngleStatusUpdate_fast();
extern void CAN_UTAStatusUpdate(void);


//"read" functions that read values in
extern void CAN_MessageCheck_10Hz();
extern void CAN_MessageCheck_100Hz();

extern float CAN_GetBMSFdb(void);
extern float CAN_GetBMSReq(void);

extern float CAN_GetGenControlRef();

extern enum GenControlStateVariables CAN_GetGenCtlrModeCmd();

extern uint32_t CAN_getErrorCode();

// *******************************
// State Machine Functions
// *******************************
extern enum MainStateVariables MainStateMachine(enum CritFaultVariables , uint32_t, bool );
extern enum MainStateVariables MainState_Get(void);

extern uint32_t MainState_GetStateTicker(void);

extern bool getElmoStatus(void);
extern bool GenControlMotionStates(enum GenControlStateVariables);
extern enum GenControlStateVariables GenControllerStateMachine(enum MainStateVariables , enum GenControlStateVariables , enum CritFaultVariables , bool  , float , float , float);
extern enum HighMotorControlVar MotorControlStateMachine(enum BuildLevels , enum MainStateVariables, enum CritFaultVariables );
// *******************************
// Parameter Functions
// *******************************
extern void LoadFromEEPROM(void);

extern void Init_Inverter(enum Inverters);
extern void Init_Motor(enum Motors);
extern void InitParameterArray(void);
extern void Init_CPU_Timers();

// *******************************
// Elmo Functions
// *******************************
extern void Init_Elmos(void);
extern void ElmoStateMachine(enum MainStateVariables ,  enum CritFaultVariables , float );

// *******************************
// SPI Functions
// *******************************
extern void Init_SPI_Periph(void);
extern void SPI_PingEncoder(void);

extern void Init_QEP(void);
extern void Init_Datalog(void);
extern void Init_I2C();


extern enum CritFaultVariables HiBWProtection();
extern uint32_t HiBW_getErrorCode(void);

extern void Check_ECAP_ISR(void );

extern void Init_GenController(void);
extern int32 SpeedRPM(void);
extern float getGCAmpsCommand(void);
extern bool GCEnergizedStates(enum GenControlStateVariables );

// ****************************************************************************
// External Variables -- linkage to other modules
// ****************************************************************************
extern struct MotorParameters FD_Motor;
extern struct ECUParameters ECU_Params;
extern struct debounced_signal StartGPIO;

// **********************************************************
// ********************* Functions **************************
// **********************************************************

// State Machine function prototypes
//------------------------------------

// A branch states
void A1();  //state A1

// B branch states
void B1(void);  //state B1

// C branch states
void C1(void);  //state C1


// ****************************************************************************
// External Variables -- linkage to other modules
// ****************************************************************************

//Why do we need a function to check a compiler define? We should just put it in the F28x_Project header where all these switches live (VV)
bool isUberTest()
{
    return UBER_TEST;
}


// ****************************************************************************
// Variables for CPU control
// ****************************************************************************

uint32_t	VTimer0[4]={0,0,0,0};		// Virtual Timers slaved off CPU Timer 0 (A events)
uint32_t	VTimer1[4]={0,0,0,0}; 		// Virtual Timers slaved off CPU Timer 1 (B events)
uint32_t    VTimer2[4]={0,0,0,0}; 		// Virtual Timers slaved off CPU Timer 2 (C events)


//*********************** USER Variables *************************************
// Global variables used in this system
//****************************************************************************
uint16_t TimeSinceReset = 0;        //Timer that increments once a second

enum Inverters gInverterSel = HPS400_SP;     //Selected Inverter
enum Motors    gMotorSel    = LP_DHA_120_6PWYE; //Selected motor
enum ECUControlModes gECUCtrlMode   = ECUCtrl_Speed; //Selected ECU Control Mode

// Signals between modules -- protect these variables when reading/writing them
// Signals used to communicate to the interrupt routine (controller module) and the state machine
float ECUControlRefSig=0.0;
float BMSReqSig = 0.0;
float systemRPMSig = 0.0;

enum MainStateVariables MainStateSig = Main_Init;

enum GenControlStateVariables GenControlStateSig = GenControllerDisabled;
float GenTorqueCommandAmpsSig = 0.0;
enum HighMotorControlVar MotorControlStateSig = e_MCInit;

enum BuildLevels BuildLevelSig = e_OpenLoop;
float MotorControlRefSig=0.0;

float BMSFdbSig=0.0;                   //Current reading from the BMS to pass into ECU control loop

bool ElmoStatusSig = false;

bool SCIFaults = false;
float rxMasterValue1_sig=0.0;
float rxMasterValue2_sig=0.0;

uint32_t ErrorCode = 0;
uint32_t gCANErrorCode = 0;
uint32_t gADCWarnings = 0;
float gSystemRPM = 0.0;

enum CritFaultVariables HiBW_CritFault_Sig   =   FLT_CRIT_NONE;        //  Fault signal passed from ISR back to foreground fault detection
enum CritFaultVariables HiBW_CritFault_Int   =   FLT_CRIT_NONE;     //  Fault signal passed from ISR into main control loop
enum CritFaultVariables CritFaults_Int       =   FLT_CRIT_NONE;          // Local fault status variables
enum CritFaultVariables CritFaults       =   FLT_CRIT_NONE;          // Local fault status variables
enum CritFaultVariables LastFault_Int        =   FLT_CRIT_NONE;

uint32_t ErrorCodeSig = 0;
bool HiBW_CritFault_Latch=false;
bool MotorControlModeCmd = true;

// Utility counter variables
uint32_t IsrTicker = 0;

uint32_t ATicker=0;
uint32_t BTicker=0;
uint32_t CTicker=0;

//********************************Initialize Controller*********************************
void InitMainSystem()
{
#ifdef FLASH
// Copy time critical code and Flash setup code to RAM
// The  RamfuncsLoadStart, RamfuncsLoadEnd, and RamfuncsRunStart
// symbols are created by the linker. Refer to the linker files.
    memcpy(&RamfuncsRunStart, &RamfuncsLoadStart, (uint32_t)&RamfuncsLoadSize);
#endif

    //  Initialize System Control:
    // PLL, WatchDog, enable Peripheral Clocks
    // This example function is found in the F28M3Xx_SysCtrl.c file.
    InitSysCtrl();      // Start CPU timer clocks and all peripheral clocks, init Flash banks, disable watchdog (DEBUG), watchdog service and CPU clock PLL stuff too

    // Only used if running from FLASH
    // Note that the variable FLASH is defined by the compiler

#ifdef FLASH
// Call Flash Initialization to setup flash waitstates
// This function must reside in RAM
    InitFlash_Bank0();
    InitFlash_Bank1();  // Call the flash wrapper init function - MRR modifed for 77xS with bank 0,1
#endif //(FLASH)

    // Clear all interrupts and initialize PIE vector table:

    // Disable CPU interrupts
    DINT;

    // Initialize the PIE control registers to their default state.
    // The default state is all PIE interrupts disabled and flags
    // are cleared.
    // This function is found in the F28M3Xx_PieCtrl.c file.
    InitPieCtrl();

    // Disable CPU interrupts and clear all CPU interrupt flags:
    IER = 0x0000;
    IFR = 0x0000;
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    // This will populate the entire table, even if the interrupt
    // is not used in this example.  This is useful for debug purposes.
    // The shell ISR routines are found in F28M3Xx_DefaultIsr.c.
    // This function is found in F28M3Xx_PieVect.c.
    InitPieVectTable();

    Init_CPU_Timers();

    //Calling InitParameterArray() before initializing values in Motor and Inverter structs
    //Because we use the parameter array to validate the parameter values before we allow a start
    InitParameterArray();
    Init_Inverter(gInverterSel);
    Init_Motor(gMotorSel);

//    ADC_UpdateLPFPoles(); Todo - make sure this aligns with the new ADC signal structs (VV)

// Timing sync for background loops
// Timer period definitions found in device specific PeripheralHeaderIncludes.h
    CpuTimer0Regs.PRD.all =  CPU.Sec / Inverter_Params.A_TASK_HZ;     // A tasks
    CpuTimer1Regs.PRD.all =  CPU.Sec / Inverter_Params.B_TASK_HZ;     // B tasks
    CpuTimer2Regs.PRD.all =  CPU.Sec / Inverter_Params.C_TASK_HZ; // C tasks

    Init_PWM();     // Initialize PWM modules for motor phases and interrupt triggers

    Init_ECAP();

    Init_GPIO();    // Initialize all the GPIO direction, function, MUX registers

    Init_LED();     //  Initialize LED outputs and blinking variables

    Init_Analog();      // Initialize analog subsystem and inputs and SOCs, also do zero'ing of A/D inputs (takes some time)

    Init_Datalog();     // Init datalogging structures

    Init_CAN_Periph();

    Init_I2C();

    if(Inverter_Params.Electronics == DriveExternal)
    {
        Init_Elmos();
    }

    if(Inverter_Params.Genset != GensetNone)
    {
        if(Inverter_Params.Genset == GensetPassive && gECUCtrlMode == ECUCtrl_Power)
        {
            gECUCtrlMode = ECUCtrl_Current;
        }
        InitECUParams(gECUCtrlMode);
        Init_ECU(gECUCtrlMode);
        Init_ECU_SCI();
    }

    if(Motor_Params.ObsEn & SNS_ENC_SSI)
    {
        Init_SPI_Periph();
    }


    EINT;
    //Todo- uncomment this once eeprom figured out (VV)
//    LoadFromEEPROM();
    DINT;

    //Todo- uncomment this once serial stuff is fixed (VV)
//    Init_SCI();
//    InitParseRoutine();                       // Init for parse routine.
//    ClearBufferRelatedParam();

// *************************************************

    if(Inverter_Params.Electronics == DriveInternal)
    {
        Init_Controller();                  // Initialize the motor controller
        if(Motor_Params.ObsEn & SNS_ENC_QEP)
        {
            Init_QEP();         // Init modules for QEP (may not be needed, but here for future expansion)
        }
    }

#if 0
// Fault Masks for debugging

#if (DISABLETRIPZONES == 1)
    FaultMask |= Utripflag;
    FaultMask |= Vtripflag;
    FaultMask |= Wtripflag;
    FaultMask |= I_COMP_U;
    FaultMask |= I_COMP_V;
    FaultMask |= I_COMP_W;
#endif

#if (DISABLE_AD_FAULTS == 1)
    FaultMask |= I_ADC_U_HI;
    FaultMask |= I_ADC_V_HI;
    FaultMask |= I_ADC_W_HI;
    FaultMask |= I_BUS_HI;
    FaultMask |= BusOV;
    FaultMask |= BusUV;
#endif

#if (DISABLEFAULTGPIO == 0)
    FaultMask |= (Fault_HU | Fault_LU | Fault_HV | Fault_LV | Fault_HW | Fault_LW | UnotRDY | VnotRDY | WnotRDY );

#endif

#if (DISABLE_VWFAULTGPIO == 1)
    FaultMask |= (Fault_HV | Fault_LV | VnotRDY ) | (Fault_HW | Fault_LW | WnotRDY );
#endif

#if (DISABLE_VFAULTGPIO == 1)
    FaultMask |= (Fault_HV | Fault_LV | VnotRDY );
#endif

#if (DISABLE_RDYGPIO == 1)
    FaultMask |= (UnotRDY | VnotRDY | WnotRDY );
#endif

#if (DISABLE_WFAULTGPIO == 1)
    FaultMask |= (Fault_HW | Fault_LW | WnotRDY );
#endif

#if (DISABLELOOPFAULTS == 1)
    FaultMask |= ( IQ_LOOP_FLT | ID_LOOP_FLT | OVERSPEED_FLT | ANGLE_EST_FLT);
#endif

#if (DISABLECOMMFAULTS == 1)
    //FaultMask |= (SYNC_FLT | BMS_FLT | PARTNER_FLT | CommTimeout);
    FaultMask |= (SYNC_FLT | PARTNER_FLT | CommTimeout);
#endif
#endif


    if(Inverter_Params.Controller==GeneratorController || Inverter_Params.Genset == GensetPassive)
    {
        //Want to initialize this even for a passive genset
        Init_GenController();
    }

// ****************************************************************************
// ****************************************************************************
//ISR Mapping
// ****************************************************************************
// ****************************************************************************
    EALLOW;

    //If ADC B is not used, the interrupt will never trigger. H
    // ADC B EOC of SOC3 is used to trigger Controller interrupt
    AdcbRegs.ADCINTSEL1N2.bit.INT1SEL  = 3;         // EOC3 is trigger for ADC B Int 1
    AdcbRegs.ADCINTSEL1N2.bit.INT1CONT = 1;         // Continuous-- re-pulse even if not cleared
    AdcbRegs.ADCINTSEL1N2.bit.INT1E    = 1;         // Interrupt enabled

    PieVectTable.ADCB1_INT = &MotorControlISR;      // Set interrupt vector for main control ISR

    PieCtrlRegs.PIEIER1.bit.INTx2  = 1;  // Enable ADCB1INT in PIE group 1 - B channel int - currents

//  IER |= M_INT3; // Enable group 3 interrupts
    IER |= M_INT1; // Enable group 1 interrupts -- should be ADC B int1 for completion of conversion in group 1 interrupts
    EINT;          // Enable Global interrupt INTM
    ERTM;          // Enable Global realtime interrupt DBGM
    EDIS;
}

void main(void)
{
   InitMainSystem();

	// set up the watchdog
    EALLOW;
    WdRegs.WDCR.all = 0x0028;
    EDIS;
	for(;;)  //infinite loop
	{
		/****************************************************************************/
		/*  Do timer based stuff here  												*/
		/****************************************************************************/
		// loop rate synchronizer for A-tasks
		if(CpuTimer0Regs.TCR.bit.TIF == 1)	// Is it time to run "A" tasks?
		{
			CpuTimer0Regs.TCR.bit.TIF = 1;	// clear flag

			//-----------------------------------------------------------
			A1();	// Run the A-timer tasks
			//-----------------------------------------------------------
		    inc(&ATicker);
		}

		// loop rate synchronizer for B-tasks
		if(CpuTimer1Regs.TCR.bit.TIF == 1)
		{
			CpuTimer1Regs.TCR.bit.TIF = 1;				// clear flag

			//-----------------------------------------------------------
			B1();		// Run the B-timer tasks
			//-----------------------------------------------------------
			inc(&BTicker);
		}

		// loop rate synchronizer for C-tasks
		if(CpuTimer2Regs.TCR.bit.TIF == 1)
		{
			CpuTimer2Regs.TCR.bit.TIF = 1;				// clear flag

			//-----------------------------------------------------------
			C1();		// Run C timer tasks
			//-----------------------------------------------------------
			inc(&CTicker);
		}
	}
}

/*Function: A1
 * ------------------
 * Runs every 500us
 * Fast main wait loop, debounces GPIO and resets watchdog
 * Fast serial comm
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */
void A1(void)
{
    ServiceDog();       //Watchdog timer reset

    Debounce_GPIO();    //Debounce digital inputs
#if(!DISABLECANCOMMFAULTS)
        gCANErrorCode = CAN_getErrorCode(); //Check for CAN comms loss
#endif

#if(!SYNCMODE_FIX)
    // Start check message when out of Init state and controller is not in solo mode
    if( (Main_State != Init) && (Inverter_Params.SyncMode != SOLO) )
    {
      checkForMessages();
      if(GetState() != Init)
      {
          DINT;
          rxMasterValue1_sig = getMasterValue1_sig();
          rxMasterValue2_sig = getMasterValue2_sig();
          EINT;
      }
    }



    if(isServoFollower(Inverter_Params.SyncMode))
    {
        // CAN angle offset
//        CAN_AngleMsgCheck();
    }
#endif

    DINT;

    EINT;

#ifndef FixMeLater
    // *******************************************************
    // Current limit setting / tuning in Debug environment
    // *******************************************************
    EALLOW;


      CMP_DIG_FILTER(&Cmpss3Regs, SHUNT_curHi, SHUNT_curLo);  // Bus current comparator
// need to figure out how this comparator would trip the PWM -- is the trip zone set up???  DEBUG - MRR
//  and why do we need to run this periodicaly?  shouldn't comparator be hardware-only function?

    EDIS;
#endif
}

/*Function: B1
 * ------------------
 * Runs every 833.33us
 * 1200Hz wait loop:
 * Receives fault signal from interrupt
 * Runs slow, top level state machines for main, generator controller and Todo motor controller
 * Gets control references and sets the corresponding signal to pass to main ISRs
 * 100Hz and 10Hz CAN updates
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */

void B1(void)
//----------------------------------- USER ----------------------------------------
//----------------------------------------  char *endptr;
{
    static uint16_t timer_10Hz = 0, timer_100Hz = 0, timer_200Hz = 0, timer_pwm = 0;        //Define timer prescalar variables
    float GenControlRef = 0.0;
    static enum GenControlStateVariables GenControlModeCmd = GenControllerDisabled;
    static enum MainStateVariables Main_State = Main_Init;

#if(!FAKE_SYSTEM_RPM)
    if(Inverter_Params.GPIO_TACH_NUM != 0)
    {
        DINT;
        LPF_MACRO(gSystemRPM, ECAP2_GetTachRpm(), RPM_LPF_ALPHA)
        EINT;
    }
    else if(Inverter_Params.Electronics == DriveInternal)
    {
        DINT;
        LPF_MACRO(gSystemRPM, SpeedRPM(), RPM_LPF_ALPHA)
        EINT;
    }

    systemRPMSig = gSystemRPM;  //Signal to pass to the interrupt
#endif

#if (!SYNCMODE_FIX)
    // Transmitting messages when master and slave are not Init state and controller is not in solo mode
    if( (Main_State != Init) && (Inverter_Params.SyncMode != SOLO))
    {
        transmitMessage(Main_State, Inverter_Params.SyncMode, Faults);
    }
#endif

        if(timer_200Hz < Inverter_Params.B_TASK_HZ/200L)
        {
            timer_200Hz++;      //Increment 200Hz timer
        }
        else
        {
            DINT;
#if(!FAKE_WARNINGS)
            gADCWarnings = getADCWarnings();        //ADC warning bits - Todo - define a CAN message that spits these out
#endif

            //Stitch together fault bits, etc. for main and "slow" state machines
            CritFaults_Int = HiBW_CritFault_Sig;      //  Get fault signal from high bandwidth detection in interrupt routine
            CritFaults = CritFaults_Int;            //Future - OR this with any slower critical fault detect stuff/mask stuff out if needed
            ErrorCode = ErrorCodeSig;

#if(!DISABLECANCOMMFAULTS)
            //CAN Error code being generated in the A tasks
            //If something HiBW already caused a fault, then CAN error code not so important, we'll ignroe
            //If there are no critical faults but we've lost comm, then CAN Error code will want to go through
            if( (gCANErrorCode != 0) && (CritFaults_Int == FLT_CRIT_NONE) )
            {
                CritFaults |= FLT_CRIT_COMM_CAN;
                ErrorCode = gCANErrorCode;
            }
#endif

#if(!DISABLESERCOMMFAULTS)
            if( (SCIFaults == true) && (CritFaults_Int == FLT_CRIT_NONE) )
            {
                CritFaults |= FLT_CRIT_COMM_SER;
                ErrorCode = 1;  //Dummy error code
            }
#endif

            //If state machine has updated state due to high BW fault, clear the latched highBW fault so we can come out of FaultState
            if(Main_State == Main_Fault && (HiBW_CritFault_Sig !=FLT_CRIT_NONE) )
            {
                HiBW_CritFault_Sig = (enum CritFaultVariables)FLT_CRIT_NONE;
                ErrorCodeSig = 0;
            }


            if( Inverter_Params.Controller == GeneratorController )
            {
                switch(Inverter_Params.StartStopSel)
                {
                    case StSel_CAN:
#if(!ENGINE_TUNING_MAPPING)
                        GenControlModeCmd = (enum GenControlStateVariables)CAN_GetGenCtlrModeCmd();
#else
                        if(CAN_GetGenCtlrModeCmd() == GenControllerDisabled || CAN_GetGenCtrlrModeCmd() == GenControllerBoost || CAN_GetGenCtrlrModeCmd() == GenControllerStart)
                        {
                            GenControllerModeCmd = (enum GenControlStateVariables)CAN_GetGenCtrlrModeCmd();
                        }
                        else
                        {
                            GenControlModeCmd = GenControllerDisabled;
                        }

#endif
                        break;
                    case StSel_GPIO:
                        if(GetStart_Debounced()==1)
                        {
                            GenControlModeCmd = GenControllerCharge;
                        }
                        else
                        {
                            GenControlModeCmd = GenControllerDisabled;
                        }
                        break;
                    case StSel_PWM:
                        if(inRange(ECAP1_GetPulseus(), Inverter_Params.StartPWMHigh_us*1.05, Inverter_Params.StartPWMHigh_us*0.95))
                        {
                            //Confirm we are getting the 'on' signal for at least 1 second before switching modes
                            if(timer_pwm>=200)
                            {
                                timer_pwm=200;
#if(!COMPRESSIONTESTMODE)
                                GenControlModeCmd = GenControllerCharge;
#else
                                GenControlModeCmd = GenControllerBoost; //Put it into boost with control ref 0
#endif
                            }
                            else
                            {
                                timer_pwm++;
                            }
                        }
                        else
                        {
                            //Confirm we are getting the 'off' command for at least 1 second
                            if(timer_pwm<=0)
                            {
                                timer_pwm=0;
                                GenControlModeCmd = GenControllerDisabled;
                            }
                            else
                            {
                                timer_pwm--;
                            }
                        }
                        break;
                }

                switch(Inverter_Params.ControlRefSelect)
                {
                    case CRef_CAN:
#if(!COMPRESSIONTESTMODE)
                        if(GenControlModeCmd == GenControllerCharge)
                        {
                            GenControlRef = (float)CAN_GetBMSReq();    //If the FC has commanded battery charge, take the control reference from the BMS
                            BMSFdbSig = (float)CAN_GetBMSFdb();
                        }
                        else if(GenControlModeCmd == GenControllerAlternator)
                        {
                            GenControlRef = (float)0.0;    //If the FC has commanded battery charge, take the control reference from the BMS
                            BMSFdbSig = (float)CAN_GetBMSFdb();
                        }
                        else if(GenControlModeCmd == GenControllerBoost)
                        {
                            //If the FC has commanded BOOST, take control reference from the FC over CAN, 0-100% boost command
                            GenControlRef = (float)CAN_GetGenControlRef();
                        }
                        else
                        {
                            GenControlRef = 0;
                        }
#else
                        GenControlRef = 0;
#endif
                        break;

                    case CRef_Analog:
                        break;

                    case CRef_Serial:
                        break;
                    default:
                        break;
                }
#if(ECU_SPEED_POWERMAP)
                ECUControlRefSig = (float)CAN_GetGenControlRef();
#else
                if( Inverter_Params.Genset != GensetNone )
                {
                    ECUControlRefSig = 0.0;    //Redundant, but ECU will not take an external reference if it's running with a generator controller. Will be the RPM/Power -> throttle LUT
                    BMSReqSig = (float)CAN_GetBMSReq();
                }
            }
#endif
            else if(Inverter_Params.Controller == MotorController)
            {
                switch(Inverter_Params.StartStopSel)
                {
                    case StSel_CAN:
                        if(CAN_GetGenCtlrModeCmd()==GenControllerBoost)
                        {
                            MotorControlModeCmd = 1;
                        }
                        else
                        {
                            MotorControlModeCmd = 0;
                        }

                        break;
                    case StSel_GPIO:
                        MotorControlModeCmd = GetStart_Debounced();
                        /*
                        if( (Main_State == Main_Fault) && StartGPIO.forced == 1)
                        {
                            StartGPIO.forceval = 0; //Reset START so I don't have to manually do it
                        }*/
                        break;
                    case StSel_PWM:
                        break;
                }

                switch(Inverter_Params.ControlRefSelect)
                {
                    //Todo - attach local motor control ref variable to what's coming in over CAN, as well as grab commandmode
                    case CRef_CAN:
                        MotorControlRefSig = CAN_GetGenControlRef()/Motor_Params.BaseSpeed;
                        break;

                    case CRef_Analog:
                        MotorControlRefSig = adcGetSignalValue(adc_id_speedknob);  //[0.0,1.0]
                        break;

                    case CRef_Serial:
                        break;
                    case CRef_Debugger:
                        //Manually set MotorControlRefSig in debugger;
                        break;
                    default:
                        break;
                }
            }            
            else if(Inverter_Params.Genset == GensetPassive)
            {
                switch(Inverter_Params.ControlRefSelect)
                {
                    case CRef_CAN:
                        GenControlModeCmd = (enum GenControlStateVariables)CAN_GetGenCtlrModeCmd(); //Still want this command to guide the ECU state
                        switch(GetECUCtrlMode())
                        {
                            case ECUCtrl_Manual:
                                //Should this be a different function call?
                                ECUControlRefSig = saturate(0, 1.0, (float)CAN_GetGenControlRef()/100.0);
                                break;

                            case ECUCtrl_Speed:
                                ECUControlRefSig = saturate(0, (float)ECU_Params.LIM_SPD_HI, (float)CAN_GetGenControlRef());
                                break;

                            case ECUCtrl_Voltage:
                                //Todo- Add a "maximum commandable volts" parameter somewhere
                                ECUControlRefSig = saturate(0, (float)14.0*4.2, (float)CAN_GetGenControlRef());
                                break;

                            case ECUCtrl_Current:
                                ECUControlRefSig = (float)CAN_GetBMSReq();
                                BMSFdbSig = (float)CAN_GetBMSFdb();                //BMSFdbSig to send to interrupt. Checking for this message in 100Hz tasks
                                break;
                            default:
                                //Shouldn't be able to run ECUCtrl_Power
                                break;
                        }
                        break;

                    case CRef_Analog:
                        break;

                    case CRef_Serial:
                        break;
                    default:
                        break;
                }
            }

            if(Inverter_Params.Genset!=GensetNone || Inverter_Params.Controller==GeneratorController)
            {
                Main_State = MainStateMachine(CritFaults, ErrorCode, GCEnergizedStates(GenControlModeCmd) );
                //Want to run the generator controller state machine even if we are a passive genset so we don't need to rewrite transitions
                //Set the state signal to send to interrupt
                GenControlStateSig = GenControllerStateMachine(Main_State, GenControlModeCmd, CritFaults, ElmoStatusSig, GenControlRef, CAN_GetBMSFdb(), gSystemRPM);
                GenTorqueCommandAmpsSig = getGCAmpsCommand();
            }
            if(Inverter_Params.Controller == MotorController)
            {
                if(Inverter_Params.Electronics == DriveInternal)
                {
                    //Todo
                    //Run top level motor control state machine (the one with lockrotor/ramp/commutate)
                    //Run main state machine w/motor controller command "energized states" passed in
                    BuildLevelSig = Motor_Params.BuildLevel;
                    Main_State = MainStateMachine(CritFaults, ErrorCodeSig, MotorControlModeCmd );
                    MotorControlStateSig = MotorControlStateMachine(Motor_Params.BuildLevel, Main_State, CritFaults);
                }
                else if(Inverter_Params.Electronics == DriveExternal)
                {
                    /*Todo
                     * Top level elmo state machine that lets us set control ref (maybe speed loop)
                     */
                    //Run main state machine w/motor controller command "energized states" passed in
                    //Main_State = MainStateMachine(CritFaults, ErrorCodeSig, ElmoControlEnergizedStates(ElmoControlModeCmd) );
                }
            }

            MainStateSig = Main_State;

            EINT;

            timer_200Hz=1;      //Reset 100Hz timer
        }

        if(timer_100Hz < Inverter_Params.B_TASK_HZ/100L)
        {
            timer_100Hz++;      //Increment 100Hz timer
        }
        else
        {
            DINT;
            if(Inverter_Params.InverterSel == HONDA_LAUNCHPAD_DEBUG28379 || Inverter_Params.InverterSel == HONDA_LAUNCHPAD_DEBUG28377)
            {
//                setTestParamAmp(ECAP2_GetPulsePU());  //Todo - set up ECAP2 pulse
            }

            AnalogSensorSuite_100Hz();

            CAN_Heartbeat_100Hz();
            CAN_MessageCheck_100Hz();

            EINT;

            timer_100Hz=1;      //Reset 100Hz timer
        }


        if(timer_10Hz < Inverter_Params.B_TASK_HZ/10L)
        {
            timer_10Hz++;   //Increment 10Hz Timer
        }
        else
        {
            DINT;
            AnalogSensorSuite_10Hz();
            Check_ECAP_ISR();  //Check for tach ECAP timeout

            if(Inverter_Params.Genset != GensetNone)
            {
                SCI_ECU_ProcessMessage();
#if(!DISABLESERCOMMFAULTS)
                SCIFaults = Fault_Detect_ECUSCI();
#endif
            }

            CAN_MessageCheck_10Hz();
            CAN_Heartbeat_10Hz();

            EINT;

            timer_10Hz=1;   //Reset 10Hz timer
        }

    DINT;

    CAN_RunBroadcast();

    EINT;

#if(!SYNCMODE_FIX)
    // Rotor Sync
    if(isServoFollower(Inverter_Params.SyncMode) || isSolo(Inverter_Params.SyncMode))
    {
        CAN_MessageCheck_10Hz();
        CAN_TimerRx2=1;
    }
    else
    {
        CAN_TimerRx2++;
    }
#endif
}

/*Function: C1
 * ------------------
 * Runs every 167ms
 * 6Hz wait loop:
 * Slow CAN updates
 * LED routine
 * Genset controller pings ECU for engine temp
 *
 * Outputs:
 * None
 *
 * Inputs:
 * None
 */

void C1(void)
{
    static uint16_t timer_1Hz=0, timer_2Hz=0;   //Define timer prescalar variables

    if(timer_2Hz < Inverter_Params.C_TASK_HZ/2L)
    {
        timer_2Hz++;    //Increment the 2Hz timer
    }
    else
    {
        DINT;

        EINT;

        if(Inverter_Params.Genset != GensetNone)
        {
            ECU_PingCHT();  //Query ECU for information (CHT, future injector pulse)
        }

        timer_2Hz = 1;  //Reset the 2Hz timer
    }

	if(timer_1Hz < Inverter_Params.C_TASK_HZ/1L)
	{
        timer_1Hz++;        //Increment 1Hz timer
	}
	else
	{
        DINT;

        TimeSinceReset++;
        g_TimeSinceReset = TimeSinceReset;

        CAN_Heartbeat_1Hz();

        EINT;

        timer_1Hz=1;        //Reset 1Hz timer
	}

	EINT;

	//Figure out correct LED toggle rate
	if (MainState_Get() >= Main_Inactive)		// Toggle faster if Estop is OK
	{
		if (MainState_Get() == Main_Active)
		{
			SetLED1_BlinkRate(FAST);
		}
		else
		{
			SetLED1_BlinkRate(MEDIUM);
		}
	}
	else
	{
		SetLED1_BlinkRate(SLOW);
	}


    // Run LED control functions
    Run_LED();              // Toggle LED1 at the correct rate

	if( (CritFaults_Int) || (MainState_Get() == Main_Fault) )		// If any faults turn on red light
    {
        LED2_ON();     // Turn on LED2
    }
    else
    {
        LED2_OFF();    // Turn off LED2
    }

	CAN_Heartbeat_6Hz();
}

//Main high-speed ISR
interrupt void MotorControlISR(void)
{
    static uint32_t ECU_Prescale_Timer=0, Elmo_Prescale_Timer=0;

    interruptEntryTime = (uint32_t)CpuTimer0Regs.TIM.all;
    interruptOffTime = interruptExitTime - interruptEntryTime;
    AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;         // Clear interrupt flag
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP1;        // Acknowledge group 1 interrupt

    // Debug -- FixMe -- clear interrupt flag here, and then check at end of routine if flag is set again i.e. we missed an interrupt

    EINT;

    // Verifying the ISR
    inc(&IsrTicker);

    if(Motor_Params.ObsEn & SNS_ENC_SSI)
    {
        static uint16_t tempi;
        //Todo - put this under a 'is there an SSI encoder on' (VV)
        if(tempi < 1){
            tempi++;
        }
        else
        {
            SPI_PingEncoder();
            tempi = 0;
        }

    }

    // Read current/voltage sensors
    AnalogSensorSuite_Int();

    HiBW_CritFault_Int = (enum CritFaultVariables)HiBWProtection();

    if( (HiBW_CritFault_Int != FLT_CRIT_NONE) && (HiBW_CritFault_Latch == false) )
    {
        HiBW_CritFault_Sig = HiBW_CritFault_Int;
        ErrorCodeSig = HiBW_getErrorCode();
        HiBW_CritFault_Latch=true;
    }

    /* Run the Loop */

    if(Inverter_Params.Genset != GensetNone)
    {
        if(ECU_Prescale_Timer < Motor_Params.LOOP_FREQ_KHZ/ECU_Params.LOOP_FREQ_KHZ)
        {
            ECU_Prescale_Timer++;
        }
        else
        {
            ECU_State_Machine(MainStateSig , GenControlStateSig , CritFaults_Int , ECUControlRefSig , BMSFdbSig , GetECUCtrlMode(), systemRPMSig, BMSReqSig);
            ECU_Prescale_Timer=1;
        }
    }

    if(Inverter_Params.Electronics == DriveExternal)
    {
        if(Elmo_Prescale_Timer < Motor_Params.LOOP_FREQ_KHZ*1000.0/Elmo_Params.LOOP_FREQ_HZ)
        {
            Elmo_Prescale_Timer++;
        }
        else
        {
            ElmoStateMachine(MainStateSig, HiBW_CritFault_Int, GenTorqueCommandAmpsSig);
            ElmoStatusSig = getElmoStatus();    //Checks if either elmo is faulted
            Elmo_Prescale_Timer=1;
        }
    }
    else if(Inverter_Params.Electronics == DriveInternal)
    {
        MainControlLoop(BuildLevelSig, MainStateSig, MotorControlStateSig, HiBW_CritFault_Int, MotorControlRefSig);
    }


    //Release the critical fault latch if main state has updated to respond to the fault
    if(MainStateSig == Main_Fault)
    {
        HiBW_CritFault_Latch = false;
    }

    //Check Interrupt timing. If interrupt trigger flag is high before the end of this loop, bad!
    interruptExitTime = (uint32_t)CpuTimer0Regs.TIM.all;
    interruptOnTime = interruptEntryTime - interruptExitTime;
    AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;          // Clear interrupt flag
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP1;        // Acknowledge group 1 interrupt

    if( AdcbRegs.ADCINTFLGCLR.bit.ADCINT1 == 1 )         //Check interrupt flag
    {
        ResetByInfiniteLoop();
    }
}            // MainISR Ends Here

void ResetByInfiniteLoop(void)
{
    for(;;){}
}

float getSystemRPM(void) {return gSystemRPM;}
uint32_t getmainADCWarnings(void) {return gADCWarnings;}
uint16_t getmainTimeSinceReset(void) {return TimeSinceReset;}

// ****************************************************************************
// ****************************************************************************
//TODO  DMC Protection Against Over Current Protection
// ****************************************************************************
// ****************************************************************************
#ifndef DeleteMe
//definitions for selecting DACH reference
#define REFERENCE_VDDA     0

//definitions for COMPH input selection
#define NEGIN_DAC          0
#define NEGIN_PIN          1

//definitions for CTRIPH/CTRIPOUTH output selection
#define CTRIP_ASYNCH       0
#define CTRIP_SYNCH        1
#define CTRIP_FILTER       2
#define CTRIP_LATCH        3

void cmpssConfig(volatile struct CMPSS_REGS *v, int16 Hi, int16 Lo)
{
    //Enable CMPSS
    v->COMPCTL.bit.COMPDACE = 1;
    //NEG signal comes from DAC for the low comparator
    v->COMPCTL.bit.COMPLSOURCE = NEGIN_DAC;
    //NEG signal comes from DAC for the high comparator
    v->COMPCTL.bit.COMPHSOURCE = NEGIN_DAC;
    //Use VDDA as the reference for comparator DACs
    v->COMPDACCTL.bit.SELREF = REFERENCE_VDDA;
    //Set DAC to H~75% and L ~25% values
    v->DACHVALS.bit.DACVAL = Hi;
    v->DACLVALS.bit.DACVAL = Lo;
    // comparator oputput is "not" inverted for high compare event
    v->COMPCTL.bit.COMPHINV = 0;
    // Comparator output is inverted for for low compare event
    v->COMPCTL.bit.COMPLINV = 1;
    // Configure Digital Filter
    //Maximum CLKPRESCALE value provides the most time between samples
    v->CTRIPHFILCLKCTL.bit.CLKPRESCALE = clkPrescale;  //30;   /* Max count of 1023 */
    //Maximum SAMPWIN value provides largest number of samples
    v->CTRIPHFILCTL.bit.SAMPWIN        = sampwin;  //0x1F;
    //Maximum THRESH value requires static value for entire window
    //  THRESH should be GREATER than half of SAMPWIN
    v->CTRIPHFILCTL.bit.THRESH         = thresh;  //0x1F;
    //Reset filter logic & start filtering
    v->CTRIPHFILCTL.bit.FILINIT        = 1;
    // Configure CTRIPOUT path
    //Digital filter output feeds CTRIPH and CTRIPOUTH
    v->COMPCTL.bit.CTRIPHSEL           = CTRIP_FILTER;
    v->COMPCTL.bit.CTRIPOUTHSEL        = CTRIP_FILTER;
    // Make sure the asynchronous path compare high and low event
    // does not go to the OR gate with latched digital filter output
    v->COMPCTL.bit.ASYNCHEN = 0;
    v->COMPCTL.bit.ASYNCLEN = 0;
    //Comparator hysteresis control , set to 2x typical value
    v->COMPHYSCTL.bit.COMPHYS = 2;
    // Dac value is updated on sysclock
    v->COMPDACCTL.bit.SWLOADSEL = 0;
    // ramp is bypassed
    v->COMPDACCTL.bit.DACSOURCE = 0;
    // Clear the latched comparator events
    v->COMPSTSCLR.bit.HLATCHCLR = 1;
    v->COMPSTSCLR.bit.LLATCHCLR = 1;

    return;
}
#endif

#ifndef FixMeLater
void GUI_Checking(void)
{
//PWM modification when in disabled, disabled no start, or estop states
    if ((MainState_Get() == Disabled || MainState_Get() == DisableNoStart || MainState_Get() == EstopState) && (GUI_PWM_en == 1))
    {
        SwitchingFrequencyKHZ = Motor_Params.PWM_FREQ_KHZ;
        T_PWM = CPU.System_freq*1000L/SwitchingFrequencyKHZ;

        Set_PWM_freq(Motor_Params.PWM_FREQ_KHZ);

        // MR -- where are T_PWM, SwitchingFrequencyKHZ, etc used? In Interrupts?  If so, then should DINT around this, or create signals to update them...
        // MR -- probably need to make sure we reset controllers and PWM init() so that all these PWM changes are properly propagated through the system
    }
    GUI_PWM_en = 0; // Always clear the GUI PWM button -- even if we are not in the right state, we want to clear button (and not txfer) so that the button pressed
                    // is not "remembered" and suddenly takes place when the state goes to Estop

    //Speed Loop Prescalar modification when in Disabled, Disabled no start, or Estop State
    if ((StateSig == Disabled || StateSig == DisableNoStart || StateSig == EstopState) && (GUI_LOOP_en == 1))
    {
        SPEED_LOOP_PRESCALERG = Motor_Params.SPEED_LOOP_PRESCALE;
        Motor_Params.T_LOOP = 0.001/Motor_Params.LOOP_FREQ_KHZ;
        Motor_Params.T_SPDLOOP = .001/Motor_Params.LOOP_FREQ_KHZ/SPEED_LOOP_PRESCALERG;
        pid_spd.param.Ki   = _IQ(Motor_Params.T_SPDLOOP*Motor_Params.spd_KI);
        rc1.RampDelayMax = (.0000305*(Motor_Params.LOOP_FREQ_KHZ*1000.0*Motor_Params.RAMP_to_1_Secs)/SPEED_LOOP_PRESCALERG);
        Init_Controller();
    }
    GUI_LOOP_en = 0;

    //Reset factory defaults
    if ((StateSig == Disabled || StateSig == DisableNoStart || StateSig == EstopState) && (GUI_Factory_Reset >= 1))
    {
        GrabCompiler();
        Init_PWM();     // Initialize PWM modules for motor phases
        Init_GPIO();    // Initialize all the GPIO direction, function, MUX registers
        Init_LED();     //  Initialize LED outputs and blinking variables
        Init_Analog();      // Initialize analog subsystem and inputs and SOCs, also do zero'ing of A/D inputs (takes some time)
        Init_Datalog();     // Init datalogging structures
        Init_QEP();
        Init_Controller();    // Init modules for QEP (may not be needed, but here for future expansion)
        InitTempSensor(Inverter_Params.VCCA_REF);
        Init_SCI();
        ClearBufferRelatedParam();
        LoopCount = 0;
        ErrorCount = 0;
        LoopParseCount = 0;
        Motor_Params.T_LOOP = 0.001/Motor_Params.LOOP_FREQ_KHZ;    // Sampling period (sec), see parameter.h
        Motor_Params.T_SPDLOOP = .001/Motor_Params.LOOP_FREQ_KHZ/Motor_Params.SPEED_LOOP_PRESCALE;
        SwitchingFrequencyKHZ = Motor_Params.PWM_FREQ_KHZ;
        T_PWM = CPU.System_freq*1000L/SwitchingFrequencyKHZ;
    }
    GUI_Factory_Reset = 0;

    if ((StateSig == Disabled || StateSig == DisableNoStart || StateSig == EstopState) && (GUI_Impedance >= 1))
    {
        Init_Controller();
    }
    GUI_Impedance = 0;

    if ((StateSig == Disabled || StateSig == DisableNoStart || StateSig == EstopState) && (EPROM_Save >= 1))       //Save settings to EPROM
    {

       //do nothing for now
    }
    EPROM_Save = 0;

    //Load settings from EPROM
    if ((StateSig == Disabled || StateSig == DisableNoStart || StateSig == EstopState) && (EPROM_Load >= 1))
    {
        //do nothing for now
    }
    EPROM_Load = 0;
}
#endif

#ifndef FixMeLater      // need to set up the Estop input as a trip zone
void HVDMC_Protection(void)
{
    EALLOW;

                        // Are there any enable lines for the drivers that need to be set too?
    // Configure GPIO used for Trip Mechanism

    //GPIO input for reading the status of the LEM-overcurrent macro block (active low), GPIO40
    //could trip PWM based on this, if desired
    // Configure as Input
    GpioCtrlRegs.GPBPUD.bit.GPIO40  = 1; // disable pull ups
    GpioCtrlRegs.GPBMUX1.bit.GPIO40 = 0; // choose GPIO for mux option
    GpioCtrlRegs.GPBDIR.bit.GPIO40  = 0; // set as input
    GpioCtrlRegs.GPBINV.bit.GPIO40  = 1;  //invert the input such that '0' is good and '1' is bad after inversion


    InputXbarRegs.INPUT1SELECT = 40; //Select GPIO40 as INPUTXBAR1

    //Clearing the Fault(active low), GPIO41,
    // Configure as Output
    GpioCtrlRegs.GPBPUD.bit.GPIO41  = 1; // disable pull ups
    GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0; // choose GPIO for mux option
    GpioCtrlRegs.GPBDIR.bit.GPIO41  = 1; // set as output
    GpioDataRegs.GPBSET.bit.GPIO41  = 1;

    //Forcing IPM Shutdown (Trip) using GPIO58 (Active high)
    // Configure as Output
    GpioCtrlRegs.GPBPUD.bit.GPIO58   = 1; // disable pull ups
    GpioCtrlRegs.GPBMUX2.bit.GPIO58  = 0; // choose GPIO for mux option
    GpioCtrlRegs.GPBDIR.bit.GPIO58   = 1; // set as output
    GpioDataRegs.GPBCLEAR.bit.GPIO58 = 1;
#endif


#ifndef FixMeLater
    // Shunt Current phase V(ADC A4, COMP2) and W(ADC C2, COMP6), High Low Compare event trips
    cmpssConfig(&Cmpss2Regs, SHUNT_curHi, SHUNT_curLo);  //Enable CMPSS2 - Shunt V
    cmpssConfig(&Cmpss6Regs, SHUNT_curHi, SHUNT_curLo);  //Enable CMPSS6 - Shunt U

    // Configure TRIP 4 to OR the High and Low trips from both comparator 1 & 3
    // Clear everything first
    EPwmXbarRegs.TRIP4MUX0TO15CFG.all  = 0x0000;
    EPwmXbarRegs.TRIP4MUX16TO31CFG.all = 0x0000;
    // Enable Muxes for ored input of CMPSS1H and 1L, i.e. .1 mux for Mux0
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX0  = 1;  //cmpss1
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX4  = 1;  //cmpss3
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX2  = 1;  //cmpss2
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX10 = 1;  //cmpss6

    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX1 = 1;  //inputxbar1

    // Disable all the muxes first
    EPwmXbarRegs.TRIP4MUXENABLE.all = 0x0000;
    // Enable Mux 0  OR Mux 4 to generate TRIP4
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX0  = 1;
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX4  = 1;
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX2  = 1;
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX10 = 1;
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX1  = 1;

    EPwm1Regs.DCTRIPSEL.bit.DCAHCOMPSEL = 3; //Trip 4 is the input to the DCAHCOMPSEL
    EPwm1Regs.TZDCSEL.bit.DCAEVT1       = TZ_DCAH_HI;
    EPwm1Regs.DCACTL.bit.EVT1SRCSEL     = DC_EVT1;
    EPwm1Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;
    EPwm1Regs.TZSEL.bit.DCAEVT1         = 1;

    EPwm2Regs.DCTRIPSEL.bit.DCAHCOMPSEL = 3; //Trip 4 is the input to the DCAHCOMPSEL
    EPwm2Regs.TZDCSEL.bit.DCAEVT1       = TZ_DCAH_HI;
    EPwm2Regs.DCACTL.bit.EVT1SRCSEL     = DC_EVT1;
    EPwm2Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;
    EPwm2Regs.TZSEL.bit.DCAEVT1         = 1;

    EPwm3Regs.DCTRIPSEL.bit.DCAHCOMPSEL = 3; //Trip 4 is the input to the DCAHCOMPSEL
    EPwm3Regs.TZDCSEL.bit.DCAEVT1       = TZ_DCAH_HI;
    EPwm3Regs.DCACTL.bit.EVT1SRCSEL     = DC_EVT1;
    EPwm3Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;
    EPwm3Regs.TZSEL.bit.DCAEVT1         = 1;


    EPwm7Regs.TZSEL.bit.CBC6 = 0x1; // Emulator Stop
    EPwm8Regs.TZSEL.bit.CBC6 = 0x1; // Emulator Stop
    EPwm9Regs.TZSEL.bit.CBC6 = 0x1; // Emulator Stop

    // What do we want the OST/CBC events to do?
    // TZA events can force EPWMxA
    // TZB events can force EPWMxB

    EPwm7Regs.TZCTL.bit.TZA = TZ_FORCE_LO; // EPWMxA will go low
    EPwm7Regs.TZCTL.bit.TZB = TZ_FORCE_LO; // EPWMxB will go low

    EPwm8Regs.TZCTL.bit.TZA = TZ_FORCE_LO; // EPWMxA will go low
    EPwm8Regs.TZCTL.bit.TZB = TZ_FORCE_LO; // EPWMxB will go low

    EPwm9Regs.TZCTL.bit.TZA = TZ_FORCE_LO; // EPWMxA will go low
    EPwm9Regs.TZCTL.bit.TZB = TZ_FORCE_LO; // EPWMxB will go low

    // Clear any spurious OV trip
    EPwm7Regs.TZCLR.bit.DCAEVT1 = 1;
    EPwm8Regs.TZCLR.bit.DCAEVT1 = 1;
    EPwm9Regs.TZCLR.bit.DCAEVT1 = 1;

    EPwm7Regs.TZCLR.bit.OST = 1;
    EPwm8Regs.TZCLR.bit.OST = 1;
    EPwm9Regs.TZCLR.bit.OST = 1;

    EDIS;
#endif

//************************** End of Prot. Conf. ***************************//

/****************************************************************************
 * End of Code *
 * ***************************************************************************/
