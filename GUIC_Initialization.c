//###########################################################################
// FILE:    GUIC_Initialization.c
// TITLE:   Converts compiler Defined Variables to ones in memory.
// AUTHOR:  Greyson Miller
// NOTES:
//###########################################################################

// $Creation Date: Thu July  5  2017 $

//###########################################################################

#include "F2837S_files/F28x_Project.h"
#include "Header/ParameterMap.h"
#include "Header/GUIC_Header.h"
#include "Header/ECU.h"
#include <math.h>
#include "Header/Globals.h"

volatile struct CPU_timings CPU;
volatile struct InverterParameters Inverter_Params;
volatile struct MotorParameters Motor_Params;
// Hello
struct InverterParameters FD_Inverter;
struct MotorParameters FD_Motor;

// ********************************
// Function Prototypes
// ********************************
void Init_CPU_Timers();
void Init_Inverter(enum Inverters);
void Init_Motor(enum Motors);

bool validateMotorParams(void);
bool validateInverterParams(void);

void RestoreMotorDefaults(void);
void RestoreInverterDefaults(void);

void UpdateDependentMotorParams(void);
void ProcessSaveRestoreCommand(unsigned char);
void ProcessSetParameterCommand(unsigned char, float);

bool isTorqueModeWithPositionSensor(enum BuildLevels, enum Observers_Angles );
bool usingPositionSensor(enum Observers_Angles CommAng );

extern struct ECUParameters ECU_Params;

extern bool EEPROM_save_motor(struct MotorParameters *, uint32_t );
extern bool EEPROM_save_inverter(struct InverterParameters *, uint32_t );
extern bool EEPROM_load_motor(struct MotorParameters *, uint32_t );
extern bool EEPROM_load_inverter(struct InverterParameters *, uint32_t );
extern bool EEPROM_save_ECU(struct ECUParameters *, uint32_t);
extern bool EEPROM_load_ECU(struct ECUParameters *, uint32_t);

extern void Init_Controller(void);
extern void Init_CAN_Periph(void);
extern void Init_SPI_Periph(void);
extern void SetupObsGPIO(void);
extern void ADC_UpdateLPFPoles(void);
extern void ResetCMPSSLimits(void);;

extern void ECU_UpdateGains(bool);
extern enum ECUControlModes GetECUCtrlMode(void);
extern void RestoreECUDefaults(void);
extern void Init_ECU(enum ECUControlModes);
extern void InitECUParams(enum ECUControlModes);
extern void ECU_UpdateLPF(void);
extern void GenControlUpdateLPF(void);
extern void ECU_UpdateCoolingMode(void);

extern void GenControl_UpdateGains(void);

bool isCommAngSetToPositionSensored()
{
    bool out = 0;
    if(Motor_Params.CommAng == SNS_ENC_QEP || Motor_Params.CommAng == SNS_ENC_SSI || Motor_Params.CommAng == SNS_HALLRAW || Motor_Params.CommAng == SNS_HALLPLL)
    {
        out = 1;
    }
    return out;
}

void Init_CPU_Timers(void)
{
    //if (Selected_Device==F28_2837xS_sel)
        CPU.System_freq = 200;                              // System clock in MHz
        CPU.nanoSec = CPU.System_freq/1000.0;
        CPU.microSec = CPU.System_freq;
        CPU.milliSec = 1000L*CPU.System_freq;
        CPU.Sec = 1000L*CPU.System_freq*1000L;
}

void Init_Inverter(enum Inverters Inv)
{
    int i=0;
    unsigned char major = 100;
    unsigned char minor = 1;
    unsigned char build = 1;

    Inverter_Params.FirmwareVersion = ( (major << 16) | (minor << 8) | (build << 0) ) ;
    Inverter_Params.UniqueSerial = 0x0;

    Inverter_Params.InverterSel = Inv;

    // *****************************************
    // The following registers are not defined in
    // every inverrter, so set these to NULL and
    // in ADC.c make checks for whether these are]
    // defined, and init based on that.
    // *****************************************

    Inverter_Params.ADC_REG_IU = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_IV = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_IW = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_IBUS = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_IBUS2 = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_VBUS = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_VU = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_VV = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_VW = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_TCPU = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_TMODULE = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_THSINK = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_TPDB = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_TBRD = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_TSTATOR = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_SPEEDKNOB = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_SPARE1 = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_SPARE2 = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_TAMB = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_3V3 = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_5V = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_1V2 = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_12VBAT = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_T_COOLANT = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_T_OIL = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_IFP = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_I12BAT = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_I24V = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_I28V = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_24V = (uint16_t *)NULL;
    Inverter_Params.ADC_REG_28V = (uint16_t *)NULL;



    //Initialize SOCx map elements to be negative
    //Connect SOCxCTL elements only for positive
    //SOCx map elements
    for(i=0;i<ADC_NUM_SOC;i++)
    {
        Inverter_Params.ADC_SOCA_MAP[i]=-1;
        Inverter_Params.ADC_SOCB_MAP[i]=-1;
        Inverter_Params.ADC_SOCC_MAP[i]=-1;
        Inverter_Params.ADC_SOCD_MAP[i]=-1;
    }


    switch(Inverter_Params.InverterSel)
    {
    case HONDA_LAUNCHPAD_DEBUG28379:
           Inverter_Params.Sense_VCCA_REF = 3.3;
//            Inverter_Params.Sense_Current_M = 100.0;
//            Inverter_Params.Sense_Current_M = 469.04/1.65;  //LEM 150-S
            Inverter_Params.Sense_Current_M = 469.04/1.65;
            Inverter_Params.Sense_Current_B = 0;
//            Inverter_Params.Sense_Voltage_M = 70.4;
//            Inverter_Params.Sense_Voltage_M = 72.4;
            Inverter_Params.Sense_Voltage_M = 85.47;
            Inverter_Params.Sense_Voltage_B = 0;

            Inverter_Params.Controller  = GeneratorController;
            Inverter_Params.Genset      = GensetActive;
            Inverter_Params.Electronics = DriveExternal;
            Inverter_Params.SyncMode = SERVO_MASTER;


               Inverter_Params.Sense_Current_M = 500*1.1;    // Amps at full scale on the ADC, corrected to make CAN measurements match Yokogawa/LEM on 2/10/2021
               Inverter_Params.Sense_Voltage_M = 409.0;
               Inverter_Params.Sense_TempP0 = -32.147;
               Inverter_Params.Sense_TempP1 = 211.24;
               Inverter_Params.Sense_TempP2 = 0.0;


                Inverter_Params.Sense_VCCA_REF = 3.3;
                Inverter_Params.A_TASK_HZ = 2000L;
                Inverter_Params.B_TASK_HZ = 1200L;
                Inverter_Params.C_TASK_HZ = 6L;

                Inverter_Params.DeadBand = (CPU.nanoSec)*1000;   // Deadband between high and low switches in nanoseconds

                Inverter_Params.BaseVoltage = 1.0;        // Apparently only used in sliding mode estimator -- voltage that gives VBus ADC measurement = 1
                // Transform ADC input 9.09K/(560K+560K+9.09K) = .00805 => 3.3/.00805 = 409.93V is Vbus ADC = 1
                Inverter_Params.BaseCurrent = 1.0;            //  Max. measurable peak curr. Apparently only used in sliding mode estimator - current in amps that gives current = 1 in ADC input routine

                Inverter_Params.LIM_I_DC_CRIT_H = 100.0;
                Inverter_Params.LIM_I_DC_CRIT_H = -100.0;
                Inverter_Params.LIM_I_PHS = 250.0;
                Inverter_Params.LIM_V_DC_CRIT_H = 900.0;
                Inverter_Params.LIM_T_CPU_CRIT_H = 65;
                Inverter_Params.LIM_T_DEV_CRIT_H = 105.0;

                // Select knob type: speedKnob (0), angleErrorKnob (1), IQDisturbanceKnob (2)
                Inverter_Params.KnobSelect = speedKnob;

 //               Inverter_Params.GPIO_START_NUM = 58;
//                Inverter_Params.GPIO_ESTOP_NUM = 60;

                Inverter_Params.GPIO_START_NUM = 22;
                Inverter_Params.GPIO_ESTOP_NUM = 67;

                Inverter_Params.GPIO_LED1_NUM = 34;             // 28379 LaunchPad LED
                Inverter_Params.GPIO_LED2_NUM= 31;              // 28379 LaunchPad LED


                // ***********************GATE DRIVER SIGNALS***********************
                // Fault/Desat signals from pre-drivers
 /*               Inverter_Params.GPIO_FLTHU_NUM = 7;
                Inverter_Params.GPIO_FLTLU_NUM = 9;
                Inverter_Params.GPIO_FLTHV_NUM = 60;
                Inverter_Params.GPIO_FLTLV_NUM = 19;
                Inverter_Params.GPIO_FLTHW_NUM = 11;
                Inverter_Params.GPIO_FLTLW_NUM = 15;
                */
                Inverter_Params.GPIO_FLTHU_NUM = 6;
                Inverter_Params.GPIO_FLTLU_NUM = 8;
                Inverter_Params.GPIO_FLTHV_NUM = 18;
                Inverter_Params.GPIO_FLTLV_NUM = 32;
                Inverter_Params.GPIO_FLTHW_NUM = 10;
                Inverter_Params.GPIO_FLTLW_NUM = 14;

         //  Ready signals from gate pre-driver ICs (line 378 in IDDK settings)
                /*
                Inverter_Params.GPIO_RDYU_NUM = 6;
                Inverter_Params.GPIO_RDYV_NUM = 18;
                Inverter_Params.GPIO_RDYW_NUM = 10;
                */
                Inverter_Params.GPIO_RDYU_NUM = 7;
                Inverter_Params.GPIO_RDYV_NUM = 60;
                Inverter_Params.GPIO_RDYW_NUM = 11;

         // Reset signal to all gate pre-driver ICs
                Inverter_Params.GPIO_NRST_NUM = 94;

                //  Enable for gate post drivers
                Inverter_Params.GPIO_ENGD_NUM = 99;
                Inverter_Params.GPIO_FLT_OUTPUT_NUM = 98;

                // ************************END GATE DRIVER SIGNALS***********************


                // ************************COMM GPIOS***********************
                Inverter_Params.GPIO_RS422TX_NUM = 65;
                Inverter_Params.GPIO_RS422RX_NUM = 64;
                Inverter_Params.GPIO_RS422TXEN_NUM = 63;


                Inverter_Params.GPIO_I2C_SDA_NUM = 40;
                Inverter_Params.GPIO_I2C_SCL_NUM = 41;

                Inverter_Params.GPIO_USB_DP_NUM = 43;
                Inverter_Params.GPIO_USB_DM_NUM = 42;
                Inverter_Params.GPIO_USB_V_NUM = 59;

                Inverter_Params.GPIO_CAN_RX_NUM = 17;
                Inverter_Params.GPIO_CAN_TX_NUM = 12;

                Inverter_Params.GPIO_SPI_CLK_NUM = 52;
                Inverter_Params.GPIO_SPI_SOMI_NUM = 51;

                Inverter_Params.GPIO_HALLAB_NUM = 58;
                Inverter_Params.GPIO_HALLBC_NUM =59;
                Inverter_Params.GPIO_HALLCA_NUM = 61;

                // *********************** Phase PWM defines ***************************

                Inverter_Params.PWM_U_NUM = 1;
                Inverter_Params.PWM_V_NUM = 2;
                Inverter_Params.PWM_W_NUM = 3;

                //ADC A SETUP

                // ADC Related defines - eventually in inverter section of setup to configure which channels are which signals
                Inverter_Params.ADC_REG_IU = &AdcaResultRegs.ADCRESULT0;
                Inverter_Params.ADC_CHAN_IU = 2;

                Inverter_Params.ADC_REG_IV = &AdcaResultRegs.ADCRESULT1;
                Inverter_Params.ADC_CHAN_IV = 4;

                Inverter_Params.ADC_REG_IW = &AdcaResultRegs.ADCRESULT2;
                Inverter_Params.ADC_CHAN_IW = 14;

                Inverter_Params.ADC_REG_TCPU = &AdcaResultRegs.ADCRESULT3;
                Inverter_Params.ADC_CHAN_TCPU = 13;

#if(HRD_TEST)
                Inverter_Params.ADC_REG_TV2 = &AdcaResultRegs.ADCRESULT4;
                Inverter_Params.ADC_CHAN_TV2 = 3;

                Inverter_Params.ADC_REG_TU1 = &AdcaResultRegs.ADCRESULT5;
                Inverter_Params.ADC_CHAN_TU1 = 15;

                Inverter_Params.ADC_REG_TU2 = &AdcaResultRegs.ADCRESULT6;
                Inverter_Params.ADC_CHAN_TU2 = 0;

                Inverter_Params.ADC_REG_TW1 = &AdcaResultRegs.ADCRESULT7;
                Inverter_Params.ADC_CHAN_TW1 = 5;

                Inverter_Params.ADC_REG_TV1 = &AdcaResultRegs.ADCRESULT8;
                Inverter_Params.ADC_CHAN_TV1 = 1;


                Inverter_Params.ADC_SOCA_MAP[0] = Inverter_Params.ADC_CHAN_IU;
                Inverter_Params.ADC_SOCA_MAP[1] = Inverter_Params.ADC_CHAN_IV;
                Inverter_Params.ADC_SOCA_MAP[2] = Inverter_Params.ADC_CHAN_IW;
                Inverter_Params.ADC_SOCA_MAP[3] = Inverter_Params.ADC_CHAN_TCPU;
                Inverter_Params.ADC_SOCA_MAP[4] = Inverter_Params.ADC_CHAN_TV2;
                Inverter_Params.ADC_SOCA_MAP[5] = Inverter_Params.ADC_CHAN_TU1;
                Inverter_Params.ADC_SOCA_MAP[6] = Inverter_Params.ADC_CHAN_TU2;
                Inverter_Params.ADC_SOCA_MAP[7] = Inverter_Params.ADC_CHAN_TW1;
                Inverter_Params.ADC_SOCA_MAP[8] = Inverter_Params.ADC_CHAN_TV1;
#endif

                //ADC B SETUP

                Inverter_Params.ADC_REG_VU = &AdcbResultRegs.ADCRESULT0;    //Replaced by IFB_BUS on launchpad
                Inverter_Params.ADC_CHAN_VU = 3;

                Inverter_Params.ADC_REG_VV = &AdcbResultRegs.ADCRESULT1;
                Inverter_Params.ADC_CHAN_VV = 4;

                Inverter_Params.ADC_REG_VW = &AdcbResultRegs.ADCRESULT2;
                Inverter_Params.ADC_CHAN_VW = 5;

                Inverter_Params.ADC_REG_SPEEDKNOB = &AdcbResultRegs.ADCRESULT3;
                Inverter_Params.ADC_CHAN_SPEEDKNOB = 0;

                Inverter_Params.ADC_REG_SPARE1 = &AdcbResultRegs.ADCRESULT4;
                Inverter_Params.ADC_CHAN_SPARE1 = 1;

                Inverter_Params.ADC_REG_SPARE2 = &AdcbResultRegs.ADCRESULT5;
                Inverter_Params.ADC_CHAN_SPARE2 = 2;

                Inverter_Params.ADC_SOCB_MAP[0] = Inverter_Params.ADC_CHAN_VU;
                Inverter_Params.ADC_SOCB_MAP[1] = Inverter_Params.ADC_CHAN_VV;
                Inverter_Params.ADC_SOCB_MAP[2] = Inverter_Params.ADC_CHAN_VW;
                Inverter_Params.ADC_SOCB_MAP[3] = Inverter_Params.ADC_CHAN_SPEEDKNOB;
                Inverter_Params.ADC_SOCB_MAP[4] = Inverter_Params.ADC_CHAN_SPARE1;
                Inverter_Params.ADC_SOCB_MAP[5] = Inverter_Params.ADC_CHAN_SPARE2;

#if(HRD_TEST)
                //ADC C SETUP
                Inverter_Params.ADC_CHAN_VBUS = 2;
                Inverter_Params.ADC_REG_VBUS = &AdccResultRegs.ADCRESULT0;

                Inverter_Params.ADC_CHAN_TCOOL = 4;
                Inverter_Params.ADC_REG_TCOOL = &AdccResultRegs.ADCRESULT1;

                Inverter_Params.ADC_CHAN_TW2 = 3;
                Inverter_Params.ADC_REG_TW2 = &AdccResultRegs.ADCRESULT2;

                Inverter_Params.ADC_SOCC_MAP[0] = Inverter_Params.ADC_CHAN_VBUS;
                Inverter_Params.ADC_SOCC_MAP[1] = Inverter_Params.ADC_CHAN_TCOOL;
                Inverter_Params.ADC_SOCC_MAP[2] = Inverter_Params.ADC_CHAN_TW2;
#endif


    //Miscellaneous

                Inverter_Params.LPF_AC_PoleHz = 1.0;
                Inverter_Params.LPF_DC_PoleHz = 1.0;
                Inverter_Params.LPF_Input_PoleHz = 1.0;

                Inverter_Params.ControlRefSelect = CRef_CAN;;
                Inverter_Params.StartStopSel = StSel_GPIO;
                Inverter_Params.FaultOutputMode = FltO_TripLow;
                Inverter_Params.AngleCommandSelect = AngleCANCmnd;

                //Uber test
                Inverter_Params.GPIO_SPEEDPULSE_NUM = 62;
                Inverter_Params.GPIO_ONCEAROUND_NUM = 66;
                Inverter_Params.GPIO_PWMDAC_ANGLEERROR = 2;
                Inverter_Params.CAN_BaseID = 0x0A;
                Inverter_Params.CAN_Bitrate= 1000000L;

                Inverter_Params.LIM_T_CPU_CRIT_H = 65;
                Inverter_Params.LIM_T_DEV_CRIT_H = 90.0;

                Inverter_Params.GPIO_TACH_NUM = 58;
                break;

    case HONDA_LAUNCHPAD_DEBUG28377: // IO set up for Honda single phase test right now

           Inverter_Params.SyncMode = SERVO_MASTER;

           Inverter_Params.Sense_Current_M = 250.0;
           Inverter_Params.Sense_Voltage_M = 409.0;
           Inverter_Params.Sense_TempP0 = -32.147;
           Inverter_Params.Sense_TempP1 = 211.24;
           Inverter_Params.Sense_TempP2 = 0.0;


            Inverter_Params.Sense_VCCA_REF = 3.3;
            Inverter_Params.A_TASK_HZ = 2000L;
            Inverter_Params.B_TASK_HZ = 1200L;
            Inverter_Params.C_TASK_HZ = 6L;

            Inverter_Params.DeadBand = (CPU.nanoSec)*1000;   // Deadband between high and low switches in nanoseconds

            Inverter_Params.BaseVoltage = 1.0;        // Apparently only used in sliding mode estimator -- voltage that gives VBus ADC measurement = 1
            // Transform ADC input 9.09K/(560K+560K+9.09K) = .00805 => 3.3/.00805 = 409.93V is Vbus ADC = 1
            Inverter_Params.BaseCurrent = 1.0;            //  Max. measurable peak curr. Apparently only used in sliding mode estimator - current in amps that gives current = 1 in ADC input routine

            Inverter_Params.LIM_I_DC_CRIT_H = 100.0;
            Inverter_Params.LIM_I_DC_CRIT_H = -100.0;
            Inverter_Params.LIM_I_PHS = 200.0;
            Inverter_Params.LIM_V_DC_CRIT_H = 400.0;
            Inverter_Params.LIM_T_CPU_CRIT_H = 65;
            Inverter_Params.LIM_T_DEV_CRIT_H = 150.0;

            Inverter_Params.LIM_I_DC_CRIT_H = 150;
            Inverter_Params.LIM_I_DC_CRIT_L = -100;
            Inverter_Params.LIM_I_DC_WARN_H = 90;
            Inverter_Params.LIM_I_DC_WARN_L = -50;

            Inverter_Params.LIM_V_DC_CRIT_H = 65;
            Inverter_Params.LIM_V_DC_CRIT_L = 40;
            Inverter_Params.LIM_V_DC_WARN_H = 58;   //Fully charged 14S 4.2V/cell
            Inverter_Params.LIM_V_DC_WARN_L = 45;   //3.2V/cell

            Inverter_Params.LIM_T_CPU_CRIT_H = 70;
            Inverter_Params.LIM_T_CPU_CRIT_L = -10;
            Inverter_Params.LIM_T_CPU_WARN_H = 60;
            Inverter_Params.LIM_T_CPU_WARN_L = -10;

            Inverter_Params.LIM_T_DEV_CRIT_H = 80;
            Inverter_Params.LIM_T_DEV_CRIT_L = -10;
            Inverter_Params.LIM_T_DEV_WARN_H = 50;
            Inverter_Params.LIM_T_DEV_WARN_L = -10;

            Inverter_Params.LIM_T_PDB_CRIT_H = 70;
            Inverter_Params.LIM_T_PDB_CRIT_L = -10;
            Inverter_Params.LIM_T_PDB_WARN_H = 60;
            Inverter_Params.LIM_T_PDB_WARN_L = -10;

            Inverter_Params.LIM_T_HEATSINK_CRIT_H = 80;
            Inverter_Params.LIM_T_HEATSINK_CRIT_L = -10;
            Inverter_Params.LIM_T_HEATSINK_WARN_H = 50;
            Inverter_Params.LIM_T_HEATSINK_WARN_L = -10;

            Inverter_Params.LIM_T_LOGIC_CRIT_H = 60;
            Inverter_Params.LIM_T_LOGIC_CRIT_L = -10;
            Inverter_Params.LIM_T_LOGIC_WARN_H = 50;
            Inverter_Params.LIM_T_LOGIC_WARN_L = -10;

            Inverter_Params.GPIO_START_NUM = 68;
            // Select knob type: speedKnob (0), angleErrorKnob (1), IQDisturbanceKnob (2)
            Inverter_Params.KnobSelect = speedKnob;

            Inverter_Params.Sense_Current_M = 400.0;
            Inverter_Params.Sense_Voltage_M = 800.0;

            Inverter_Params.GPIO_START_NUM = 99;
            Inverter_Params.GPIO_ESTOP_NUM = 60;

            // LEDs on 28377 LaunchPAD are 12 and 13 -- but those conflict with PWM 7A, 7B
            //  So for this debug session the are being moved to 41 and 69

            if(Inverter_Params.Electronics == DriveInternal)
            {
                //  Enable for gate post drivers
                Inverter_Params.GPIO_ENGD_NUM = 75;
            }
            Inverter_Params.GPIO_LED1_NUM = 41;             //12
            Inverter_Params.GPIO_LED2_NUM= 69;              //13


            // ***********************GATE DRIVER SIGNALS***********************
            // Fault/Desat signals from pre-drivers
            Inverter_Params.GPIO_FLTHU_NUM = 10;
            Inverter_Params.GPIO_FLTLU_NUM = 11;
            Inverter_Params.GPIO_FLTHV_NUM = 18;
            Inverter_Params.GPIO_FLTLV_NUM = 19;
            Inverter_Params.GPIO_FLTHW_NUM = 58;
            Inverter_Params.GPIO_FLTLW_NUM = 59;

     //  Ready signals from gate pre-driver ICs (line 378 in IDDK settings)
            Inverter_Params.GPIO_RDYU_NUM = 4;
            Inverter_Params.GPIO_RDYV_NUM = 73;

            Inverter_Params.GPIO_RDYW_NUM =  89;// 71;

     // Reset signal to all gate pre-driver ICs
            Inverter_Params.GPIO_NRST_NUM = 90; // 90;

            //  Enable for gate post drivers
            Inverter_Params.GPIO_ENGD_NUM = 61; // 99
            Inverter_Params.GPIO_FLT_OUTPUT_NUM = 3;


            if(Inverter_Params.Genset != GensetNone)
            {
                Inverter_Params.GPIO_THROTTLE_NUM = 3;
                Inverter_Params.GPIO_EFI_EN_NUM = 88;
                Inverter_Params.GPIO_TACH_NUM = 85;
                Inverter_Params.GPIO_ELMO_START_NUM = 14;
                Inverter_Params.GPIO_EDF_NUM = 12;
            }

            if(Inverter_Params.Electronics==DriveExternal)
            {
                Inverter_Params.GPIO_ELMO_STO_NUM = 16;
                Inverter_Params.GPIO_ELMO_TC_1_NUM = 4; //PWMDAC torque command
                Inverter_Params.GPIO_ELMO_EN_1_NUM = 44; //Enable signal
                Inverter_Params.GPIO_ELMO_FLT_1_NUM = 49; //Fault signal
                Inverter_Params.GPIO_ELMO_TC_2_NUM = 5;
                Inverter_Params.GPIO_ELMO_EN_2_NUM = 45;
                Inverter_Params.GPIO_ELMO_FLT_2_NUM = 48;
            }

            Inverter_Params.GPIO_I2C_SDA_NUM = 91;
            Inverter_Params.GPIO_I2C_SCL_NUM = 92;

            Inverter_Params.GPIO_USB_DP_NUM = 43;
            Inverter_Params.GPIO_USB_DM_NUM = 42;
            Inverter_Params.GPIO_USB_V_NUM = 59;

            Inverter_Params.GPIO_CAN_RX_NUM = 70;
            Inverter_Params.GPIO_CAN_TX_NUM = 71;

            Inverter_Params.GPIO_SPI_CLK_NUM = 65;
            Inverter_Params.GPIO_SPI_SOMI_NUM = 63;  // was 64

            Inverter_Params.GPIO_HALLAB_NUM = 78;
            Inverter_Params.GPIO_HALLBC_NUM = 3;
            Inverter_Params.GPIO_HALLCA_NUM = 61;

            // *********************** Phase PWM defines ***************************

            Inverter_Params.PWM_U_NUM = 7;
            Inverter_Params.PWM_V_NUM = 8;
            Inverter_Params.PWM_W_NUM = 9;

            // ADC Related defines - eventually in inverter section of setup to configure which channels are which signals
            Inverter_Params.ADC_REG_IBUS = &AdcbResultRegs.ADCRESULT0;

            Inverter_Params.ADC_REG_IU = &AdcaResultRegs.ADCRESULT0;
            Inverter_Params.ADC_CHAN_IU = 2;

            Inverter_Params.ADC_REG_IV = &AdcaResultRegs.ADCRESULT1;
            Inverter_Params.ADC_CHAN_IV = 4;

            Inverter_Params.ADC_REG_IW = &AdcaResultRegs.ADCRESULT2;
            Inverter_Params.ADC_CHAN_IW = 14;

            Inverter_Params.ADC_REG_VBUS = &AdcaResultRegs.ADCRESULT3;
            Inverter_Params.ADC_CHAN_VBUS = 5;

            Inverter_Params.ADC_REG_TMODULE = &AdcaResultRegs.ADCRESULT4;
            Inverter_Params.ADC_CHAN_TMODULE = 3;

            Inverter_Params.ADC_REG_TCPU = &AdcaResultRegs.ADCRESULT5;
            Inverter_Params.ADC_CHAN_TCPU = 13;

            //Inverter_Params.ADC_REG_VU = &AdcbResultRegs.ADCRESULT0;    Replaced by IFB_BUS on launchpad
            Inverter_Params.ADC_CHAN_VU = 3;

            Inverter_Params.ADC_REG_VV = &AdcbResultRegs.ADCRESULT1;
            Inverter_Params.ADC_CHAN_VV = 4;

            Inverter_Params.ADC_REG_VW = &AdcbResultRegs.ADCRESULT2;
            Inverter_Params.ADC_CHAN_VW = 5;

            Inverter_Params.ADC_REG_SPEEDKNOB = &AdcbResultRegs.ADCRESULT3;
            Inverter_Params.ADC_CHAN_SPEEDKNOB = 0;

            Inverter_Params.ADC_REG_SPARE1 = &AdcbResultRegs.ADCRESULT4;
            Inverter_Params.ADC_CHAN_SPARE1 = 1;

            Inverter_Params.ADC_REG_SPARE2 = &AdcbResultRegs.ADCRESULT5;
            Inverter_Params.ADC_CHAN_SPARE2 = 2;

            Inverter_Params.ADC_SOCA_MAP[0] = Inverter_Params.ADC_CHAN_IU;
            Inverter_Params.ADC_SOCA_MAP[1] = Inverter_Params.ADC_CHAN_IV;
            Inverter_Params.ADC_SOCA_MAP[2] = Inverter_Params.ADC_CHAN_IW;
            Inverter_Params.ADC_SOCA_MAP[3] = Inverter_Params.ADC_CHAN_VBUS;
            Inverter_Params.ADC_SOCA_MAP[4] = Inverter_Params.ADC_CHAN_THSINK;
            Inverter_Params.ADC_SOCA_MAP[5] = Inverter_Params.ADC_CHAN_TCPU;

            Inverter_Params.ADC_SOCB_MAP[0] = Inverter_Params.ADC_CHAN_VU;
            Inverter_Params.ADC_SOCB_MAP[1] = Inverter_Params.ADC_CHAN_VV;
            Inverter_Params.ADC_SOCB_MAP[2] = Inverter_Params.ADC_CHAN_VW;
            Inverter_Params.ADC_SOCB_MAP[3] = Inverter_Params.ADC_CHAN_SPEEDKNOB;
            Inverter_Params.ADC_SOCB_MAP[4] = Inverter_Params.ADC_CHAN_SPARE1;
            Inverter_Params.ADC_SOCB_MAP[5] = Inverter_Params.ADC_CHAN_SPARE2;
//Miscellaneous

            Inverter_Params.LPF_AC_PoleHz = 1.0;
            Inverter_Params.LPF_DC_PoleHz = 1.0;
            Inverter_Params.LPF_Input_PoleHz = 1.0;

            Inverter_Params.ControlRefSelect = CRef_Analog;;
            Inverter_Params.StartStopSel = StSel_GPIO;
            Inverter_Params.FaultOutputMode = FltO_TripLow;
            Inverter_Params.AngleCommandSelect = AngleCANCmnd;

           //Uber test
            Inverter_Params.GPIO_SPEEDPULSE_NUM = 62;
            Inverter_Params.GPIO_ONCEAROUND_NUM = 63;
            Inverter_Params.GPIO_PWMDAC_ANGLEERROR = 2;
            Inverter_Params.CAN_BaseID = 0x00;
            Inverter_Params.CAN_Bitrate= 100000L;
            break;

        case LAUNCHPAD_MC28377:
            Inverter_Params.Controller  = MotorController;
            Inverter_Params.Genset      = GensetNone;
            Inverter_Params.Electronics = DriveInternal;

            Inverter_Params.SyncMode = SOLO;
            Inverter_Params.Sense_VCCA_REF = 3.278;

            Inverter_Params.Sense_Current_M = 125.0;
            Inverter_Params.Sense_Voltage_M = 610.0;
            Inverter_Params.Sense_TempP0 = 160.38;
            Inverter_Params.Sense_TempP1 = 116.02;
            Inverter_Params.Sense_TempP2 = -764.46;

            Inverter_Params.A_TASK_HZ = 2000L;
            Inverter_Params.B_TASK_HZ = 1200L;
            Inverter_Params.C_TASK_HZ = 6L;

            // Define the DeadBand between high and low side switches
            Inverter_Params.DeadBand = (CPU.nanoSec)*500;   // Deadband between high and low switches in nanoseconds

            Inverter_Params.BaseVoltage = 1;        // Apparently only used in sliding mode estimator -- voltage that gives VBus ADC measurement = 1
            Inverter_Params.BaseCurrent = 1;            //  Max. measurable peak curr. Apparently only used in sliding mode estimator - current in amps that gives current = 1 in ADC input routine

            Inverter_Params.LIM_I_DC_CRIT_H = 50;
            Inverter_Params.LIM_I_DC_CRIT_L = -10;
            Inverter_Params.LIM_I_DC_WARN_H = 30;
            Inverter_Params.LIM_I_DC_WARN_L = -5;

            Inverter_Params.LIM_V_DC_CRIT_H = 35;
            Inverter_Params.LIM_V_DC_CRIT_L = 20;
            Inverter_Params.LIM_V_DC_WARN_H = 58;   //Fully charged 14S 4.2V/cell
            Inverter_Params.LIM_V_DC_WARN_L = 40;   //3.2V/cell

            Inverter_Params.LIM_T_CPU_CRIT_H = 70;
            Inverter_Params.LIM_T_CPU_CRIT_L = -10;
            Inverter_Params.LIM_T_CPU_WARN_H = 60;
            Inverter_Params.LIM_T_CPU_WARN_L = -10;

            Inverter_Params.LIM_T_DEV_CRIT_H = 80;
            Inverter_Params.LIM_T_DEV_CRIT_L = -100;
            Inverter_Params.LIM_T_DEV_WARN_H = 50;
            Inverter_Params.LIM_T_DEV_WARN_L = -10;

            Inverter_Params.GPIO_START_NUM = 68;

            Inverter_Params.GPIO_ESTOP_NUM = 67;

            Inverter_Params.GPIO_LED1_NUM = 73;

            Inverter_Params.GPIO_LED2_NUM= 74;


            //***********************GATE DRIVER SIGNALS***********************
            // Fault/Desat signals from pre-drivers
            Inverter_Params.GPIO_FLTHU_NUM = 10;

            Inverter_Params.GPIO_FLTLU_NUM = 11;

            Inverter_Params.GPIO_FLTHV_NUM = 18;

            Inverter_Params.GPIO_FLTLV_NUM = 19;

            Inverter_Params.GPIO_FLTHW_NUM = 20;

            Inverter_Params.GPIO_FLTLW_NUM = 21;

     //  Ready signals from gate pre-driver ICs (line 378 in IDDK settings)
            Inverter_Params.GPIO_RDYU_NUM = 4;

            Inverter_Params.GPIO_RDYV_NUM = 73;

            Inverter_Params.GPIO_RDYW_NUM = 85;

     // Reset signal to all gate pre-driver ICs
            Inverter_Params.GPIO_NRST_NUM = 90;

            //  Enable for gate post drivers
            Inverter_Params.GPIO_ENGD_NUM = 99;


            //************************END GATE DRIVER SIGNALS***********************


            //************************COMM GPIOS***********************
            Inverter_Params.GPIO_RS422TX_NUM = 86;
            Inverter_Params.GPIO_RS422RX_NUM = 87;
            Inverter_Params.GPIO_RS422TXEN_NUM = 89;

            Inverter_Params.GPIO_I2C_SDA_NUM = 91;
            Inverter_Params.GPIO_I2C_SCL_NUM = 92;

            Inverter_Params.GPIO_USB_DP_NUM = 43;
            Inverter_Params.GPIO_USB_DM_NUM = 42;
            Inverter_Params.GPIO_USB_V_NUM = 59;

            Inverter_Params.GPIO_CAN_RX_NUM = 70;
            Inverter_Params.GPIO_CAN_TX_NUM = 71;

            Inverter_Params.GPIO_FLT_OUTPUT_NUM = 2;

            //*********************** Phase PWM defines ***************************
            Inverter_Params.PWM_U_NUM = 7;
            Inverter_Params.PWM_V_NUM = 8;
            Inverter_Params.PWM_W_NUM = 9;

            // ADC Related defines - eventually in inverter section of setup to configure which channels are which signals
            Inverter_Params.ADC_REG_IU = &AdcaResultRegs.ADCRESULT0;
            Inverter_Params.ADC_CHAN_IU = 2;

            Inverter_Params.ADC_REG_IV = &AdcaResultRegs.ADCRESULT1;
            Inverter_Params.ADC_CHAN_IV = 4;

            Inverter_Params.ADC_REG_IW = &AdcaResultRegs.ADCRESULT2;
            Inverter_Params.ADC_CHAN_IW = 14;

            Inverter_Params.ADC_REG_VBUS = &AdcaResultRegs.ADCRESULT3;
            Inverter_Params.ADC_CHAN_VBUS = 5;

            Inverter_Params.ADC_REG_TMODULE = &AdcaResultRegs.ADCRESULT4;
            Inverter_Params.ADC_CHAN_TMODULE = 3;

            Inverter_Params.ADC_REG_TCPU = &AdcaResultRegs.ADCRESULT5;
            Inverter_Params.ADC_CHAN_TCPU = 13;

            Inverter_Params.ADC_REG_VU = &AdcbResultRegs.ADCRESULT0;
            Inverter_Params.ADC_CHAN_VU = 3;

            Inverter_Params.ADC_REG_VV = &AdcbResultRegs.ADCRESULT1;
            Inverter_Params.ADC_CHAN_VV = 4;

            Inverter_Params.ADC_REG_VW = &AdcbResultRegs.ADCRESULT2;
            Inverter_Params.ADC_CHAN_VW = 5;

            Inverter_Params.ADC_REG_SPEEDKNOB = &AdcbResultRegs.ADCRESULT3;
            Inverter_Params.ADC_CHAN_SPEEDKNOB = 0;

            Inverter_Params.ADC_REG_SPARE1 = &AdcbResultRegs.ADCRESULT4;
            Inverter_Params.ADC_CHAN_SPARE1 = 1;

            Inverter_Params.ADC_REG_SPARE2 = &AdcbResultRegs.ADCRESULT5;
            Inverter_Params.ADC_CHAN_SPARE2 = 2;

            Inverter_Params.ADC_SOCA_MAP[0] = Inverter_Params.ADC_CHAN_IU;
            Inverter_Params.ADC_SOCA_MAP[1] = Inverter_Params.ADC_CHAN_IV;
            Inverter_Params.ADC_SOCA_MAP[2] = Inverter_Params.ADC_CHAN_IW;
            Inverter_Params.ADC_SOCA_MAP[3] = Inverter_Params.ADC_CHAN_VBUS;
            Inverter_Params.ADC_SOCA_MAP[4] = Inverter_Params.ADC_CHAN_TMODULE;
            Inverter_Params.ADC_SOCA_MAP[5] = Inverter_Params.ADC_CHAN_TCPU;

            Inverter_Params.ADC_SOCB_MAP[0] = Inverter_Params.ADC_CHAN_VU;
            Inverter_Params.ADC_SOCB_MAP[1] = Inverter_Params.ADC_CHAN_VV;
            Inverter_Params.ADC_SOCB_MAP[2] = Inverter_Params.ADC_CHAN_VW;
            Inverter_Params.ADC_SOCB_MAP[3] = Inverter_Params.ADC_CHAN_SPEEDKNOB;
            Inverter_Params.ADC_SOCB_MAP[4] = Inverter_Params.ADC_CHAN_SPARE1;
            Inverter_Params.ADC_SOCB_MAP[5] = Inverter_Params.ADC_CHAN_SPARE2;
//Miscellaneous

            Inverter_Params.ControlRefSelect = CRef_Serial;
            Inverter_Params.KnobSelect = speedKnob;
            Inverter_Params.StartStopSel = StSel_GPIO;
            Inverter_Params.FaultOutputMode = FltO_TripLow;

            break;


        case MC15:
            Inverter_Params.Controller  = MotorController;
            Inverter_Params.Genset      = GensetNone;
            Inverter_Params.Electronics = DriveInternal;

            Inverter_Params.SyncMode = SOLO;
            Inverter_Params.Sense_VCCA_REF = 3.278;

            Inverter_Params.Sense_Current_M = 125.0;
            Inverter_Params.Sense_Voltage_M = 610.0;
            Inverter_Params.Sense_TempP0 = 160.38;
            Inverter_Params.Sense_TempP1 = 116.02;
            Inverter_Params.Sense_TempP2 = -764.46;

            Inverter_Params.A_TASK_HZ = 2000L;
            Inverter_Params.B_TASK_HZ = 1200L;
            Inverter_Params.C_TASK_HZ = 6L;

            // Define the DeadBand between high and low side switches
            Inverter_Params.DeadBand = (CPU.nanoSec)*500;   // Deadband between high and low switches in nanoseconds

            Inverter_Params.BaseVoltage = 1;        // Apparently only used in sliding mode estimator -- voltage that gives VBus ADC measurement = 1
            Inverter_Params.BaseCurrent = 1;            //  Max. measurable peak curr. Apparently only used in sliding mode estimator - current in amps that gives current = 1 in ADC input routine

            Inverter_Params.LIM_I_DC_CRIT_H = 50;
            Inverter_Params.LIM_I_DC_CRIT_L = -10;
            Inverter_Params.LIM_I_DC_WARN_H = 30;
            Inverter_Params.LIM_I_DC_WARN_L = -5;

            Inverter_Params.LIM_V_DC_CRIT_H = 35;
            Inverter_Params.LIM_V_DC_CRIT_L = 20;
            Inverter_Params.LIM_V_DC_WARN_H = 58;   //Fully charged 14S 4.2V/cell
            Inverter_Params.LIM_V_DC_WARN_L = 40;   //3.2V/cell

            Inverter_Params.LIM_T_CPU_CRIT_H = 70;
            Inverter_Params.LIM_T_CPU_CRIT_L = -10;
            Inverter_Params.LIM_T_CPU_WARN_H = 60;
            Inverter_Params.LIM_T_CPU_WARN_L = -10;

            Inverter_Params.LIM_T_DEV_CRIT_H = 80;
            Inverter_Params.LIM_T_DEV_CRIT_L = -100;
            Inverter_Params.LIM_T_DEV_WARN_H = 50;
            Inverter_Params.LIM_T_DEV_WARN_L = -10;

            Inverter_Params.GPIO_START_NUM = 58;

            Inverter_Params.GPIO_ESTOP_NUM = 60;

            Inverter_Params.GPIO_LED1_NUM = 41;

            Inverter_Params.GPIO_LED2_NUM= 69;


            //***********************GATE DRIVER SIGNALS***********************
            // Fault/Desat signals from pre-drivers
            Inverter_Params.GPIO_FLTHU_NUM = 10;

            Inverter_Params.GPIO_FLTLU_NUM = 11;

            Inverter_Params.GPIO_FLTHV_NUM = 18;

            Inverter_Params.GPIO_FLTLV_NUM = 19;

            Inverter_Params.GPIO_FLTHW_NUM = 20;

            Inverter_Params.GPIO_FLTLW_NUM = 21;

     //  Ready signals from gate pre-driver ICs (line 378 in IDDK settings)
            Inverter_Params.GPIO_RDYU_NUM = 4;

            Inverter_Params.GPIO_RDYV_NUM = 73;

            Inverter_Params.GPIO_RDYW_NUM = 85;

     // Reset signal to all gate pre-driver ICs
            Inverter_Params.GPIO_NRST_NUM = 90;

            //  Enable for gate post drivers
            Inverter_Params.GPIO_ENGD_NUM = 99;


            //************************END GATE DRIVER SIGNALS***********************


            //************************COMM GPIOS***********************
            Inverter_Params.GPIO_RS422TX_NUM = 86;
            Inverter_Params.GPIO_RS422RX_NUM = 87;
            Inverter_Params.GPIO_RS422TXEN_NUM = 89;

            Inverter_Params.GPIO_I2C_SDA_NUM = 91;
            Inverter_Params.GPIO_I2C_SCL_NUM = 92;

            Inverter_Params.GPIO_USB_DP_NUM = 43;
            Inverter_Params.GPIO_USB_DM_NUM = 42;
            Inverter_Params.GPIO_USB_V_NUM = 59;

            Inverter_Params.GPIO_CAN_RX_NUM = 70;
            Inverter_Params.GPIO_CAN_TX_NUM = 71;

            Inverter_Params.GPIO_FLT_OUTPUT_NUM = 2;

            //*********************** Phase PWM defines ***************************
            Inverter_Params.PWM_U_NUM = 7;
            Inverter_Params.PWM_V_NUM = 8;
            Inverter_Params.PWM_W_NUM = 9;

            // ADC Related defines - eventually in inverter section of setup to configure which channels are which signals
            Inverter_Params.ADC_REG_IU = &AdcaResultRegs.ADCRESULT0;
            Inverter_Params.ADC_CHAN_IU = 2;

            Inverter_Params.ADC_REG_IV = &AdcaResultRegs.ADCRESULT1;
            Inverter_Params.ADC_CHAN_IV = 4;

            Inverter_Params.ADC_REG_IW = &AdcaResultRegs.ADCRESULT2;
            Inverter_Params.ADC_CHAN_IW = 14;

            Inverter_Params.ADC_REG_VBUS = &AdcaResultRegs.ADCRESULT3;
            Inverter_Params.ADC_CHAN_VBUS = 5;

            Inverter_Params.ADC_REG_TMODULE = &AdcaResultRegs.ADCRESULT4;
            Inverter_Params.ADC_CHAN_TMODULE = 3;

            Inverter_Params.ADC_REG_TCPU = &AdcaResultRegs.ADCRESULT5;
            Inverter_Params.ADC_CHAN_TCPU = 13;

            Inverter_Params.ADC_REG_VU = &AdcbResultRegs.ADCRESULT0;
            Inverter_Params.ADC_CHAN_VU = 3;

            Inverter_Params.ADC_REG_VV = &AdcbResultRegs.ADCRESULT1;
            Inverter_Params.ADC_CHAN_VV = 4;

            Inverter_Params.ADC_REG_VW = &AdcbResultRegs.ADCRESULT2;
            Inverter_Params.ADC_CHAN_VW = 5;

            Inverter_Params.ADC_REG_SPEEDKNOB = &AdcbResultRegs.ADCRESULT3;
            Inverter_Params.ADC_CHAN_SPEEDKNOB = 0;

            Inverter_Params.ADC_REG_SPARE1 = &AdcbResultRegs.ADCRESULT4;
            Inverter_Params.ADC_CHAN_SPARE1 = 1;

            Inverter_Params.ADC_REG_SPARE2 = &AdcbResultRegs.ADCRESULT5;
            Inverter_Params.ADC_CHAN_SPARE2 = 2;

            Inverter_Params.ADC_SOCA_MAP[0] = Inverter_Params.ADC_CHAN_IU;
            Inverter_Params.ADC_SOCA_MAP[1] = Inverter_Params.ADC_CHAN_IV;
            Inverter_Params.ADC_SOCA_MAP[2] = Inverter_Params.ADC_CHAN_IW;
            Inverter_Params.ADC_SOCA_MAP[3] = Inverter_Params.ADC_CHAN_VBUS;
            Inverter_Params.ADC_SOCA_MAP[4] = Inverter_Params.ADC_CHAN_TMODULE;
            Inverter_Params.ADC_SOCA_MAP[5] = Inverter_Params.ADC_CHAN_TCPU;

            Inverter_Params.ADC_SOCB_MAP[0] = Inverter_Params.ADC_CHAN_VU;
            Inverter_Params.ADC_SOCB_MAP[1] = Inverter_Params.ADC_CHAN_VV;
            Inverter_Params.ADC_SOCB_MAP[2] = Inverter_Params.ADC_CHAN_VW;
            Inverter_Params.ADC_SOCB_MAP[3] = Inverter_Params.ADC_CHAN_SPEEDKNOB;
            Inverter_Params.ADC_SOCB_MAP[4] = Inverter_Params.ADC_CHAN_SPARE1;
            Inverter_Params.ADC_SOCB_MAP[5] = Inverter_Params.ADC_CHAN_SPARE2;
//Miscellaneous

            Inverter_Params.ControlRefSelect = CRef_Debugger;
            Inverter_Params.StartStopSel = StSel_GPIO;
            Inverter_Params.FaultOutputMode = FltO_Disabled;

            Inverter_Params.CAN_BaseID = 0x0A;
            Inverter_Params.CAN_REG_BASE = 0x00048000;
            Inverter_Params.CAN_Bitrate= 1000000L;
            break;


        case MC40:
            Inverter_Params.Controller  = MotorController;
            Inverter_Params.Genset      = GensetNone;
            Inverter_Params.Electronics = DriveInternal;
            Inverter_Params.SyncMode = SOLO;
            Inverter_Params.Sense_VCCA_REF = 3.285;

            Inverter_Params.Sense_Current_M = 250.0;
            Inverter_Params.Sense_Voltage_M = 409.0;
            Inverter_Params.Sense_TempP0 = -32.147;
            Inverter_Params.Sense_TempP1 = 211.24;
            Inverter_Params.Sense_TempP2 = 0.0;

            Inverter_Params.A_TASK_HZ = 2000L;
            Inverter_Params.B_TASK_HZ = 1200L;
            Inverter_Params.C_TASK_HZ = 6L;

            //******************* Sync Mode ********************
            // Define the DeadBand between high and low side switches
            Inverter_Params.DeadBand = (CPU.nanoSec)*500;   // Deadband between high and low switches in nanoseconds

            Inverter_Params.BaseVoltage = 1.0;        // Apparently only used in sliding mode estimator -- voltage that gives VBus ADC measurement = 1
            Inverter_Params.BaseCurrent = 1.0;            //  Max. measurable peak curr. Apparently only used in sliding mode estimator - current in amps that gives current = 1 in ADC input routine

            Inverter_Params.LIM_I_DC_CRIT_H = 110.0;             // ADC current reading (PU) that will trip an overcurrent fault
            Inverter_Params.LIM_V_DC_CRIT_H = 415.0;
            Inverter_Params.LIM_V_DC_CRIT_L = 340.0;
            Inverter_Params.LIM_T_CPU_CRIT_H = 65;
            Inverter_Params.LIM_T_DEV_CRIT_H = 150.0;

            // Select knob type: speedKnob (0), angleErrorKnob (1), IQDisturbanceKnob (2)
            Inverter_Params.KnobSelect = speedKnob;

            Inverter_Params.GPIO_START_NUM = 58;
            Inverter_Params.GPIO_ESTOP_NUM = 60;

            Inverter_Params.GPIO_LED1_NUM = 41;             //12
            Inverter_Params.GPIO_LED2_NUM= 69;              //13

            //***********************GATE DRIVER SIGNALS***********************
            // Fault/Desat signals from pre-drivers
            Inverter_Params.GPIO_FLTHU_NUM = 10;

            Inverter_Params.GPIO_FLTLU_NUM = 11;

            Inverter_Params.GPIO_FLTHV_NUM = 18;

            Inverter_Params.GPIO_FLTLV_NUM = 19;

            Inverter_Params.GPIO_FLTHW_NUM = 20;

            Inverter_Params.GPIO_FLTLW_NUM = 21;

            //  Ready signals from gate pre-driver ICs (line 378 in IDDK settings)
            Inverter_Params.GPIO_RDYU_NUM = 4;

            Inverter_Params.GPIO_RDYV_NUM = 73;

            Inverter_Params.GPIO_RDYW_NUM = 85;

            // Reset signal to all gate pre-driver ICs
            Inverter_Params.GPIO_NRST_NUM = 90;

            //  Enable for gate post drivers
            Inverter_Params.GPIO_ENGD_NUM = 99;
            Inverter_Params.GPIO_FLT_OUTPUT_NUM = 3;


            //************************COMM GPIOS***********************
            Inverter_Params.GPIO_RS422TX_NUM = 86;
            Inverter_Params.GPIO_RS422RX_NUM = 87;
            Inverter_Params.GPIO_RS422TXEN_NUM = 89;

            Inverter_Params.GPIO_I2C_SDA_NUM = 91;
            Inverter_Params.GPIO_I2C_SCL_NUM = 92;

            Inverter_Params.GPIO_USB_DP_NUM = 43;
            Inverter_Params.GPIO_USB_DM_NUM = 42;
            Inverter_Params.GPIO_USB_V_NUM = 59;

            Inverter_Params.GPIO_CAN_RX_NUM = 70;
            Inverter_Params.GPIO_CAN_TX_NUM = 71;

            Inverter_Params.GPIO_SPI_CLK_NUM = 65;
            Inverter_Params.GPIO_SPI_SOMI_NUM = 64;

            Inverter_Params.GPIO_HALLAB_NUM = 78;
            Inverter_Params.GPIO_HALLBC_NUM = 3;
            Inverter_Params.GPIO_HALLCA_NUM = 61;

            //*********************** Phase PWM defines ***************************
            Inverter_Params.PWM_U_NUM = 7;
            Inverter_Params.PWM_V_NUM = 8;
            Inverter_Params.PWM_W_NUM = 9;

            // ADC Related defines - eventually in inverter section of setup to configure which channels are which signals
            //#define IFB_BUS AdcbResultRegs.ADCRESULT0
            Inverter_Params.ADC_REG_IU = &AdcaResultRegs.ADCRESULT0;
            Inverter_Params.ADC_CHAN_IU = 2;

            Inverter_Params.ADC_REG_IV = &AdcaResultRegs.ADCRESULT1;
            Inverter_Params.ADC_CHAN_IV = 4;

            Inverter_Params.ADC_REG_IW = &AdcaResultRegs.ADCRESULT2;
            Inverter_Params.ADC_CHAN_IW = 14;

            Inverter_Params.ADC_REG_VBUS = &AdcaResultRegs.ADCRESULT3;
            Inverter_Params.ADC_CHAN_VBUS = 5;

            Inverter_Params.ADC_REG_TMODULE = &AdcaResultRegs.ADCRESULT4;
            Inverter_Params.ADC_CHAN_TMODULE = 3;

            Inverter_Params.ADC_REG_TCPU = &AdcaResultRegs.ADCRESULT5;
            Inverter_Params.ADC_CHAN_TCPU = 13;

            Inverter_Params.ADC_REG_VU = &AdcbResultRegs.ADCRESULT4;
            Inverter_Params.ADC_CHAN_VU = 3;

            Inverter_Params.ADC_REG_VV = &AdcbResultRegs.ADCRESULT1;
            Inverter_Params.ADC_CHAN_VV = 4;

            Inverter_Params.ADC_REG_VW = &AdcbResultRegs.ADCRESULT2;
            Inverter_Params.ADC_CHAN_VW = 5;

            Inverter_Params.ADC_REG_SPEEDKNOB = &AdcbResultRegs.ADCRESULT3;
            Inverter_Params.ADC_CHAN_SPEEDKNOB = 0;

            Inverter_Params.ADC_REG_SPARE1 = &AdcbResultRegs.ADCRESULT0;
            Inverter_Params.ADC_CHAN_SPARE1 = 1;

            Inverter_Params.ADC_REG_SPARE2 = &AdcbResultRegs.ADCRESULT5;
            Inverter_Params.ADC_CHAN_SPARE2 = 2;

            Inverter_Params.ADC_SOCA_MAP[0] = Inverter_Params.ADC_CHAN_IU;
            Inverter_Params.ADC_SOCA_MAP[1] = Inverter_Params.ADC_CHAN_IV;
            Inverter_Params.ADC_SOCA_MAP[2] = Inverter_Params.ADC_CHAN_IW;
            Inverter_Params.ADC_SOCA_MAP[3] = Inverter_Params.ADC_CHAN_VBUS;
            Inverter_Params.ADC_SOCA_MAP[4] = Inverter_Params.ADC_CHAN_TMODULE;
            Inverter_Params.ADC_SOCA_MAP[5] = Inverter_Params.ADC_CHAN_TCPU;

            Inverter_Params.ADC_SOCB_MAP[0] = Inverter_Params.ADC_CHAN_VU;
            Inverter_Params.ADC_SOCB_MAP[1] = Inverter_Params.ADC_CHAN_VV;
            Inverter_Params.ADC_SOCB_MAP[2] = Inverter_Params.ADC_CHAN_VW;
            Inverter_Params.ADC_SOCB_MAP[3] = Inverter_Params.ADC_CHAN_SPEEDKNOB;
            Inverter_Params.ADC_SOCB_MAP[4] = Inverter_Params.ADC_CHAN_SPARE1;
            Inverter_Params.ADC_SOCB_MAP[5] = Inverter_Params.ADC_CHAN_SPARE2;

            Inverter_Params.LPF_AC_PoleHz = 1.0;
            Inverter_Params.LPF_DC_PoleHz = 1.0;
            Inverter_Params.LPF_Input_PoleHz = 1.0;

            Inverter_Params.ControlRefSelect = CRef_Debugger;

            Inverter_Params.StartStopSel = StSel_GPIO;
            Inverter_Params.FaultOutputMode = FltO_Disabled;

            Inverter_Params.AngleCommandSelect = AngleCANCmnd;

            //Uber test
            Inverter_Params.GPIO_SPEEDPULSE_NUM = 62;
            Inverter_Params.GPIO_ONCEAROUND_NUM = 63;
            Inverter_Params.GPIO_PWMDAC_ANGLEERROR = 2;
            Inverter_Params.CAN_BaseID = 0x0A;
            Inverter_Params.CAN_REG_BASE = 0x00048000;
            Inverter_Params.CAN_Bitrate= 1000000L;
            break;


        case ELMO_PMU:
            Inverter_Params.SyncMode = GEN_SOLO;

            Inverter_Params.Controller  = GeneratorController;
            Inverter_Params.Genset      = GensetActive;
            Inverter_Params.Electronics = DriveExternal;


            Inverter_Params.Sense_VCCA_REF = 3.298;
            Inverter_Params.Sense_Current_M = 469.04/1.65;
            Inverter_Params.Sense_Voltage_M = 100.41622;
            Inverter_Params.Sense_Voltage_B = 0.23652;

            Inverter_Params.Sense_TempP0 = 103.57887;
            Inverter_Params.Sense_TempP1 = 9.43578;
            Inverter_Params.Sense_TempP2 = -114.41388;

            Inverter_Params.A_TASK_HZ = 1000L;
            Inverter_Params.B_TASK_HZ = 1200L;
            Inverter_Params.C_TASK_HZ = 6L;

            Inverter_Params.DeadBand = (CPU.nanoSec)*500;   // Deadband between high and low switches in nanoseconds

            Inverter_Params.BaseVoltage = 1;        // Apparently only used in sliding mode estimator -- voltage that gives VBus ADC measurement = 1
            Inverter_Params.BaseCurrent = 1;            //  Max. measurable peak curr. Apparently only used in sliding mode estimator - current in amps that gives current = 1 in ADC input routine

            Inverter_Params.LIM_T_CPU_CRIT_H = 75;

            Inverter_Params.LIM_I_DC_CRIT_H = 175.0;          // ADC current reading (PU) that will trip an overcurrent fault
            Inverter_Params.LIM_I_DC_CRIT_L = -175.0;
            Inverter_Params.LIM_I_DC_WARN_H = 90;
            Inverter_Params.LIM_I_DC_WARN_L = -50;

            Inverter_Params.LIM_V_DC_CRIT_H = 75;
            Inverter_Params.LIM_V_DC_CRIT_L = 30;
            Inverter_Params.LIM_V_DC_WARN_H = 58;   //Fully charged 14S 4.2V/cell
            Inverter_Params.LIM_V_DC_WARN_L = 45;   //3.2V/cell

            Inverter_Params.LIM_T_CPU_CRIT_H = 70;
            Inverter_Params.LIM_T_CPU_CRIT_L = -10;
            Inverter_Params.LIM_T_CPU_WARN_H = 60;
            Inverter_Params.LIM_T_CPU_WARN_L = -10;

            Inverter_Params.LIM_T_DEV_CRIT_H = 80;
            Inverter_Params.LIM_T_DEV_CRIT_L = -10;
            Inverter_Params.LIM_T_DEV_WARN_H = 50;
            Inverter_Params.LIM_T_DEV_WARN_L = -10;

            Inverter_Params.LIM_T_PDB_CRIT_H = 75;
            Inverter_Params.LIM_T_PDB_CRIT_L = -10;
            Inverter_Params.LIM_T_PDB_WARN_H = 60;
            Inverter_Params.LIM_T_PDB_WARN_L = -10;

            Inverter_Params.LIM_T_HEATSINK_CRIT_H = 80;
            Inverter_Params.LIM_T_HEATSINK_CRIT_L = -10;
            Inverter_Params.LIM_T_HEATSINK_WARN_H = 50;
            Inverter_Params.LIM_T_HEATSINK_WARN_L = -10;

            Inverter_Params.LIM_T_LOGIC_CRIT_H = 65;
            Inverter_Params.LIM_T_LOGIC_CRIT_L = -10;
            Inverter_Params.LIM_T_LOGIC_WARN_H = 50;
            Inverter_Params.LIM_T_LOGIC_WARN_L = -10;

            Inverter_Params.GPIO_START_NUM = 68;

            Inverter_Params.GPIO_ESTOP_NUM = 67;

            Inverter_Params.GPIO_LED1_NUM = 73;

            Inverter_Params.GPIO_LED2_NUM= 74;

            if(Inverter_Params.Electronics == DriveInternal)
            {
                //  Enable for gate post drivers
                Inverter_Params.GPIO_ENGD_NUM = 75;
            }


            Inverter_Params.GPIO_RS422TX_NUM = 93;
            Inverter_Params.GPIO_RS422RX_NUM = 94;
            Inverter_Params.GPIO_RS422TXEN_NUM = 99;

            Inverter_Params.GPIO_I2C_SDA_NUM = 91;
            Inverter_Params.GPIO_I2C_SCL_NUM = 92;

            Inverter_Params.GPIO_SPI_CLK_NUM = 58;
            Inverter_Params.GPIO_SPI_CS1_NUM = 82;
            Inverter_Params.GPIO_SPI_CS2_NUM = 83;
            Inverter_Params.GPIO_SPI_SOMI_NUM = 61;

            Inverter_Params.GPIO_CAN_RX_NUM = 70;
            Inverter_Params.GPIO_CAN_TX_NUM = 71;

            Inverter_Params.GPIO_HALLAB_NUM = 78;
            Inverter_Params.GPIO_HALLBC_NUM = 79;
            Inverter_Params.GPIO_HALLCA_NUM = 80;

            Inverter_Params.GPIO_FLT_OUTPUT_NUM = 6;

            if(Inverter_Params.Genset != GensetNone)
            {
                Inverter_Params.GPIO_THROTTLE_NUM = 3;
                Inverter_Params.GPIO_EFI_EN_NUM = 88;
                Inverter_Params.GPIO_TACH_NUM = 78;
                Inverter_Params.GPIO_EDF_NUM = 2;
                Inverter_Params.GPIO_RS232RX_NUM = 87;
                Inverter_Params.GPIO_RS232TX_NUM = 86;
            }

            if(Inverter_Params.Electronics == DriveExternal)
            {
                Inverter_Params.GPIO_ELMO_STO_NUM = 75;
                Inverter_Params.GPIO_ELMO_START_NUM = 14;

                Inverter_Params.GPIO_ELMO_TC_1_NUM = 4; //PWMDAC torque command
                Inverter_Params.GPIO_ELMO_EN_1_NUM = 44; //Enable signal
                Inverter_Params.GPIO_ELMO_FLT_1_NUM = 49; //Fault signal

                Inverter_Params.GPIO_ELMO_TC_2_NUM = 5;
                Inverter_Params.GPIO_ELMO_EN_2_NUM = 45;
                Inverter_Params.GPIO_ELMO_FLT_2_NUM = 48;
            }


            //*********************** Phase PWM defines ***************************

            Inverter_Params.PWM_U_NUM = 10;

            Inverter_Params.PWM_V_NUM = 8;

            Inverter_Params.PWM_W_NUM = 9;

            // ADC Related defines - eventually in inverter section of setup to configure which channels are which signals

          /*
           * ADC A
           */
          Inverter_Params.ADC_REG_THSINK = &AdcaResultRegs.ADCRESULT0;    //Heatsink Temperature
          Inverter_Params.ADC_CHAN_THSINK = 15;

          Inverter_Params.ADC_REG_TCPU = &AdcaResultRegs.ADCRESULT1; //CPU Temperature
          Inverter_Params.ADC_CHAN_TCPU = 13;

          Inverter_Params.ADC_REG_TPDB = &AdcaResultRegs.ADCRESULT2; //CPU Temperature
          Inverter_Params.ADC_CHAN_TPDB = 5;

          Inverter_Params.ADC_REG_TBRD = &AdcaResultRegs.ADCRESULT3; //CPU Temperature
          Inverter_Params.ADC_CHAN_TBRD = 3;

          Inverter_Params.ADC_REG_TSTATOR = &AdcaResultRegs.ADCRESULT4; //CPU Temperature
          Inverter_Params.ADC_CHAN_TSTATOR = 0;

          Inverter_Params.ADC_SOCA_MAP[0] = Inverter_Params.ADC_CHAN_THSINK;
          Inverter_Params.ADC_SOCA_MAP[1] = Inverter_Params.ADC_CHAN_TCPU;
          Inverter_Params.ADC_SOCA_MAP[2] = Inverter_Params.ADC_CHAN_TPDB;
          Inverter_Params.ADC_SOCA_MAP[3] = Inverter_Params.ADC_CHAN_TBRD;
          Inverter_Params.ADC_SOCA_MAP[4] = Inverter_Params.ADC_CHAN_TSTATOR;

          /*
           * ADC B
           */
          //Dummy defines to get the interrupt to properly trigger
          Inverter_Params.ADC_SOCB_MAP[0] = 0;
          Inverter_Params.ADC_SOCB_MAP[1] = 0;
          Inverter_Params.ADC_SOCB_MAP[2] = 0;
          Inverter_Params.ADC_SOCB_MAP[3] = 0;

          /*
           * ADC C
           */

          Inverter_Params.ADC_REG_VBUS = &AdccResultRegs.ADCRESULT0;       //Bus Voltage
          Inverter_Params.ADC_CHAN_VBUS = 4;

          Inverter_Params.ADC_SOCC_MAP[0] = Inverter_Params.ADC_CHAN_VBUS;
          /*
           * ADC D
           */

          Inverter_Params.ADC_REG_IBUS = &AdcdResultRegs.ADCRESULT0;       //Elmo 1 Current
          Inverter_Params.ADC_CHAN_IBUS = 1;

          Inverter_Params.ADC_REG_IBUS2 = &AdcdResultRegs.ADCRESULT1;       //Elmo 2 Current
          Inverter_Params.ADC_CHAN_IBUS2 = 0;

          Inverter_Params.ADC_REG_SPARE1 = &AdcdResultRegs.ADCRESULT2;
          Inverter_Params.ADC_CHAN_SPARE1 = 3;

          Inverter_Params.ADC_SOCD_MAP[0] = Inverter_Params.ADC_CHAN_IBUS;
          Inverter_Params.ADC_SOCD_MAP[1] = Inverter_Params.ADC_CHAN_IBUS2;
          Inverter_Params.ADC_SOCD_MAP[2] = Inverter_Params.ADC_CHAN_SPARE1;

            Inverter_Params.ControlRefSelect = CRef_CAN;
            Inverter_Params.StartStopSel = StSel_PWM;
            Inverter_Params.StartPWMHigh_us = 2200;
            Inverter_Params.StartPWMLow_us = 800;
            Inverter_Params.FaultOutputMode = FltO_Disabled;

            Inverter_Params.CAN_BaseID = 0x0A;
            Inverter_Params.CAN_REG_BASE = 0x00048000;  //taken from hw_memmap.h
            Inverter_Params.CAN_Bitrate = 1000000L;
            Inverter_Params.StructVersion = 1; //Change this every time we update the structure
            break;

        case HPS400_SP:
            //Note - since this board is specifically a passive genset board, not making any type checks here, and hard
            //setting all GPIOs (for example, completely got rid of ENGD assignment here) (VV)
            Inverter_Params.SyncMode = GEN_SOLO;

            Inverter_Params.Controller  = GeneratorController;
            Inverter_Params.Genset      = GensetPassive;
            Inverter_Params.Electronics = DriveNone;


            //Todo- calibrate V and I sensors, update values
            Inverter_Params.Sense_Current_M = 381.8776;
            Inverter_Params.Sense_Current_B = 381.8776;
            Inverter_Params.Sense_Voltage_M = 100.41622;
            Inverter_Params.Sense_Voltage_B = 0.23652;

            Inverter_Params.Sense_TempP0 = 103.57887;
            Inverter_Params.Sense_TempP1 = 9.43578;
            Inverter_Params.Sense_TempP2 = -114.41388;

            Inverter_Params.A_TASK_HZ = 2000L;
            Inverter_Params.B_TASK_HZ = 1200L;
            Inverter_Params.C_TASK_HZ = 6L;

            Inverter_Params.DeadBand = (CPU.nanoSec)*500;   // Deadband between high and low switches in nanoseconds

            Inverter_Params.BaseVoltage = 1;        // Apparently only used in sliding mode estimator -- voltage that gives VBus ADC measurement = 1
            Inverter_Params.BaseCurrent = 1;            //  Max. measurable peak curr. Apparently only used in sliding mode estimator - current in amps that gives current = 1 in ADC input routine

            Inverter_Params.LIM_T_CPU_CRIT_H = 75;

            Inverter_Params.LIM_I_DC_CRIT_H = 175.0;          // ADC current reading (PU) that will trip an overcurrent fault
            Inverter_Params.LIM_I_DC_CRIT_L = -175.0;
            Inverter_Params.LIM_I_DC_WARN_H = 90;
            Inverter_Params.LIM_I_DC_WARN_L = -50;

            Inverter_Params.LIM_V_DC_CRIT_H = 75;
            Inverter_Params.LIM_V_DC_CRIT_L = 30;
            Inverter_Params.LIM_V_DC_WARN_H = 58;   //Fully charged 14S 4.2V/cell
            Inverter_Params.LIM_V_DC_WARN_L = 45;   //3.2V/cell

            Inverter_Params.LIM_T_CPU_CRIT_H = 70;
            Inverter_Params.LIM_T_CPU_CRIT_L = -10;
            Inverter_Params.LIM_T_CPU_WARN_H = 60;
            Inverter_Params.LIM_T_CPU_WARN_L = -10;

            Inverter_Params.LIM_T_DEV_CRIT_H = 80;
            Inverter_Params.LIM_T_DEV_CRIT_L = -10;
            Inverter_Params.LIM_T_DEV_WARN_H = 50;
            Inverter_Params.LIM_T_DEV_WARN_L = -10;

            Inverter_Params.LIM_T_PDB_CRIT_H = 75;
            Inverter_Params.LIM_T_PDB_CRIT_L = -10;
            Inverter_Params.LIM_T_PDB_WARN_H = 60;
            Inverter_Params.LIM_T_PDB_WARN_L = -10;

            Inverter_Params.LIM_T_HEATSINK_CRIT_H = 80;
            Inverter_Params.LIM_T_HEATSINK_CRIT_L = -10;
            Inverter_Params.LIM_T_HEATSINK_WARN_H = 50;
            Inverter_Params.LIM_T_HEATSINK_WARN_L = -10;

            Inverter_Params.LIM_T_LOGIC_CRIT_H = 65;
            Inverter_Params.LIM_T_LOGIC_CRIT_L = -10;
            Inverter_Params.LIM_T_LOGIC_WARN_H = 50;
            Inverter_Params.LIM_T_LOGIC_WARN_L = -10;

            Inverter_Params.GPIO_START_NUM = 68;

            Inverter_Params.GPIO_ESTOP_NUM = 67;
            Inverter_Params.GPIO_ESTOP2_NUM = 66;

            Inverter_Params.GPIO_PWM_RADF_NUM = 2; //EPWM2A - mux 1
            Inverter_Params.GPIO_PWM_RECTF_NUM = 5; //EPWM3B - mux 1
            Inverter_Params.GPIO_ECAP_FUEL_L_NUM = 3;  //Use XBAR
            Inverter_Params.GPIO_ECAP_FUEL_R_NUM = 4;  //Use XBAR
            Inverter_Params.GPIO_ECAP_RADT1_NUM = 9; //Use XBAR
            Inverter_Params.GPIO_ECAP_RADT2_NUM = 11; //Use XBAR
            Inverter_Params.GPIO_ECAP_BFRF_NUM = 7; //Use XBAR

            Inverter_Params.GPIO_OILLVL_NUM = 23;
            Inverter_Params.GPIO_24V_EN_NUM = 24;
            Inverter_Params.GPIO_24V_FLT_NUM = 25;
            Inverter_Params.GPIO_28V_EN_NUM = 30;
            Inverter_Params.GPIO_28V_FLT_NUM = 31;

            Inverter_Params.GPIO_TBUS_SDA_NUM = 34; //Mux 6, I2CB
            Inverter_Params.GPIO_TBUS_SCL_NUM = 35; //Mux 6, I2CB

            Inverter_Params.GPIO_SERVO_TXEN_NUM = 27;
            Inverter_Params.GPIO_SERVO_RX_NUM = 28;//Mux 1, SCID
            Inverter_Params.GPIO_SERVO_TX_NUM = 29;

            Inverter_Params.GPIO_IGN1EN_NUM = 87;
            Inverter_Params.GPIO_IGN2EN_NUM = 88;

            Inverter_Params.GPIO_LED1_NUM = 73;

            Inverter_Params.GPIO_LED2_NUM= 74;

            Inverter_Params.GPIO_RS422TX_NUM = 93;
            Inverter_Params.GPIO_RS422RX_NUM = 94;
            Inverter_Params.GPIO_RS422TXEN_NUM = 99;

            Inverter_Params.GPIO_I2C_SDA_NUM = 91;
            Inverter_Params.GPIO_I2C_SCL_NUM = 92;

            Inverter_Params.GPIO_CAN_RX_NUM = 70;
            Inverter_Params.GPIO_CAN_TX_NUM = 71;

            Inverter_Params.GPIO_FLT_OUTPUT_NUM = 22; //Changed this to GPIO22 from army schematic.

            Inverter_Params.GPIO_THROTTLE_NUM = 18; //Changed this to GPIO18 from army schematic. Mux 5
            Inverter_Params.GPIO_TACH_NUM = 75;     //Changed this to GPIO75 from army schematic


            //*********************** Phase PWM defines ***************************

            Inverter_Params.PWM_U_NUM = 8;

            Inverter_Params.PWM_V_NUM = 9;

            Inverter_Params.PWM_W_NUM = 10;

            // ADC Related defines - eventually in inverter section of setup to configure which channels are which signals

          /*
           * ADC A
           */

            Inverter_Params.ADC_REG_TSTATOR = &AdcaResultRegs.ADCRESULT0; //CPU Temperature
            Inverter_Params.ADC_CHAN_TSTATOR = 0;

            Inverter_Params.ADC_REG_T_OIL = &AdcaResultRegs.ADCRESULT1; //Oil Temp
            Inverter_Params.ADC_CHAN_T_OIL = 1;

            Inverter_Params.ADC_REG_TBRD = &AdcaResultRegs.ADCRESULT2; //Board Temperature
            Inverter_Params.ADC_CHAN_TBRD = 2;

            Inverter_Params.ADC_REG_1V2 = &AdcaResultRegs.ADCRESULT3; //1.2V supply sense
            Inverter_Params.ADC_CHAN_1V2 = 3;

            Inverter_Params.ADC_REG_TPDB = &AdcaResultRegs.ADCRESULT4; //PDB Temperature
            Inverter_Params.ADC_CHAN_TPDB = 4;

            Inverter_Params.ADC_REG_TCPU = &AdcaResultRegs.ADCRESULT5; //CPU Temperature
            Inverter_Params.ADC_CHAN_TCPU = 13;

            Inverter_Params.ADC_REG_T_COOLANT = &AdcaResultRegs.ADCRESULT6; //Coolant Temperature
            Inverter_Params.ADC_CHAN_T_COOLANT = 14;

            Inverter_Params.ADC_REG_THSINK = &AdcaResultRegs.ADCRESULT7; //Coolant Temperature
            Inverter_Params.ADC_CHAN_THSINK = 15;

            Inverter_Params.ADC_SOCA_MAP[0] = Inverter_Params.ADC_CHAN_TSTATOR;
            Inverter_Params.ADC_SOCA_MAP[1] = Inverter_Params.ADC_CHAN_T_OIL;
            Inverter_Params.ADC_SOCA_MAP[2] = Inverter_Params.ADC_CHAN_TBRD;
            Inverter_Params.ADC_SOCA_MAP[3] = Inverter_Params.ADC_CHAN_1V2;
            Inverter_Params.ADC_SOCA_MAP[4] = Inverter_Params.ADC_CHAN_TPDB;
            Inverter_Params.ADC_SOCA_MAP[5] = Inverter_Params.ADC_CHAN_TCPU;
            Inverter_Params.ADC_SOCA_MAP[6] = Inverter_Params.ADC_CHAN_T_COOLANT;
            Inverter_Params.ADC_SOCA_MAP[7] = Inverter_Params.ADC_CHAN_THSINK;


          /*
           * ADC B
           */

            Inverter_Params.ADC_REG_3V3 = &AdcbResultRegs.ADCRESULT0;   //3.3V supply sense
            Inverter_Params.ADC_CHAN_3V3 = 1;

            Inverter_Params.ADC_REG_5V = &AdcbResultRegs.ADCRESULT1;   //5V supply sense
            Inverter_Params.ADC_CHAN_5V = 2;

            Inverter_Params.ADC_REG_TAMB = &AdcbResultRegs.ADCRESULT2;   //Ambient temp sensor
            Inverter_Params.ADC_CHAN_TAMB = 3;


            Inverter_Params.ADC_SOCB_MAP[0] = Inverter_Params.ADC_CHAN_3V3;
            Inverter_Params.ADC_SOCB_MAP[1] = Inverter_Params.ADC_CHAN_5V;
            Inverter_Params.ADC_SOCB_MAP[2] = Inverter_Params.ADC_CHAN_TAMB;

          /*
           * ADC C
           */

          Inverter_Params.ADC_REG_IFP = &AdccResultRegs.ADCRESULT0;       //Fuel pump current(?)
          Inverter_Params.ADC_CHAN_IFP = 2;

          Inverter_Params.ADC_REG_12VBAT = &AdccResultRegs.ADCRESULT1;       //12V battery current sense
          Inverter_Params.ADC_CHAN_12VBAT = 3;

          Inverter_Params.ADC_REG_I24V = &AdccResultRegs.ADCRESULT2;       //24V supply current
          Inverter_Params.ADC_CHAN_I24V = 4;

          Inverter_Params.ADC_SOCC_MAP[0] = Inverter_Params.ADC_CHAN_IFP;
          Inverter_Params.ADC_SOCC_MAP[1] = Inverter_Params.ADC_CHAN_12VBAT;
          Inverter_Params.ADC_SOCC_MAP[2] = Inverter_Params.ADC_CHAN_I24V;

          /*
           * ADC D
           */

          Inverter_Params.ADC_REG_VBUS = &AdcdResultRegs.ADCRESULT0;       //HV bus voltage sense
          Inverter_Params.ADC_CHAN_VBUS = 0;

          Inverter_Params.ADC_REG_24V = &AdcdResultRegs.ADCRESULT1;       //24V supply voltage sense
          Inverter_Params.ADC_CHAN_24V = 1;

          Inverter_Params.ADC_REG_IBUS = &AdcdResultRegs.ADCRESULT2;    //HV bus current sense
          Inverter_Params.ADC_CHAN_IBUS = 2;

          Inverter_Params.ADC_REG_12VBAT = &AdcdResultRegs.ADCRESULT3;  //12V battery voltage sense
          Inverter_Params.ADC_CHAN_12VBAT = 3;

          Inverter_Params.ADC_REG_I28V = &AdcdResultRegs.ADCRESULT4;    //28V bus current sense
          Inverter_Params.ADC_CHAN_I28V = 4;

          Inverter_Params.ADC_SOCD_MAP[0] = Inverter_Params.ADC_CHAN_VBUS;
          Inverter_Params.ADC_SOCD_MAP[1] = Inverter_Params.ADC_CHAN_24V;
          Inverter_Params.ADC_SOCD_MAP[2] = Inverter_Params.ADC_CHAN_IBUS;
          Inverter_Params.ADC_SOCD_MAP[1] = Inverter_Params.ADC_CHAN_12VBAT;
          Inverter_Params.ADC_SOCD_MAP[2] = Inverter_Params.ADC_CHAN_I28V;

          Inverter_Params.ControlRefSelect = CRef_CAN;
          Inverter_Params.StartStopSel = StSel_PWM;
          Inverter_Params.StartPWMHigh_us = 2200;
          Inverter_Params.StartPWMLow_us = 800;
          Inverter_Params.FaultOutputMode = FltO_Disabled;

          Inverter_Params.CAN_BaseID = 0x0A;
          Inverter_Params.CAN_REG_BASE = 0x00048000;  //taken from hw_memmap.h
          Inverter_Params.CAN_Bitrate = 1000000L;
          Inverter_Params.StructVersion = 1; //Change this every time we update the structure
        break;
        default:
            //Undefined or bad inverter select - reset!
            asm ("      ESTOP0");
    }   //End of inverter_sel switch

    if(!validateInverterParams())
    {
        asm ("      ESTOP0");
    }


    FD_Inverter = Inverter_Params;
}

void Init_Motor(enum Motors Mot)
{
    Motor_Params.MotorSel = Mot;

    switch(Motor_Params.MotorSel)
    {
/*************************************************************************************************/
/*************************************** JOBY_JM140 **********************************************/
/*************************************************************************************************/
        case JOBY_JM140:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 35;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 35;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.MAINISR_PRESCALE = 1;

            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SL_ORTEGA;
            Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_SPEED | MC_PI_CURRENT);

            Motor_Params.FOC_DQ_Decouple = false;

            Motor_Params.StatorResistance = 0.065;
            Motor_Params.StatorInductance = 0.000098;

            Motor_Params.Poles = 20;                      // Number of poles - based on Scott McAfee email not sure about HV motor though

            // Define the base quantities
            Motor_Params.BaseFrequency = 1500;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1 (1200 ~ 6500 rpm @ 11 pole pairs
            Motor_Params.BaseSpeed = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2.0));

            // Define rotation - speed limits
            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.01;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency

            Motor_Params.LIM_SPD_DRV = 10;      // Acceleration limit (derivative of speed estimate -- base speed/per second units
                                                        // so a 1.0 here would mean acceleration to achieve base speed in one second

            // Define tuning parameters for speed loop  -- speed loop not tuned for Joby motor yet
            Motor_Params.spd_KP = 0.75 * Inverter_Params.Sense_Current_M;
            Motor_Params.spd_KI = 1.0 * Inverter_Params.Sense_Current_M;

            Motor_Params.pos_KP = 0.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KI = 0.0 * Inverter_Params.Sense_Current_M;

            Motor_Params.BusV_Nominal = 325.0;
            Motor_Params.ILOOP_BW = 2*PI*2000.0;
            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.4 default
            Motor_Params.LIM_ID_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);    // -0.4 default

            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.8 default
            Motor_Params.LIM_IQ_LO = -1.0;    // -0.8 default

            Motor_Params.DQDC_FFGain = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;

            Motor_Params.Start_VLockRotor = 0.5;     // Current during locked rotor segment of startup

            Motor_Params.Start_IQOL = 5.0;            //  Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 10.0;            // Current setting for open angle loop startup ramp - D axis

            Motor_Params.Start_TransitionRpm = 0.08;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.RAMP_to_1_Secs = 240.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 240.0;

            Motor_Params.Start_Hold_Time = 1000L*2;             // 1 second hold time
            Motor_Params.Start_SpeedTol = .0003;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = .05;
            Motor_Params.Start_EnableControllerRef = 1;            // Maximum speed setting where drive can be enabled

            Motor_Params.Kv = 41.54/1000.0; //Kv in volts/rpm - Measured
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain
            break;
/*************************************************************************************************/
/*************************************** LP DHA 4T3PY ********************************************/
/*************************************************************************************************/
        case LP_DHA_4T3PY:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 100;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 50;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop

            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.FOC_DQ_Decouple = false;

            // Integrators
            Motor_Params.id_integrator = 14.1;
            Motor_Params.iq_integrator = 6.16;
            Motor_Params.spd_integrator = 0.0000000148;

            Motor_Params.Poles = 32;                      // Number of poles - based on Scott McAfee email not sure about HV motor though

            // Define the electrical motor parameters
            Motor_Params.StatorResistance = (0.29/2.0);            // Stator resistance (ohm) - measured phase-phase resistance/2
    //        Motor_Params.StatorInductance = (0.000118/2.0);           // Using large load inductors
            Motor_Params.StatorInductance = (0.00001812/2.0);             //Using smaller load inductors
            //vv-questionable why it was working at 50/2...


            Motor_Params.Kv = 34.4/1000.0; //Kv in volts/rpm
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);

            // Define the base quantities
            Motor_Params.BaseFrequency = 2100;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1

            // Define rotation - speed limits

            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.1;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency

            Motor_Params.LIM_SPD_DRV = 10;       // Acceleration limit (derivative of speed estimate -- base speed/per second units
                                                        // so a 1.0 here would mean acceleration to achieve base speed in one second
            Motor_Params.LIM_POS_HI = 1.0;
            Motor_Params.LIM_POS_LO = -0.1;

            Motor_Params.spd_KP = 20.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.spd_KI = 4.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KP = 0.002 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KI = 0.004 * Inverter_Params.Sense_Current_M;

            Motor_Params.BusV_Nominal = 325.0;
            Motor_Params.ILOOP_BW = 2*PI*1000.0;

            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage
            Motor_Params.LIM_ID_LO = -Motor_Params.BusV_Nominal/sqrt(3.0);    // max phase voltage

            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage
            Motor_Params.LIM_IQ_LO = -10.0;

            Motor_Params.Start_VLockRotor = 3.0;     // Current during locked rotor segment of startup
            Motor_Params.Start_IQOL = 3;            //  Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 6;            // Current setting for open angle loop startup ramp - D axis
            Motor_Params.Start_TransitionRpm = 0.1;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.RAMP_to_1_Secs = 240.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 240.0;

            // Define tuning parameters for angle estimator
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain

            // Define timing and startup sequencing stuff -- where do these values come from?
            Motor_Params.Start_Hold_Time = 1000L*3;             // 1 second hold time
            //#define SPEED_TOL -1                  // Tolerance so state never transitions to commutation speed control (comment for level 3c or higher)
            Motor_Params.Start_SpeedTol = .05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = .07;
            Motor_Params.Start_EnableControllerRef = .6;            // Maximum speed setting where drive can be enabled
            break;
/*************************************************************************************************/
/********************************** LP DHA 4T3PY CAMIEM ******************************************/
/*************************************************************************************************/
        case LP_DHA_4T3PY_CAMIEM:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 100;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 50;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.MAINISR_PRESCALE = 2;      //PWM cycles per interrupt cycle

            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SL_ORTEGA;
            Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_SPEED | MC_PI_CURRENT);

            Motor_Params.FOC_DQ_Decouple = false;

            // Integrators
            Motor_Params.id_integrator = 14.1;
            Motor_Params.iq_integrator = 6.16;
            Motor_Params.spd_integrator = 0.0000000148;

            Motor_Params.Poles = 32;                      // Number of poles - based on Scott McAfee email not sure about HV motor though

            Motor_Params.StatorResistance = (0.29/2.0);            // Stator resistance (ohm) - measured phase-phase resistance/2
            Motor_Params.StatorInductance = (0.000100/2.0);             //Using smaller load inductors

//            Motor_Params.Kv = 0.70*34.4/1000.0; //Kv in volts/rpm     //Ethan
            Motor_Params.Kv = 24.51/1000.0; //Kv in volts/rpm           //Derek
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);

            Motor_Params.BaseFrequency = 2100;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1
            Motor_Params.BaseSpeed = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2.0));

            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.1;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency

            Motor_Params.LIM_SPD_DRV = 10;       // Acceleration limit (derivative of speed estimate -- base speed/per second units
                                                        // so a 1.0 here would mean acceleration to achieve base speed in one second
            Motor_Params.LIM_POS_HI = 1.0;
            Motor_Params.LIM_POS_LO = -0.1;

            Motor_Params.spd_KP = 4.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.spd_KI = 8.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KP = 0.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KI = 0.0 * Inverter_Params.Sense_Current_M;

            Motor_Params.BusV_Nominal = 270.0;
            Motor_Params.ILOOP_BW = 2*PI*1000.0;

            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage
            Motor_Params.LIM_ID_LO = -Motor_Params.BusV_Nominal/sqrt(3.0);    // max phase voltage

            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage
            Motor_Params.LIM_IQ_LO = -2.5;

            Motor_Params.Start_VLockRotor = 3.0;     // Current during locked rotor segment of startup
            Motor_Params.Start_IQOL = 2.0;            //  Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 4.0;            // Current setting for open angle loop startup ramp - D axis
            Motor_Params.Start_TransitionRpm = 0.1;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.RAMP_to_1_Secs = 240.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 240.0;

            // Define tuning parameters for angle estimator
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain

            // Define timing and startup sequencing stuff -- where do these values come from?
            Motor_Params.Start_Hold_Time = 1000L*3;             // 1 second hold time
            //#define SPEED_TOL -1                  // Tolerance so state never transitions to commutation speed control (comment for level 3c or higher)
            Motor_Params.Start_SpeedTol = .05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = 1.0;
            Motor_Params.Start_EnableControllerRef = .6;            // Maximum speed setting where drive can be enabled
            break;
/*************************************************************************************************/
/************************************* LP DHA 1T6PY **********************************************/
/*************************************************************************************************/
        case LP_DHA_1T6PY:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 50;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 50;      // Control Loop Execution Frequency, kHz
            Motor_Params.MAINISR_PRESCALE = 1;
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.slave_phase_shift = -30.0/360.0;   // negative means slave is lagging and positive means slave is ahead.
            Motor_Params.Poles = 32;                      // Number of poles

            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SL_ORTEGA;

            Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_CURRENT|MC_PI_SPEED);

            Motor_Params.FOC_DQ_Decouple = false;



            // Define the electrical motor parameters
//            Motor_Params.StatorResistance = (0.0825/2.0);            // Stator resistance (ohm) - measured phase-phase resistance/2
            Motor_Params.StatorResistance = (0.090/2.0);            // measured phase-phase with air core inductors
//            Motor_Params.StatorInductance = (0.0000445/2.0);             //big magnetic core inductors
//            Motor_Params.StatorInductance = (0.00001085/2.0);            //Air core inductors
            Motor_Params.StatorInductance = (0.0000250/2.0);            //Air core inductors

//            Motor_Params.Kv = 8.18/1000.0; //Kv in terminal to terminal volts per rpm for 1 turn winding
            Motor_Params.Kv = 7.8/1000.0;   //Terminal to terminal peak voltage per kRPM, spaced out plates
            Motor_Params.Kt = (3/sqrt(2))*Motor_Params.Kv/(2.0*PI/60.0); //Torque constant derived from Ke
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);

            // Define the base quantities
            Motor_Params.BaseFrequency = 1600;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1 -> 6000 rpm @ 32 poles
            Motor_Params.BaseSpeed = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2.0));

            // Define rotation - speed limits

            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = 0.005;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency

            Motor_Params.LIM_SPD_DRV = 10;       // Acceleration limit (derivative of speed estimate -- base speed/per second units
                                                        // so a 1.0 here would mean acceleration to achieve base speed in one second

            // Define tuning parameters for speed loop  -- speed loop not tuned for Joby motor yet
            Motor_Params.spd_KP = 0.50 * Inverter_Params.Sense_Current_M;
            Motor_Params.spd_KI = 2.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KP = 0.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KI = 0.0 * Inverter_Params.Sense_Current_M;
            // I gain really should have some connection to the loop_frequency and speed_loop_prescalar as well

            Motor_Params.BusV_Nominal = 50.0;
            Motor_Params.ILOOP_BW = (2.0*PI*1500.0);
            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.4 default
            Motor_Params.LIM_ID_LO = -Motor_Params.BusV_Nominal/sqrt(3.0);    // -0.4 default

            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.8 default
            Motor_Params.LIM_IQ_LO = -2.0; //-25.0/sqrt(3.0);    // -0.8 default

            Motor_Params.Start_VLockRotor = 1.0;             //Rotor lock voltage
            Motor_Params.Start_IQOL = 4.0;            // Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 8.0;            // Current setting for open angle loop startup ramp - D axis
            Motor_Params.Start_TransitionRpm = 0.05;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.RAMP_to_1_Secs = 240.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
                  //Baseline project was 5 for 10 kHz loop and 200 Hz base freq
            Motor_Params.RAMP_down_Secs = 240.0;                                                                    // changing this changes how fast the speed ramp accelerates
                                                                                    //Ramp slew is .0000305 per count (speed loop * RampDelayMax) counts
                                                                                    // So slew rate is .0000305*LOOP_FREQUENCY*1000/SPEED_LOOP_PRESCALER/RAMP_RATE_DELAY

            // Define tuning parameters for angle estimator
            //= 4*rpm0*(2*pi/60)/KV^2;
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain

            // Define timing and startup sequencing stuff -- where do these values come from?
            Motor_Params.Start_Hold_Time = 1000L*3;             // 1 second hold time
            Motor_Params.Start_SpeedTol = .05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = .070;
            Motor_Params.Start_EnableControllerRef = 1.0;            // Maximum speed setting where drive can be enabled


            Motor_Params.LIM_T_STATOR_WARN_H = 195.0;
            Motor_Params.LIM_T_STATOR_WARN_L = -10.0;
            Motor_Params.LIM_T_STATOR_CRIT_H = 205.0;
            Motor_Params.LIM_T_STATOR_CRIT_L = -10.0;

            break;
/*************************************************************************************************/
/********************************** LP DHA 1T6PY CRYO ********************************************/
/*************************************************************************************************/
        case LP_DHA_1T6PY_CRYO:

            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            Motor_Params.BuildLevel = e_SpeedLoop;
            Motor_Params.PWM_FREQ_KHZ = 100;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 50;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.slave_phase_shift = -0.1667;   // negative means slave is lagging and positive means slave is ahead.
            Motor_Params.Poles = 32;                      // Number of poles

            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.FOC_DQ_Decouple = false;

            // Integrators
            Motor_Params.id_integrator = 3.08;
            Motor_Params.iq_integrator = 4.04;
            Motor_Params.spd_integrator = 0.001763951;

            // Define the electrical motor parameters
            Motor_Params.StatorResistance = (0.19/2.0);            // Stator resistance (ohm) - measured phase-phase resistance/2
            Motor_Params.StatorInductance = (0.000007);             //Using smaller load inductors


            Motor_Params.Kv = 16.4/1000.0; //Kv in terminal to terminal volts per rpm for 1 turn winding
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);

            // Define the base quantities
            Motor_Params.BaseFrequency = 2000;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1 -> 6000 rpm @ 32 poles

            // Define rotation - speed limits

            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.1;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency

            Motor_Params.LIM_SPD_DRV = 10;       // Acceleration limit (derivative of speed estimate -- base speed/per second units
                                                        // so a 1.0 here would mean acceleration to achieve base speed in one second

            // Define tuning parameters for speed loop  -- speed loop not tuned for Joby motor yet
            Motor_Params.spd_KP = 0.50 * Inverter_Params.Sense_Current_M;
            Motor_Params.spd_KI = 2.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KP = 0.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KI = 0.0 * Inverter_Params.Sense_Current_M;

            // I gain really should have some connection to the loop_frequency and speed_loop_prescalar as well

            // Define tuning parameters for current loops (I gains multiplied by T_LOOP when assigned) -- where do these values come from?
            //  Gains selected from simulink simulation, attempting to place PI zero to match
    //        Motor_Params.BusV_Nominal = 400.0;                       // Voltage for which the current loop parameters were tuned
    //        Motor_Params.NOMINAL_BUSV_PU = Motor_Params.BusV_Nominal/Inverter_Params.BaseVoltage;

            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance/5;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.LIM_ID_HI = 120.0/sqrt(3.0);     // 0.4 default
            Motor_Params.LIM_ID_LO = -105.0/sqrt(3.0);    // -0.4 default

            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance/5;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.LIM_IQ_HI = 120.0/sqrt(3.0);     // 0.8 default
            Motor_Params.LIM_IQ_LO = -4; //-25.0/sqrt(3.0);    // -0.8 default

            //  Startup sequence stuff -- where do these values come from? -- mostly from trial and error during startup tuning
            //  was IQ = .65, ID = .25, CL speed = .02, ramp dely = 20
            // HV motor has about a 4X voltage, so we will divide all currents by 4X for now
            Motor_Params.Start_VLockRotor = 1.0;     // voltage during locked rotor segment of startup

            // At 6 Phase, slave is able to commutate with these values
            Motor_Params.Start_IQOL = 15.0;            //  Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 22.0;            // Current setting for open angle loop startup ramp - D axis

            Motor_Params.Start_TransitionRpm = 0.07;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.RAMP_to_1_Secs = 500.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
                  //Baseline project was 5 for 10 kHz loop and 200 Hz base freq
            Motor_Params.RAMP_down_Secs = 500.0;                                                                    // changing this changes how fast the speed ramp accelerates
                                                                                    //Ramp slew is .0000305 per count (speed loop * RampDelayMax) counts
                                                                                    // So slew rate is .0000305*LOOP_FREQUENCY*1000/SPEED_LOOP_PRESCALER/RAMP_RATE_DELAY


            // Define tuning parameters for angle estimator
            //= 4*rpm0*(2*pi/60)/KV^2;
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain
    //        Motor_Params.Gamma = -(4.0*400.0*2.0*PI/(60.0*powf(Motor_Params.Psi,2.0)))*16;
    //        Motor_Params.Gamma = -82480.55387;


            // Define timing and startup sequencing stuff -- where do these values come from?
            Motor_Params.Start_Hold_Time = 1000L*3;             // 1 second hold time
            //#define SPEED_TOL -1                  // Tolerance so state never transitions to commutation speed control (comment for level 3c or higher)
            Motor_Params.Start_SpeedTol = .05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = .080;
            Motor_Params.Start_EnableControllerRef = .6;            // Maximum speed setting where drive can be enabled
            break;
/*************************************************************************************************/
/********************************** EMRAX 208 ****************************************************/
/*************************************************************************************************/
        case EMRAX_208:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            // !!! MUST READ WHEN RUNNING WITH TORQUE MODE !!!
            // Positive speed/torque command is when the motor is spinning clockwise, when you are facing towards the motor's shaft.
            // Negative speed/torque command is when the motor is spinning counter clockwise, when you are facing towards the motor's shaft.
            // Please refer Confluence page and documentation.
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 35;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 35;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.MAINISR_PRESCALE = 1;

            Motor_Params.QepMode = positionAndDirection;
            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SL_ORTEGA;
            // If a position sensored is being used, it needs to be calibrated.
            if(isCommAngSetToPositionSensored() == 1)
            {
                Motor_Params.PositionSensorOffset = -0.81;
            }
            else
            {
                Motor_Params.PositionSensorOffset = 0.0;
            }

            Motor_Params.FOC_DQ_Decouple = false;
            Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_SPEED|MC_PI_CURRENT);

            Motor_Params.ObserverCommAndMechAngleSwitch = 0;
            // Integrators
            Motor_Params.id_integrator = 2.60;
            Motor_Params.iq_integrator = 8.54;
            Motor_Params.spd_integrator = 0.000000040919;

            Motor_Params.Poles = 20;

            //Parameters for Ortega Observer
            // Define the electrical motor parameters
            Motor_Params.StatorResistance = (0.0508/2.0);            // Stator resistance (ohm) - measured phase-phase resistance/2
            Motor_Params.StatorInductance = (0.000234/2.0);
            Motor_Params.Kv = 66.667/1000.0; //Kv in volts/rpm
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain

            // Define the base quantities
            Motor_Params.BaseFrequency = 1200;            // 7200RPM is SpeedRef=1
            Motor_Params.BaseSpeed     = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2.0));

            Motor_Params.spd_KP = 3.0;
            Motor_Params.spd_KI = 0.15;
//            Motor_Params.pos_KP = 2.5;
//            Motor_Params.pos_KI = 0.0001;
            Motor_Params.pos_KP = 2.5;
            Motor_Params.pos_KI = .00001;
            Motor_Params.pos_KD = 8000.0;
            Motor_Params.pos_C1 = .001;
            Motor_Params.pos_C2 = .999;

            Motor_Params.BusV_Nominal = 175.0;
            Motor_Params.ILOOP_BW = (2*PI*1000.0);
            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

            Motor_Params.Start_Hold_Time = 1000L*3;             // 1 second hold time
            Motor_Params.Start_VLockRotor = 1.75;     // Current during locked rotor segment of startup
            Motor_Params.Start_IQOL = 8.0;            //  Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 16.0;            // Current setting for open angle loop startup ramp - D axis
            Motor_Params.Start_TransitionRpm = 700.0;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.Start_SpeedTol = 0.05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = 1.0;

            // For UBER in torque mode (level 5), we want the enableControllerRef to set higher
            if( Motor_Params.BuildLevel == e_TorqueMode )
            {
                Motor_Params.Start_EnableControllerRef = 30.0;            // Maximum controller ref setting where drive can be enabled
            }
            else
            {
                Motor_Params.Start_EnableControllerRef = 1.0;            // Maximum controller ref setting where drive can be enabled
            }

            Motor_Params.RAMP_to_1_Secs = 400.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 400.0;


            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.1;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency
            Motor_Params.LIM_SPD_DRV = 10;                       // Acceleration limit (derivative of speed estimate -- base speed/per second units
            Motor_Params.LIM_POS_HI = 1.0;
            Motor_Params.LIM_POS_LO = -0.1;
            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage

            // For UBER in torque mode (level 5), we want the IQ_LO set to -1.0*Motor_Params.BusV_Nominal/sqrt(3.0)
            if( Motor_Params.BuildLevel != e_TorqueMode )
            {
                Motor_Params.LIM_IQ_LO = -5.0;
            }
            else
            {
                Motor_Params.LIM_IQ_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);
            }

            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage
            Motor_Params.LIM_ID_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);    // max phase voltage

            Motor_Params.LIM_I_DC_CRIT_H = 100.0;
            Motor_Params.LIM_I_DC_CRIT_L = -100.0;
            Motor_Params.LIM_I_PHS    = 75.0;
            Motor_Params.LIM_V_DC_CRIT_H = Motor_Params.BusV_Nominal+30.0;
            Motor_Params.LIM_V_DC_CRIT_L = Motor_Params.BusV_Nominal-15.0;

            break;
/*************************************************************************************************/
/********************************** KDE 7215XF ***************************************************/
/*************************************************************************************************/
        case KDE_7215XF:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 20;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 20;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.MAINISR_PRESCALE = 1;

            Motor_Params.QepMode = positionAndDirection;
            Motor_Params.ObsEn = SL_ORTEGA | SNS_ENC_QEP;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SNS_ENC_QEP;
            Motor_Params.FOC_DQ_Decouple = false;
            Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_SPEED | MC_PI_CURRENT |MC_PID_POS);

            Motor_Params.StatorResistance = 0.057;
            Motor_Params.StatorInductance = 0.000045/2.0;           // Stator inductance (H) (phase-phase / 2) - Taken from 7T motor in Scott McAfee email, x(8/7)^2

            Motor_Params.Poles = 22;                      // Number of poles - based on Scott McAfee email not sure about HV motor though

            // Define the base quantities
            Motor_Params.BaseFrequency = 1500;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1 (1200 ~ 6500 rpm @ 11 pole pairs
            Motor_Params.BaseSpeed = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2));

            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.01;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency

            Motor_Params.LIM_SPD_DRV = 10;      // Acceleration limit (derivative of speed estimate -- base speed/per second units
                                                        // so a 1.0 here would mean acceleration to achieve base speed in one second
            Motor_Params.LIM_POS_HI = 1.0;
            Motor_Params.LIM_POS_LO = -0.1;

            // Define tuning parameters for speed loop  -- speed loop not tuned for Joby motor yet
//            Motor_Params.spd_KP = 5.0 * Inverter_Params.Sense_Current_M;
//            Motor_Params.spd_KI = 5.0 * Inverter_Params.Sense_Current_M;
//            Motor_Params.pos_KP = 3.5 * Inverter_Params.Sense_Current_M;
//            Motor_Params.pos_KI = 0.00006 * Inverter_Params.Sense_Current_M;
            Motor_Params.spd_KP = 5.0;
            Motor_Params.spd_KI = 2.0;
            Motor_Params.pos_KP = 0.80;
            Motor_Params.pos_KI = 0.00006;

            Motor_Params.BusV_Nominal = 37.0;
            Motor_Params.ILOOP_BW = 2*PI*1000.0;
            if(Motor_Params.CommAng == SL_ORTEGA)
            {
                Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
                Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

                Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
                Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            }
            else if(Motor_Params.CommAng == SL_SMOPOS)
            {
                Motor_Params.id_KP = 0.25;
                Motor_Params.id_KI = 2000.0;

                Motor_Params.iq_KP = 0.25;
                Motor_Params.iq_KI = 2000.0;
            }

            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.4 default
            Motor_Params.LIM_ID_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);    // -0.4 default*
            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.8 default
            Motor_Params.LIM_IQ_LO = -5.0;    // -0.8 default

            Motor_Params.DQDC_FFGain = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.Start_VLockRotor = 0.5;     // Current during locked rotor segment of startup

            if(Motor_Params.CommAng ==SL_ORTEGA)
            {
                Motor_Params.Start_IQOL = 5.0;            //  Current setting for open angle loop startup ramp - Q axis
                Motor_Params.Start_IDOL = 10.0;            // Current setting for open angle loop startup ramp - D axis
            }
            else if(Motor_Params.CommAng == SL_SMOPOS)
            {
                Motor_Params.Start_IQOL = 2.5;            //  Current setting for open angle loop startup ramp - Q axis
                Motor_Params.Start_IDOL = 5.0;            // Current setting for open angle loop startup ramp - D axis
            }

            Motor_Params.Start_TransitionRpm = 0.08;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.RAMP_to_1_Secs = 240.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 240.0;

            Motor_Params.Start_Hold_Time = 1000L*2;             // 1 second hold time
            Motor_Params.Start_SpeedTol = .001;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = 1.0;
            Motor_Params.Start_EnableControllerRef = 1;            // Maximum speed setting where drive can be enabled

            Motor_Params.Kv = (1.0/135.0); //Kv in volts/rpm
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain
            break;
/*************************************************************************************************/
/******************************** LP DHA 2T3PY ***************************************************/
/*************************************************************************************************/
        case LP_DHA_2T3PY:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 100;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 50;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop

            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SNS_HALLPLL;
            Motor_Params.FOC_DQ_Decouple = true;

            Motor_Params.Poles = 32;                      // Number of poles - based on Scott McAfee email not sure about HV motor though

            // Define the electrical motor parameters
            Motor_Params.StatorResistance = (0.143/2.0);            // Stator resistance (ohm) - measured phase-phase resistance/2
    //        Motor_Params.StatorInductance = (0.000118/2.0);           // Using large load inductors
            Motor_Params.StatorInductance = (0.000088/2.0);             //Using smaller load inductors


            Motor_Params.Kv = 15.95/1000.0; //Kv in volts/rpm
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);

            Motor_Params.BaseSpeed = 13125;
            // Define the base quantities
            Motor_Params.BaseFrequency = Motor_Params.BaseSpeed*(Motor_Params.Poles/2)/60;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1

            // Define rotation - speed limits

            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.1;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency

            Motor_Params.LIM_SPD_DRV = 10;       // Acceleration limit (derivative of speed estimate -- base speed/per second units
                                                        // so a 1.0 here would mean acceleration to achieve base speed in one second

            // Define tuning parameters for speed loop  -- speed loop not tuned for Joby motor yet
            Motor_Params.spd_KP = 0.5 * Inverter_Params.Sense_Current_M;
            Motor_Params.spd_KI = 4.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KP = 0.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KI = 0.0 * Inverter_Params.Sense_Current_M;
                                        // Since HV motor has Ke ~4x, but inertia is same, then speed loop (speed to current/torque
                                        // should be 4X lower since higher Ke is added gain into loop
                                        // I gain really should have some connection to the loop_frequency and speed_loop_prescalar as well

            // Define tuning parameters for current loops (I gains multiplied by T_LOOP when assigned) -- where do these values come from?
            //  Gains selected from simulink simulation, attempting to place PI zero to match
    //        Motor_Params.BusV_Nominal = 400.0;                       // Voltage for which the current loop parameters were tuned
    //        Motor_Params.NOMINAL_BUSV_PU = Motor_Params.BusV_Nominal/Inverter_Params.BaseVoltage;

            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance / Motor_Params.id_KP;
            Motor_Params.LIM_ID_HI = 325.0/sqrt(3.0);     // 0.4 default
            Motor_Params.LIM_ID_LO = -325.0/sqrt(3.0);    // -0.4 default

            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance / Motor_Params.iq_KP;
            Motor_Params.LIM_IQ_HI = 325.0/sqrt(3.0);     // 0.8 default
            Motor_Params.LIM_IQ_LO = 0.0;    // -0.8 default

            Motor_Params.DQDC_FFGain = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;

            //  Startup sequence stuff -- where do these values come from? -- mostly from trial and error during startup tuning
            //  was IQ = .65, ID = .25, CL speed = .02, ramp dely = 20
            // HV motor has about a 4X voltage, so we will divide all currents by 4X for now
            Motor_Params.Start_VLockRotor = 2.0;     // Current during locked rotor segment of startup
            Motor_Params.Start_IQOL = 3.0;            //  Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 6.0;            // Current setting for open angle loop startup ramp - D axis
            Motor_Params.Start_TransitionRpm = 0.05;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.RAMP_to_1_Secs = 360.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 360.0;
                  //Baseline project was 5 for 10 kHz loop and 200 Hz base freq

                                                                                    // changing this changes how fast the speed ramp accelerates
                                                                                    //Ramp slew is .0000305 per count (speed loop * RampDelayMax) counts
                                                                                    // So slew rate is .0000305*LOOP_FREQUENCY*1000/SPEED_LOOP_PRESCALER/RAMP_RATE_DELAY

            // Define tuning parameters for angle estimator
            //= 4*rpm0*(2*pi/60)/KV^2;
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain


            // Define timing and startup sequencing stuff -- where do these values come from?
            Motor_Params.Start_Hold_Time = 1000L*3;             // 1 second hold time
            //#define SPEED_TOL -1                  // Tolerance so state never transitions to commutation speed control (comment for level 3c or higher)
            Motor_Params.Start_SpeedTol = .05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = .025;
            Motor_Params.Start_EnableControllerRef = .6;            // Maximum speed setting where drive can be enabled
            break;
/*************************************************************************************************/
/****************************** NEU MOTOR 8057 ***************************************************/
/*************************************************************************************************/
        case NEU_MOTOR_8057:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 35;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 35;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.MAINISR_PRESCALE = 1;

            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SL_ORTEGA;
            Motor_Params.FOC_DQ_Decouple = false;
            Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_SPEED | MC_PI_CURRENT);

            Motor_Params.StatorResistance = 0.385/2.0;
            Motor_Params.StatorInductance = 0.000684/2.0;

            Motor_Params.Poles = 16;

            // Define the base quantities
            Motor_Params.BaseFrequency = 866;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1 (1200 ~ 6500 rpm @ 11 pole pairs
            Motor_Params.BaseSpeed = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2));

            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.01;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency

            Motor_Params.LIM_SPD_DRV = 10;      // Acceleration limit (derivative of speed estimate -- base speed/per second units
                                                        // so a 1.0 here would mean acceleration to achieve base speed in one second

            // Define tuning parameters for speed loop  -- speed loop not tuned for Joby motor yet
            Motor_Params.spd_KP = 0.5 * Inverter_Params.Sense_Current_M;
            Motor_Params.spd_KI = 2.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KP = 0.0 * Inverter_Params.Sense_Current_M;
            Motor_Params.pos_KI = 0.0 * Inverter_Params.Sense_Current_M;

            Motor_Params.BusV_Nominal = 400.0;
            Motor_Params.ILOOP_BW = 2*PI*1000.0;

            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.4 default
            Motor_Params.LIM_ID_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);    // -0.4 default*
            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.8 default
            Motor_Params.LIM_IQ_LO = -10.0;    // -0.8 default

            Motor_Params.DQDC_FFGain = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.Start_VLockRotor = 0.5;     // Current during locked rotor segment of startup

            Motor_Params.Start_IDOL = 4.0;
            Motor_Params.Start_IQOL = 2.0;

            Motor_Params.Start_TransitionRpm = 0.10;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.RAMP_to_1_Secs = 240.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 240.0;

            Motor_Params.Start_Hold_Time = 1000L*3;             // 1 second hold time
            Motor_Params.Start_SpeedTol = .05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = .07;
            Motor_Params.Start_EnableControllerRef = 1.0;            // Maximum speed setting where drive can be enabled

            Motor_Params.Kv = 0.054857; //Kv in volts/rpm
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain
            break;


/*************************************************************************************************/
/********************************** HONDA INVERTER ****************************************************/
/*************************************************************************************************/
        case HONDA_INVERTER_TEST:
            // Note: Selecting LEVEL5 with a position sensor set the controller to "Torque Mode." This will skip Lockrotor and Ramp state and goes striaght to Commutate State and CommandRef will be using Amps.
            // !!! MUST READ WHEN RUNNING WITH TORQUE MODE !!!
            // Positive speed/torque command is when the motor is spinning clockwise, when you are facing towards the motor's shaft.
            // Negative speed/torque command is when the motor is spinning counter clockwise, when you are facing towards the motor's shaft.
            // Please refer Confluence page and documentation.
            Motor_Params.BuildLevel = e_OpenLoop;

            Motor_Params.PWM_FREQ_KHZ = 40;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 20;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.MAINISR_PRESCALE = 2;

            Motor_Params.QepMode = positionAndDirection;
            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SL_ORTEGA;
            // If a position sensored is being used, it needs to be calibrated.
            if(isCommAngSetToPositionSensored() == 1)
            {
                Motor_Params.PositionSensorOffset = -0.81;
            }
            else
            {
                Motor_Params.PositionSensorOffset = 0.0;
            }

            Motor_Params.FOC_DQ_Decouple = false;
            Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_SPEED|MC_PI_CURRENT);

            Motor_Params.ObserverCommAndMechAngleSwitch = 0;
            // Integrators
            Motor_Params.id_integrator = 2.60;
            Motor_Params.iq_integrator = 8.54;
            Motor_Params.spd_integrator = 0.000000040919;

            Motor_Params.Poles = 2;

            //Parameters for Ortega Observer
            // Define the electrical motor parameters
            Motor_Params.StatorResistance = (0.0508/2.0);            // Stator resistance (ohm) - measured phase-phase resistance/2
            Motor_Params.StatorInductance = (0.000234/2.0);
            Motor_Params.Kv = 66.667/1000.0; //Kv in volts/rpm
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain

            // Define the base quantities
            Motor_Params.BaseFrequency = 1;            // 7200RPM is SpeedRef=1
            Motor_Params.BaseSpeed     = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2.0));

            Motor_Params.spd_KP = 3.0;
            Motor_Params.spd_KI = 0.15;
//            Motor_Params.pos_KP = 2.5;
//            Motor_Params.pos_KI = 0.0001;
            Motor_Params.pos_KP = 2.5;
            Motor_Params.pos_KI = .00001;
            Motor_Params.pos_KD = 8000.0;
            Motor_Params.pos_C1 = .001;
            Motor_Params.pos_C2 = .999;

            Motor_Params.BusV_Nominal = 175.0;
            Motor_Params.ILOOP_BW = (2*PI*1000.0);
            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;


            Motor_Params.Start_Hold_Time = 1000L*3;             // 1 second hold time
            Motor_Params.Start_VLockRotor = 1.75;     // Current during locked rotor segment of startup
            Motor_Params.Start_IQOL = 8.0;            //  Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 16.0;            // Current setting for open angle loop startup ramp - D axis
            Motor_Params.Start_TransitionRpm = 700.0;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
            Motor_Params.Start_SpeedTol = 0.05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_AngTol = 1.0;

            // For UBER in torque mode (level 5), we want the enableControllerRef to set higher
            if( Motor_Params.BuildLevel == e_TorqueMode )
            {
                Motor_Params.Start_EnableControllerRef = 30.0;            // Maximum controller ref setting where drive can be enabled
            }
            else
            {
                Motor_Params.Start_EnableControllerRef = 1.0;            // Maximum controller ref setting where drive can be enabled
            }

            Motor_Params.RAMP_to_1_Secs = 400.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 400.0;


            Motor_Params.LIM_SPD_HI = 1.0;
            Motor_Params.LIM_SPD_LO = -0.1;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency
            Motor_Params.LIM_SPD_DRV = 10;                       // Acceleration limit (derivative of speed estimate -- base speed/per second units
            Motor_Params.LIM_POS_HI = 1.0;
            Motor_Params.LIM_POS_LO = -0.1;
            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage

            // For UBER in torque mode (level 5), we want the IQ_LO set to -1.0*Motor_Params.BusV_Nominal/sqrt(3.0)
            if( Motor_Params.BuildLevel != e_TorqueMode )
            {
                Motor_Params.LIM_IQ_LO = -5.0;
            }
            else
            {
                Motor_Params.LIM_IQ_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);
            }

            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage
            Motor_Params.LIM_ID_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);    // max phase voltage

            Motor_Params.LIM_I_DC_CRIT_H = 100.0;
            Motor_Params.LIM_I_DC_CRIT_H = -100.0;
            Motor_Params.LIM_I_PHS    = 75.0;
            Motor_Params.LIM_V_DC_CRIT_H = Motor_Params.BusV_Nominal+30.0;
            Motor_Params.LIM_V_DC_CRIT_L = Motor_Params.BusV_Nominal-15.0;

            break;

        case TMOTOR_U8:
            Motor_Params.BuildLevel = e_SpeedLoop;

            Motor_Params.PWM_FREQ_KHZ = 35;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
            Motor_Params.LOOP_FREQ_KHZ = 35;      // Control Loop Execution Frequency, kHz
            Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
            Motor_Params.MAINISR_PRESCALE = 1;

            Motor_Params.ObsEn = SL_ORTEGA;
            Motor_Params.CommAng = SL_ORTEGA;
            Motor_Params.MechAng = SL_ORTEGA;
            Motor_Params.FOC_DQ_Decouple = false;
            Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_SPEED | MC_PI_CURRENT);

            Motor_Params.StatorResistance = 0.030;
            Motor_Params.StatorInductance = 0.000010/2.0;           // Stator inductance (H) (phase-phase / 2) - Taken from 7T motor in Scott McAfee email, x(8/7)^2

            Motor_Params.Poles = 42;                      // Number of poles - based on Scott McAfee email not sure about HV motor though

            // Define the base quantities
            Motor_Params.BaseFrequency = 350;            // Base electrical frequency (Hz) -- electrical frequency for speed = 1 (1200 ~ 6500 rpm @ 11 pole pairs
            Motor_Params.BaseSpeed = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2));


            // Define tuning parameters for speed loop  -- speed loop not tuned for Joby motor yet
            Motor_Params.spd_KP = 0.75;
            Motor_Params.spd_KI = 8.0;

            Motor_Params.BusV_Nominal = 24.0;
            Motor_Params.ILOOP_BW = 2*PI*500.0;
            Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

            Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

            Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.4 default
            Motor_Params.LIM_ID_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);    // -0.4 default*
            Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // 0.8 default
            Motor_Params.LIM_IQ_LO = -5.0;    // -0.8 default

            Motor_Params.DQDC_FFGain = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
            Motor_Params.Start_VLockRotor = 0.5;     // Current during locked rotor segment of startup

            Motor_Params.Start_IQOL = 2.0;            //  Current setting for open angle loop startup ramp - Q axis
            Motor_Params.Start_IDOL = 4.0;            // Current setting for open angle loop startup ramp - D axis
            Motor_Params.Start_LockRotorAngle = 0;
            Motor_Params.RAMP_to_1_Secs = 240.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
            Motor_Params.RAMP_down_Secs = 240.0;

            Motor_Params.Start_Hold_Time = 1000L*2;             // 1 second hold time
            Motor_Params.Start_TransitionRpm = 300;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
            Motor_Params.Start_SpeedTol = .05;
            Motor_Params.Start_AngTol = 1;           // How long in !Enable before another start can happen (seconds)
            Motor_Params.Start_EnableControllerRef = 1;            // Maximum speed setting where drive can be enabled

            Motor_Params.Kv = (1.0/170.0); //Kv in volts/rpm
            Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);
            Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain

            Motor_Params.LIM_I_PHS = 10.0;
            break;

        case LP_DHA_120_6PWYE:
                Motor_Params.BuildLevel = e_SpeedLoop;

                Motor_Params.PWM_FREQ_KHZ = 70;      // PWM Switching Frequency in kHz (must be integer multiple of LOOP_FREQUENCY)
                Motor_Params.LOOP_FREQ_KHZ = 35;      // Control Loop Execution Frequency, kHz
                Motor_Params.SPEED_LOOP_PRESCALE = 5;       // Amount to divide control loop by to get speed loop
                Motor_Params.MAINISR_PRESCALE = 2;

                Motor_Params.QepMode = qep_none;
                Motor_Params.ObsEn = SL_ORTEGA;
                Motor_Params.CommAng = SL_ORTEGA;
                Motor_Params.MechAng = SL_ORTEGA;
                Motor_Params.FOC_DQ_Decouple = false;
                Motor_Params.LoopsEn = (enum ControlLoops)(MC_PI_SPEED|MC_PI_CURRENT);

                // Integrators
                Motor_Params.id_integrator = 2.60;
                Motor_Params.iq_integrator = 8.54;
                Motor_Params.spd_integrator = 0.000000040919;

                Motor_Params.Poles = 60;

                //Parameters for Ortega Observer
                // Define the electrical motor parameters
                Motor_Params.StatorResistance = (0.100/2.0);            // Stator resistance (ohm) - measured phase-phase resistance/2
                Motor_Params.StatorInductance = (0.000024/2.0);
                Motor_Params.Kv = 40/1000.0; //Kv in volts/rpm
                Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);
                Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);  // Observer gain

                // Define the base quantities
                Motor_Params.BaseFrequency = 3500;            // 7200RPM is SpeedRef=1
                Motor_Params.BaseSpeed     = Motor_Params.BaseFrequency*(60.0/(Motor_Params.Poles/2.0));

                Motor_Params.spd_KP = 0.5;
                Motor_Params.spd_KI = 4.0;
                Motor_Params.pos_KP = 0.05;
                Motor_Params.pos_KI = 250;

                Motor_Params.BusV_Nominal = 325.0;
                Motor_Params.ILOOP_BW = (2*PI*1000.0);
                Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
                Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
                Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
                Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;

                Motor_Params.DQDC_FFGain = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;

                Motor_Params.Start_Hold_Time = 1000L*5;             // 1 second hold time
                Motor_Params.Start_VLockRotor = 0.1;     // Current during locked rotor segment of startup
                Motor_Params.Start_IQOL = 2;            //  Current setting for open angle loop startup ramp - Q axis
                Motor_Params.Start_IDOL = 4;            // Current setting for open angle loop startup ramp - D axis
                Motor_Params.Start_TransitionRpm = 400.0;   // Reference speed setting for when the speed loop is closed, also the speed at which the open loop ramps up to
                Motor_Params.Start_SpeedTol = 0.05;             // Nominal Tolerance when setpoint speed is close enough to target speed to go closed loop on angle (uncomment for level 3b or lower)
                Motor_Params.Start_AngTol = 1;
                Motor_Params.Start_EnableControllerRef = 1.0;            // Maximum speed setting where drive can be enabled

                Motor_Params.RAMP_to_1_Secs = 600.0;        // Number of seconds speed command will take to ramp from 0 to 1 (full base speed) -- slew rate limit
                Motor_Params.RAMP_down_Secs = 600.0;


                Motor_Params.LIM_SPD_HI = 1.0;
                Motor_Params.LIM_SPD_LO = -0.1;                     // Max and Min speeds for a speed fault to trip - relative to the speed at base frequency
                Motor_Params.LIM_SPD_DRV = 10;                       // Acceleration limit (derivative of speed estimate -- base speed/per second units
                Motor_Params.LIM_POS_HI = 1.0;
                Motor_Params.LIM_POS_LO = -0.1;
                Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage
                Motor_Params.LIM_IQ_LO = -5.0;
                Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);     // max phase voltage
                Motor_Params.LIM_ID_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);    // max phase voltage

                Motor_Params.LIM_I_DC_CRIT_H = 30.0;
                Motor_Params.LIM_I_DC_CRIT_L = -10.0;
                Motor_Params.LIM_I_PHS    = 60.0;
                Motor_Params.LIM_V_DC_CRIT_H = Motor_Params.BusV_Nominal+20.0;
                Motor_Params.LIM_V_DC_CRIT_L = Motor_Params.BusV_Nominal-20.0;

                break;

        default:
            asm ("      ESTOP0");
    }
    Motor_Params.T_LOOP = 0.001/Motor_Params.LOOP_FREQ_KHZ;    // Sampling period (sec), see parameter.h
    Motor_Params.T_SPDLOOP = 0.001/(Motor_Params.LOOP_FREQ_KHZ/Motor_Params.SPEED_LOOP_PRESCALE);
    Motor_Params.StructVersion = 1; //Change every time we update structure

//    if(!(validateMotorParams()))
//    {
//        asm ("      ESTOP0");
//    }

    FD_Motor = Motor_Params;
}

bool validateMotorParams(void)
{
    bool result = true;
    int i = 0;
    if(Motor_Params.BuildLevel < e_OpenLoop || Motor_Params.BuildLevel > e_SpeedLoop)
    {
        result = false;
    }
    if(Motor_Params.ObsEn==0 || Motor_Params.CommAng == 0 || Motor_Params.MechAng == 0)
    {
        result = false;
    }
    if( (Motor_Params.ObsEn & Motor_Params.CommAng == 0) )  //Ensure observer for selected commutation angle is activated
    {
        result = false;
    }
    if( (Motor_Params.ObsEn & Motor_Params.MechAng == 0) ) //Ensure observer for selected mechanical angle is activated
    {
        result =  false;
    }
    if( (Motor_Params.CommAng & (Motor_Params.CommAng-1)) != 0) //Ensure only one commutation angle is selected
    {
        result =  false;
    }
    if( (Motor_Params.MechAng & (Motor_Params.MechAng-1)) != 0) //Ensure only one mechanical angle is selected
    {
        result =  false;
    }
    if( ( (Motor_Params.ObsEn >> 2) & ((Motor_Params.ObsEn >> 2)-1) ) != 0) //Ensure not more than one of the sensored observers is enabled (not enough GPIOs to do more than one!)
    {
        result = false;
    }
    if(Motor_Params.LIM_I_DC_CRIT_H > Inverter_Params.LIM_I_DC_CRIT_H)
    {
        result = false;
    }
    if(Motor_Params.LIM_I_DC_CRIT_L < Inverter_Params.LIM_I_DC_CRIT_L)
    {
        result = false;
    }
    if(Motor_Params.LIM_I_PHS > Inverter_Params.LIM_I_PHS)
    {
        result = false;
    }
    if(Motor_Params.LIM_V_DC_CRIT_H > Inverter_Params.LIM_V_DC_CRIT_H)
    {
        result = false;
    }


    return result;
}

bool validateInverterParams(void)
{
    bool result = true;
    int i=0;

    if(Inverter_Params.Controller == MotorController && Inverter_Params.Genset != GensetNone)
    {
        //Can't have a motor controller w/genset
        result = false;
    }
    if(Inverter_Params.Controller == MotorController && Inverter_Params.Electronics == DriveNone)
    {
        //Can't have a motor controller w/out PWM-able electronics
        result = false;
    }

    if(Inverter_Params.Controller == GeneratorController && Inverter_Params.Electronics == DriveNone)
    {
        //Can't have a generator controller w/out PWM-able electronics
        result = false;
    }
    if(Inverter_Params.Genset == GensetPassive && Inverter_Params.Electronics != DriveNone)
    {
        //Can't have a passive genset with PWM-able electronics
        result = false;
    }

    return result;
}

void UpdateDependentMotorParams(void)
{
    //Updating current loop gains
    Motor_Params.id_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
    Motor_Params.id_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
    Motor_Params.iq_KP = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;
    Motor_Params.iq_KI = Motor_Params.ILOOP_BW*Motor_Params.StatorResistance;
    Motor_Params.DQDC_FFGain = Motor_Params.ILOOP_BW*Motor_Params.StatorInductance;

    Motor_Params.LIM_ID_HI = Motor_Params.BusV_Nominal/sqrt(3.0);
    Motor_Params.LIM_ID_LO = -1.0*Motor_Params.BusV_Nominal/sqrt(3.0);
    Motor_Params.LIM_IQ_HI = Motor_Params.BusV_Nominal/sqrt(3.0);

    Motor_Params.Psi = Motor_Params.Kv*(60.0/(2*PI))/(sqrt(3.0))/(Motor_Params.Poles/2.0);
    Motor_Params.Gamma = (4.0*100.0*(2.0*PI/60.0)*(Motor_Params.Poles/2.0)) / powf(Motor_Params.Psi,2.0);

    if(!validateMotorParams())
    {
        asm ("      ESTOP0");
    }
}

void RestoreMotorDefaults(void)
{
    Motor_Params = FD_Motor;
}

void RestoreInverterDefaults(void)
{
    Inverter_Params = FD_Inverter;
}

unsigned char GetMotorVersion(void)
{
    return Motor_Params.StructVersion;
}

unsigned char GetInverterVersion(void)
{
    return Inverter_Params.StructVersion;
}

void ProcessSaveRestoreCommand(unsigned char param)
{
#if 0
    enum SLRAction {
        Load = 0x04,
        Save = 0x08,
        Defaults = 0x0C
    };

    enum Target {
        tMotor_ECU =0x1,
        tInverter = 0x2
    };

    enum SLRAction action =(enum SLRAction)(param & 0x0C);

    if(action==Load)
    {
        if(isGenerator(Inverter_Params.SyncMode))
        {
            if(param & tMotor_ECU) EEPROM_load_ECU((struct ECUParameters *)&ECU_Params, ECU_Params.StructVersion);
        }
        else
        {
            if(param & tMotor_ECU) EEPROM_load_motor((struct MotorParameters *)&Motor_Params, Motor_Params.StructVersion);
        }

        if(param & tInverter) EEPROM_load_inverter((struct InverterParameters *)&Inverter_Params, Inverter_Params.StructVersion);
    }
    else if(action==Save)
    {
        if(isGenerator(Inverter_Params.SyncMode))
        {
            if(param & tMotor_ECU) EEPROM_save_ECU((struct ECUParameters *)&ECU_Params, ECU_Params.StructVersion);
        }
        else
        {
            if(param & tMotor_ECU) EEPROM_save_motor((struct MotorParameters *)&Motor_Params, Motor_Params.StructVersion);
        }

        if(param & tInverter) EEPROM_save_inverter((struct InverterParameters *)&Inverter_Params, Inverter_Params.StructVersion);
    }
    else if(action==Defaults)
    {
        if(isGenerator(Inverter_Params.SyncMode))
        {
            if(param & tMotor_ECU) RestoreECUDefaults();
        }
        else
        {
            if(param & tMotor_ECU) RestoreMotorDefaults();
        }

        if(param & tInverter) RestoreInverterDefaults();
    }
#endif
}

void ProcessSetParameterCommand(unsigned char param, float value)
{
    //Note: this fcn call is already inside a DINT/EINT;

    float PrevParamVal = GetParameterValue(param);

    if((param != ID_SERIAL_NUMBER) && (param != ID_SW_VERSION))
    {
        SetParameterValue(param, value);

        if(param == ID_THRT_KP || param == ID_THRT_KI || param == ID_THRT_KD || param == ID_ECU_CC_KP || param == ID_ECU_CC_KI || param == ID_ECU_CC_KD)
        {
            if(PrevParamVal)
            ECU_UpdateGains(false);
        }

        if(param == ID_GC_KP || param == ID_GC_KI || param == ID_GC_KD)
        {
            GenControl_UpdateGains();
        }

        if(param==ID_LPF_GC_DC || param==ID_LPF_GC_FF)
        {
            GenControlUpdateLPF();
        }

        if(param==ID_LPF_ECU_DC || param == ID_LPF_ECU_FF)
        {
            ECU_UpdateLPF();
        }

        if(param == ID_COOL_MODE)
        {
            ECU_UpdateCoolingMode();
        }
    }
}

bool isMasterOrSolo(enum SyncMode sm)
{
    bool result=false;
    if( (sm==SOLO)||(sm==GEN_SOLO)||(sm==PARALLEL_MASTER)||(sm==FOLLOWER_MASTER)||(sm==SERVO_MASTER)||(sm==GEN_MASTER))
    {
        result=true;
    }

    return result;
}

// Return true, when in Torque Mode and using any position sensor devices.
bool isTorqueModeWithPositionSensor(enum BuildLevels BuildLevel, enum Observers_Angles CommAng )
{
    bool result = false;
    if( (BuildLevel == e_TorqueMode) && ( (CommAng == SNS_ENC_SSI) || (CommAng == SNS_ENC_QEP) || (CommAng == SNS_HALLRAW) || (CommAng == SNS_HALLPLL) ) )
    {
        result = true;
    }
    return result;
}

bool usingPositionSensor(enum Observers_Angles CommAng )
{
    bool result = false;
    if( (CommAng == SNS_ENC_SSI) || (CommAng == SNS_ENC_QEP) || (CommAng == SNS_HALLRAW) || (CommAng == SNS_HALLPLL) )
    {
        result = true;
    }
    return result;
}

bool isSlave(enum SyncMode sm)
{
    bool result=false;
    if(sm==FOLLOWER_SLAVE||sm==PARALLEL_SLAVE||sm==SERVO_SLAVE||sm==GEN_SLAVE)
    {
        result=true;
    }
    return result;
}

bool isGenerator(enum SyncMode sm)
{
    bool result = false;
    if(sm==GEN_MASTER || sm==GEN_SOLO || sm==GEN_SLAVE)
    {
        result = true;
    }
    return result;
}

bool isSolo(enum SyncMode sm)
{
    bool result=false;
    if(sm==SOLO||sm==GEN_SOLO)
    {
        result = true;
    }
    return result;
}

bool isServoFollower(enum SyncMode sm)
{
    bool result = false;
    if(sm==SERVO_SOLO || sm==FOLLOWER_SOLO|| sm==SERVO_MASTER || sm==FOLLOWER_MASTER|| sm==SERVO_SLAVE || sm==FOLLOWER_SLAVE)
    {
        result = true;
    }
    return result;
}


