/*
 * ECU_Initialization.c
 *
 *  Created on: Sep 11, 2018
 *      Author: vvarahamurthy
 */
#include "F2837S_files/F28x_Project.h"
#include "Header/GUIC_Header.h"
#include "Header/ECU.h"
#include "Header/Globals.h"

enum ECUControlModes GetECUCtrlMode(void);
void RestoreECUDefaults(void);

struct ECUParameters ECU_Params;
struct ECUParameters FD_ECU;



void InitECUParams(enum ECUControlModes CtrlMode)
{
    ECU_Params.LOOP_FREQ_KHZ = 1.0;        //Loop frequency in kHz
    ECU_Params.CoolingLoopFreqKHz = 0.01; //Cooling loop frequency in kHz (running at 10Hz)

    ECU_Params.T_LOOP = 0.001/ECU_Params.LOOP_FREQ_KHZ;

    ECU_Params.ThrottleFreq = 50.0; //Frequency in Hz of the throttle PWM signal
    ECU_Params.FanPWMFreq = 50.0;   //Frequency in Hz of the PWM signal to the ESCs powering the EDFs

    ECU_Params.PWMDAC_NUM_EDF = 1;
    if(Inverter_Params.InverterSel==ELMO_PMU)
    {
        ECU_Params.PWMDAC_NUM_THROTTLE = 2;
    }
    else if(Inverter_Params.InverterSel==HPS400_SP)
    {
        ECU_Params.PWMDAC_NUM_THROTTLE = 1;
    }



    ECU_Params.StartingThrottleHi = 0.2;  //0 to 1 represents 0 to 100%
    ECU_Params.StartingThrottleLo = 0.12;  //0 to 1 represents 0 to 100%

    ECU_Params.StartupEnrichTime = 15;

    ECU_Params.CoolingThreshLoC = 60.0;
    ECU_Params.CoolingThreshHiC = 70.0;
    ECU_Params.CoolingTargetC = 121.0;
    ECU_Params.MinCHTC = 65.0;

    ECU_Params.ControlMode = CtrlMode;

    ECU_Params.pid_cooling_kd = 0.0;
    ECU_Params.pid_cooling_ki = 0.05;
    ECU_Params.pid_cooling_kp = 0.01;

    ECU_Params.pid_current_kd = 0.0;
    ECU_Params.pid_current_ki = 0.01;
    ECU_Params.pid_current_kp = 0.0025;

    ECU_Params.pid_speed_kd = 0.0;
    ECU_Params.pid_speed_ki = .0002;
    ECU_Params.pid_speed_kp = .000001;

    ECU_Params.pid_voltage_kd = 0.0;
    ECU_Params.pid_voltage_ki = 0.0075;
    ECU_Params.pid_voltage_kp = 0.01;

    ECU_Params.pid_power_kd = 0.0;
    ECU_Params.pid_power_ki = 0.0001;
    ECU_Params.pid_power_kp = 0.000001;

#if(NEWCONTROL_MODE)
    ECU_Params.pid_chgctrl_kd = 0.0;
    ECU_Params.pid_chgctrl_ki = 2.0;
    ECU_Params.pid_chgctrl_kp = 0.1;
#endif

    ECU_Params.CoolingMode = Cool_Manual;
    ECU_Params.CoolingDuty = 0.0;

    switch(CtrlMode)
    {
        case ECUCtrl_Speed:
            ECU_Params.activepid_kp = ECU_Params.pid_speed_kp;
            ECU_Params.activepid_ki = ECU_Params.pid_speed_ki;
            ECU_Params.activepid_kd = ECU_Params.pid_speed_kd;
            break;
        case ECUCtrl_Voltage:
            ECU_Params.activepid_kp = ECU_Params.pid_voltage_kp;
            ECU_Params.activepid_ki = ECU_Params.pid_voltage_ki;
            ECU_Params.activepid_kd = ECU_Params.pid_voltage_kd;
            break;
        case ECUCtrl_Current:
            ECU_Params.activepid_kp = ECU_Params.pid_current_kp;
            ECU_Params.activepid_ki = ECU_Params.pid_current_ki;
            ECU_Params.activepid_kd = ECU_Params.pid_current_kd;
            break;
        case ECUCtrl_Power:
            ECU_Params.activepid_kp = ECU_Params.pid_power_kp;
            ECU_Params.activepid_ki = ECU_Params.pid_power_ki;
            ECU_Params.activepid_kd = ECU_Params.pid_power_kd;
            break;
        default:
            ECU_Params.activepid_kp = 0;
            ECU_Params.activepid_ki = 0;
            ECU_Params.activepid_kd = 0;
            break;
    }

    ECU_Params.feedfw_int = 0.0;
    ECU_Params.feedfw_mi  = 0.0;
    ECU_Params.feedfw_mv  = 0.0;
    ECU_Params.feedfw_gain = 0.8;

    ECU_Params.DCP_LPF_Hz = 1.0;
    ECU_Params.DCP_LPF_Alpha = (1.0-expf(-2.0*PI*ECU_Params.DCP_LPF_Hz/ECU_Params.LOOP_FREQ_KHZ/1000.0));

    ECU_Params.FF_LPF_Hz = 1.0;
    ECU_Params.FF_LPF_Alpha = (1.0-expf(-2.0*PI*ECU_Params.FF_LPF_Hz/ECU_Params.LOOP_FREQ_KHZ/1000.0));

    ECU_Params.LIM_SPD_HI = 8000;

    ECU_Params.MinBusPower_H = 1500;
    ECU_Params.MinBusPower_L = 1000;

    ECU_Params.IdleSpeed = 4000.0;

    ECU_Params.StructVersion = 0x00;

    ECU_Params.PowerSpeedSlope = 0.85074;
    ECU_Params.PowerSpeedIntercept = 1739.35977;

#if 0
    ECU_Params.PowerThrottleSlope_Lo = .0000882868;
    ECU_Params.PowerThrottleIntercept_Lo = 0.1602498809;
#endif
    ECU_Params.PowerThrottleSlope_Lo = 0.000144092168515623;
    ECU_Params.SpeedThrottleSlope_Lo = -0.0000678463198353197;
    ECU_Params.PowerThrottleIntercept_Lo = 0.283603445977454;

    ECU_Params.PowerThrottleSlope_Hi = 0.0009065000;
    ECU_Params.PowerThrottleIntercept_Hi = -4.1726166667;

    ECU_Params.MinSpeedRef = 3000;
    ECU_Params.MaxSpeedRef = 7000;

    ECU_Params.MaxGenPower = 5400;

    ECU_Params.LIM_CHT_HI = 149;
    ECU_Params.LIM_CHT_LO = 0;

    FD_ECU = ECU_Params;
}

void RestoreECUDefaults(void)
{
    ECU_Params = FD_ECU;
}

enum ECUControlModes GetECUCtrlMode(void) {return ECU_Params.ControlMode;}
