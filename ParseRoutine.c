/*
 * ParseRoutine.c
 *
 *  Created on: Jun 22, 2018
 *      Author: guydeong
 *  Description: This is a Parse routine that has:
    - purge();                                  // delete data
    - pRewind();                                 // points back to the beginning of the buffer
    - underflowed();                            // set flag when buffer is empty
    - getFloat(uint32_t *);                       // get an IEEE float then parse the data
    - getCRC(unsigned char *);                  // get the CRC value in the message
    - discard(uint16_t);                  //
    - getValues(unsigned char *);               // get Values
    - getOpCode(unsigned char *);               // get Op code
    - getParameter(unsigned char *);            // get Parameter
    - getStartCode(unsigned char *);            // get start code
    - getByte();                                // get byte
    - context();                                // get the message
 */


#include "F2837S_files/F28x_Project.h"
#include "IQMathLib.h"
#include "Header/RingBuffer.h"
#include "Header/CRC.h"
#include "Header/UART_Comm.h"
#include "Header/State_machine.h"
#include "Header/GUIC_Header.h"
#include "Header/ParseRoutine.h"

#define INFINITY        (__INFINITY__)
#define NAN             (__NAN__)
#define MAX_NUM_TRIES   3

// Global

RINGBUFFER   gOutTxRingBuffer;
RINGBUFFER   gInRxRingBuffer;

Parse_Object txParseMessage;
Parse_Object rxParseMessage;
Parse_Object tempParseMessage;
extern float
      DlogCh1,
      DlogCh2,
      DlogCh3,
      DlogCh4;
extern void Run_Datalog(void);

uint16_t gCountNumOfTries;                  // Count number of tries
uint16_t gCountNumberOfValues;              // Count number of values
uint16_t gMaxNumberOfValues;                // Max number of values
uint16_t gCountMsgBytes;                    // Count number of bytes in parsing messaged

uint16_t gNumberOfBytesValues;            // Count number of bytes for values
uint16_t gCrcCounter;                     // Count number of bytes in CRC


bool gPairModeComplete;                     // bool for master and slave connections

uint32_t failedMessageCounter;              // Counts the number of failed messages
uint32_t successMessageCounter;             // Counts the number of successful messages

unsigned char gCRCTemp;                         // Store the 2 bytes CRC (unsigned char is 2 bytes)

int16_t gNewCharCounter = 0;                        // Count of received characters

unsigned char UARTErrorCtr=10;
void decrementErrorCounter(void);
void resetErrorCounter(void);
uint32_t getSuccessMsgCounter(void);
enum FaultVariable Fault_Detect_UART(void);
unsigned short getLargestIntStoppedAt();
bool isPairModeCompleted();

extern void convertFloatToBytes(unsigned char *, float *);
/****************************************/
enum ParseStates { Start, Op, Parameter, Values, Crc , Unsuccessful} ParsingState;

unsigned char contextString[CMD_BUFFER_SIZE];

unsigned char gValueArray[4];

extern float getRampGenAngle();

#if 0
// returns the status of gMasterSlaveConnected
bool isMasterSlaveConnected()

{
    gPairModeComplete = 1;
}

void pairModeFailed()
{
    gPairModeComplete = 0;
}
// return tx buffer
RINGBUFFER * getTxRingBuffer()
{
    return &gOutTxRingBuffer;
}

// return rx buffer
RINGBUFFER * getRxRingBuffer()
{
    return &gInRxRingBuffer;
}

// Returns rx parse message
unsigned char getRxState()
{
    unsigned char rxState = 0;
    if( (rxParseMessage.opCode == 128) || (rxParseMessage.opCode == 129) || (rxParseMessage.opCode == 127)  )
    {
        rxState = rxParseMessage.parameter;
    }
    return rxState;
}

// Return one of the signals
float getMasterValue1_sig(){
    return rxParseMessage.values[0];
}

// Return the 2nd signal
float getMasterValue2_sig(){
    return rxParseMessage.values[1];
}

void InitParseRoutine()
{
    int i;
    intStoppedAt = 0;
    ParsingState    = Start;
    gCRCTemp        = 0;
    gPairModeComplete = 0;      // set status to false
    // Counters
    gNewCharCounter           = 0;
    gNumberOfBytesValues      = 0;
    gCrcCounter               = 0;
    gCountNumOfTries          = 0;
    gCountNumberOfValues      = 0;
    gCountMsgBytes            = 0;
    failedMessageCounter      = 0;
    successMessageCounter     = 0;

    // Default expect one value
    gMaxNumberOfValues = 1;

    rxParseMessage.startCode  = 0;
    rxParseMessage.opCode     = 0;
    rxParseMessage.parameter  = 1;

    tempParseMessage.startCode = 0;
    tempParseMessage.opCode = 0;
    tempParseMessage.parameter = 1;

    txParseMessage.startCode = 0;
    txParseMessage.opCode = 0;
    txParseMessage.parameter = 0;

    for(i=0;i<MAX_NUM_VALUES;i++)
    {
        rxParseMessage.values[i]=0x00;
        txParseMessage.values[i]=0x00;
        tempParseMessage.values[i]=0x00;
    }

}

bool checkInitParseRoutine()
{
    bool isTrue = 0;    // Set false
    if(
        ParsingState    == Start &&
        gCRCTemp        == 0 &&
        gPairModeComplete == 0 &&
        gNewCharCounter           == 0 &&
        gNumberOfBytesValues      == 0 &&
        gCrcCounter               == 0 &&
        gCountNumOfTries          == 0 &&
        gCountNumberOfValues      == 0 &&
        gCountMsgBytes            == 0 &&
        failedMessageCounter      == 0 &&
        successMessageCounter     == 0 &&
        gMaxNumberOfValues == 1 &&
        rxParseMessage.startCode  == 0 &&
        rxParseMessage.opCode     == 0 &&
        rxParseMessage.parameter  == 1 &&
        tempParseMessage.startCode == 0 &&
        tempParseMessage.opCode == 0 &&
        tempParseMessage.parameter == 1 &&
        txParseMessage.startCode == 0 &&
        txParseMessage.opCode == 0 &&
        txParseMessage.parameter == 0
            )
    {
        isTrue = 1;
    }
        return isTrue;
}


// Increment success msg counter
void incrementSuccessMsgCounter()
{
    if( successMessageCounter < ULONG_MAX )
    {
        successMessageCounter++;
    }
    else
    {
        successMessageCounter=1;
    }
    resetErrorCounter();
}

// Increment failed msg counter
void incrementFailedMsgCounter()
{
    if( failedMessageCounter < ULONG_MAX)
    {
        failedMessageCounter++;
    }
    else
    {
        failedMessageCounter=1;
    }
    decrementErrorCounter();
}

// When called it gets all the values in FIFO and store to parse buffer.
void fillParseBuffer()
{
    DINT;
    while(ScibRegs.SCIFFRX.bit.RXFFST > 0)
    {
        rb_push(&gInRxRingBuffer,ScibRegs.SCIRXBUF.all);
    }
    EINT;
}

// Return the number of failed messages
uint32_t getFailedMsgCounter()
{
    return failedMessageCounter;
}

uint32_t getSuccessMsgCounter()
{
    return successMessageCounter;
}

// Convert 2 words char into one float
void convertCharToFloat(float *destination, unsigned char *source)
{
    union{
        float tFloat;
        unsigned char tBytes[2];
    }bytes2;

    // First source byte is the MSB and second source byte is the next byte of MSB
    bytes2.tBytes[1] =  (source[0] << 8) & 0xFF00;
    bytes2.tBytes[1] |= (source[1] & 0x00FF);

    // Third source byte is the next byte and last source byte is LSB
    bytes2.tBytes[0] =  (source[2] << 8) & 0xFF00;
    bytes2.tBytes[0] |= (source[3] & 0x00FF);

    *destination = bytes2.tFloat;

}

// Set everything in buffer to NULL
void clearContextString()
{
    uint16_t i;
    for(i = 0; i < CMD_BUFFER_SIZE; i++)
    {
        contextString[i] = NULL;
    }
}

// Returns true when
bool isParseStillProcessing(){
    return isReadAtTail(&gInRxRingBuffer);
}

// Return if ringBuffer is empty
bool underflowed(){
    // Inside isRingBufferEmpty calls an error
    return isRingBufferEmpty(&gInRxRingBuffer);
}

// Reset buffer
void purge()
{
    rb_flush(&gInRxRingBuffer);
}

// The head is pointed back to the beginning of the buffer
bool pRewind()
{
    rb_rewind(&gInRxRingBuffer);
    return 1;
}

// Advances the input pointer past n bytes in the parse buffer. Calls getByte() to move the pointer and refills the parse buffer.
bool discard(uint16_t discardNumberOfTimes)
{
    bool discardSucceed = 1;                    // set true
    uint16_t i;

    // Check if index is within the buffer
    if(discardNumberOfTimes >= CMD_BUFFER_SIZE)
    {
        // Index can't be bigger than buffer. Failed!
        // error(ILLEGAL_ACCESS);
        discardSucceed = 0;
    }
    else
    {
        // Keep popping until reached the number of times
        for(i = 0; i < discardNumberOfTimes; i ++)
        {
            DINT;
            rb_deleteHead(&gInRxRingBuffer);
            rb_pop(&gInRxRingBuffer);
            EINT;
        }// end of for loop

    }// end of if and else

    return discardSucceed;
}

// Reads through the parse buffer
unsigned char getByte()
{
    DINT;
    unsigned char temp = rb_read(&gInRxRingBuffer);
    EINT;

   return temp;
}



bool getStartCode(unsigned char *msgByte)
{
    bool foundStartMarker = 0;  // set false

    // If msgByte = 0xFF then return true, otherwise false
    if( *msgByte == 0xFF )
    {
        DINT;
        tempParseMessage.startCode = 0xFF;
        EINT;
        foundStartMarker = 1;
    }

    return foundStartMarker;
}

bool getOpCode(unsigned char *msgByte)
{
    // Set to false
    bool matchedFound = 0;

    // check if the msg is one byte and match any of the op code
    switch(*msgByte)
    {
        case 1:     // INIT match!
            matchedFound = 1;
            // Save msg into opCode
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 2:     // RESET match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 3:     // START match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 4:     // GET match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 5:     // PUT match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 7:     // HandShake match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 10:    // STARTGUI match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 11:    // STOPGUI match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 127:   // Servo -> Follower match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 128:   // M->S comm match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 129:   // S->M match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        case 254:   //  ESTOP match!
            matchedFound = 1;
            DINT;
            tempParseMessage.opCode = *msgByte;
            EINT;
        break;

        default:        // No match return false
            matchedFound = 0;
        break;

    }// End of switch case

    return matchedFound;
}

bool getParameter(unsigned char *msgByte)
{
    bool foundParameter = 0;        // Set false

    // Change this when Parameter is COMPLETELY defined
    uint16_t MAX_NUMBER_OF_PARAM = 100;

    // Check if msg match any of the parameters
    if( *msgByte <= MAX_NUMBER_OF_PARAM )
    {
        foundParameter = 1;

        if( (tempParseMessage.opCode == 4) || (tempParseMessage.opCode == 5) )
        {
            // The parameter determines the number of values we expect.
            DINT;
            gMaxNumberOfValues = tempParseMessage.parameter = *msgByte;
            EINT;
        }
        else if ( tempParseMessage.opCode == 128 || tempParseMessage.opCode == 129 )
        {
            // We expect to get 2 values
            gMaxNumberOfValues = 2;
            DINT;
            tempParseMessage.parameter = *msgByte;
            EINT;
        }
        else if( tempParseMessage.opCode == 127 )
        {
            // We expect to get one value
            gMaxNumberOfValues = 2;
            DINT;
            tempParseMessage.parameter = *msgByte;
            EINT;
        }
        else
        {
            // Save msgByte into parameter
            DINT;
            tempParseMessage.parameter = *msgByte;
            EINT;
        }
    }
    else
    {
        foundParameter = 0;
    }

    return foundParameter;
}


// Return true when get a valid float number, otherwise fail
bool getValues(unsigned char *byte)
{
    bool getValuesSuccessFlag = 1; // Set true

    // Save the first word into array
    if( gNumberOfBytesValues < 3)
    {
        // Save value into the array and set flag to zero because not finished
        gValueArray[gNumberOfBytesValues] = *byte;
        gNumberOfBytesValues++;
    }
    else
    {
        // On the fourth byte the array to float and call getFloat
        gValueArray[gNumberOfBytesValues] = *byte;
        gCountMsgBytes = gCountMsgBytes + (gNumberOfBytesValues + 1);
        gNumberOfBytesValues = 0;
        // Place the array into float variable.
        float values = 0;
        convertCharToFloat(&values, gValueArray);

        // If fail set getValuesSuccessFlag to 0
        if( !getFloat(&values) )
        {
            getValuesSuccessFlag = 0;
        }
    }

    return getValuesSuccessFlag;
}

// Gets a single IEEE float. Does not need to call getByte because buffer is already filled
bool getFloat(float *f)
{
    bool getFloatSuccess = 1;           // Set true

    // Check if float is a valid value
    if( sizeof(f)/sizeof(float) != 1)
    {
        // Float failed
        getFloatSuccess = 0;
    }
    else if( *f == NAN || *f == INFINITY || *f == (-INFINITY) )
    {
        // Float failed
        getFloatSuccess = 0;
    }
    else
    {
        // Save to tempParseMessage values
        memcpy(&tempParseMessage.values[gCountNumberOfValues], f, sizeof(f) );
        // Increment number of values
    }

    if(gCountNumberOfValues < MAX_NUM_VALUES)
    {
        gCountNumberOfValues++;
    }
    else
    {
        //error(ILLEGAL_ACCESS);
    }

    return getFloatSuccess;
}

// Checks the CRC value
bool getCRC(unsigned char *crcMsg)
{
    // set bool to false
    bool doesCrcMatch   = 0;
    unsigned char crcResult = 0;
    uint16_t tempMsgBytes = gCountMsgBytes + 1;
    // if the first byte of CRC is received, save in MSB of global variable
    if( gCrcCounter == 0 )
    {
        gCRCTemp = (*crcMsg << 8) & 0xFF00;
        doesCrcMatch = 1;
    }
    else
    {
        // Save second byte at LSB
        gCRCTemp |= (*crcMsg & 0x00FF);
        // Save the crc calculation
        crcResult = (unsigned char)run16CrcVcu(tempMsgBytes ,&tempParseMessage);
        tempParseMessage.crc = crcResult;
        // Check if the CRC calculation matches with message CRC
        ( crcResult != gCRCTemp ) ?  (doesCrcMatch = 0) : (doesCrcMatch = 1);
        pairModeComplete();
        if(tempParseMessage.opCode == 0x07 && doesCrcMatch == 1)
        {

        }
    }

    return doesCrcMatch;
}

// Save the
// When a complete message is found, save the tempParseMessage to rxParseMessage
void process()
{
    rxParseMessage.startCode = tempParseMessage.startCode;
    rxParseMessage.opCode = tempParseMessage.opCode;
    rxParseMessage.parameter = tempParseMessage.parameter;

    uint16_t i;
    for( i = 0; i < MAX_NUM_VALUES; i++)
    {
        rxParseMessage.values[i] = tempParseMessage.values[i];
    }
}

// Returns the largest failed case
unsigned short getLargestIntStoppedAt()
{
    static unsigned short max = 0;
    if( max < intStoppedAt)
    {
        max = intStoppedAt;
    }
    return max;
}

unsigned int getMaxNumberOfBytes(unsigned int value)
{
    static unsigned int max = 0;
    if( max < value)
    {
        max = value;
    }
    return max;
}

// Parsing a message
char * parseRoutine(unsigned char byte)
{
    intStoppedAt = NOTHING;

    // Transitions and action of all states
    switch(ParsingState)
    {

    case Start:
        if( !getStartCode(&byte) )
        {
            // Keep looping until 0xFF is found
            ParsingState = Start;
            discard(gCountMsgBytes + 1);
            gCountMsgBytes = 0;
        }
        else
        {
            // When message is detected reset intStoppedAt
            intStoppedAt = NOTHING;
            ParsingState = Op;
            gCountMsgBytes++;
        }
    break;

    case Op:
        if( !getOpCode(&byte) )
        {
            gCountNumOfTries++;
            ParsingState = Start;

            if( gCountNumOfTries < MAX_NUM_TRIES )
            {
                pRewind();
            }
            else
            {
                intStoppedAt = OPCODE;
                //error(FAILED_TO_PARSE);
                discard(gCountMsgBytes + 1);
                gCountMsgBytes = 0;
                gCountNumOfTries = 0;
                incrementFailedMsgCounter();
            }
            // Reset after discard
            gCountMsgBytes = 0;

        }
        else
        {
            gCountMsgBytes++;
            ParsingState = Parameter;
        }
    break;

    case Parameter:
        if( !getParameter(&byte) )
        {
            ParsingState = Start;
            gCountNumOfTries++;

            if( gCountNumOfTries < MAX_NUM_TRIES )
            {
                pRewind();
            }
            else
            {
                intStoppedAt = PARAMETER;
                //error(FAILED_TO_PARSE);
                discard(gCountMsgBytes + 1);
                gCountNumOfTries = 0;
                gCountMsgBytes = 0;
                incrementFailedMsgCounter();
            }
            gCountMsgBytes   = 0;
        }
        else
        {
            ParsingState = Values;
            gCountMsgBytes++;
        }
    break;

    case Values:

        // The only time it will fail is when undefined and infinity values. We handle that in process

        getValues(&byte);

        if(gCountNumberOfValues >= gMaxNumberOfValues)
        {
            gNumberOfBytesValues = 0;
            gCountNumberOfValues = 0;
            ParsingState = Crc;
            valuesNumberOfBytes = gCountMsgBytes;
        }
        else
        {
            ParsingState = Values;
        }
        //gCountMsgBytes++;

    break;

    case Crc:
        if(getCRC(&byte))
        {
            gCountMsgBytes++;

            if( gCrcCounter < 1 )
            {
                gCrcCounter++;
                ParsingState = Crc;
            }
            else
            {
                discard(gCountMsgBytes);
                gCountMsgBytes = 0;
                gCrcCounter = 0;
                ParsingState = Start;

                // Process when no error in the message
                if( intStoppedAt == NOTHING)
                {
                    process();
                    incrementSuccessMsgCounter();
                }
            }
        }
        else
        {
            ParsingState = Start;
            intStoppedAt = CRC;
            //error(FAILED_TO_PARSE);
            discard(gCountMsgBytes + 1);
            gCountNumOfTries = 0;
            gCountMsgBytes = 0;
            gCrcCounter = 0;
            incrementFailedMsgCounter();
        }
    break;

    default: // If lost in a state return to init
        ParsingState = Start;
    break;

    }// end of switch

    // For debugging
    MaxMsgBytes = getMaxNumberOfBytes(gCountMsgBytes);
    MaxIntStoppedAt = getLargestIntStoppedAt();
    // If number of failed message is greater than 1000, then pair mode failed
    // this is temporary, i just need to a variable to check the status of pairing.
    if( getFailedMsgCounter() <= 1000 )
    {
        pairModeComplete();
    }
    else
    {
        pairModeFailed();
    }
    return 0;
}

//*******************************Transmitting Messages ***************************

// Convert Enum to float
float convertEnumToBytes(enum FaultVariable Faults)
{
    union{
        float tFloat;
        enum FaultVariable tFaults;
    }convertFault;

    convertFault.tFaults = Faults;

    return convertFault.tFloat;
}

// Convert float to Char
void convertFloatToBytes(unsigned char *destination, float *source)
{
    union{
        float tFloat;
        unsigned char tBytes[2];
    }bytes2;
    unsigned char temp = 0;
    DINT;
    bytes2.tFloat = *source;
    EINT;
    // Switch bytes
    temp = bytes2.tBytes[0];
    bytes2.tBytes[0] = bytes2.tBytes[1];
    bytes2.tBytes[1] = temp;

    destination[0] = bytes2.tBytes[0];
    destination[1] = bytes2.tBytes[1];

}

// Structure the Start and op in message
void setStartAndOpValues(char * txMessageStruct, uint16_t op)
{
    // Set start value
    DINT;
    txMessageStruct[0]  =    txParseMessage.startCode = 0xFF;
    EINT;

    // If op between 1 to 5, set those values
    if( op >= 1 && op <= 7 )
    {
        DINT;
        txMessageStruct[1] = txParseMessage.opCode = op;
        EINT;
    }
    // Otherwise, set those values to the corresponding hex values. For example, if 10, set to 0x0A.
    else if( op == 10 )
    {
        DINT;
        txMessageStruct[1] = txParseMessage.opCode = 0x0A;
        EINT;
    }
    else if( op == 11)
    {
        DINT;
        txMessageStruct[1] = txParseMessage.opCode = 0x0B;
        EINT;
    }
    else if( op == 127 )
    {
        DINT;
        txMessageStruct[1] = txParseMessage.opCode = 0x7F;
        EINT;
    }
    else if ( op == 129)
    {
        DINT;
        txMessageStruct[1]  = txParseMessage.opCode    = 0x81;
        EINT;
    }
    else if( op == 128)
    {
        DINT;
        txMessageStruct[1]  = txParseMessage.opCode    = 0x80;
        EINT;
    }
    else if( op == 254 )
    {
        DINT;
        txMessageStruct[1] = txParseMessage.opCode = 0xFE;
        EINT;
    }
    // If op value is not listed, set to zero and give an error
    else
    {

        DINT;
        txMessageStruct[1] = 0x00;
        // error(FAILED_TO_TX_MESSAGE);
        EINT;
    }
}

// Structure the parameter message
void setParameterValue(char * txMessageStruct, enum MainStateVariable MainState, enum SyncMode syncMode)
{
    // For now, sends the main states. Later will add other parameters
    if(MainState <= NumberOfStates)
    {
        DINT;
        txMessageStruct[2] = txParseMessage.parameter = (unsigned char)MainState;
        EINT;
    }
    else
    {
        DINT;
        // error(INVALID_STATE);
        txMessageStruct[2] = txParseMessage.parameter = Init;
        EINT;
    }
}

// Structure the values in message. For now, we are sending two values.
void setValues(char * txMessageStruct, enum MainStateVariable MainState, enum SyncMode syncMode, enum FaultVariable Faults, uint16_t numberOfValues)
{
    // Local variables
    float txMasterValue1 = 0, txMasterValue2 = 0;
    unsigned char  value1[2], value2[2];

    // Structure the message depending on the parallel mode
    if( MainState == Init)
    {
        DINT;
        txParseMessage.values[0] = txMasterValue1 = 0;
        txParseMessage.values[1] = txMasterValue2 = 0;
        EINT;
    }
    else if( syncMode == GEN_MASTER)
    {
        // Setting the values0 & values1 depending what state we are in.// park1.Angle + slave_phase_shift
        switch(MainState)
        {
          case LockRotor:
              DINT;
              txParseMessage.values[0] = txMasterValue1 = getAlphaVoltageCMD();     // svgen.Ualpha
              txParseMessage.values[1] = txMasterValue2 = getBetaVoltageCMD();      // svgen.Ubeta
              EINT;
          break;

          case Ramp:
              DINT;
              txParseMessage.values[0] = txMasterValue1 = getRampAngle();           // park1.Angle + slave_phase_shift + atan(ipark -D/Q)
              txParseMessage.values[1] = txMasterValue2 = getRampCurrent();         // temporarily vector magnitude of ipark1.Qs and ipark1.Ds
              EINT;
          break;

          case Commutate:
              DINT;
              txParseMessage.values[0] = txMasterValue1 = getAngle();               // park1.Angle
              txParseMessage.values[1] = txMasterValue2 = getQCurrentCMD();         // temporarily vector magnitude of ipark1.Qs and ipark1.Ds
              EINT;
          break;

          default:
              DINT;
              txParseMessage.values[0] = txMasterValue1 = 0;
              txParseMessage.values[1] = txMasterValue2 = 0;
              EINT;
          break;
        }
    }
    else if( syncMode == PARALLEL_SLAVE || syncMode == GEN_SLAVE)
    {
        // For slave, we want to send only the Faults bits only.
        DINT;
        txParseMessage.values[0] = txMasterValue1 = convertEnumToBytes(Faults);
        txParseMessage.values[1] = txMasterValue2 = 0;
        EINT;
    }
    else if( (syncMode == SERVO_MASTER || syncMode == SERVO_SLAVE) && MainState == Commutate)
    {
        // Servo will send controller ramp generator angle
        DINT;
        txParseMessage.values[0] = txMasterValue1 = getRampGenAngle();
        txParseMessage.values[1] = txMasterValue2 = 0;
        EINT;
    }
    else if( syncMode == SOLO )
    {
        // For now, solo sends only 0's
        DINT;
        txParseMessage.values[0] = txMasterValue1 = 0;
        txParseMessage.values[1] = txMasterValue2 = 0;
        EINT;
    }

    // Convert float to bytes
    convertFloatToBytes(value1, &txMasterValue1);
    convertFloatToBytes(value2, &txMasterValue2);

    // Send values
    DINT;
    txMessageStruct[3]  =  __byte((int16_t*)value1, 1);
    txMessageStruct[4]  =  __byte((int16_t*)value1, 0);
    txMessageStruct[5]  =  __byte((int16_t*)value1, 3);
    txMessageStruct[6]  =  __byte((int16_t*)value1, 2);
    txMessageStruct[7]  =  __byte((int16_t*)value2, 1);
    txMessageStruct[8]  =  __byte((int16_t*)value2, 0);
    txMessageStruct[9]  =  __byte((int16_t*)value2, 3);
    txMessageStruct[10] =  __byte((int16_t*)value2, 2);
    EINT;
}

// Structure the CRC values and set them
void setCRCValues(char * txMessageStruct, uint16_t numberOfBytes)
{
    // Local Variable
    unsigned char crcResult = 0;

    // Do the CRC calculations and save in structure
    crcResult = (unsigned char)run16CrcVcu(numberOfBytes ,&txParseMessage);
    DINT;
    txParseMessage.crc = crcResult;
    txMessageStruct[11] = (crcResult >> 8);
    txMessageStruct[12] = (crcResult & 0xFF);
    EINT;
}

// Check if message structure is correct
bool checkMsgStruct(char * txMessageStruct, uint16_t op)
{
    bool correctMessage = 1;    // Set true

    if( txMessageStruct[0] != 0xFF
        || txMessageStruct[1] != op
        || txMessageStruct[2] != txParseMessage.parameter
        || txMessageStruct[3] != __byte( (int16_t*)txParseMessage.values, 0)
        || txMessageStruct[4] != __byte( (int16_t*)txParseMessage.values, 1)
        || txMessageStruct[5] != __byte( (int16_t*)txParseMessage.values, 2)
        || txMessageStruct[6] != __byte( (int16_t*)txParseMessage.values, 3)
        || txMessageStruct[7] != __byte( (int16_t*)txParseMessage.values, 4)
        || txMessageStruct[8] != __byte( (int16_t*)txParseMessage.values, 5)
        || txMessageStruct[9] != __byte( (int16_t*)txParseMessage.values, 6)
        || txMessageStruct[10] != __byte( (int16_t*)txParseMessage.values, 7)
        || txMessageStruct[11] != __byte( (int16_t*)txParseMessage.crc, 0 )
        || txMessageStruct[12] != __byte( (int16_t*)txParseMessage.crc, 1 )
    )
    {
        correctMessage = 0;
    }
    return correctMessage;
}

enum FaultVariable Fault_Detect_UART(void)
{
    enum FaultVariable Flt=None;
    static uint32_t PrevSucCtr;

    //10 A1 iterations without seeing a valid message and we should fault
    if(PrevSucCtr==getSuccessMsgCounter())
    {
        decrementErrorCounter();
    }

    PrevSucCtr=getSuccessMsgCounter();

    if(UARTErrorCtr==0)
    {
        Flt |= SYNC_FLT;
    }
    if(getRxState()==FaultState)
    {
        Flt |= PARTNER_FLT;
    }

    return Flt;
}

void decrementErrorCounter(void)
{
    if(UARTErrorCtr>0)
    {
        UARTErrorCtr--;
    }
}

void resetErrorCounter(void)
{
    if(UARTErrorCtr<10)
    {
        UARTErrorCtr=10;
    }
}
#endif
