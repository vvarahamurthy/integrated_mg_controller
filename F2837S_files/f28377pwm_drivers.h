/* ==================================================================================
 *
 * Created on 2015-12-19 by MRR from f2833xpwm_mr.h from the ETR project (which was derived from the HVPM TI project.
 * and modified for the 28377S processor
 * This file is originally derived from a TI 2833x driver header file that can be found in the ControlSuite/TI libraries
										File name  : F2833xPWM.H
										Target     : TMS320F2833x family
===================================================================================*/

#ifndef __F28377_PWMDRV_H__
#define __F28377_PWMDRV_H__

#include "F2837S_files/F28x_Project.h"
#include "IQMathLib.h"
#include "F2837S_files/f2837xbmsk.h"
#include "f2837xS_epwm.h"
#include "f2837xs_EPwm_defines.h"


//  #include "f2833xdrvlib.h"  Maybe we don't need this in the new project???

/*----------------------------------------------------------------------------
Initialization constant for the F2833X Time-Base Control Registers for PWM Generation. 
Sets up the timer to run free upon emulation suspend, count up-down mode
prescaler 1.
MR - are the PWM registers synchronized?  I don't see sync enabled, data sheet suggests process where
MR - they must all be set up, then TBCLKSYNC =1 is set to get synchronization
----------------------------------------------------------------------------*/
#define PWMTB_INIT_STATE ( FREE_RUN_FLAG +        \
                         PRDLD_SHADOW  +          \
                         TIMER_CNT_UPDN +         \
                         HSPCLKDIV_PRESCALE_X_1 + \
                         CLKDIV_PRESCALE_X_1  +   \
                         PHSDIR_CNT_UP    +       \
                         CNTLD_DISABLE )

/*----------------------------------------------------------------------------
Initialization constant for the F2833X Compare Control Register. 
----------------------------------------------------------------------------*/
#define CMPCTL_INIT_STATE ( LOADAMODE_ZRO + \
                            LOADBMODE_ZRO  )   // + \
  //                          SHDWAMODE_SHADOW + \
  //                          SHDWBMODE_SHADOW )
								//  Only load the CPMA/CMPB registers on TB counter = 0 to prevent glitching
								// MR - Removed code here that was disabling the shadow register for A, B
/*----------------------------------------------------------------------------
Initialization constant for the F2833X Action Qualifier Output A Register. 
----------------------------------------------------------------------------*/
#define AQCTLA_INIT_STATE ( CAD_SET + CAU_CLEAR )

/*----------------------------------------------------------------------------
Initialization constant for the F2833X Dead-Band Generator registers for PWM Generation. 
Sets up the dead band for PWM and sets up dead band values.
----------------------------------------------------------------------------*/
#define DBCTL_INIT_STATE  (BP_ENABLE + POLSEL_ACTIVE_HI_CMP)	// MR - this assumes DBCTL[IN_MODE} is 00 for channel A source

#define DBCNT_INIT_STATE   10   // 10 counts = 66.7 nsec (delay)  (for TBCLK = SYSCLK/1)

/*----------------------------------------------------------------------------
Initialization constant for the F2833X PWM Chopper Control register for PWM Generation. 
----------------------------------------------------------------------------*/
#define  PCCTL_INIT_STATE  CHPEN_DISABLE

/*----------------------------------------------------------------------------
Initialization constant for the F2833X Trip Zone Select Register 
----------------------------------------------------------------------------*/
#define  TZSEL_INIT_STATE  ENABLE_TZ1_OST
              //  MR changed from DISABLE_TZSEL to ENABLE_TZ3_CBC for the E-stop button
				//  MR put back to DISABLE_TZSEL since HW input had EMI on E-stop button
				// 20140127 -- MR Changed to ENABLE_TZ1_OST for the "fault" input (3 gate drivers *and*) on Shaun's Cree drive hardware (GPIO-12)
				//				Right now we are using "OST" - one shot -- since if we get a fault we want to shut down
				//             The user will have to Estop, then clear estop to re-enable the PWMs and clear the one-shot
				//				Eventually we will likely make this a "CBC" cycle by cycle fault so it can just "current limit" on a momentary overloads
				//				and then the software will have to poll and if it is continually tripping it will have to eventually signal the sofware to
				//				flag an error and give up...
/*----------------------------------------------------------------------------
Initialization constant for the F2833X Trip Zone Control Register 
----------------------------------------------------------------------------*/
#define  TZCTL_INIT_STATE ( TZA_FORCE_LO + TZB_FORCE_LO + \
                            DCAEVT1_FORCE_LO +  DCBEVT1_FORCE_LO + \
		                    DCAEVT2_FORCE_LO + DCBEVT2_FORCE_LO )
							// Disable TZ1 for now until level shifter is installed
                          //  MR - changed all of these to FORCE_LO from HI_Z
/*-----------------------------------------------------------------------------
	Define the structure of the PWM Driver Object 
-----------------------------------------------------------------------------*/
typedef struct {   
        Uint16 PeriodMax;   // Parameter: PWM Half-Period in CPU clock cycles (Q0)
        Uint16 HalfPerMax;	// Parameter: Half of PeriodMax (Q0)
        Uint16 Deadband;    // Parameter: PWM deadband in CPU clock cycles (Q0) 
        _iq MfuncC1;        // Input: EPWM1 A&B Duty cycle ratio (Q24)
        _iq MfuncC2;        // Input: EPWM2 A&B Duty cycle ratio (Q24)
        _iq MfuncC3;        // Input: EPWM3 A&B Duty cycle ratio (Q24)
        } PWMGEN ;    


/*------------------------------------------------------------------------------
	Default Initializers for the F2833X PWMGEN Object 
------------------------------------------------------------------------------*/
#define F2833X_FC_PWM_GEN    { 1000,  \
							   1000,  \
                              	200,  \
                              0x4000, \
                              0x4000, \
                              0x4000, \
                             }
                             

#define PWMGEN_DEFAULTS 	F2833X_FC_PWM_GEN

/*------------------------------------------------------------------------------
	PWM Init & PWM Update Macro Definitions
------------------------------------------------------------------------------*/


#define PWM_DISABLE_MACRO(ch1,ch2,ch3)									\
        EALLOW;                       /* Enable EALLOW */					\
        /* Clear bits in Trip Force Register - ACTIVE LOW -- or maybe active high?	*/				\
        (*ePWM[ch1]).TZFRC.bit.OST = 1;									\
        (*ePWM[ch2]).TZFRC.bit.OST = 1;									\
        (*ePWM[ch3]).TZFRC.bit.OST = 1;									\
        	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	\
        EDIS;                        /* Disable EALLOW*/



#define PWM_ENABLE_MACRO(ch1,ch2,ch3)										\
        EALLOW;                       /* Enable EALLOW */					\
        /* Set bits in Trip Force Register - ACTIVE LOW -- OR high?*/					\
     /*   (*ePWM[ch1]).TZFRC.bit.OST = 0;	*/								\
     /*   (*ePWM[ch2]).TZFRC.bit.OST = 0;	*/								\
     /*   (*ePWM[ch3]).TZFRC.bit.OST = 0;	*/								\
        	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	\
        /* Clear one shot (force and trip)*/										\
   	 	(*ePWM[ch1]).TZCLR.bit.OST = 1;										\
        (*ePWM[ch2]).TZCLR.bit.OST = 1;										\
        (*ePWM[ch3]).TZCLR.bit.OST = 1;										\
        																	\
        EDIS;                         /* Disable EALLOW*/


#define PWM_MACRO(ch1,ch2,ch3,v)													\
																					\
/*  Mfuncx range is (-1,1)														*/	\
/*  The code below changes PeriodMax*Mfuncx range ....  						*/	\
/*  from (-PeriodMax,PeriodMax) to (0,PeriodMax) where HalfPerMax=PeriodMax/2	*/	\
																					\
	(*ePWM[ch1]).CMPA.bit.CMPA = _IQmpy(v.HalfPerMax,v.MfuncC1)+ v.HalfPerMax;		\
	(*ePWM[ch2]).CMPA.bit.CMPA = _IQmpy(v.HalfPerMax,v.MfuncC2)+ v.HalfPerMax;		\
	(*ePWM[ch3]).CMPA.bit.CMPA = _IQmpy(v.HalfPerMax,v.MfuncC3)+ v.HalfPerMax;


#endif  // __F28377_PWMDRV_H__
