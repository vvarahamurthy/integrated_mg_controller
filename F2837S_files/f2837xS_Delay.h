//###########################################################################
//
// FILE:   F2837xS_Delay.h
//
// TITLE:  F2837xS Delay function header files
//
//###########################################################################
//      By MRR  2018-05-23
//          Derived from a very small subset of F2837xS_Examples.h by TI
//          Including all of examples was linking too much stuff and references together when all that was needed was the delay function
//          Also trying to not use ControlSuite definition for F2837xS_Defines.h in Module_I2C.c, but including examples to get the
//              delay function seemed to also find ControlSuite definition instead of local definition.  this is start of longer
//              process to simpify the messy TI include structure that includes too many things in too many places.
//
//###########################################################################

#ifndef F2837xS_DELAY_H
#define F2837xS_DELAY_H


/*-----------------------------------------------------------------------------
      Specify the clock rate of the CPU (SYSCLKOUT) in nS.

      Take into account the input clock frequency and the PLL multiplier
      selected in step 1.

      Use one of the values provided, or define your own.
      The trailing L is required tells the compiler to treat
      the number as a 64-bit value.

      Only one statement should be uncommented.

      Example:   200 MHz devices:
                 CLKIN is a 10 MHz crystal or internal 10 MHz oscillator

                 In step 1 the user specified the PLL multiplier = 40 for a
                 200 MHz CPU clock (SYSCLKOUT = 200 MHz).

                 In this case, the CPU_RATE will be 5.000L
                 Uncomment the line: #define CPU_RATE 5.000L

-----------------------------------------------------------------------------*/

#define CPU_RATE   5.00L   // for a 200MHz CPU clock speed (SYSCLKOUT)
//#define CPU_RATE   5.263L   // for a 190MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   5.556L   // for a 180MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   5.882L   // for a 170MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   6.250L   // for a 160MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   6.667L   // for a 150MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   7.143L   // for a 140MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   7.692L   // for a 130MHz CPU clock speed  (SYSCLKOUT)
//#define CPU_RATE   8.333L   // for a 120MHz CPU clock speed  (SYSCLKOUT)

//----------------------------------------------------------------------------

extern void F28x_usDelay(long LoopCount);
// DO NOT MODIFY THIS LINE.
#define DELAY_US(A)  F28x_usDelay(((((long double) A * 1000.0L) / (long double)CPU_RATE) - 9.0L) / 5.0L)

#endif  // end of F2837xS_DELAY_H definition

//===========================================================================
// End of file.
//===========================================================================
