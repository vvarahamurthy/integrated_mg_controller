
//###########################################################################
//
// FILE:   F28x_Project.h
//
// TITLE:  F28x Project Headerfile and Examples Include File
//
//###########################################################################
// $TI Release: F2837xS Support Library v180 $
// $Release Date: Fri Nov  6 16:27:58 CST 2015 $
// $Copyright: Copyright (C) 2014-2015 Texas Instruments Incorporated -
//             http://www.ti.com/ ALL RIGHTS RESERVED $
//###########################################################################

#ifndef F28X_PROJECT_H
#define F28X_PROJECT_H

#define   MATH_TYPE      1

#include "F2837xS_Cla_typedefs.h"  // F2837xS CLA Type definitions
#include "F2837xS_device.h"        // F2837xS Headerfile Include File
// Debug defines

#define GDEBUG                      0       // think this is a George debugging flag, not sure what it does, need to fix (MR)
#define SYNCMODE_FIX                1       //This flag will conditional compile out the syncmode stuff that needs to be redone after we get motors spinning (VV)



#define DISABLE_AD_FAULTS           0       // Disable AD faults
#define DISABLETRIPZONES            0       // Disable state machine faults from trip zone faults (from on-board comparators or anything else configured to trip zone)
#define DISABLEGPIOFAULTS           0       // Disable state machine faults from GPIO inputs from gate driver fault detect signals
#define DISABLELOOPFAULTS           0   // Disable faults from PI loops saturating/going out of bounds
#define DISABLECANCOMMFAULTS        1
#define DISABLESERCOMMFAULTS        1
#define DISABLETRIPZONES            0       // Disable state machine faults from trip zone faults (from on-board comparators or anything else configured to trip zone)
#define DISABLETEMPERATUREFAULTS    0
#define DISABLE_WFAULTGPIO          0          // Disable just the GPIO faults for phase W, U and V are operational for half bridge test
#define DISABLE_VWFAULTGPIO         0          // Disable just the GPIO faults for phase V and W; U faults are operational for half bridge test
#define DISABLE_RDYGPIO             0       //Disable all the "RDY" GPIO faults
#define DISABLESPEEDFAULTS          0   // Disables over speed, zero speed faults

#define FAKE_FAULT_ERR 0
#define FAKE_SYSTEM_RPM 0
#define FAKE_ECU 0
#define FAKE_WARNINGS 0
#define FAKE_GENCONTROL 0

//Engine Unit Test Modes
#define NEGATIVE_BOOST 0
#define NEWCONTROL_MODE 0
#define READTHROTTLE 0
#define COMPRESSIONTESTMODE 0
#define ECU_SPEED_POWERMAP 0

#endif  // end of F28X_PROJECT_H definition

