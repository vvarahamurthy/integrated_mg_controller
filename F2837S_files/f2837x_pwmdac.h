/* ==================================================================================
File name:        F2803XPWMDAC_2xPM.H                     
                    
Originator:	Digital Control Systems Group
			Texas Instruments
Description:  
 Header file containing data type, object, macro definitions and initializers
 This file is specific to "Multi-Axis + PFC" kit and configure PWM 2A & PWM 2B as DAC pins

Target: TMS320F280x family
=====================================================================================
History:
-------------------------------------------------------------------------------------
 04-15-2010	Version 1.1 
 04-18-2016 Modified by MRR to change from EPWM4 to EPWM2 for the motor controller/engine controller
------------------------------------------------------------------------------------*/

#ifndef __F2837X_PWMDAC_H__
#define __F2837X_PWMDAC_H__

#include "F2837S_files/F28x_Project.h"
#include "F2837S_files/f2837xbmsk.h"

/*----------------------------------------------------------------------------
Initialization constant for the F280X Time-Base Control Registers for PWM Generation. 
Sets up the timer to run free upon emulation suspend, count up-down mode
prescaler 1.
----------------------------------------------------------------------------*/
#define PWMDAC_INIT_STATE ( FREE_RUN_FLAG +         \
                            PRDLD_IMMEDIATE  +       \
                            TIMER_CNT_UPDN +         \
                            HSPCLKDIV_PRESCALE_X_1 + \
                            CLKDIV_PRESCALE_X_32  +   \
                            PHSDIR_CNT_UP    +       \
                            CNTLD_DISABLE )


/*----------------------------------------------------------------------------
Initialization constant for the F280X Compare Control Register. 
----------------------------------------------------------------------------*/
#define PWMDAC_CMPCTL_INIT_STATE ( LOADAMODE_ZRO + \
                                   LOADBMODE_ZRO + \
                                   SHDWAMODE_SHADOW + \
                                   SHDWBMODE_SHADOW )

/*----------------------------------------------------------------------------
Initialization constant for the F280X Action Qualifier Output A Register. 
----------------------------------------------------------------------------*/
#define PWMDAC_AQCTLA_INIT_STATE ( CAD_SET + CAU_CLEAR )
#define PWMDAC_AQCTLB_INIT_STATE ( CBD_SET + CBU_CLEAR )

/*----------------------------------------------------------------------------
Initialization constant for the F280X Dead-Band Generator registers for PWM Generation. 
Sets up the dead band for PWMDAC and sets up dead band values.
----------------------------------------------------------------------------*/
#define PWMDAC_DBCTL_INIT_STATE   BP_DISABLE 

/*----------------------------------------------------------------------------
Initialization constant for the F280X PWM Chopper Control register for PWM Generation. 
----------------------------------------------------------------------------*/
#define  PWMDAC_PCCTL_INIT_STATE  CHPEN_DISABLE

/*----------------------------------------------------------------------------
Initialization constant for the F280X Trip Zone Select Register 
----------------------------------------------------------------------------*/
#define  PWMDAC_TZSEL_INIT_STATE  DISABLE_TZSEL
              
/*----------------------------------------------------------------------------
Initialization constant for the F280X Trip Zone Control Register 
----------------------------------------------------------------------------*/
#define  PWMDAC_TZCTL_INIT_STATE ( TZA_HI_Z + TZB_HI_Z + \
                                   DCAEVT1_HI_Z + DCAEVT2_HI_Z + \
                                   DCBEVT1_HI_Z + DCBEVT2_HI_Z )

#define  PWMDAC_TZCTL_INIT_STATE_OSTLO ( TZA_FORCE_LO + TZB_FORCE_LO + \
                                   DCAEVT1_HI_Z + DCAEVT2_HI_Z + \
                                   DCBEVT1_HI_Z + DCBEVT2_HI_Z )

/*-----------------------------------------------------------------------------
Define the structure of the PWMDAC Driver Object 
-----------------------------------------------------------------------------*/
#if 0
typedef struct {   
  	int16 *PwmDacInPointer0;   	// Input: Pointer to source data output on PWMDAC channel 0 
	int16 *PwmDacInPointer1;    // Input: Pointer to source data output on PWMDAC channel 1 
	int16 *PwmDacInPointer2;    // Input: Pointer to source data output on PWMDAC channel 2
	int16 *PwmDacInPointer3;    // Input: Pointer to source data output on PWMDAC channel 3  
	Uint16 PeriodMax;     		// Parameter: PWMDAC half period in number of clocks  (Q0) 
 	} PWMDAC ;

#endif

typedef struct {
    int16 PwmDacIn1;    // Input: Pointer to source data output on PWMDAC channel 0
    int16 PwmDacIn2;    // Input: Pointer to source data output on PWMDAC channel 1
    Uint16 PeriodMax;           // Parameter: PWMDAC half period in number of clocks  (Q0)
    } PWMDAC ;

/*-----------------------------------------------------------------------------
Define a PWMDAC_handle
-----------------------------------------------------------------------------*/
typedef PWMDAC *PWMDAC_handle;

/*------------------------------------------------------------------------------
Default Initializers for the F280X PWMGEN Object 
------------------------------------------------------------------------------*/
#define F2837X_PWMDAC_DEFAULTS   { 0,   \
                                  0,    \
                                  500,  \
                                 }
                                 
#define PWMDAC_DEFAULTS     F2837X_PWMDAC_DEFAULTS

#endif  // __F280X_PWMDAC_H__
