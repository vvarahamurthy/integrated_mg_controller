#include "F2837S_files/F28x_Project.h"
#include "F2837xS_Pie_defines.h"
#include "IQMathLib.h"
#include "Header/GUIC_Header.h"
#include "Header/State_machine.h"
#include "Header/RingBuffer.h"
#include "Header/Serial_Cmd_Monitor.h"
#include "Header/CRC.h"
#include "Header/UART_Comm.h"
#include "Header/ParseRoutine.h"
#include "Header/Globals.h"

#define DeleteMe    1
#define FixMeLater  1


uint16_t g_ReceivedChar = 0;      // Actual received characters
uint16_t g_newChar = 0;       // Count of received characters

#if(SCI_MODE==FIFOINT)

extern RINGBUFFER gOutTxRingBuffer;
extern int getCount(RINGBUFFER *);
uint16_t g_TxIntCtr = 0;

extern RINGBUFFER gInRxRingBuffer;
extern int getCount(RINGBUFFER *);
extern bool isParseStillProcessing(void);
extern void fillParseBuffer();
uint16_t g_RxIntCtr = 0;
uint16_t LoopCount=0;
uint16_t LoopParseCount=0;
// Validate variables
bool isSCIRegValidated;

// Rx error counter
unsigned int gResetRxErrorCounter;


#endif


// Initialize SCI
void Init_SCI()
{
    // Note: Clocks were turned on to the SCIA peripheral
    // in the InitSysCtrl() function

    /* SCICCR: 1 stop bit, parity disabled, no loopback,
    *  async mode, idle-line protocol, 8-bits per char (same as 0x0007)
    */

    setup_sci_interrupt();

    ScibRegs.SCICCR.bit.ADDRIDLE_MODE = 0;  // Idle line mode - RS232 compatible
    ScibRegs.SCICCR.bit.LOOPBKENA = 0;  // Loopback disabled
    ScibRegs.SCICCR.bit.PARITY =    0;      // Even parity
    ScibRegs.SCICCR.bit.SCICHAR = 7;        // 8 data bits
    ScibRegs.SCICCR.bit.PARITYENA = 0;  // Parity disabled
    ScibRegs.SCICCR.bit.STOPBITS = 0;   // 1 stop bit

    ScibRegs.SCICTL1.bit.RXENA = 1;  // Enable RX
    ScibRegs.SCICTL1.bit.TXENA = 1;  // Enable TX

    ScibRegs.SCIPRI.bit.FREESOFT = 1; // Disable RX ERR, SLEEP, TXWAKE

    ScibRegs.SCICTL2.bit.TXINTENA = 0;          // Single char TX interrupt not enabled
    ScibRegs.SCICTL2.bit.RXBKINTENA = 1;        // Single char RX interrupt enabled (but over-ridden by RXFFIENA if FIFO enabled)

    ScibRegs.SCIHBAUD.all = (((uint32_t)CPU.Sec/4L)/(SCI_BAUD_RATE * 8L) - 1)/256;
    ScibRegs.SCILBAUD.all = (((uint32_t)CPU.Sec/4L)/(SCI_BAUD_RATE * 8L) - 1)%256;

#if(SCI_MODE==FIFOINT)

    ScibRegs.SCIFFTX.bit.SCIFFENA = 1;                  // Enable FIFO mode for the SCI

    ScibRegs.SCIFFTX.bit.TXFIFORESET=0;
    ScibRegs.SCIFFRX.bit.RXFIFORESET=0;                 // Clear TX and RX FIFO status bits (count bits) and hold reset

    ScibRegs.SCIFFTX.bit.TXFFIL = TX_FIFO_LEVEL;        //.all Originally 0xC020 -- level TX FIFO interrupts at
    ScibRegs.SCIFFTX.bit.TXFFINTCLR=1;                  // Clear any pending interrupts - write 1 to clear
    ScibRegs.SCIFFTX.bit.TXFFIENA = 0;                  // Disable TX FIFO interrupt if com is in FIFO mode, code will enable when FIFO is loaded


    ScibRegs.SCIFFRX.bit.RXFFIL = RX_FIFO_LEVEL;        //.all Originally 0x0021 - -level RX FIFO interrupts at
    ScibRegs.SCIFFRX.bit.RXFFOVRCLR=1;                  // Clear Overflow flag
    ScibRegs.SCIFFRX.bit.RXFFINTCLR=1;                  // Clear Interrupt flag - write 1 to clear
    ScibRegs.SCIFFRX.bit.RXFFIENA = 1;                  // Enable receive FIFO interrupt

    ScibRegs.SCIFFTX.bit.SCIRST = 1;                    // Remove reset from the SCI and SCI FIFO
    ScibRegs.SCIFFCT.all=0x00;                          // No autobaud, no delay between transfers

    ScibRegs.SCIFFTX.bit.TXFIFORESET=1;
    ScibRegs.SCIFFRX.bit.RXFIFORESET=1;         // Allow RX, RX FIFO status bit (counters) to operate
#else
    ScibRegs.SCIFFTX.bit.TXFFIENA = 0;
#endif

    ScibRegs.SCICTL1.all =0x0023;//0x0023;     // Relinquish SCI from Reset

#if(SCI_MODE==FIFOINT)
    rb_init(&gOutTxRingBuffer, CMD_BUFFER_SIZE);
    rb_init(&gInRxRingBuffer,CMD_BUFFER_SIZE);
#endif
}

#if(!SYNCMODE_FIX)
// ==========================================================
//                          Transmit
// ==========================================================

// Print function to COM
void printCom(char * msgString)
{
    unsigned short index = 0;

    // Transmit those messages
    while(msgString[index] != '\0' || index >= 65534 )
    {
        DINT;
        Write8bitByteToCOM(msgString[index]);
        sci_start_xmit();
        EINT;
        index++;
    }
}

// Structure a message for handshake between master and slave

void transmitHandShake(enum MainStateVariables MainState, enum SyncMode syncMode)
{
    char txMessageStruct[MAX_MSG_BYTES + 1];

    // Step 1: Structure start and op values
    setStartAndOpValues(txMessageStruct, 0x07);    // Master structured message

    // Step 2: Structure the parameter. For now, it just sends the current main state
    setParameterValue(txMessageStruct, MainState, syncMode);

    // Step 3: Structure the values. For now, we are setting the numberOfValues to 0 DOES NOT DO ANYTHING
    setValues(txMessageStruct, MainState, syncMode, Faults, 0);

    // Step 4: Structure the CRC.
    setCRCValues(txMessageStruct, MAX_MSG_BYTES);
   // Run_Datalog();

    // Step 5: Transmit all the bytes
    unsigned short i;
    for( i = 0; i < MAX_MSG_BYTES; i++ )
    {
        Write8bitByteToCOM(txMessageStruct[i]);
        sci_start_xmit();
    }

}

// Structure a message and transmit it.
void transmitMessage(enum MainStateVariables MainState, enum SyncMode syncMode, enum GPIOFaultVariables Faults)
{
    char txMessageStruct[MAX_MSG_BYTES + 1];
    unsigned int op = 0;

    if( syncMode == PARALLEL_MASTER || syncMode == GEN_MASTER)
    {
        op = 128;
    }
    else if( syncMode == PARALLEL_SLAVE || syncMode == GEN_SLAVE)
    {
        op = 129;
    }
    else if( syncMode == SERVO_MASTER || syncMode == FOLLOWER_MASTER)
    {
        op = 127;
    }

    // Step 1: Structure start and op values
    setStartAndOpValues(txMessageStruct, op);    // Master structured message

    // Step 2: Structure the parameter. For now, it just sends the current main state
    setParameterValue(txMessageStruct, MainState, syncMode);

    // Step 3: Structure the values. For now, we are setting the numberOfValues to 0 DOES NOT DO ANYTHING
    setValues(txMessageStruct, MainState, syncMode, Faults, 0);

    // Step 4: Structure the CRC.
    setCRCValues(txMessageStruct, MAX_MSG_BYTES);

    // Step 5: Transmit all the bytes
    unsigned short i;
    for( i = 0; i < MAX_MSG_BYTES; i++ )
    {
        Write8bitByteToCOM(txMessageStruct[i]);
        sci_start_xmit();
    }

}



// ==========================================================
//                          Old Code
// ==========================================================

// return count
uint16_t getNewCharCount( )
{
    return g_newChar;
}

// similar to getReceivedChar

void decrementNewCharCounter()
{
    if(g_newChar > 0 )
    {
        g_newChar--;
    }
}

//return the next Char in the fifo, reduce the g_newChar by 1
uint16_t getReceivedChar()
{

#if(SCI_MODE==INDCHAR)
    g_newChar--;
	return g_ReceivedChar;
#else       // In FIFO mode, first read anything in the hardware FIFO into the software ring buffer, then push out the first character in the ring buffer
    while(ScibRegs.SCIFFRX.bit.RXFFST > 0)
    {
        rb_push(&gInRxRingBuffer,ScibRegs.SCIRXBUF.all);
         g_newChar++;
    }
    // g_newChar--;
	return 0; //(uint16_t) rb_pop(&gInRxRingBuffer);                   // should check that there isn't buffer underrun before popping...
#endif
}

void setup_sci_interrupt(void)
{
	// Interrupts that are used in this example are re-mapped to
	// ISR functions found within this file.
	EALLOW;  // This is needed to write to EALLOW protected registers
	PieVectTable.SCIB_RX_INT = &sciRxFifoIsr;
	PieCtrlRegs.PIEIER9.bit.INTx3  = 1;     // PIE Group 9, INT3 (SCI-B RX)
#if(SCI_MODE==FIFOINT)
	PieVectTable.SCIB_TX_INT = &sciTxFifoIsr;
	PieCtrlRegs.PIEIER9.bit.INTx4 = 1;      // PIE Group 9, INT4 (SCI-B TX)
#endif
	IER |= M_INT9; // Enable group 9 interrupts (UART monitor example).
	EDIS;   // This is needed to disable write to EALLOW protected registers
}


// Transmit a character from the SCI
void sci_xmit(int a)
{
#if(SCI_MODE==INDCHAR)          // If in individual char mode then just write byte to TXBUF, block until able to write & transmit
    while (ScibRegs.SCIFFTX.bit.TXFFST != 0) {}
    ScibRegs.SCITXBUF.all=a;

#else       // If in FIFO mode then this just puts characters in FIFO, and transmitting commmand must start transmission process with separate call once all characters are in the buffer
    //Add character to output ring buffer
    rb_push(&gOutTxRingBuffer, (a & 0xff));

#endif

}

#if(SCI_MODE==FIFOINT)
void sci_start_xmit(void)   // After loading the transmit buffer with sci_xmit in FIFO mode then this must be called to kick off the (interrupt driven) transmisison process
{
    while( (ScibRegs.SCIFFTX.bit.TXFFST < 16) && (getCount(&gOutTxRingBuffer)>0) )
    {
        g_TxIntCtr++;
        SCIREGS.SCITXBUF.all=rb_pop(&gOutTxRingBuffer);
    }
    SCIREGS.SCIFFTX.bit.TXFFINTCLR = 1;         // Clear interrupt flag -- seems like flag can only be cleared once FIFO is not empty...
    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;           // Clear interrupt acknowledge flag -- in case flag was set somehow...
    ScibRegs.SCIFFTX.bit.TXFFIENA = 1;          // Enable interrupt when FIFO is empty
}
#endif

//
// RX Handler for RX FIFO full
//
__interrupt void sciRxFifoIsr(void)
{
                // Need to check for RX error (framing/overflow) here since that can cause interrupt as well
                // Need to check RX flags and see if it is RXRDY/RXFIFO or and error flag that caused interrupt
                // And also need to check that interrupt on error flag is enabled -- or find another way to poll
                // to deal with RX errors. --MR
#if(SCI_MODE==INDCHAR)
    // Get character
    g_ReceivedChar = ScibRegs.SCIRXBUF.all;
    g_newChar++;

#else           // FIFO version receives more than one char -- recieves whole RX FIFO

// while FIFO has characters copy them to the ring buffer, increment g_newChar
    while(ScibRegs.SCIFFRX.bit.RXFFST > 0)
    {
        rb_push(&gInRxRingBuffer,ScibRegs.SCIRXBUF.all);
        g_newChar++;
    }
    g_RxIntCtr++;

#endif
    ScibRegs.SCIFFRX.bit.RXFFOVRCLR=1;   // Clear Overflow flag
    ScibRegs.SCIFFRX.bit.RXFFINTCLR=1;   // Clear Interrupt flag

    PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;       // Issue PIE ack
    EINT;
}

//
// TX interrupt handler for FIFO empty on TX
//
#if(SCI_MODE==FIFOINT)
__interrupt void sciTxFifoIsr(void)
{
	EINT;

// while ring buffer has charactersand FIFO has space copy them to TX FIFO and increment TX counter
	while( (ScibRegs.SCIFFTX.bit.TXFFST < 16) && (getCount(&gOutTxRingBuffer)>0) )
	{
	    SCIREGS.SCITXBUF.all=rb_pop(&gOutTxRingBuffer);
	}

	if(ScibRegs.SCIFFTX.bit.TXFFST == 0)
	{
	    ScibRegs.SCIFFTX.bit.TXFFIENA = 0;                  // Disable TX FIFO interrupt if com is in FIFO mode, code will enable when FIFO is loaded
	}
    g_TxIntCtr++;

	SCIREGS.SCIFFTX.bit.TXFFINTCLR = 1;         // Clear interrupt flag
	PieCtrlRegs.PIEACK.all |= PIEACK_GROUP9;
}
#endif

// Checks if there is RX error
bool noRxError()
{
    return ScibRegs.SCIRXST.bit.RXERROR;
}

void SCI_Reset(void)
{
    // Disable TX FIFO interrupt
    ScibRegs.SCIFFTX.bit.TXFFIENA = 0;

    // Do a software reset by setting to 1 and back to 0.
    ScibRegs.SCICTL1.bit.SWRESET = 0;
    ScibRegs.SCICTL1.bit.SWRESET = 1;

    // Reset the FIFO status bits for both rx and tx. Must set to 0 and back to 1
    ScibRegs.SCIFFTX.bit.TXFIFORESET    = 0;
    ScibRegs.SCIFFRX.bit.RXFIFORESET    = 0;
    ScibRegs.SCIFFTX.bit.TXFIFORESET    = 1;
    ScibRegs.SCIFFRX.bit.RXFIFORESET    = 1;
}

// Increment rxErrorCounter when RXERROR is set to 1
void incrementResetRxErrorCounter()
{
    if(gResetRxErrorCounter < INT_MAX )
    {
        gResetRxErrorCounter++;
    }
}

// Perform a software reset if SCI is getting an RXERROR
void resetRxError()
{
    incrementResetRxErrorCounter();
    // Flush out Tx and RX buffers
    rb_flush( getRxRingBuffer() );
    rb_flush( getTxRingBuffer() );

    SCI_Reset();
}

#ifndef FixMeLater
void validatingRegisters()
{

    // Checks SCI

    // PIE Group 9, INT3 (SCI-B RX) & PIE Group 9, INT4 (SCI-B TX)
    if( PieCtrlRegs.PIEIER9.bit.INTx3           == 1
        && PieCtrlRegs.PIEIER9.bit.INTx4        == 1
        && ScibRegs.SCICCR.bit.ADDRIDLE_MODE    == 0
        && ScibRegs.SCICCR.bit.LOOPBKENA        == 0
        && ScibRegs.SCICCR.bit.PARITY           == 0
        && ScibRegs.SCICCR.bit.SCICHAR          == 7
        && ScibRegs.SCICCR.bit.PARITYENA        == 0
        && ScibRegs.SCICCR.bit.STOPBITS         == 0
        && ScibRegs.SCICTL1.bit.RXENA           == 1
        && ScibRegs.SCICTL1.bit.TXENA           == 1
        && ScibRegs.SCIPRI.bit.FREESOFT         == 1
        && ScibRegs.SCICTL2.bit.TXINTENA        == 0
        && ScibRegs.SCICTL2.bit.RXBKINTENA      == 1
        && ScibRegs.SCIFFTX.bit.SCIFFENA        == 1
        && ScibRegs.SCIFFTX.bit.TXFFIL          == 0    // TX_FIFO_LEVEL
        && ScibRegs.SCIFFRX.bit.RXFFIL          == 15   // RX_FIFO_LEVEL
        && ScibRegs.SCIFFRX.bit.RXFFIENA        == 1
        && ScibRegs.SCIFFTX.bit.SCIRST          == 1
        && ScibRegs.SCIFFCT.all                 == 0x00
        && ScibRegs.SCIFFTX.bit.TXFIFORESET     == 1
        && ScibRegs.SCIFFRX.bit.RXFIFORESET     == 1
        && ScibRegs.SCICTL1.all                 == 0x0023
        )
    {
        isSCIRegValidated = 1;
    }

}

void InitValidation()
{
    // false
    isSCIRegValidated = 0;
}

bool checkRegisters()
{
    return isSCIRegValidated;
}
#endif

void checkForMessages()
{
    /*  Handle SCI serial comm stuff here (should this be moved to a timed event rather than "as fast as possible" in the main loop?  Look at baud rate and FIFO size to determine... */
    // Debug -- should loop here until getNewCharCount is zero... what happens if we buffer up multiple large Read Date commands,
                                          //  can this block waiting to finish all?  Should this somehow stop looping if a message is completed and processed in receivedDataCommand()
    // and then come back next time around for the next command?

    // If something in fifo, pass to ring until empty. Increment gNewCharCounter for every pass.
    while( ScibRegs.SCIFFRX.bit.RXFFST > 0 )
    {
        fillParseBuffer();      // Does not return anything and does not decrement g_newChar
        LoopCount++;
    }

    // Loop until gNewCharCounter is 0
    while( !isParseStillProcessing() && (noRxError() == 0) )
    {
        // Parse if message is found
        parseRoutine(getByte());
        LoopParseCount++;
    }

    // If RXERROR is set to 1, try to perform a Rx reset.
    if( noRxError() == 1 )
    {
        resetRxError();
    }
}

#endif
